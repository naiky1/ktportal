import Navbar from './navbar.jsx'
import './login_type_selection.css'
import {Link} from 'react-router-dom'

function login_type_selection()
{

    return(
    <div className="bg" style={{backgroundColor:"#003C71"}}>
        <Navbar noHomeLogout={true}/>
        <div className='Login_type' >
	        <h1 style={{color:"#FFFFFF"}}>CRM Knowledge Transfer Portal</h1>
	        <Link to="/login" className="btn btn-info btn-lg" style={{width:'15%', padding:'1%', marginTop:'5%'}} state={{type:"trainee"}}>Login as Trainee</Link>
	         &emsp;
	        <Link to="/login" className="btn btn-info btn-lg" style={{width:'15%', padding:'1%', marginTop:'5%'}} state={{type:"trainer"}}>Login as Trainer</Link>
	         &emsp;
	        <Link to="/login" className="btn btn-info btn-lg" style={{width:'15%', padding:'1%', marginTop:'5%'}} state={{type:"manager"}}>Login as Manager</Link>
	        &emsp;
	        <Link to="/login" className="btn btn-info btn-lg" style={{width:'15%', padding:'1%', marginTop:'5%'}} state={{type:"admin"}}>Login as Admin</Link>
	        <br/>
	        <Link to="/knowledge_base" className="btn btn-info btn-lg" style={{width:'15%', padding:'1%', marginTop:'5%'}} state={{type:"admin"}}>Go to Knowledge Base</Link>
            <br/>
            <br/>
            <br/>
        </div>
        <div class="footer">
            <p><span class="align-left">Developed by: Sidharth Saxena, Yogesh Naik</span>© 2024 Copyright: Boston Scientific India</p>
        </div>

    </div>

    )

}

export default login_type_selection;