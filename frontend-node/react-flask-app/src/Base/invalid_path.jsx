import Navbar from './navbar.jsx'
import './login_type_selection.css'
import {Link} from 'react-router-dom'

function Invalid_Path()
{

    return(
    <div className="bg" style={{backgroundColor:"#003C71"}}>
        <Navbar noHomeLogout={true}/>
        <div className='Login_type' >
	        <h1 style={{color:"#FFFFFF"}}>Oops! It appears you have entered an unidentifiable URL.</h1>
            <br/>
            <Link to="/" className="btn btn-info btn-lg" style={{width:'15%', padding:'1%', marginTop:'5%'}} state={{type:"trainee"}}>Go Back to Login Page</Link>
        </div>
        <div class="footer">
            <p><span class="align-left">Developed by: Sidharth Saxena, Yogesh Naik</span>© 2024 Copyright: Boston Scientific India</p>
        </div>

    </div>

    )

}

export default Invalid_Path;