
export function login(userEmail,userType)
{
   localStorage.setItem('userEmail',userEmail.toLowerCase())
   localStorage.setItem('userType',userType)
}

export function logout(setUserEmail,setUserType)
{
    setUserEmail(null)
    setUserType(null)
    localStorage.removeItem('userEmail')
    localStorage.removeItem('userType')
}