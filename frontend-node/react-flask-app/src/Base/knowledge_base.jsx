import React, {useEffect, useState} from 'react'
import Navbar from './navbar.jsx'
import './knowledge_base.css'
import { useLocation } from 'react-router-dom';
import axios from 'axios'
import Popup from 'reactjs-popup'
import {useNavigate} from 'react-router-dom'
import { AlertTitle, CircularProgress, Tooltip } from '@mui/material';
import Zoom from '@mui/material/Zoom';
import Alert from '@mui/material/Alert';
//import Countdown from 'react-countdown' For showing the time till which the otp is active
import Switch from '@mui/material/Switch';
import FormControlLabel from '@mui/material/FormControlLabel';
import CancelIcon from '@mui/icons-material/Cancel';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Collapse from '@mui/material/Collapse';



function Knowledge_base(props)
{
    //To get the login type from the previous page
    const location = useLocation();

    //For showing loading screen
    const [loadingPopup,setLoadingPopup] = useState(false)

    //For storing project names if needed
    const [projectNames,setProjectNames] = React.useState([]);

    //For storing project IDs if needed
    const [trainingNames,settrainingNames] = React.useState([]);

    //For storing project IDs if needed
    const [trainingPaths,settrainingPaths] = React.useState([]);

    const [expanded, setExpanded] = React.useState(false);

    const fetch_details = async () =>
    {
    const result = await axios.get("http://10.220.20.246:8000/knowledge_base");
    setProjectNames(result.data.projectNames);
    settrainingNames(result.data.training_names);
    settrainingPaths(result.data.training_paths);
    }

    useEffect(() =>
    {
        fetch_details()
    },[])


    function Collapsible(props)
    {
    const [isOpen, setIsOpen] = useState(false);

    return <div className="collapsible">
        <button style={{width:"40%", textAlign:"left", color:"#fff", backgroundColor:"#5bc0de", borderRadius:"20px"}} className="btn btn-primary btn-lg" onClick={() => setIsOpen(!isOpen)}>
            <table style={{width:"100%"}}>
            <tr style={{lineHeight:"40%"}}>
            <td>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-briefcase" viewBox="0 0 16 16">
                  <path d="M6.5 1A1.5 1.5 0 0 0 5 2.5V3H1.5A1.5 1.5 0 0 0 0 4.5v8A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-8A1.5 1.5 0 0 0 14.5 3H11v-.5A1.5 1.5 0 0 0 9.5 1zm0 1h3a.5.5 0 0 1 .5.5V3H6v-.5a.5.5 0 0 1 .5-.5m1.886 6.914L15 7.151V12.5a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5V7.15l6.614 1.764a1.5 1.5 0 0 0 .772 0M1.5 4h13a.5.5 0 0 1 .5.5v1.616L8.129 7.948a.5.5 0 0 1-.258 0L1 6.116V4.5a.5.5 0 0 1 .5-.5"/>
                </svg>
            </td>
            <td style={{width:"60%", textAlign:"left", paddingLeft:"2%", fontWeight:"500"}}>{props.project}</td>
            <td style={{width:"60%", textAlign:"right", paddingRight:"5%"}}>{isOpen ?<span>-</span>:<span>+</span>}</td>
            </tr>
            </table>
        </button>
        {isOpen ?

                <table className="table_content">
                    <thead>
                        <tr style={{lineHeight:"300%",marginLeft:"2%"}}>
                            <th style={{width:"5%",paddingLeft:"2%", scope:"col"}}></th>
                            <th style={{width:"60%",paddingLeft:"12%",scope:"col"}}>Training Topic</th>
                            <th style={{width:"40%",paddingLeft:"2%",scope:"col"}}>Training Material</th>
                        </tr>
                    </thead>

                    <tbody>
                        {
                        trainingNames[props.projindex].map( (el,index) =>
                        (
                            <tr>
                                <td style={{paddingLeft:"12%"}}><ul><li></li></ul></td>
                                <td style={{paddingLeft:"12%"}}>
                                    {el}
                                </td>
                                <td style={{paddingLeft:"4%"}}><a href={trainingPaths[[props.projindex]][index]} target="_blank">Content</a></td>
                            </tr>
                            ))
                        }
                    </tbody>
                </table>
                : null}
    </div>
    }

  return (
    <div className="bg">
    <Navbar noHomeLogout={true}/>
    <h1>Project Knowledge Base</h1>
                <div className="knowledge_base">
                <div>
                {
                projectNames.map( (projele, projindex) =>
                        (
                            <div>
                            <center>
                                <div style={{width:"80%"}} className="py-2">
                                    <Collapsible project={projele} projindex={projindex}/>
                                    <br/>
                                </div>
                            </center>
                            </div>
                        ))
                }
                </div>

            <div class="footer">
                <p><span class="align-left">Developed by: Sidharth Saxena, Yogesh Naik</span>© 2024 Copyright: Boston Scientific India</p>
            </div>

          {/* LOADING SCREEN */}
          <Popup open={loadingPopup} hideBackdrop closeOnDocumentClick={false} onClose={()=>{setLoadingPopup(false)}}>
               <center>
                 <p style={{color:"#003C71", fontSize:"130%", margin:"3%"}}><center>PLEASE WAIT...</center></p>
                 <br/>
                 <CircularProgress/>
                 <br/>
                 <br/>
               </center>
          </Popup> 
           
        </div>
    </div>
  );

}

export default Knowledge_base;
