import React, {useEffect, useState} from 'react'
import Navbar from './navbar.jsx'
import './login.css'
import { useLocation } from 'react-router-dom';
import axios from 'axios'
import Popup from 'reactjs-popup'
import {useNavigate} from 'react-router-dom'
import { AlertTitle, CircularProgress, Tooltip } from '@mui/material';
import Zoom from '@mui/material/Zoom';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
//import Countdown from 'react-countdown' For showing the time till which the otp is active
import Switch from '@mui/material/Switch';
import FormControlLabel from '@mui/material/FormControlLabel';
import CancelIcon from '@mui/icons-material/Cancel';


function Login(props)
{

   

    //To redirect to the appropriate dashboard if the credentials are valid
    let navigate = useNavigate();

    //To get the login type from the previous page
    const location = useLocation();

    //User type must be selected on the previous page (login type selection), else by default go to trainee
    var login_type
    try
    {
        login_type = location.state.type.toUpperCase();
    }
    catch
    {
        login_type = "TRAINEE";
    }

    //storing whether user was found or not (Needed to decide whether to redirect the user back to first page or stay at login page)
    const [userNotFound,setUserNotFound]= useState(false)
    

    //For showing loading screen
    const [loadingPopup,setLoadingPopup] = useState(false)

    //Declaring variables to store user email, popup display status and popup message
    const [userEmail,setUserEmail]=React.useState("");
    const [otpSent,setOtpSent]=React.useState(false);
    const [otp,setOtp]=React.useState(null)
    const [popup,setPopup] = React.useState(false);
    const [popupMessage,setPopupMessage] = React.useState("");
    const [otpSnackBar,setOtpSnackBar] = React.useState(false);
    const [otpSnackBarMessage,setOtpSnackBarMessage] = React.useState("");
    const [otpSnackBarType,setOtpSnackBarType] = React.useState("success");
    //const [timeUp,setTimeUp] = React.useState(false)   //NECESSARY FOR COUTNDOWN
    //const [start,setStart] = React.useState(Date.now()) //NECESSARY FOR COUTNDOWN
    const [cKey,setCKey] = React.useState(1)
    const [otpId,setOtpId] = React.useState(0)

    {/*DEV OPTIONS - DISABLING OTP SYSTEM DURING DEVELOPMENT 
    const [toggle,setToggle] = React.useState(true)*/}

    async function send_otp()
    {
        //Email Field Validations
        if(userEmail==="")
        {
            setPopupMessage("Email field cannot be blank.");
            setPopup(true);
            
        }
        else if(!/^[a-zA-Z]+[a-zA-Z0-9]*\.[a-zA-Z]+[a-zA-Z0-9]*@bsci.com$/.test(userEmail))
        {
            setPopupMessage("Invalid email format detected. Please enter a valid email address (Ex : firstname.lastname@bsci.com)")
            setPopup(true)
        }
        else
        {
            setLoadingPopup(true)
            const details = {
                email:userEmail.replace(/\s/g, "").toLowerCase(),
                //Storing user type in title case
                type:login_type[0].toUpperCase() + login_type.slice(1).toLowerCase()
            }

            //Adding user details to url for GET request
            const params = new URLSearchParams(details);
            const URLWithParams = `http://10.220.20.246:8000/checkUser?${params}`;
            await axios.get(URLWithParams).then( (res) =>
            {
                setLoadingPopup(false)
                setUserNotFound(true)
                const result=res.data.result
                //result=1 implies that the user does not already exist in the database
                if(result==1)
                {                   
                    if(login_type==="ADMIN")
                    {
                        setPopupMessage("No admin found with this email.")
                        setPopup(true)
                    }
                    else if(login_type==="MANAGER")
                    {
                        setPopupMessage("It appears that there is no manager in the system with this email address. Please ask the admin to add you.")
                        setPopup(true)
                    }
                    else if(login_type==="TRAINER")
                    {
                        setPopupMessage("It appears that there is no trainer in the system with this email address. Please ask your manager to add you.")
                        setPopup(true)
                    }
                    else if(login_type==="TRAINEE")
                    {
                        setPopupMessage("The Knowledge Transfer (KT) Process has not been initiated for you yet. Please ask your manager to initiate the KT process for you.")
                        setPopup(true)
                    }
                }
                else
                {
                    /*DEV OPTIONS - DISABLING OTP SYSTEM DURING DEVELOPMENT
                    if(!toggle)
                    {*/
                        setOtpSnackBarMessage("OTP sent successfully!")
                        setOtpSnackBarType("success")
                        setOtpSnackBar(true)
                        setOtpSent(true)
                        //setStart(Date.now())  //NECESSARY FOR COUTNDOWN
                        setCKey(cKey+1) // Changing the key resets the timer
                        setOtpId(otpId+1) //Changing the id resets the text field
                        axios.post("http://10.220.20.246:8000/Login",details)  /*
                    }
                    else         
                    {          
                        props.setUserEmail(userEmail)
                        props.setUserType(String(login_type.charAt(0).toUpperCase()+login_type.slice(1).toLowerCase()))
                        props.login(userEmail,String(login_type.charAt(0).toUpperCase()+login_type.slice(1).toLowerCase()))                  
                        navigate("/"+login_type.toLowerCase()+"_dashboard",{replace:true})        
                    }    */         
                }
            })
        }
    }

    async function login()
    {
        //Email Field Validations
        if(userEmail==="")
        {
            setPopupMessage("Email field cannot be blank.");
            setPopup(true);
            setOtpSent(false)
        }
        if(otp==="")
        {
            setPopupMessage("OTP field cannot be blank.");
            setPopup(true);
        }
        if(!Number(otp)||Number(otp)>999999)
        {
            setPopupMessage("Invalid OTP (Please enter a 6 digit number).");
            setPopup(true);
        }
        else
        {
            setLoadingPopup(true)
            const details = {
                email:userEmail.replace(/\s/g, "").toLowerCase(),
                //Storing user type in title case
                type:login_type[0].toUpperCase() + login_type.slice(1).toLowerCase(),
                otp:otp //If sent as a number, it may alter the value. Ex : otp = 008732. Number(otp) returns 8732 which will not match the value in the DB
            }

            //Adding user details to url for GET request
            const params = new URLSearchParams(details);
            const URLWithParams = `http://10.220.20.246:8000/verifyOTP?${params}`;
            await axios.get(URLWithParams).then( (res) =>
            {
                setLoadingPopup(false)
                if(res.data.correctOtp==1)
                {
                    props.setUserEmail(userEmail)
                    props.setUserType(String(login_type.charAt(0).toUpperCase()+login_type.slice(1).toLowerCase()))
                    props.login(userEmail,String(login_type.charAt(0).toUpperCase()+login_type.slice(1).toLowerCase()))                  
                    navigate("/"+login_type.toLowerCase()+"_dashboard",{replace:true})        
                }
                else
                {
                    setPopupMessage("Incorrect OTP Entered. Please Retry!")
                    setPopup(true)
                }
            })
        }

       

    }

    //In case user is already logged in
    useEffect(()=>{
        if(props.userEmail!=null&&props.userType==String(login_type.charAt(0).toUpperCase()+login_type.slice(1).toLowerCase()))
        {
            navigate("/"+login_type.toLowerCase()+"_dashboard",{replace:true})
        }

    },[])





  return (
    <div className="bg">
    <Navbar noHomeLogout={true}/>
        <div className="login">

            <h1>{login_type} LOGIN</h1>
            <br/>
            {
                otpSent
                ?
                <div>
	 	            <h3>Enter One Time Password</h3>
                    <p>{"(received on : "+userEmail.replace(/\s/g, "").toLowerCase()+")"}</p>
	 	            <br/>
                    {/* 'e' denotes an event (change in value) and target stores the tag where it occurred and target.value returns the changed value that is stored in userEmail*/}    
	 	            <center><input type="text" defaultValue={null} value={otp} onChange={(e)=>(setOtp(e.target.value))} required className="form-control number" id={otpId} placeholder="Enter OTP..." style={{width:"50%"}} name="otp"/></center>
	                <br/>
	                <div id="login_button" onClick={login} type="button" className="btn btn-info btn-lg" style={{padding:"1%",marginTop:"2%", width:"10%"}}>
                        Login
                    </div>
                    &emsp;
                    <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>Resend a One-Time-Password to your registered BSCI email address</span>}>
                        <div id="get_otp_button" onClick={send_otp} type="button" className="btn btn-info btn-lg" style={{padding:"1%",marginTop:"2%", width:"10%"}}>
                            Resend OTP
                        </div>
                    </Tooltip>
                    <br/>
                    <br/>
                    {/*
                    When multiple users were using the system it was found that this 5 minute approach slowed down the server. A potential solution is scheudling a job of 
                    deleting the otp 5 minutes after the user logs in (like a cron job in unix systems). For now the OTP is valid till the user logs in/ requests another otp
                    OTP valid till : <Countdown date={start + 300100} key={cKey} onComplete={()=>{setTimeUp("True")}}/> //Unit is  in milliseconds, 300 seconds*1000=300100 milliseconds (5 minutes) 
                    <Snackbar open={timeUp} autoHideDuration={6000} onClose={()=>{setTimeUp(false)}}>
                        <Alert variant="filled" onClose={()=>{setTimeUp(false)}} severity="error">
                          <AlertTitle><strong style={{fontSize:"120%"}}>OTP no longer valid!</strong></AlertTitle>
                        </Alert>
                    </Snackbar>
                    */}

                </div>
                :
                <div>
	 	            <h3>Email Address (Official BSCI)</h3>
	 	            <br/>
                    {/* 'e' denotes an event (change in value) and target stores the tag where it occurred and target.value returns the changed value that is stored in userEmail*/}    
	 	            <center><input type="email" value={userEmail} onChange={(e)=>(setUserEmail(e.target.value))} required className="form-control email1" id="email" placeholder="Enter Email..." style={{width:"50%"}} name="email"/></center>
	                <br/>
	                <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>Receive a One-Time-Password on your registered BSCI email address</span>}>
                        <div id="get_otp_button" onClick={send_otp} type="button" className="btn btn-info btn-lg" style={{padding:"1%",marginTop:"2%", width:"8%"}}>
                            Get OTP
                        </div>
                    </Tooltip>
                  
                    {/*DEV OPTIONS - DISABLING OTP SYSTEM DURING DEVELOPMENT
                    <br/>
                    <br/>
                    <span><FormControlLabel control={<Switch checked={toggle} onChange={()=>{setToggle(!toggle)}}/>} style={{color:"white"}} label="DISABLE OTP" /> &emsp; </span>    
                    */}
                </div>

            }
            <div class="footer">
                <p><span class="align-left">Developed by: Sidharth Saxena, Yogesh Naik</span>© 2024 Copyright: Boston Scientific India</p>
            </div>

            <Popup open={popup} closeOnDocumentClick onClose={()=>{setPopup(false); if(!otpSent&&userNotFound){navigate("/",{replace:true})};}}>
               <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setPopup(false); if(!otpSent&&userNotFound){navigate("/",{replace:true})};}}/>
               <center>
                 <br/>
                 <p style={{color:"#003C71", fontSize:"130%", paddingLeft:"2%",paddingRight:"2%"}}><center>{popupMessage}</center></p>
                 <br/>
                 <div className='btn btn-info btn-lg' onClick={()=>{setPopup(false); if(!otpSent&&userNotFound){navigate("/",{replace:true})};}}>Ok</div>
                 <br/>
                 <br/>
               </center>
            </Popup> 

            {/* OTP STATUS SNACKBAR (SENT OR INVALID) */}
            <Snackbar open={otpSnackBar} autoHideDuration={6000} onClose={()=>{setOtpSnackBar(false)}} >
              <Alert variant="filled" onClose={()=>{setOtpSnackBar(false)}} severity={otpSnackBarType}>
                <AlertTitle><strong style={{fontSize:"120%"}}>{otpSnackBarMessage}</strong></AlertTitle>
              </Alert>
            </Snackbar>

          {/* LOADING SCREEN */}
          <Popup open={loadingPopup} hideBackdrop closeOnDocumentClick={false} onClose={()=>{setLoadingPopup(false)}}>
               <center>
                 <p style={{color:"#003C71", fontSize:"130%", margin:"3%"}}><center>PLEASE WAIT...</center></p>
                 <br/>
                 <CircularProgress/>
                 <br/>
                 <br/>
               </center>
          </Popup> 
           
        </div>
    </div>
  );

}

export default Login;
