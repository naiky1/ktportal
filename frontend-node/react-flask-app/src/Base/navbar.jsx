import './navbar.css'
import {Link} from 'react-router-dom';
import Tooltip from '@mui/material/Tooltip';
import Zoom from '@mui/material/Zoom';
import {logout} from './login_functions.jsx' 
import HomeIcon from '@mui/icons-material/Home';
import LogoutIcon from '@mui/icons-material/Logout';
import AddBoxIcon from '@mui/icons-material/AddBox';

function Navbar(props)
{

    return(
    <div>
        <body style={{margin:"0px"}} className="body-content">
            <header className="navbar">
                <table style={{width:"100%"}}>
                    <tr style={{width:"100%"}}>
                        <td className="logo">
            				<a href="/">
            					<img src="https://performancemanager4.successfactors.com/public/ui-resource/BScientific/83;mod=400b4062b5272113ccc2aec6f8a666ec" alt="BSCI Logo"/>
            				</a>
                        </td>
            			<td className="home">
                            {
                                props.noHomeLogout

                                ?
                                <></>
                                :

            				    <a href={props.dashboard_link} className="btn btn-info btn-lg">                                   
            				        <HomeIcon fontSize='inherit'/> Home
            				    </a>
                            }
                        </td>

            			<td className="CRM-logo">                     
                        {
                            props.type=="Manager"||props.type=="Admin"&&!props.noAddTrainee                        
                            ? 
                            <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>Add a trainer and initiate KT</span>}>
                               <Link to="/add_trainee_initiate_kt" state={{type:props.type}} className='btn btn-info btn-lg' >
                                    <AddBoxIcon fontSize='inherit'/> Add Trainee KT Plan
                               </Link>
                            </Tooltip> 
                            : 
                            <></>
                        }
                        &emsp;
                        {
                            props.noHomeLogout
                            ?
                            <></>
                            :
                            <Link to="/" onClick={()=>{logout(props.setUserEmail,props.setUserType)}} className='btn btn-info btn-lg'>
                                <LogoutIcon fontSize='inherit'/> Logout
                            </Link>
                        }&emsp;
            			NEW JOINEE ONBOARDING
                        </td>
                    </tr>
                </table>
            </header>
        </body>
    </div>

    )
}
export default Navbar;