import React, { useEffect, useState } from 'react'
import Login_type_selection from './Base/login_type_selection.jsx'
import Login from './Base/login.jsx'
import Knowledge_base from './Base/knowledge_base.jsx'
import Trainee_dashboard from './User_Types/Trainee/trainee_dashboard.jsx'
import Assessment_Page from './User_Types/Trainee/Assessment_Page.jsx'
import Trainer_dashboard from './User_Types/Trainer/trainer_dashboard.jsx'
import Manager_dashboard from './User_Types/Manager/manager_dashboard.jsx'
import Trainee_progress from './User_Types/Trainer/trainee_progress.jsx'
import Re_Assign_Trainings from './User_Types/Trainer/re_assign_trainings.jsx'
import Trainee_progress_manager from './User_Types/Manager/trainee_progress_manger.jsx'
import Trainee_learning_path from './User_Types/Manager/trainee_learning_path.jsx'
import Add_trainee_initiate_kt from './User_Types/Manager/add_trainee_initiate_kt.jsx'
import Admin_dashboard from './User_Types/Admin/admin_dashboard.jsx'
import Manage_users from './User_Types/Admin/manage_users.jsx'
import {Routes, BrowserRouter as Router, Route} from 'react-router-dom'
import {login} from './Base/login_functions.jsx' 
import Invalid_Path from './Base/invalid_path.jsx'


function App()
{
  const [userEmail,setUserEmail] = useState(localStorage.getItem('userEmail')?localStorage.getItem('userEmail'):null)
  const [userType,setUserType] = useState(localStorage.getItem('userType')?localStorage.getItem('userType'):null)

    return (

        <>
        <Router>
          <Routes>
         {/* For redirecting to homepage if invalid url is entered*/}
        <Route path='*' element={<Invalid_Path/>} />
    		<Route exact path="/" element =
              {
               <Login_type_selection/>
              }
            />
            <Route exact path="/login" element =
              {
                <Login login={login} userEmail={userEmail} setUserEmail={setUserEmail} setUserType={setUserType} userType={userType} /> //To set user details for dashboard and subsequent use cases
              }
            />
            <Route exact path="/knowledge_base" element =
              {
                <Knowledge_base login={login} userEmail={userEmail} setUserEmail={setUserEmail} setUserType={setUserType} userType={userType} /> //To set user details for dashboard and subsequent use cases
              }
            />
            <Route exact path="/trainee_dashboard" element =
            {
             //Passing user details necessary for rendering the dashboard
             userEmail && userType=="Trainee" ? <Trainee_dashboard userEmail={userEmail} setUserEmail={setUserEmail}  setUserType={setUserType}/> : <Login_type_selection/>
            }
            />
            <Route exact path="/trainer_dashboard" element =
            {
              userEmail && userType=="Trainer" ? <Trainer_dashboard userEmail={userEmail} setUserEmail={setUserEmail}  setUserType={setUserType}/> : <Login_type_selection/>
            }
            />
            <Route exact path="/trainee_progress" element =
            {
              userEmail && (userType=="Trainer"||userType=="Admin") ? <Trainee_progress userEmail={userEmail} setUserEmail={setUserEmail}  setUserType={setUserType}/> : <Login_type_selection/>
            }
            />
            <Route exact path="/reverse_kt_re_assign_trainings" element =
            {
              userEmail && userType=="Trainer" ? <Re_Assign_Trainings userEmail={userEmail} setUserEmail={setUserEmail}  setUserType={setUserType}/> : <Login_type_selection/>
            }
            />
            <Route exact path="/manager_dashboard" element =
            {
             //Passing user details necessary for rendering the dashboard
              userEmail && userType=="Manager" ? <Manager_dashboard userEmail={userEmail} setUserEmail={setUserEmail}   setUserType={setUserType}/> : <Login_type_selection/>
            }
            />
            <Route exact path="/trainee_progress_manager" element =
            {
             //Passing user details necessary for rendering the dashboard
              userEmail && userType=="Manager" ? <Trainee_progress_manager userEmail={userEmail} setUserEmail={setUserEmail}   setUserType={setUserType}/> : <Login_type_selection/>
            }
            />
             <Route exact path="/trainee_learning_plan" element =
            {
             //Passing user details necessary for rendering the dashboard
              userEmail && (userType=="Trainer"||userType=="Admin"||userType=="Manager") ? <Trainee_learning_path userEmail={userEmail} setUserEmail={setUserEmail}   setUserType={setUserType}/> : <Login_type_selection/>
            }
            />
             <Route exact path="/add_trainee_initiate_kt" element =
            {
              userEmail && (userType=="Manager"||userType=="Admin")  ? <Add_trainee_initiate_kt userEmail={userEmail} setUserEmail={setUserEmail}  setUserType={setUserType}/> : <Login_type_selection/>
            }
            />
            <Route exact path="/admin_dashboard" element =
            {
              userEmail && userType=="Admin" ? <Admin_dashboard userEmail={userEmail} setUserEmail={setUserEmail} setUserType={setUserType}/> : <Login_type_selection/>
            }
            />
            <Route exact path="/manage_users" element =
            {
              userEmail && userType=="Admin" ? <Manage_users userEmail={userEmail} setUserEmail={setUserEmail} setUserType={setUserType}/> : <Login_type_selection/>
            }
            />
            <Route exact path="/assessment_page" element =
            {
             //Passing user details necessary for rendering the dashboard
             userEmail && userType=="Trainee" ? <Assessment_Page userEmail={userEmail} setUserEmail={setUserEmail}  setUserType={setUserType}/> : <Login_type_selection/>
            }
          />
           </Routes>
        </Router>
        </>
    );
}

export default App