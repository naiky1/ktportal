import React, {useEffect, useState} from 'react';
import Navbar from '../../Base/navbar.jsx'
import './add_trainee_initiate_kt.css';
import { useLocation } from 'react-router-dom';
import axios from 'axios';
import Popup from 'reactjs-popup';
import {useNavigate} from 'react-router-dom';
import TextField from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import { CircularProgress } from '@mui/material';
import CancelIcon from '@mui/icons-material/Cancel';


function Add_trainee_initiate_kt(props)
{

    //To get the user type from the previous page
    const location = useLocation();
    const type = location.state.type;
    const managerEmail = type=="Manager"?props.userEmail:null

    //Loading Screen
    const [loadingPopup,setLoadingPopup] = useState(false)
    
    //To redirect back to dashboard
    let navigate = useNavigate();

    const traineetypes = [
        "All Modules",
        "Only Mandatory Modules",
    ];

    //For storing project names if needed
    const [projectNames,setProjectNames] = React.useState([]);
    //For storing project IDs if needed
    const [projectIds,setProjectIds] = React.useState([]);

    //If the user is logging in for the first time, details need to be stored
    const [userTemp,setUserTemp] = React.useState({
        email:"",
        name:"",
        traineeType:null,
        trainerEmail:null,
        trainerName:"",
        managerEmail:managerEmail,
        managerName:"",
        projectAssigned:null
    });

    const [isChecked, setChecked] = React.useState(false);

    //Trainers for drop-down
    const [trainers,setTrainers] = useState([])

    //Managers for drop-down
    const [managers,setManagers] = useState([])

    //For Adding Trainer Popup
    const [addTrainerPopup,setAddTrainerPopup] =  useState(false)
    const [newTrainerEmail,setNewTrainerEmail] = useState("")

    //For Adding Manager Popup
    const [addManagerPopup,setAddManagerPopup] =  useState(false)
    const [newManagerEmail,setNewManagerEmail] = useState("")


    //Declaring variables to store user email, popup display status and popup message
    const [popup,setPopup] = React.useState(false);
    const [popupMessage,setPopupMessage] = React.useState("");
    const [addedTraineeFlag,setAddedTraineeFlag] = React.useState(false)

    //Fetching Project Names
    const fetch_details = async () =>
    {
        const result = await axios.get("http://10.220.20.246:8000/fetchAllProjectsDetails");
        setProjectNames(result.data.projectNames);
        setProjectIds(result.data.projectIds);

        if(managerEmail)
        {
          const details = 
          {
              userEmail : managerEmail,
              userType : "Manager"
          }
          const Params = new URLSearchParams(details);
          const URLWithParams = `http://10.220.20.246:8000/fetchUserName?${Params}`;
          await axios.get(URLWithParams).then( res=> {userTemp.managerName=res.data.userName})
        }
        else
        {
          await axios.get("http://10.220.20.246:8000/fetchAllManagers").then(res=>{setManagers(res.data.managers)})
        }
        await axios.get("http://10.220.20.246:8000/fetchAllTrainers").then(res=>{setTrainers(res.data.trainers)})
    }

    useEffect(() =>
    {
        fetch_details()
    },[])


    function HandleChangeTemp(e)
    {
        // 'e' denotes an event (change in value) and target stores the tag where it occurred. target.name returns the name of the tag where the change happened target.value returns the changed value
        const {name,value} = e.target

        setUserTemp({
                ...userTemp, //This keeps all attributes that are not edited (based on e.target.name), the same.
                [name]:value // Only updates the attribute where a change occurred.
            })
    }

    function handleChange(e)
    {
   setChecked(e.target.checked);
    }

    //Extracting name from email (format : firstName.lastName@bsci.com)
    function nameExtractor(email)
    {
        const result = email.split(".");
        const firstName = result[0][0].toUpperCase() + result[0].slice(1).toLowerCase();
        const result2 = result[1].split("@");
        const lastName = result2[0][0].toUpperCase() + result2[0].slice(1).toLowerCase();
        var fullName = firstName+" "+lastName; 
        fullName=fullName.replace(/[0-9]+/g,"");
        return fullName;
    }

    async function initiateKt()
    {
        if(userTemp.email===""||userTemp.traineeType===null||userTemp.trainerEmail===null||userTemp.projectAssigned===null||userTemp.managerEmail===null)
        {
            setPopupMessage("None of these fields can be blank.")
            setPopup(true)
        }
        else if(!/^[a-zA-Z0-9]+\.[a-zA-Z0-9]+@bsci.com$/.test(userTemp.email))
        {
            setPopupMessage("Invalid trainee email format detected. Please enter a valid email address (Ex : firstname.lastname@bsci.com)")
            setPopup(true)
        }
        else
        {
            setLoadingPopup(true)
            userTemp.name=nameExtractor(userTemp.email);
            userTemp.trainerName=nameExtractor(userTemp.trainerEmail);
            await axios.post("http://10.220.20.246:8000/addTraineeInitiateKt",userTemp).then(res=>
            {
                setLoadingPopup(false)
                res.data.success==1?setAddedTraineeFlag(true):setAddedTraineeFlag(false)               
                setPopupMessage(String(res.data.message));
                setPopup(true);
                
            })   
        }
    }

    function addNewTrainerClick()
    {
      setAddTrainerPopup(true);
    }
    function addNewManagerClick()
    {
      setAddManagerPopup(true);
    }

    async function addTrainer()
    {    

      if(newTrainerEmail==="")
      {
            setPopupMessage("No Email Entered!");
            setPopup(true);
            setAddTrainerPopup(false);
      }
      else if(!/^[a-zA-Z0-9]+\.[a-zA-Z0-9]+@bsci.com$/.test(newTrainerEmail))
      {
            setPopupMessage("Invalid email format detected. Please enter a valid email address (Ex : firstname.lastname@bsci.com)")
            setPopup(true);
            setAddTrainerPopup(false);
      }
      else
      {
          setAddTrainerPopup(false);
          setLoadingPopup(true)
          const newTrainerName=nameExtractor(newTrainerEmail);
          const details={
            userEmail:newTrainerEmail,
            userName:newTrainerName,
            userType:"Trainer"
          }
          await axios.post("http://10.220.20.246:8000/addUser",details).then(res=>
          {
            setLoadingPopup(false)
            setPopupMessage(res.data.message);
            setPopup(true);
            fetch_details();
          })
          
      }
    }

    async function addManager()
    {
    
      if(newManagerEmail==="")
      {
          setPopupMessage("No Email Entered!");
          setPopup(true);
          setAddManagerPopup(false);
      }
      else if(!/^[a-zA-Z0-9]+\.[a-zA-Z0-9]+@bsci.com$/.test(newManagerEmail))
      {
          setPopupMessage("Invalid email format detected. Please enter a valid email address (Ex : firstname.lastname@bsci.com)")
          setPopup(true);
          setAddManagerPopup(false);
      }
      else
      {
          setAddManagerPopup(false)
          setLoadingPopup(true)
          const newManagerName=nameExtractor(newManagerEmail);
          const details={
            userEmail:newManagerEmail,
            userName:newManagerName,
            userType:"Manager"
          }
          await axios.post("http://10.220.20.246:8000/addUser",details).then(res=>
          {
            setLoadingPopup(false)
            setPopupMessage(res.data.message);
            setPopup(true);
            fetch_details();
          })
          
      }
    }



  return (
    <div className="bg">
    <Navbar setUserEmail={props.setUserEmail} setUserType={props.setUserType} dashboard_link={"/"+location.state.type.toLowerCase()+"_dashboard"}/>
    <div className="login">
         <Popup open={popup} closeOnDocumentClick onClose={()=>{setPopup(false)}}>
           <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setPopup(false);}}/>
            <center>
              <br/>              
              <p style={{color:"#003C71", fontSize:"130%"}}><center>{popupMessage}</center></p>
              <br/>
              <div className='btn btn-info btn-lg' onClick={()=>{setPopup(false); if(addedTraineeFlag==true){managerEmail?navigate("/manager_dashboard",{replace:true}):navigate("/admin_dashboard",{replace:true})}}}>Ok</div>
              <br/>          
              <br/>
            </center>
         </Popup>
         <Popup open={addTrainerPopup} closeOnDocumentClick onClose={()=>{setAddTrainerPopup(false);}}>
            <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setAddTrainerPopup(false);}}/>
            <center >
              <br/>
              <h2>ADD TRAINER</h2>
              <br/>
              <h3>Enter Official BSCI Email Address</h3>
              <input type="text" onChange={(e)=>(setNewTrainerEmail(e.target.value))} style={{width:"80%", padding:"2%"}} value={newTrainerEmail} placeholder={"Enter Trainer Email..."}/>
              <br/>
              <br/>
              <div className='btn btn-info btn-lg' onClick={addTrainer}>Add</div> &emsp;
              <div className='btn btn-info btn-lg' onClick={()=>{setAddTrainerPopup(false);}}>Cancel</div>
              <br/>         
              <br/>
            </center>
         </Popup>
         <Popup open={addManagerPopup} closeOnDocumentClick onClose={()=>{setAddManagerPopup(false);}}>
          <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setAddManagerPopup(false);}}/>
            <center >
              <br/>
              <h2>ADD MANAGER</h2>
              <br/>
              <h3>Enter Official BSCI Email Address</h3>
              <input type="text" onChange={(e)=>(setNewManagerEmail(e.target.value))} style={{width:"80%", padding:"2%"}} value={newManagerEmail} placeholder={"Enter Manager Email..."}/>
              <br/>
              <br/>
              <div className='btn btn-info btn-lg' onClick={addManager}>Add</div> &emsp;
              <div className='btn btn-info btn-lg' onClick={()=>{setAddManagerPopup(false);}}>Cancel</div>
              <br/>         
              <br/>
            </center>
         </Popup>

            {/* LOADING SCREEN */}
            <Popup open={loadingPopup} hideBackdrop closeOnDocumentClick={false} onClose={()=>{setLoadingPopup(false)}}>
               <center>
                 <p style={{color:"#003C71", fontSize:"130%", margin:"3%"}}><center>PLEASE WAIT...</center></p>
                 <br/>
                 <CircularProgress/>
                 <br/>
                 <br/>
               </center>
            </Popup> 

        <div style={{width:"80%",background:"white",borderRadius:"10px",margin:"auto",color:"#003C71"}}>
            <br/>
            <h1>ADD TRAINEE AND INITIATE KT</h1>
            <br/>
	 	    <h3>Trainee Email Address (Official BSCI)</h3>
	 	    <br/>
	 	    <center>
                <TextField style={{background:"white",color:"white",width:"50%"}} variant="outlined" name="email" value={userTemp.email} onChange={HandleChangeTemp}  label="Trainee Email" />
            </center>
	 	    <br/>
	 	    <h3>Training Requirement</h3>
	 	    <center>
                <FormControl style={{width:"50%"}}>
                <span>
                  <InputLabel id="Trainee_Type_Select">Module Type</InputLabel>
                  <Select
                    labelId="Trainee_Type_Select"
                    value={userTemp.traineeType}
                     style={{width:"100%"}}
                     label="Training Requirement"
                     onChange={(e)=>setUserTemp({...userTemp, traineeType:e.target.value})}
                    >
                    {
                      traineetypes.map((traineetype) =>
                      (
                        <MenuItem value={traineetype}>{traineetype}</MenuItem>
                      ))
                    }
                  </Select>
                </span>
            </FormControl>
            </center>
	 	    <br/>
	 	    <h3>Trainer</h3>
	 	    <br/>
	 	      <center>
             <FormControl style={{width:"50%"}}>
                <span>
                  <InputLabel id="Trainer_Select">Trainer</InputLabel>
                  <Select                            
                    labelId="Trainer_Select"
                     value={userTemp.trainerEmail}
                     style={{width:"100%"}}
                     label="Trainer"
                     onChange={(e)=>{e.target.value=="add new"?addNewTrainerClick(e):setUserTemp({...userTemp, trainerEmail:e.target.value})}}                               
                    >   
                    <MenuItem value={null}>None</MenuItem>         
                    <MenuItem value="add new"><AddCircleOutlineIcon/>&emsp;Add New Trainer</MenuItem>     
                    {                                                           
                      trainers.map((trainer) => 
                      (                        
                        <MenuItem value={trainer.User_Email}>{trainer.User_Name}</MenuItem>
                      ))
                    }                   
                  </Select>                 
                </span>
            </FormControl>    
            </center>
	        <br/>
	 	    <h3>Manager</h3>
	 	    <br/>
	 	      <center>
          {
            managerEmail 
            ?
            <TextField variant="filled" label="Manager" value={userTemp.managerName} InputProps={{readOnly: true}} style={{width:"50%"}}/>
            :
            <FormControl style={{width:"50%"}}>
                <span>
                  <InputLabel id="Manager_Select">Manager</InputLabel>
                  <Select                            
                    labelId="Manager_Select"
                     value={userTemp.managerEmail}
                     style={{width:"100%"}}
                     label="Manager"
                     onChange={(e)=>{e.target.value=="add new"?addNewManagerClick(e):setUserTemp({...userTemp, managerEmail:e.target.value})}}                               
                    >   
                    <MenuItem value={null}>None</MenuItem>         
                    <MenuItem value="add new"><AddCircleOutlineIcon/>&emsp;Add New Manager</MenuItem>     
                    {                                                           
                      managers.map((manager) => 
                      (                        
                        <MenuItem value={manager.User_Email}>{manager.User_Name}</MenuItem>
                      ))
                    }                   
                  </Select>                 
                </span>
            </FormControl>  
          }
          </center>
	      <br/>
	 	    <h3>Project Assigned</h3>
	 	    <br/>
	 	    <center>
             <FormControl style={{width:"50%"}}>
                <span>
                  <InputLabel id="Trainer_Select">Project</InputLabel>
                  <Select                            
                    labelId="Trainer_Select"
                     value={userTemp.projectAssigned}
                     style={{width:"100%"}}
                     label="Project"
                     onChange={(e)=>{setUserTemp({...userTemp, projectAssigned:e.target.value})}}                               
                    >   
                    <MenuItem value={null}>None</MenuItem>         
                    {                                                           
                      projectIds.map((projectId,index) => 
                      (                        
                        <MenuItem value={projectId}>{projectNames[index]}</MenuItem>
                      ))
                    }                   
                  </Select>                 
                </span>
            </FormControl> 
            </center>
	        <br/>
	        <button onClick={initiateKt} className="btn btn-success btn-lg" style={{padding:"1%",marginTop:"2%"}}>Initiate KT</button>
            <br/>
            <br/>
        </div>
        <br/>
    </div>
    </div>
  );
}

export default Add_trainee_initiate_kt;
