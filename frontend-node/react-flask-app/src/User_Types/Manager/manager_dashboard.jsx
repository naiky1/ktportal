import React,{useState,useEffect} from 'react'
import Navbar from '../../Base/navbar.jsx'
import './manager_dashboard.css'
import axios from 'axios'
import Popup from 'reactjs-popup'
import {Link} from 'react-router-dom'
import Paper from '@mui/material/Paper';
import { Line } from 'rc-progress';
import DataTable from 'react-data-table-component'
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import MenuBookIcon from '@mui/icons-material/MenuBook';
import ChildCareIcon from '@mui/icons-material/ChildCare';
import SchoolIcon from '@mui/icons-material/School';
import Switch from '@mui/material/Switch';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import Tooltip from '@mui/material/Tooltip';
import Zoom from '@mui/material/Zoom';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import { CircularProgress } from '@mui/material'
import CancelIcon from '@mui/icons-material/Cancel';

function Manager_Dashboard(props)
{
    //Dynamic CSS Variables
    const [ktCompletionStyling,setKtCompletionStyling] = useState({
        whitegradient:0,
        greengradient:0,
        hoverbgsize:0
    })

    const managerEmail= props.userEmail;
    const [managerName,setManagerName] = useState("");

    //Loading Screen
    const [loadingPopup,setLoadingPopup] = useState(false)

    //Table Data Table
    const [tableData,setTableData] = useState([]);
    const [filteredTableData,setFilteredTableData] = useState([]);
    const [traineeSearch,setTraineeSearch] = useState("");
    const [trainerSearch,setTrainerSearch] = useState("");

    //Toggle for switch
    const [projectSwitchChecked,setProjectSwitchChecked] = useState(false)
    const [editProjectToggle,setEditProjectToggle] = useState(false);
    const [trainerSwitchChecked,setTrainerSwitchChecked] = useState(false)
    const [editTrainerToggle,setEditTrainerToggle] = useState(false);

    //Dashboard Data
    const [trainerCount,setTrainerCount] = useState(0)
    const [ktsCompleteCount,setKtsCompleteCount] = useState(0)

    //New Trainer and Project values
    const [newTrainerIds,setNewTrainerIds] = useState([])
    const [newProjectIds,setNewProjectIds] = useState([])
    const [trainers,setTrainers] = useState([])
    const [projects,setProjects] = useState([])

    //row height for the table needs to be higher than default to accommodate the row height increase when project/trainer is to be edited
    const customStyles =
    {
        rows:
        {
          style: { minHeight: "10vh", /* override the row height*/ }
        }
    }


    //Searching Table Data
    useEffect(() =>
    {
        const result = tableData.filter( (row) =>
        {
            if(trainerSearch=="")
                return row.traineeName.toLowerCase().match(traineeSearch.toLowerCase());
            else if(traineeSearch=="")
                return row.trainerName.toLowerCase().match(trainerSearch.toLowerCase());
            else
                return (row.trainerName.toLowerCase().match(trainerSearch.toLowerCase())&&row.traineeName.toLowerCase().match(traineeSearch.toLowerCase()));
        })
        setFilteredTableData(result);

    },[trainerSearch,traineeSearch]);



    //For Popup
    const [popup,setPopup] =  useState(false)
    const [popupMessage,setPopupMessage] = useState([])
    const [innerPopup,setInnerPopup] = useState(false)
    const [innerPopupMessage,setInnerPopupMessage] = useState("")

    //For Popup 2 (Saving Changes)
    const [popup2,setPopup2] =  useState(false)
    const [popupMessage2,setPopupMessage2] = useState("")

    //For Adding Trainer Popup
    const [addTrainerPopup,setAddTrainerPopup] =  useState(false)
    const [newTrainerEmail,setNewTrainerEmail] = useState("")


    const fetch_details = async () =>
    {
        const details = {
            userEmail : managerEmail,
            userType : "Manager"
        }
        const Params = new URLSearchParams(details);
        const URLWithParams = `http://10.220.20.246:8000/fetchUserName?${Params}`;
        await axios.get(URLWithParams).then( res=> {setManagerName(res.data.userName)})       
        const ManagerTableURLWithParams = `http://10.220.20.246:8000/fetchManagerTableDetails?${Params}`;
        await axios.get(ManagerTableURLWithParams).then( res=>
        {
            const progress=res.data.progress;
            if(progress<50)
            {
                ktCompletionStyling.whitegradient=100-progress;
                ktCompletionStyling.greengradient=progress;
                ktCompletionStyling.hoverbgsize=100;
            }
            else
            {
                ktCompletionStyling.whitegradient=50;
                ktCompletionStyling.greengradient=50;
                ktCompletionStyling.hoverbgsize=2*progress;
            }
            setTableData(res.data.tableData);
            setFilteredTableData(res.data.tableData);
            setTrainerCount(res.data.trainerCount);
            setKtsCompleteCount(res.data.ktsCompleteCount);

        });
        await axios.get("http://10.220.20.246:8000/fetchAllProjects").then(res=>{setProjects(res.data.projects)})
        await axios.get("http://10.220.20.246:8000/fetchAllTrainers").then(res=>{setTrainers(res.data.trainers)})
    }


     useEffect(() =>
    {
        fetch_details()

    },[])

    useEffect(() =>
    {
      setNewTrainerIds([])

    },[editTrainerToggle])

    useEffect(() =>
    {
      setNewProjectIds([])

    },[editProjectToggle])


    function addNewTrainerClick()
    {
      setAddTrainerPopup(true);
    }


    //TABLE HEADERS
    var columns =[
      {
        name:"S. No.",
        width:"7%",
        selector: (row) => row.index,
        sortable : true,
        center:true
      },
      {
        name:"TRAINEE",
        width:"13%",
        selector: (row) => row.traineeName,
        sortable : true,
        cell :  (row) =>
                        <div style={{width:"100%"}}>
                          {row.traineeName+" - "+row.progress+'%'}
                          <Line percent={row.progress} strokeWidth={4} strokeColor={row.status==1?"#0F9D58":"#DB4437"} />
                        </div> ,
        center:true
      },
      {
        name:"TRAINER",
        width:"13%",
        selector: (row) => row.trainerName,
        sortable : true,
        cell: (row) =>  <div style={{width:"100%"}}>
                        {
                            editTrainerToggle
                            ?
                            <FormControl fullWidth>
                            <span>
                              <InputLabel id="Trainer_Select">Change Trainer</InputLabel>
                              <Select                            
                                labelId="Trainer_Select"
                                 value={newTrainerIds[row.index-1]}
                                 style={{width:"100%"}}
                                 label="Change Trainer"
                                 onChange={(e)=>{e.target.value=="add new"?addNewTrainerClick(e):newTrainerIds[row.index-1]=e.target.value}}                               
                                >   
                                <MenuItem value={null}>None</MenuItem>         
                                <MenuItem value="add new"><AddCircleOutlineIcon/>&emsp;Add New Trainer</MenuItem>     
                               {                                                           
                                  trainers.map( (trainer) => 
                                  (                        
                                    <MenuItem value={trainer.User_ID}>{trainer.User_Name}</MenuItem>
                                  ))
                                }
                                
                              </Select>
                              
                            </span>
                              </FormControl>

                            :
                            <span>{row.trainerName}</span>
                                        
                        }
                        </div>,
        center:true
                        
      },
      {
        name:"PROJECT",
        width:"6%",
        selector: (row) => row.project,
        sortable : true,
        cell: (row) => <div style={{width:"100%"}}>
                        {
                            editProjectToggle
                            ?
                            <FormControl fullWidth>
                            <InputLabel id="Project_Select">Change Project</InputLabel>
                            <Select
                              labelId="Project_Select"
                               value={newProjectIds[row.index-1]}
                               style={{width:"100%"}}
                               label="Change Project"
                               onChange={(e)=>{newProjectIds[row.index-1]=e.target.value}}                               
                              >                   
                             {       
                                projects.map( (project) => 
                                (                        
                                  <MenuItem value={project.Project_ID}>{project.Project_Name}</MenuItem>
                                ))
                              }
                                </Select>
                              </FormControl>

                            :
                            <span>{row.project}</span>

                        }
                        </div>,
        center:true
      },
      {
         name:"TRAINING REQUIREMENT",
         width:"12%",
        selector: (row) => row.trainingRequirementType,
        sortable : true,
        cell: (row) => <div style={{width:"100%"}}><span>{row.trainingRequirementType}</span></div>,
        center:true
      },
      {
         name:"PROGRESS",
         cell: (row) => <Link to="/trainee_progress_manager" className="btn btn-success" state={{traineeId:row.traineeId, traineeEmail:row.traineeEmail,projectId:row.projectId,traineeType:row.trainingRequirementType}}>
			    	     Progress Dashboard
			    	    </Link>,
	      center:true
      },
      {
        name:"LEARNING PLAN",
        cell: (row) => <Link to="/trainee_learning_plan" className="btn btn-warning" state={{projectId:row.projectId, traineeEmail:row.traineeEmail, projectId:row.projectId,traineeType:row.trainingRequirementType, type:"Manager"}}>
                Learning Plan
               </Link>,
        center:true
      },
      {
         name:"FEEDBACK",
         cell: (row, index) =>  <button type="button" className="btn btn-primary" onClick={()=>{setPopupMessage(row.traineeFeedbacks); setPopup(true); }}>
                                  View Feedback
                                </button>,
	       center:true
      },
       {
         name:"KT PROCESS FEEDBACK",
         cell: (row) => <button type="button" className="btn btn-primary" style={{backgroundColor:"purple"}} onClick={()=>{setPopupMessage(row.traineeKtFeedback); setPopup(true);}}>
                            KT Process Feedback
                         </button>,
	       center:true

      }



    ]

    async function saveChanges()
    {
      if( (editTrainerToggle&&!editProjectToggle&&newTrainerIds.length==0) || (!editTrainerToggle&&editProjectToggle&&newProjectIds.length==0) || (editTrainerToggle&&editProjectToggle&&newTrainerIds.length==0&&newProjectIds.length==0) )
      {
        setPopup2(false)
        setInnerPopupMessage("No Changes Detected!")
        setInnerPopup(true)       
      }
      else
      {
        setPopup2(false)
        setLoadingPopup(true)
        var traineeIds=[]
        var trainerIds=[]
        for(var i=0;i<tableData.length;i++)
        {
          traineeIds.push(tableData[i].traineeId)
          trainerIds.push(tableData[i].trainerId)
        }
        const details = {
          newTrainerIds:newTrainerIds,
          newProjectIds:newProjectIds,
          traineeIds:traineeIds,
          trainerIds:trainerIds,
          managerEmail:managerEmail     
        }
        await axios.post("http://10.220.20.246:8000/ChangeTrainersOrProjects",details).then((res) =>
        {
          setLoadingPopup(false)
          setInnerPopupMessage(res.data.response)
          setInnerPopup(true)     
          setEditProjectToggle(false)
          setProjectSwitchChecked(false)
          setTrainerSwitchChecked(false)
          setEditTrainerToggle(false)
          fetch_details()

        })
       

      }


    }

    async function addTrainer()
    {
      //Extracting name from email (format : firstName.lastName@bsci.com)
      function nameExtractor(email)
      {
          const result = email.split(".");
          const firstName = result[0][0].toUpperCase() + result[0].slice(1).toLowerCase();
          const result2 = result[1].split("@");
          const lastName = result2[0][0].toUpperCase() + result2[0].slice(1).toLowerCase();
          var fullName = firstName+" "+lastName; 
          fullName=fullName.replace(/[0-9]+/g,"");
          return fullName;
      }

      if(newTrainerEmail==="")
      {
          setInnerPopupMessage("No Email Entered!");
          setInnerPopup(true);
          setAddTrainerPopup(false);
      }
      else if(!/^[a-zA-Z0-9]+\.[a-zA-Z0-9]+@bsci.com$/.test(newTrainerEmail))
      {
          setInnerPopupMessage("Invalid email format detected. Please enter a valid email address (Ex : firstname.lastname@bsci.com)")
          setInnerPopup(true);
          setAddTrainerPopup(false);
      }
      else
      {
         setAddTrainerPopup(false);
         setLoadingPopup(true)
          const newTrainerName=nameExtractor(newTrainerEmail);
          const details={
            userEmail:newTrainerEmail,
            userName:newTrainerName,
            userType:"Trainer"
          }
          await axios.post("http://10.220.20.246:8000/addUser",details).then(res=>
          {
            setLoadingPopup(false)
            setInnerPopupMessage(res.data.message);
            setInnerPopup(true);
            fetch_details();
          })
          
      }
    }


    return(
        <div className="bg">
        <Navbar type="Manager" setUserEmail={props.setUserEmail} setUserType={props.setUserType} dashboard_link="/manager_dashboard"/>
        <h1>MANAGER DASHBOARD</h1>
        <br/>
        <h3>Welcome {managerName}!</h3>
        <br/>
         <Grid container spacing={0} style={{width:"90%", height:"25%", margin:"auto", marginLeft:"5%",marginRight:"5%"}} >
              <Grid item sm={6} style={{paddingRight:"2%"}}>
                <div className="KT_progress" style={{'--ktwhitegradient':`${ktCompletionStyling.whitegradient}%`, '--ktgreengradient':`${ktCompletionStyling.greengradient}%`,'--kthoverbgsize':`${ktCompletionStyling.hoverbgsize}%`}} >
                  <h1 style={{paddingTop:"1.5%"}}><MenuBookIcon fontSize="inherit"/> KT TRAININGS COMPLETED</h1>
                  <h2 style={{paddingBottom:"1.5%"}}>{ktsCompleteCount+"/"+tableData.length}</h2>
                <Paper elevation={20}/>
                </div>
              </Grid>
              <Grid item sm={3} >                                                        
                <div style={{width:"100%", margin:"auto"}} className="userCount">
                  <h1 style={{paddingTop:"3%"}}><ChildCareIcon fontSize="inherit"/> TRAINEES</h1>
                  <h2 style={{paddingBottom:"3%"}}>{tableData.length}</h2>
                <Paper elevation={20}/>
                </div>
              </Grid>
              <Grid item sm={3}  style={{paddingLeft:"2%"}}>
                <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>Click here to add a trainer</span>}>                                                
                  <div style={{width:"100%", margin:"auto",cursor:"pointer"}} className="userCount" onClick={addNewTrainerClick}>
                    <h1 style={{paddingTop:"3%"}}><SchoolIcon fontSize="inherit"/> TRAINERS</h1>
                    <h2 style={{paddingBottom:"3%"}}>{trainerCount}</h2>
                  <Paper elevation={20}/>
                  </div>
                </Tooltip>
              </Grid>
        </Grid>
         {/* TABLE */}
         <div style={{width:"90%", margin:"auto", boxShadow: "0px 10px 10px rgba(0, 0, 0, 0.3)"}}>
           <DataTable
             columns={columns}
             data={filteredTableData}
             paginationRowsPerPageOptions={[5,10,15,25,50,100]}
             fixedHeader
             fixedHeaderScrollHeight="100vh"
             customStyles={customStyles}
             pagination
             subHeader
             subHeaderComponent =
             {
                <span style={{margin:"2%", width:"100%"}}>
                  <TextField style={{width:"25%"}} label="Search by trainer" variant="outlined" value={trainerSearch} onChange={(e)=>{if(e.target.value[e.target.value.length-1]!="\\"){setTrainerSearch(e.target.value)}}}/> &emsp;
                  <TextField style={{width:"25%"}} label="Search by trainee" variant="outlined" value={traineeSearch} onChange={(e)=>{if(e.target.value[e.target.value.length-1]!="\\"){setTraineeSearch(e.target.value)}}}/> &emsp;
                  <FormControl component="fieldset" variant="standard">
                    <FormGroup>
                      <span>
                      <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>You can add a trainer if not found in the list</span>}>                          
                        <span><FormControlLabel control={<Switch disabled={!tableData.length} checked={trainerSwitchChecked} onChange={()=>{setTrainerSwitchChecked(!trainerSwitchChecked); setEditTrainerToggle(!editTrainerToggle)}}/>} style={{color:"black"}} label="Change Trainer" /> &emsp; </span>
                      </Tooltip>
                      {/*<Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>You can add a project if not found in the list</span>}>*/}
                        <span><FormControlLabel control={<Switch disabled={!tableData.length} checked={projectSwitchChecked} onChange={()=>{setProjectSwitchChecked(!projectSwitchChecked); setEditProjectToggle(!editProjectToggle)}}/>} style={{color:"black"}} label="Change Project" /> &emsp; </span>
                      {/*</Tooltip>*/}
                      </span>
                    </FormGroup>
                  </FormControl>
                  <button className="btn btn-success btn-lg" disabled={!editProjectToggle && !editTrainerToggle} onClick={()=>{setPopup2(true);setPopupMessage2("Are you sure you want to save these changes ?");}}>Save Changes</button>                                              
                </span>
             }
             subHeaderAlign="left"
           />
         </div>
         <br/>


	    <Popup open={popup} closeOnDocumentClick onClose={()=>{setPopup(false);}}>        
              <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setPopup(false)}}/>
              <center >
              <br/>
              <h3>FEEDBACK</h3>
              <br/>
               <p style={{color:"#003C71", fontSize:"130%", maxHeight:window.innerHeight*0.4, overflow:"auto", textAlign:"left", paddingLeft:"5%",paddingRight:"5%"}}>
              {
                popupMessage.length==0
                ?

                <center>No Feedback</center>

                :


                popupMessage.map((el,index) =>
                (

                   <div>
                        {el}
                        <br/>
                   </div>
                ))
              }
              </p>
              <br/>
              <br/>
            </center>
         </Popup>

         

         <Popup open={addTrainerPopup} closeOnDocumentClick onClose={()=>{setAddTrainerPopup(false);}}>
            <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setAddTrainerPopup(false);}}/>
            <center >
              <br/>
              <h2>ADD TRAINER</h2>
              <br/>
              <h3>Enter Official BSCI Email Address</h3>
              <input type="text" onChange={(e)=>(setNewTrainerEmail(e.target.value))} style={{width:"80%", padding:"2%"}} value={newTrainerEmail} placeholder={"Enter Trainer Email..."}/>
              <br/>
              <br/>
              <div className='btn btn-info btn-lg' onClick={addTrainer}>Add</div> &emsp;
              <div className='btn btn-info btn-lg' onClick={()=>{setAddTrainerPopup(false);}}>Cancel</div>
              <br/>         
              <br/>
            </center>
         </Popup>

         <Popup open={innerPopup} closeOnDocumentClick onClose={()=>{setInnerPopup(false)}}>
          <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setInnerPopup(false);}}/>
            <center >
              <br/>
              <center><p style={{color:"#003C71", fontSize:"130%", maxHeight:window.innerHeight*0.4, overflow:"auto"}}>{innerPopupMessage}</p></center>
              <br/>
              <div className='btn btn-info btn-lg' onClick={()=>{setInnerPopup(false)}}>Ok</div>
              <br/>
              <br/>
            </center>
         </Popup>

         <Popup open={popup2} closeOnDocumentClick onClose={()=>{setPopup2(false)}}>
         <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setPopup2(false);}}/>
            <center>
              <br/>
              <p style={{color:"#003C71", fontSize:"130%", margin:"3%"}}><center>{popupMessage2}</center></p>
              <br/>
              {           
              <center>
              <div className='btn btn-info btn-lg' onClick={saveChanges}>Yes</div> &emsp;
              <div className='btn btn-info btn-lg' onClick={()=>{setPopup2(false)}}>No</div>
              </center>
              }
              <br/>
              <br/>
            </center>
         </Popup>


         
          {/* LOADING SCREEN */}
          <Popup open={loadingPopup} hideBackdrop closeOnDocumentClick={false} onClose={()=>{setLoadingPopup(false)}}>
               <center>
                 <p style={{color:"#003C71", fontSize:"130%", margin:"3%"}}><center>PLEASE WAIT...</center></p>
                 <br/>
                 <CircularProgress/>
                 <br/>
                 <br/>
               </center>
          </Popup> 

        <br/>
        <br/>
        </div>
    )
}

export default Manager_Dashboard