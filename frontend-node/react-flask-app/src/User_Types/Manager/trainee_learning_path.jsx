import React,{useState,useEffect} from 'react'
import Navbar from '../../Base/navbar.jsx'
import './trainee_learning_path.css'
import axios from 'axios'
import Popup from 'reactjs-popup'
import Switch from '@mui/material/Switch';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import dayjs from 'dayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import TextField from '@mui/material/TextField';
import {useLocation} from 'react-router-dom'
import SwipeableViews from 'react-swipeable-views';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import DataTable from 'react-data-table-component'
import moment from 'moment'
import Tooltip from '@mui/material/Tooltip';
import Zoom from '@mui/material/Zoom';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import Badge from '@mui/material/Badge';
import { CircularProgress } from '@mui/material';
import CancelIcon from '@mui/icons-material/Cancel';

function Trainee_learning_path(props)
{
    //To get the trainee ID from the previous page
    const location = useLocation();
    const traineeEmail= location.state.traineeEmail;
    const traineeId= location.state.traineeId;
    const traineeType= location.state.traineeType;
    const projectId= location.state.projectId;

    //const [trainee,setTrainee] = useState({});
    const [traineeName,setTraineeName] = useState("")

    //Loading Screen
    const [loadingPopup,setLoadingPopup] = useState(false)

    //For tables
    const [btSearch,setBtSearch] = useState("");
    const [btTrainings,setBtTrainings] = useState([]);
    const [filteredBtTrainings,setFilteredBtTrainings] =useState([]);
    const [selectedBtTrainings,setSelectedBtTrainings] =useState([]);
    const [pstTrainings,setPstTrainings] = useState([]);
    const [pstSearch,setPstSearch] = useState("");
    const [filteredPstTrainings,setFilteredPstTrainings] =useState([]);
    const [selectedPstTrainings,setSelectedPstTrainings] =useState([]);
    const [rtSearch,setRtSearch] = useState("");
    const [rtTrainings,setRtTrainings] = useState([]);
    const [filteredRtTrainings,setFilteredRtTrainings] =useState([]);
    const [selectedRtTrainings,setSelectedRtTrainings] =useState([]);


    //Referencing the tables for de-selection of rows
    const [pstKey,setPstKey] = useState(0)
    const [btKey,setBtKey] = useState(0)
    const [rtKey,setRtKey] = useState(0)
    

    //Toggle for viewing basic trainings, project details or re-assigned trainings
    const [view,setView]= useState(0);

    //row height for re-assigned trainings table needs to be higher than default to accommodate the row height increase when due date is to be edited
    const customStyles =
    {
        rows:
        {
          style: { minHeight: "10vh", /* override the row height*/ }
        }
    }

    //to display total trainings selected
    const [count,setCount] = useState(0)

    //automatic height adjustment should happen only once all data is loaded so it is false by default.
    const [animateHeightToggle,setAnimateHeightToggle] = useState(false)

    //Toggles for switches
    const [switchChecked,setSwitchChecked] = useState(false)
    const [btSwitchChecked,setBtSwitchChecked] = useState(false)
    const [pstSwitchChecked,setPstSwitchChecked] = useState(false)

    //For Popup
    const [popup,setPopup] =  useState(false)
    const [popupMessage,setPopupMessage] = useState("")
    //const [reAssignmentCompleteFlag,setReAssignmentCompleteFlag] = useState(false)
    //const [trainingsSelectedCount,setTrainingsSelectedCount] = useState(0)
    const [saveChangesFlag,setSaveChangesFlag] = useState(false)

    //For Popup 2 (Deletion of Re-Assigned Trainings)
    const [popup2,setPopup2] =  useState(false)
    const [popupMessage2,setPopupMessage2] = useState("")
    const [deletionFlag,setDeletionFlag] = useState(false)

    //Due Date
    //const [commonDueDate,setCommonDueDate] = useState("")
    const [editAssignedDueDateToggle,setEditAssignedDueDateToggle] = useState(false);
    const [editBtAssignedDueDateToggle,setEditBtAssignedDueDateToggle] = useState(false);
    const [editPstAssignedDueDateToggle,setEditPstAssignedDueDateToggle] = useState(false);

    var maxExpandedTrainingsDueDate=null;

    //Basic Training details
    const [rowId,setRowId] = useState([]);
    const [moduleNumber,setModuleNumber] = useState([]);
    const [moduleDueDates,setModuleDueDates] = useState([]);
    const [moduleCompletionDates,setModuleCompletionDates] = useState([]);
    const [moduleName,setModuleName] = useState([]);
    const [modulePath,setModulePath] = useState([]);
    const [subModuleNumber,setSubModuleNumber] = useState([]);
    const [subModuleDueDates,setSubModuleDueDates] = useState([]);
    const [subModuleCompletionDates,setSubModuleCompletionDates] = useState([]);
    const [subModuleName,setSubModuleName] = useState([]);
    const [subModulePath,setSubModulePath] = useState([]);
    const [expandedTrainings,setExpandedTrainings] = useState([]);

    //Project details
    const [projectData,setProjectData] = useState({});
    const [projectRowId,setProjectRowId] = useState([]);
    const [projectModuleNumber,setProjectModuleNumber] = useState([]);
    const [projectModuleDueDates,setProjectModuleDueDates] = useState([]);
    const [projectModuleCompletionDates,setProjectModuleCompletionDates] = useState([]);
    const [projectModuleName,setProjectModuleName] = useState([]);
    const [projectModulePath,setProjectModulePath] = useState([]);
    const [projectSubModuleNumber,setProjectSubModuleNumber] = useState([]);
    const [projectSubModuleDueDates,setProjectSubModuleDueDates] = useState([]);
    const [projectSubModuleCompletionDates,setProjectSubModuleCompletionDates] = useState([]);
    const [projectSubModuleName,setProjectSubModuleName] = useState([]);
    const [projectSubModulePath,setProjectSubModulePath] = useState([]);
    const [projectExpandedTrainings,setProjectExpandedTrainings] = useState([]);

    //Re-Assigned Trainings
    const [rtRowId,setRtRowId] = useState([]);
    const [rtType,setRtType] = useState([]);
    const [rtModuleNumber,setRtModuleNumber] = useState([]);
    const [rtModuleName,setRtModuleName] = useState([]);
    const [rtModulePath,setRtModulePath] = useState([]);
    const [rtSubModuleNumber,setRtSubModuleNumber] = useState([]);
    const [rtSubModuleName,setRtSubModuleName] = useState([]);
    const [rtSubModulePath,setRtSubModulePath] = useState([]);
    const [rtDueDates,setRtDueDates] = useState([]);
    const [rtCompletionDates,setRtCompletionDates] = useState([]);


    //Searching
    useEffect(() =>
    {
        const result = btTrainings.filter( (task) =>
        {
            return task.trainingName.toLowerCase().match(btSearch.toLowerCase());
        })
        setFilteredBtTrainings(result);

    },[btSearch]);
    useEffect(() =>
    {
        const result = pstTrainings.filter( (task) =>
        {
            return task.trainingName.toLowerCase().match(pstSearch.toLowerCase());
        })
        setFilteredPstTrainings(result);

    },[pstSearch]);
    useEffect(() =>
    {
        const result = rtTrainings.filter( (task) =>
        {
            return task.trainingName.toLowerCase().match(rtSearch.toLowerCase());
        })
        setFilteredRtTrainings(result);

    },[rtSearch]);



    //Loading all necessary details
    const fetch_details = async () =>
    {
        setAnimateHeightToggle(false)
        const details = {
            userEmail : traineeEmail,
            userType : "Trainee",
            userId : traineeId,
            traineeType : traineeType,
            projectId : projectId
        }
        const Params = new URLSearchParams(details);
        const URLWithParams = `http://10.220.20.246:8000/fetchUserName?${Params}`;
        await axios.get(URLWithParams).then( res=> {setTraineeName(res.data.userName)})

        const traineeDetails = {
            email : traineeEmail,
            traineeType : traineeType,
            projectId : projectId
        }
        const basicparams = new URLSearchParams(traineeDetails);
        const URLWithBasicParams = `http://10.220.20.246:8000/fetchTraineeCommonTrainingProjectId?${basicparams}`;
        const basictrainingprojectid = await axios.get(URLWithBasicParams).then( res => res.data.commonprojectid)
//         const traineetype = await axios.get(URLWithBasicParams).then( res => res.data.traineetype)

        const basicTrainingsDetails = {
            trainee_email : traineeEmail,
            project_id : basictrainingprojectid,
            traineeType: traineeType,
            projectspecificprojectId : projectId
        }
        const projectTrainingsDetails = {
            trainee_email : traineeEmail,
            project_id : projectId,
            traineeType: traineeType,
        }
        const reassignedTrainingsDetails = {
            trainee_email : traineeEmail,
            project_id : projectId,
            traineeType: traineeType,
        }
        const basicTrainingsParams = new URLSearchParams(basicTrainingsDetails);
        const basicTrainingsURLWithParams = `http://10.220.20.246:8000/fetchProjectDetails?${basicTrainingsParams}`;
        const ProjectParams = new URLSearchParams(projectTrainingsDetails);
        const ProjectURLWithParams = `http://10.220.20.246:8000/fetchProjectDetails?${ProjectParams}`;
        const RtParams = new URLSearchParams(reassignedTrainingsDetails);
        const RtURLWithParams = `http://10.220.20.246:8000/fetchReAssignedTrainings?${RtParams}`;

        /* BASIC TRAININGS */
        await axios.get(basicTrainingsURLWithParams).then( res =>
        {
            const mapping =  res.data.traineeStatus;
            const modules = res.data.modules;
            const subModules=res.data.subModules;
            const length = res.data.traineeStatus.length;
            var btDetails= [];
            var expandedTrainingsDetails =  [];
            

            const ReAssignedTrainings = res.data.reAssignedTrainings;
            var ReAssignedTrainingIds = []
            for (var i=0;i<ReAssignedTrainings.length;i++)
            {
                ReAssignedTrainingIds.push(ReAssignedTrainings[i].TMSM_Mapping_ID)
            }
            for(var i=0;i<length;i++)
            {
                
                for(var j=0;j<modules.length;j++)
                {
                    if(mapping[i].Module_ID==modules[j].Module_ID)
                    {
                         rowId[i]=mapping[i].TMSM_Mapping_ID;
                         moduleDueDates[i]=mapping[i].Due_Date;
//                          if(traineetype=="All Modules")
//                          {moduleDueDates[i]=mapping[i].Due_Date}
//                          else
//                          {moduleDueDates[i]=modules[j].Mandatory==null?"N/A":mapping[i].Due_Date;}
                         moduleCompletionDates[i]=mapping[i].Completion_Date;
                         moduleNumber[i]=modules[j].Module_Number;
                         moduleName[i]=modules[j].Module_Name;
                         modulePath[i]=modules[j].Module_Path;
                    }
                }
                for(var j=0;j<subModules.length;j++)
                {
                    if(mapping[i].Sub_Module_ID==subModules[j].Sub_Module_ID)
                    {
                         subModuleNumber[i]=subModules[j].Sub_Module_Number;                       
                         subModuleName[i]=subModules[j].Sub_Module_Name;
                         subModulePath[i]=subModules[j].Sub_Module_Path;
                         subModuleDueDates[i]=mapping[i].Due_Date;
//                          if(traineetype=="All Modules")
//                          {subModuleDueDates[i]=mapping[i].Due_Date}
//                          else
//                          {subModuleDueDates[i]=subModules[j].Mandatory==null?"N/A":mapping[i].Due_Date;}
                         subModuleCompletionDates[i]=mapping[i].Completion_Date;
                    }
                    else if(subModuleNumber[i])
                    {
                        continue;
                    }
                    else
                    {
                         subModuleNumber[i]=null;
                         subModuleName[i]=null;
                         subModulePath[i]=null;
                         subModuleDueDates[i]=null;
                         subModuleCompletionDates[i]=null;
                    }
                }
            }

            //Sorting everything according to module number :
            // Create an array of objects with values from all arrays
            const combinedArray = moduleNumber.map((value, index) => ({
              value: value,
              rowIdValue: rowId[index],
              moduleDueDatesValue:moduleDueDates[index],
              moduleCompletionDatesValue:moduleCompletionDates[index],
              moduleNameValue: moduleName[index],
              modulePathValue: modulePath[index],
              subModuleNumberValue: subModuleNumber[index],
              subModuleNameValue: subModuleName[index],
              subModulePathValue: subModulePath[index],
              subModuleDueDatesValue:subModuleDueDates[index],
              subModuleCompletionDatesValue:subModuleCompletionDates[index]
            }));
            // Sort the array of objects based on the 'value' i.e. module number property
            combinedArray.sort((a, b) => a.value - b.value);
            // Extract the sorted values back to each array
            combinedArray.forEach((item, index) => {
              rowId[index]=item.rowIdValue;
              moduleDueDates[index]=item.moduleDueDatesValue
              moduleCompletionDates[index]=item.moduleCompletionDatesValue
              moduleNumber[index] = item.value;
              moduleName[index] = item.moduleNameValue;
              modulePath[index] = item.modulePathValue;
              subModuleNumber[index] = item.subModuleNumberValue;
              subModuleName[index] = item.subModuleNameValue;
              subModulePath[index] = item.subModulePathValue;
              subModuleDueDates[index] = item.subModuleDueDatesValue;
              subModuleCompletionDates[index] = item.subModuleCompletionDatesValue;
            });
            
            for(var i=0;i<length;i++)
            {

                if(btDetails.filter((training)=>(training.trainingModule==moduleNumber[i])).length==0)
                {
                    var training = {index:i,trainingModule:moduleNumber[i],trainingName:moduleName[i],trainingId:rowId[i],selectionDisabled:(subModuleNumber[i]||ReAssignedTrainingIds.includes(rowId[i]))?true:false,expansionDisabled:subModuleNumber[i]?false:true,type:"BT",expanded:true,trainingDueDate:moduleDueDates[i],trainingCompletionDate:moduleCompletionDates[i],trainingDueDateNew:null,editDueDateDisabled:subModuleNumber[i]?true:false}
                    btDetails.push(training);
                }

                if(subModuleNumber[i])
                {
                    var expandedTraining = {index:i,trainingModuleNumber:moduleNumber[i],trainingModule:subModuleNumber[i],trainingName:subModuleName[i],trainingId:rowId[i],selectionDisabled:ReAssignedTrainingIds.includes(rowId[i])?true:false,selected:false,type:"BT",trainingDueDate:projectModuleDueDates[i]=="Not Required"?maxExpandedTrainingsDueDate:projectModuleDueDates[i],trainingCompletionDate:subModuleCompletionDates[i],trainingDueDateNew:null}
                }
                else
                {
                    var expandedTraining = null
                }
                expandedTrainingsDetails.push(expandedTraining);

            }
            setExpandedTrainings(expandedTrainingsDetails.filter((training)=>(training!=null)));
            setBtTrainings(btDetails);
            setFilteredBtTrainings(btDetails);

        })

        /* PROJECT SPECIFIC TRAININGS */
        await axios.get(ProjectURLWithParams).then( res =>
        {
            setProjectData(res.data.project[0]);
            const mapping =  res.data.traineeStatus;
            const modules = res.data.modules;
            const subModules=res.data.subModules;
            const length = res.data.traineeStatus.length;
            const ReAssignedTrainings = res.data.reAssignedTrainings;

            var expandedTrainingsDetails =  [];
            var pstDetails= [];
            var ReAssignedTrainingIds = []
            for (var i=0;i<ReAssignedTrainings.length;i++)
            {
                ReAssignedTrainingIds.push(ReAssignedTrainings[i].TMSM_Mapping_ID)
            }
            for(var i=0;i<length;i++)
            {
          
                for(var j=0;j<modules.length;j++)
                {               
                    if(mapping[i].Module_ID==modules[j].Module_ID)
                    {                    
                         projectRowId[i]=mapping[i].TMSM_Mapping_ID;
                         projectModuleDueDates[i]=mapping[i].Due_Date;
//                          if(traineetype=="All Modules")
//                          {projectModuleDueDates[i]=mapping[i].Due_Date}
//                          else
//                          {projectModuleDueDates[i]=modules[j].Mandatory==null?"N/A":mapping[i].Due_Date;}
                         projectModuleCompletionDates[i]=mapping[i].Completion_Date;
                         projectModuleNumber[i]=modules[j].Module_Number;
                         projectModuleName[i]=modules[j].Module_Name;
                         projectModulePath[i]=modules[j].Module_Path;                        
                    }
                }
                for(var j=0;j<subModules.length;j++)
                {
                    if(mapping[i].Sub_Module_ID==subModules[j].Sub_Module_ID)
                    {

                         projectSubModuleNumber[i]=subModules[j].Sub_Module_Number;
                         projectSubModuleName[i]=subModules[j].Sub_Module_Name;
                         projectSubModulePath[i]=subModules[j].Sub_Module_Path;
                         projectSubModuleDueDates[i]=mapping[i].Due_Date;
//                          if(traineetype=="All Modules")
//                          {projectSubModuleDueDates[i]=mapping[i].Due_Date}
//                          else
//                          {projectSubModuleDueDates[i]=subModules[j].Mandatory==null?"N/A":mapping[i].Due_Date;}
                         projectSubModuleCompletionDates[i]=mapping[i].Completion_Date;                       
                         
                    }
                    else if(projectSubModuleNumber[i])
                    {
                        continue;
                    }
                    else
                    {
                         projectSubModuleNumber[i]=null;
                         projectSubModuleName[i]=null;
                         projectSubModulePath[i]=null;
                         projectSubModuleDueDates[i]=null;
                         projectSubModuleCompletionDates[i]=null;
                    }
                }
            }

            //Sorting everything according to module number :
            // Create an array of objects with values from all arrays
            const combinedArray = projectModuleNumber.map((value, index) => ({
              value: value,
              projectRowIdValue: projectRowId[index],
              projectModuleNameValue: projectModuleName[index],
              projectModuleDueDatesValue:projectModuleDueDates[index],
              projectModuleCompletionDatesValue:projectModuleCompletionDates[index],
              projectModulePathValue: projectModulePath[index],
              projectSubModuleNumberValue: projectSubModuleNumber[index],
              projectSubModuleNameValue: projectSubModuleName[index],
              projectSubModulePathValue: projectSubModulePath[index],
              projectSubModuleDueDatesValue:projectSubModuleDueDates[index],
              projectSubModuleCompletionDatesValue:projectSubModuleCompletionDates[index]
            }));
            // Sort the array of objects based on the 'value' i.e. module number property
            combinedArray.sort((a, b) => a.value - b.value);
            // Extract the sorted values back to each array
            combinedArray.forEach((item, index) => {
              projectRowId[index]=item.projectRowIdValue;
              projectModuleNumber[index] = item.value;
              projectModuleDueDates[index]=item.projectModuleDueDatesValue
              projectModuleCompletionDates[index]=item.projectModuleCompletionDatesValue
              projectModuleName[index] = item.projectModuleNameValue;
              projectModulePath[index] = item.projectModulePathValue;
              projectSubModuleNumber[index] = item.projectSubModuleNumberValue;
              projectSubModuleName[index] = item.projectSubModuleNameValue;
              projectSubModulePath[index] = item.projectSubModulePathValue;
              projectSubModuleDueDates[index] = item.projectSubModuleDueDatesValue;
              projectSubModuleCompletionDates[index] = item.projectSubModuleCompletionDatesValue;
            });
           

            for(var i=0;i<length;i++)
            {

                if(projectSubModuleNumber[i])
                {
                    var expandedTraining = {index:i,trainingModuleNumber:projectModuleNumber[i],trainingModule:projectSubModuleNumber[i],trainingName:projectSubModuleName[i],trainingId:projectRowId[i],selectionDisabled:ReAssignedTrainingIds.includes(projectRowId[i])?true:false,selected:false,type:"PST",trainingDueDate:projectSubModuleDueDates[i],trainingCompletionDate:projectSubModuleCompletionDates[i],trainingDueDateNew:null}
                }
                else
                {
                    var expandedTraining = null
                }
                expandedTrainingsDetails.push(expandedTraining);

            }
          
            
            for(var i=0;i<length;i++)
            {
                const pExpandedTrainings = expandedTrainingsDetails.filter((training)=>(training!=null));
                for(var k=0;k<pExpandedTrainings.length;k++)
                    {
                        if(pExpandedTrainings[k].trainingDueDate!="Not Required")
                        {
                            maxExpandedTrainingsDueDate=pExpandedTrainings[k].trainingDueDate;
                            break
                        }
                    }
                var maxExpandedTrainingsCompletionDate=null
                var completionflag=1;
                if(pstDetails.filter((training)=>(training.trainingModule==projectModuleNumber[i])).length==0)
                {
                    for(var j=0;j<pExpandedTrainings.length;j++)
                    {
                        if(pExpandedTrainings[j].trainingModuleNumber==projectModuleNumber[i])
                        {
                            if(pExpandedTrainings[j].trainingDueDate!="Not Required")
                            {
                                if(pExpandedTrainings[j].trainingDueDate>maxExpandedTrainingsDueDate)
                                {
                                    maxExpandedTrainingsDueDate=pExpandedTrainings[j].trainingDueDate;
                                    maxExpandedTrainingsCompletionDate=pExpandedTrainings[j].trainingCompletionDate;
                                }
                                if(pExpandedTrainings[j].trainingCompletionDate==null)
                                {
                                    completionflag=0;
                                }
                            }
                        }
                    }
                    if(completionflag==0)
                    {
                        maxExpandedTrainingsCompletionDate=null
                    }
                    var training = {index:i,trainingModule:projectModuleNumber[i],trainingName:projectModuleName[i],trainingId:projectRowId[i],selectionDisabled:(projectSubModuleNumber[i]||ReAssignedTrainingIds.includes(projectRowId[i]))?true:false,expansionDisabled:projectSubModuleNumber[i]?false:true,type:"PST",expanded:true,trainingDueDate:projectSubModuleNumber[i]?projectModuleDueDates[i]=="Not Required"?"Not Required":maxExpandedTrainingsDueDate:projectModuleDueDates[i],trainingCompletionDate:projectSubModuleNumber[i]?maxExpandedTrainingsCompletionDate:projectModuleCompletionDates[i],trainingDueDateNew:null,editDueDateDisabled:projectSubModuleNumber[i]?true:false}
                    pstDetails.push(training);
                    maxExpandedTrainingsDueDate=null;
                }
            }
            setProjectExpandedTrainings(expandedTrainingsDetails.filter((training)=>(training!=null)));
            setPstTrainings(pstDetails);
            setFilteredPstTrainings(pstDetails);
        })

        /* RE-ASSIGNED TRAININGS */
        await axios.get(RtURLWithParams).then( res =>
        {
            const mapping = res.data.traineeStatus
            const reAssignedTrainings =  res.data.reAssignedTrainings;
            const types =  res.data.types
            const modules = res.data.modules;
            const subModules= res.data.subModules;
            const length = res.data.reAssignedTrainings.length;
            var rtDetails= [];
            for(var i=0;i<length;i++)
            {
                rtRowId[i]=reAssignedTrainings[i].RT_ID;
                rtType[i]=types[i];
                rtDueDates[i]=reAssignedTrainings[i].Due_Date;
                rtCompletionDates[i]=reAssignedTrainings[i].Completion_Date;

                for(var j=0;j<modules.length;j++)
                {
                    if(mapping[i].Module_ID==modules[j].Module_ID)
                    {
                         rtModuleNumber[i]=modules[j].Module_Number;
                         rtModuleName[i]=modules[j].Module_Name;
                         rtModulePath[i]=modules[j].Module_Path;
                    }
                }

                for(var j=0;j<subModules.length;j++)
                {
                    if(mapping[i].Sub_Module_ID==subModules[j].Sub_Module_ID)
                    {
                         rtSubModuleNumber[i]=subModules[j].Sub_Module_Number;
                         rtSubModuleName[i]=subModules[j].Sub_Module_Name;
                         rtSubModulePath[i]=subModules[j].Sub_Module_Path;
                    }
                    else if(rtSubModuleNumber[i])
                    {
                        continue;
                    }
                    else
                    {
                         rtSubModuleNumber[i]=null;
                         rtSubModuleName[i]=null;
                         rtSubModulePath[i]=null;
                    }
                }
            }

            for(var i=0;i<length;i++)
            {
                var training = {index:i,trainingType:rtType[i],trainingModule:rtSubModuleNumber[i]?rtModuleNumber[i]+" ("+rtSubModuleNumber[i]+")":rtModuleNumber[i],trainingSubModule:rtSubModuleNumber[i],trainingName:rtSubModuleNumber[i]?rtModuleName[i]+" - "+rtSubModuleName[i]:rtModuleName[i],trainingId:rtRowId[i],trainingDueDate:rtDueDates[i],trainingDueDateNew:null,trainingCompletionDate:rtCompletionDates[i],type:"RT"}
                rtDetails.push(training);
            }
            setRtTrainings(rtDetails);
            setFilteredRtTrainings(rtDetails);

        })
        setAnimateHeightToggle(true)
    }

    useEffect(() =>
    {
        fetch_details()
    },[])

     useEffect(() =>
    {
        setCount(selectedBtTrainings.length+selectedPstTrainings.length+projectExpandedTrainings.filter((training)=>(training.selected==true)).length+expandedTrainings.filter((training)=>(training.selected==true)).length)
    },[selectedBtTrainings.length,selectedPstTrainings.length])



    function badgeColorDeterminer(subModuleTrainings,moduleNum)
    {
        subModuleTrainings=subModuleTrainings.filter((training)=>(training.trainingCompletionDate==null&&training.trainingModuleNumber==moduleNum))
        var overdueFlag=0;
        for (var i=0;i<subModuleTrainings.length;i++)
        {        
            if(subModuleTrainings[i].trainingDueDate<moment().format("YYYY-MM-DD"))
            {
                overdueFlag=1;
                
            }       
        } 
        if(overdueFlag==1)
        {
            return "error"
        }
        else
        {
            return "success"
        }

    }

    function overdueCountDeterminer(subModuleTrainings,moduleNum)
    {

        var overdueCount=0
        subModuleTrainings=subModuleTrainings.filter((training)=>(training.trainingCompletionDate==null&&training.trainingModuleNumber==moduleNum))
        for (var i=0;i<subModuleTrainings.length;i++)
        {
            if(subModuleTrainings[i].trainingDueDate<moment().format("YYYY-MM-DD"))
            {
                overdueCount+=1;          
            }            
        } 
        return overdueCount;
       
    }


    //TABLE HEADERS (Basic Trainings)
    var btColumns =[
      {
        name:"MODULE",
        width:"15%",
        selector: (row) => row.trainingModule,
        sortable : true
      },
      {
        name:"CURRICULUM",
        selector: (row) => row.trainingName,
        sortable : true,
        cell : (row) => <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>{badgeColorDeterminer(expandedTrainings,row.trainingModule)=="error"?overdueCountDeterminer(expandedTrainings,row.trainingModule)+" training(s) missed":expandedTrainings.filter((training)=>(training.trainingCompletionDate==null&&training.trainingModuleNumber==row.trainingModule)).length+" training(s) pending"}</span>}>
                          <Badge color={badgeColorDeterminer(expandedTrainings,row.trainingModule)} badgeContent={badgeColorDeterminer(expandedTrainings,row.trainingModule)=="error"?overdueCountDeterminer(expandedTrainings,row.trainingModule):expandedTrainings.filter((training)=>(training.trainingCompletionDate==null&&training.trainingModuleNumber==row.trainingModule)).length}>
                            {row.trainingName}&emsp;
                          </Badge>
                        </Tooltip>
      },
      {
        name:"DUE DATE",
        selector: (row) => row.trainingDueDate,
        cell: (row) => <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>Format : DD-MM-YYYY</span>}>
                            {
                                editBtAssignedDueDateToggle && !row.editDueDateDisabled && !row.trainingCompletionDate
                                ?
                                <LocalizationProvider dateAdapter={AdapterDayjs} >
                                <DemoContainer components={['DatePicker']} >
                                    <DatePicker format="DD-MM-YYYY" disablePast label="Due Date" value={row.trainingDueDateNew?dayjs(moment(row.trainingDueDateNew,"YYYY-MM-DD").format("MM-DD-YYYY")):null}
                                    onChange={(e)=>
                                    {
                                      if(String(e)!="Invalid Date")
                                      {
                                          const new_date = new Date(e)
                                          row.trainingDueDateNew=moment(new_date).format("YYYY-MM-DD")
                                      }
                                      else
                                      {
                                          row.trainingDueDateNew=null
                                      }                                   
                                    }}/>
                                </DemoContainer>
                                </LocalizationProvider>
                                :
                                <span>{row.trainingDueDate=="Not Required"?"Not Required":moment(row.trainingDueDate,"YYYY-MM-DD").format("DD-MM-YYYY")}</span>
                                           
                            }
                        </Tooltip>,
        sortable : true
      },
      {
        name:"COMPLETION DATE",
        selector: (row) => row.trainingCompletionDate,
        cell: (row) => <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>Format : DD-MM-YYYY</span>}>
                        <span>{row.trainingDueDate=="Not Required"?"Not Required":row.trainingCompletionDate==null?row.trainingCompletionDate:moment(row.trainingCompletionDate,"YYYY-MM-DD").format("DD-MM-YYYY")}</span>
                       </Tooltip>,
        sortable : true
      }
    ]

    //TABLE HEADERS (Project Specific Trainings)
    var pstColumns =[
        {
          name:"MODULE",
          width:"15%",
          selector: (row) => row.trainingModule,
          sortable : true
        },
        {
          name:"CURRICULUM",
          selector: (row) => row.trainingName,
          sortable : true,
          cell : (row) => <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>{badgeColorDeterminer(projectExpandedTrainings,row.trainingModule)=="error"?overdueCountDeterminer(projectExpandedTrainings,row.trainingModule)+" training(s) missed":projectExpandedTrainings.filter((training)=>(training.trainingCompletionDate==null&&training.trainingModuleNumber==row.trainingModule)).length+" training(s) pending"}</span>}>
                            <Badge color={badgeColorDeterminer(projectExpandedTrainings,row.trainingModule)} badgeContent={badgeColorDeterminer(projectExpandedTrainings,row.trainingModule)=="error"?overdueCountDeterminer(projectExpandedTrainings,row.trainingModule):projectExpandedTrainings.filter((training)=>(training.trainingCompletionDate==null&&training.trainingDueDate!="Not Required"&&training.trainingModuleNumber==row.trainingModule)).length}>
                              {row.trainingName}&emsp;
                            </Badge>
                          </Tooltip>
        },
        {
          name:"DUE DATE",
          selector: (row) => row.trainingDueDate,
          cell: (row) => <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>Format : DD-MM-YYYY</span>}>
                              {
                                  editPstAssignedDueDateToggle && !row.editDueDateDisabled && !row.trainingCompletionDate
                                  ?
                                  <LocalizationProvider dateAdapter={AdapterDayjs} >
                                  <DemoContainer components={['DatePicker']} >
                                      <DatePicker format="DD-MM-YYYY" disablePast label="Due Date" value={row.trainingDueDateNew?dayjs(moment(row.trainingDueDateNew,"YYYY-MM-DD").format("MM-DD-YYYY")):null}
                                      onChange={(e)=>
                                      {
                                        if(String(e)!="Invalid Date")
                                        {
                                            const new_date = new Date(e)
                                            row.trainingDueDateNew=moment(new_date).format("YYYY-MM-DD")
                                           
                                        }
                                        else
                                        {
                                            row.trainingDueDateNew=null
                                        }                                   
                                      }}/>
                                  </DemoContainer>
                                  </LocalizationProvider>
                                  :
                                  <span>{row.trainingDueDate=="Not Required"?"Not Required":moment(row.trainingDueDate,"YYYY-MM-DD").format("DD-MM-YYYY")}</span>

                              }
                          </Tooltip>,
          sortable : true
        },
        {
          name:"COMPLETION DATE",
          selector: (row) => row.trainingCompletionDate,
          cell: (row) => <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>Format : DD-MM-YYYY</span>}>
                          <span>{row.trainingDueDate=="Not Required"?"Not Required":row.trainingCompletionDate==null?row.trainingCompletionDate:moment(row.trainingCompletionDate,"YYYY-MM-DD").format("DD-MM-YYYY")}</span>
                         </Tooltip>,
         sortable : true
                        
        }
      ]

    //TABLE HEADERS (Re-Assigned Trainings)
    var rtColumns =[
      {
        name:"PROJECT",
        width:"12%",
        selector: (row) => row.trainingType,
        sortable : true
      },
      {
        name:"MODULE",
        width:"10%",
        selector: (row) => row.trainingModule,
        sortable : true
      },
      {
        name:"CURRICULUM",
        selector: (row) => row.trainingName,
        sortable : true
      },
      {
        name:"DUE DATE",
        width:"25%",
        selector: (row) =>row.trainingDueDate,
        cell: (row) => <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>Format : DD-MM-YYYY</span>}>
                            {
                                editAssignedDueDateToggle
                                ?
                                <LocalizationProvider dateAdapter={AdapterDayjs} >
                                <DemoContainer components={['DatePicker']} >
                                  <DatePicker format="DD-MM-YYYY" disablePast label="Due Date" value={row.trainingDueDateNew?dayjs(moment(row.trainingDueDateNew,"YYYY-MM-DD").format("MM-DD-YYYY")):null}
                                              onChange={(e)=>
                                              {
                                                if(String(e)!="Invalid Date")
                                                {
                                                    const new_date = new Date(e)
                                                    row.trainingDueDateNew=moment(new_date).format("YYYY-MM-DD")
                                                }
                                                else
                                                {
                                                    row.trainingDueDateNew=null
                                                }

                                              }}/>
                                </DemoContainer>
                                </LocalizationProvider>
                                :
                                <span>{moment(row.trainingDueDate,"YYYY-MM-DD").format("DD-MM-YYYY")}</span>

                             }
                       </Tooltip>,
        sortable : true
      },
      {
        name:"COMPLETION DATE",
        width:"16%",
        selector: (row) => row.trainingCompletionDate,
        cell: (row) => <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>Format : DD-MM-YYYY</span>}>
                        <span>{row.trainingCompletionDate==null?row.trainingCompletionDate:moment(row.trainingCompletionDate,"YYYY-MM-DD").format("DD-MM-YYYY")}</span>
                       </Tooltip>,
        sortable : true
      }
    ]

    var projectCount=[]
    var maxProjectCount=[]
    var btCount=[]
    var maxBtCount=[]
    //Expandable content
    function ExpandedComponent(rowData)
    {
        setAnimateHeightToggle(false)
        const expandableRow=rowData.data
        const [expanded,setExpanded] = useState(true)
        //Expandable rows' headers
        var innerColumns =[
          {
            name:"SUB-MODULE",
            width:"15%",
            selector: (row) => row.trainingModule,
            sortable : true
          },
          {
            name:"CURRICULUM",
            selector: (row) => row.trainingName,
            sortable : true
          },
          {
            name:"DUE DATE",
            selector: (row) =>row.trainingDueDate,
            cell: (row) => <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>Format : DD-MM-YYYY</span>}>
                                {
                                    expandableRow.type=="BT"
                                    ?

                                    editBtAssignedDueDateToggle && !row.editDueDateDisabled && !row.trainingCompletionDate
                                    ?
                                    <LocalizationProvider dateAdapter={AdapterDayjs} >
                                    <DemoContainer components={['DatePicker']} >
                                      <DatePicker format="DD-MM-YYYY" disablePast label="Due Date" value={row.trainingDueDateNew?dayjs(moment(row.trainingDueDateNew,"YYYY-MM-DD").format("MM-DD-YYYY")):null}
                                                  onChange={(e)=>
                                                  {
                                                    if(String(e)!="Invalid Date")
                                                    {
                                                        const new_date = new Date(e)
                                                        row.trainingDueDateNew=moment(new_date).format("YYYY-MM-DD")
                                                    }
                                                    else
                                                    {
                                                        row.trainingDueDateNew=null
                                                    }
    
                                                  }}/>
                                    </DemoContainer>
                                    </LocalizationProvider>
                                    :
                                    <span>{moment(row.trainingDueDate,"YYYY-MM-DD").format("DD-MM-YYYY")}</span>

                                    :

                                    editPstAssignedDueDateToggle && !row.editDueDateDisabled && !row.trainingCompletionDate
                                    ?
                                    <LocalizationProvider dateAdapter={AdapterDayjs} >
                                    <DemoContainer components={['DatePicker']} >
                                      <DatePicker format="DD-MM-YYYY" disablePast label="Due Date" value={row.trainingDueDateNew?dayjs(moment(row.trainingDueDateNew,"YYYY-MM-DD").format("MM-DD-YYYY")):null}
                                                  onChange={(e)=>
                                                  {
                                                    if(String(e)!="Invalid Date")
                                                    {
                                                        const new_date = new Date(e)
                                                        row.trainingDueDateNew=moment(new_date).format("YYYY-MM-DD")
                                                    }
                                                    else
                                                    {
                                                        row.trainingDueDateNew=null
                                                    }
    
                                                  }}/>
                                    </DemoContainer>
                                    </LocalizationProvider>
                                    :
                                    <span>{row.trainingDueDate=="Not Required"?"Not Required":moment(row.trainingDueDate,"YYYY-MM-DD").format("DD-MM-YYYY")}</span>
                                    
    
                                 }
                           </Tooltip>,
            sortable : true
          },
          {
            name:"COMPLETION DATE",
            selector: (row) => row.trainingCompletionDate,
            cell: (row) => <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>Format : DD-MM-YYYY</span>}>
                            <span>{row.trainingDueDate=="Not Required"?"Not Required":row.trainingCompletionDate==null?row.trainingCompletionDate:moment(row.trainingCompletionDate,"YYYY-MM-DD").format("DD-MM-YYYY")}</span>
                           </Tooltip>,
            sortable : true
          }
        ]
        var expandableRowData = [];

        if(expandableRow.type=="BT")
        {
            expandableRowData = expandedTrainings.filter((training)=>(training.trainingModuleNumber==expandableRow.trainingModule))
            btCount[expandableRow.index]=0;
            maxBtCount[expandableRow.index]=expandableRowData.length
        }
        else if(expandableRow.type=="PST")
        {
            expandableRowData = projectExpandedTrainings.filter((training)=>(training.trainingModuleNumber==expandableRow.trainingModule))
            projectCount[expandableRow.index]=0;
            maxProjectCount[expandableRow.index]=expandableRowData.length
        }
        setAnimateHeightToggle(true)
         


        return(
                <center style={{width:"80%"}}>
                <DataTable
                      title={expandableRow.trainingName}
                      columns={innerColumns}
                      data={expandableRowData}
                      paginationRowsPerPageOptions={[5,10,15,25,50,100]}
                      customStyles={customStyles}
                      noContextMenu={true}
                      highlightOnHover
                      pagination
                />
                
                </center>
              )
    }


    

    async function saveChanges()
    {

        var alteredReAssignedTrainingIds=[]
        var reAssignedTrainingsDueDates=[]
        var alteredBtTrainingIds=[]
        var btTrainingsDueDates=[]
        var alteredPstTrainingIds=[]
        var pstTrainingsDueDates=[]
        var alteredExpandedTrainingIds=[]
        var expandedTrainingsDueDates=[]
        var alteredProjectExpandedTrainingIds=[]
        var projectExpandedTrainingsDueDates=[]
        const alteredReAssignedTrainings = filteredRtTrainings.filter((training)=>(training.trainingDueDateNew!=null))
        const alteredBtTrainings = filteredBtTrainings.filter((training)=>(training.trainingDueDateNew!=null))
        const alteredPstTrainings = filteredPstTrainings.filter((training)=>(training.trainingDueDateNew!=null))
        const alteredexpandedTrainings = expandedTrainings.filter((training)=>(training.trainingDueDateNew!=null))
        const alteredProjectExpandedTrainings = projectExpandedTrainings.filter((training)=>(training.trainingDueDateNew!=null)) 
        if(alteredReAssignedTrainings.length==0&&alteredBtTrainings.length==0&&alteredPstTrainings.length==0&&alteredexpandedTrainings.length==0&&alteredProjectExpandedTrainings.length==0)
        {
            setPopupMessage("No valid due date changes detected!");
            setSaveChangesFlag(true);          
        }
        else if(alteredReAssignedTrainings.filter((training)=>(dayjs(moment(training.trainingDueDateNew,"YYYY-MM-DD").format("MM-DD-YYYY"))<dayjs(moment(new Date(),"YYYY-MM-DD").format("MM-DD-YYYY")))).length>0)
        {
            setPopupMessage("Some due date(s) is/are in the past!");
            setSaveChangesFlag(true);          
        }
        else if(alteredBtTrainings.filter((training)=>(dayjs(moment(training.trainingDueDateNew,"YYYY-MM-DD").format("MM-DD-YYYY"))<dayjs(moment(new Date(),"YYYY-MM-DD").format("MM-DD-YYYY")))).length>0)
        {
            setPopupMessage("Some due date(s) is/are in the past!");
            setSaveChangesFlag(true);          
        }
        else if(alteredPstTrainings.filter((training)=>(dayjs(moment(training.trainingDueDateNew,"YYYY-MM-DD").format("MM-DD-YYYY"))<dayjs(moment(new Date(),"YYYY-MM-DD").format("MM-DD-YYYY")))).length>0)
        {
            setPopupMessage("Some due date(s) is/are in the past!");
            setSaveChangesFlag(true);          
        }
        else if(alteredexpandedTrainings.filter((training)=>(dayjs(moment(training.trainingDueDateNew,"YYYY-MM-DD").format("MM-DD-YYYY"))<dayjs(moment(new Date(),"YYYY-MM-DD").format("MM-DD-YYYY")))).length>0)
        {
            setPopupMessage("Some due date(s) is/are in the past!");
            setSaveChangesFlag(true);          
        }
        else if(alteredProjectExpandedTrainings.filter((training)=>(dayjs(moment(training.trainingDueDateNew,"YYYY-MM-DD").format("MM-DD-YYYY"))<dayjs(moment(new Date(),"YYYY-MM-DD").format("MM-DD-YYYY")))).length>0)
        {
            setPopupMessage("Some due date(s) is/are in the past!");
            setSaveChangesFlag(true);          
        }
        else
        {
            setPopup(false)
            setLoadingPopup(true)
            for(var i=0;i<alteredReAssignedTrainings.length;i++)
            {
                alteredReAssignedTrainingIds.push(alteredReAssignedTrainings[i].trainingId)
                reAssignedTrainingsDueDates.push(alteredReAssignedTrainings[i].trainingDueDateNew)
            }
            for(var i=0;i<alteredBtTrainings.length;i++)
            {
                alteredBtTrainingIds.push(alteredBtTrainings[i].trainingId)
                btTrainingsDueDates.push(alteredBtTrainings[i].trainingDueDateNew)
            }
            for(var i=0;i<alteredPstTrainings.length;i++)
            {

                alteredPstTrainingIds.push(alteredPstTrainings[i].trainingId)
                pstTrainingsDueDates.push(alteredPstTrainings[i].trainingDueDateNew)
            }
            for(var i=0;i<alteredexpandedTrainings.length;i++)
            {
                alteredExpandedTrainingIds.push(alteredexpandedTrainings[i].trainingId)
                expandedTrainingsDueDates.push(alteredexpandedTrainings[i].trainingDueDateNew)
            }
            for(var i=0;i<alteredProjectExpandedTrainings.length;i++)
            {
                alteredProjectExpandedTrainingIds.push(alteredProjectExpandedTrainings[i].trainingId)
                projectExpandedTrainingsDueDates.push(alteredProjectExpandedTrainings[i].trainingDueDateNew)
            }
            const details = {
                alteredReAssignedTrainingIds:alteredReAssignedTrainingIds,
                dueDates:reAssignedTrainingsDueDates,
                alteredBtTrainingIds:alteredBtTrainingIds,
                btTrainingsDueDates:btTrainingsDueDates,
                alteredPstTrainingIds:alteredPstTrainingIds,
                pstTrainingsDueDates:pstTrainingsDueDates,
                alteredExpandedTrainingIds:alteredExpandedTrainingIds,
                expandedTrainingsDueDates:expandedTrainingsDueDates,
                alteredProjectExpandedTrainingIds:alteredProjectExpandedTrainingIds,
                projectExpandedTrainingsDueDates:projectExpandedTrainingsDueDates,

            }
            await axios.post("http://10.220.20.246:8000/ChangeReAssignedTrainingsDueDates",details).then((res)=>
            {
                setLoadingPopup(false)
                setPopupMessage(res.data.response);
                setPopup(true)
                setSaveChangesFlag(true);
                fetch_details();
            })
        }
        setSwitchChecked(false)
        setBtSwitchChecked(false)
        setPstSwitchChecked(false)
        setEditAssignedDueDateToggle(false)
        setEditBtAssignedDueDateToggle(false)
        setEditPstAssignedDueDateToggle(false)
    }

    async function deleteReAssignedTrainings()
    {
        setPopup2(false)
        setLoadingPopup(true)
        var deletionReAssignedTrainingIds=[]
        for(var i=0;i<selectedRtTrainings.length;i++)
        {
            deletionReAssignedTrainingIds.push(selectedRtTrainings[i].trainingId)
        }
        const details={
            deletionReAssignedTrainingIds:deletionReAssignedTrainingIds
        }
        await axios.post("http://10.220.20.246:8000/DeleteReAssignedTrainings",details).then((res)=>
            {
                setLoadingPopup(false)
                setPopupMessage2(res.data.response);
                setPopup2(true)
                setDeletionFlag(true);
                setSelectedRtTrainings([])
                setSelectedBtTrainings([])
                setSelectedPstTrainings([])
                setPstKey(pstKey+1)
                setBtKey(btKey+1)
                setRtKey(rtKey+1)
                for(var i=0;i<expandedTrainings.length;i++)
                {
                    expandedTrainings[i].selected=false;
                }
                for(var i=0;i<projectExpandedTrainings.length;i++)
                {
                    projectExpandedTrainings[i].selected=false;
                }
                setCount(0)
                fetch_details();
            })
    }

    const contextActions = React.useMemo(() =>
    {
        return(
            <button style={{marginRight:"5%"}} disabled={selectedRtTrainings.length>0?false:true} className="btn btn-danger btn-lg" onClick={()=>{setPopup2(true);setPopupMessage2(selectedRtTrainings.length==1?"Are you sure you delete this re-assigned training ?":"Are you sure you delete these "+selectedRtTrainings.length+" re-assigned trainings ?");}}>Delete</button>           
        );
    }, [selectedRtTrainings])

   
    return(

        <div className="bg">
        <Navbar setUserEmail={props.setUserEmail} setUserType={props.setUserType} dashboard_link={"/"+location.state.type.toLowerCase()+"_dashboard"}/>
        {
           traineeEmail!=""

           ?
            <div>
                <h1>{traineeName.toUpperCase()+"'S LEARNING PLAN"}</h1>
                <br/>
                <div className="table" style={{margin:"auto"}}>
                    <Tabs variant="fullWidth" style={{width:"100%"}} value={view} onChange={(event,value)=>{setView(value)}}>
                      <Tab label="Basic Trainings" style={{fontSize:"150%"}}/>
                      <Tab label={projectData.Project_Name+" Trainings"} style={{fontSize:"150%"}}/>
                      <Tab label="Re-Assigned Trainings" style={{fontSize:"150%"}}/>
                    </Tabs>
                    <SwipeableViews animateHeight={animateHeightToggle} index={view} onChangeIndex={(event,value)=>{setView(value);}}>

                    {/*BASIC TRAININGS */}
                    <center>
                    {/*count>0 ?  <div style={{display: 'flex', alignItems: 'center', background:"#Add8e6", color:"black", textAlign:"left", width:"100%", height:"50px", verticalAlign:"middle"}}>&emsp;{count+(count==1?" training":" trainings")+" selected"}</div>   : <div></div> */}
                    <br/>     
                        <DataTable
                        key={btKey} //This  is changed after every re-assignment or delteion to ensure that the table is reset
                        columns={btColumns}
                        data={filteredBtTrainings}
                        paginationRowsPerPageOptions={[5,10,15,25,50,100]}
                        noHeader
                        fixedHeader
                        fixedHeaderScrollHeight="100vh"
                        noContextMenu={true}
                        /*selectableRows
                        selectableRowsHighlight
                        selectableRowDisabled={row => row.selectionDisabled}
                        onSelectedRowsChange={(e) => setSelectedBtTrainings(e.selectedRows)}*/
                        customStyles={customStyles}
                        expandableRows
                        onRowExpandToggled={(e,row)=>{row.expanded=e;setAnimateHeightToggle(true);setAnimateHeightToggle(false)}}
			            expandableRowDisabled={row => row.expansionDisabled}
			            expandableRowsComponent={ExpandedComponent}
			            expandableComponentProps={{ expandableRow: (row) => row}}
                        highlightOnHover
                        pagination
                        subHeader
                        subHeaderComponent =
                        {
                                <div style={{width:"100%", textAlign:"left"}}>
                                <TextField style={{width:"25%"}} label="Search by curriculum" variant="outlined" value={btSearch} onChange={(e)=>{if(e.target.value[e.target.value.length-1]!="\\"){setBtSearch(e.target.value)}}}/> &emsp;
                                 <FormControl component="fieldset" variant="standard">
                                 <FormGroup>
                                   <FormControlLabel control={<Switch disabled={(filteredBtTrainings.filter((training)=>(training.trainingCompletionDate==null)).length+expandedTrainings.filter((training)=>(training.trainingCompletionDate==null)).length)>0?false:true}  checked={btSwitchChecked} onChange={()=>{setBtSwitchChecked(!btSwitchChecked); setEditBtAssignedDueDateToggle(!editBtAssignedDueDateToggle)}}/>} style={{color:"black"}} label="Edit Due Date" />
                                 </FormGroup>
                                 </FormControl>
                               </div>
                               

                        }
                        subHeaderAlign="left"
                      />
                      </center>

                      {/*PROJECT TRAININGS */}
                      <center>
                      {/*count>0 ?  <div style={{display: 'flex', alignItems: 'center', background:"#Add8e6", color:"black", textAlign:"left", width:"100%", height:"50px", verticalAlign:"middle"}}>&emsp;{count} trainings selected</div>   : <div></div>*/ }               
                      <br/>
                      <DataTable
                        key={pstKey} //This  is changed after every re-assignment or delteion to ensure that the table is reset
                        columns={pstColumns}
                        data={filteredPstTrainings}
                        noHeader
                        paginationRowsPerPageOptions={[5,10,15,25,50,100]}
                        fixedHeader
                        fixedHeaderScrollHeight="100vh"
                        noContextMenu={true}
                       /* selectableRows
                        selectableRowsHighlight
                        selectableRowDisabled={row => row.selectionDisabled}
                        onSelectedRowsChange={(e) => setSelectedPstTrainings(e.selectedRows)}*/
                        customStyles={customStyles}
                        expandableRows
                        onRowExpandToggled={(e,row)=>{row.expanded=e;setAnimateHeightToggle(true);setAnimateHeightToggle(false)}}
			            expandableRowDisabled={row => row.expansionDisabled}
			            expandableRowsComponent={ExpandedComponent}
			            expandableComponentProps={{ expandableRow: (row) => row }}
                        highlightOnHover
                        pagination
                        subHeader
                        subHeaderComponent =
                        {
                                 <div style={{width:"100%", textAlign:"left"}}>
                                 <TextField style={{width:"25%"}} label="Search by curriculum" variant="outlined" value={pstSearch} onChange={(e)=>{if(e.target.value[e.target.value.length-1]!="\\"){setPstSearch(e.target.value)}}}/> &emsp;
                                  <FormControl component="fieldset" variant="standard">
                                  <FormGroup>
                                    <FormControlLabel control={<Switch disabled={(filteredPstTrainings.filter((training)=>(training.trainingCompletionDate==null)).length+projectExpandedTrainings.filter((training)=>(training.trainingCompletionDate==null)).length)>0?false:true} checked={pstSwitchChecked} onChange={()=>{setPstSwitchChecked(!pstSwitchChecked); setEditPstAssignedDueDateToggle(!editPstAssignedDueDateToggle)}}/>} style={{color:"black"}} label="Edit Due Date" />
                                  </FormGroup>
                                  </FormControl>
                                </div>
                               

                        }
                        subHeaderAlign="left"
                      />

                      </center>
                      
                      {/*RE-ASSIGNED TRAININGS */}
                      <center>
                      <DataTable
                       key={rtKey} //This  is changed after every re-assignment or delteion to ensure that the table is reset
                        title={" "}
                        columns={rtColumns}
                        data={filteredRtTrainings}
                        paginationRowsPerPageOptions={[5,10,15,25,50,100]}
                        fixedHeader
                        fixedHeaderScrollHeight="100vh"
                        selectableRows
                        onSelectedRowsChange={(e) => setSelectedRtTrainings(e.selectedRows)}
                        //expandableRows
			            //expandableRowDisabled={row => row.disabled}
			            //expandableRowsComponent={ExpandedComponent}
                        contextMessage={{singular:"training",plural:"trainings",message:"selected"}}
                        highlightOnHover
                        contextActions={contextActions}
                        customStyles={customStyles}
                        noContextMenu={!selectedRtTrainings.length}
                        pagination
                        subHeader
                        subHeaderComponent =
                        {
                            <div style={{width:"100%", textAlign:"left"}}>
                                <TextField style={{width:"25%"}}  label="Search by curriculum" variant="outlined" value={rtSearch} onChange={(e)=>{if(e.target.value[e.target.value.length-1]!="\\"){setRtSearch(e.target.value)}}}/> &emsp;
                                 <FormControl component="fieldset" variant="standard">
                                 <FormGroup>
                                   <FormControlLabel control={<Switch disabled={filteredRtTrainings.filter((training)=>(training.trainingCompletionDate==null)).length?false:true} checked={switchChecked} onChange={()=>{setSwitchChecked(!switchChecked); setEditAssignedDueDateToggle(!editAssignedDueDateToggle)}}/>} style={{color:"black"}} label="Edit Due Date" />
                                 </FormGroup>
                                 </FormControl>
                            </div>

                        }
                        subHeaderAlign="left"
                    />
                      </center>
                    </SwipeableViews>
                </div>
                <br/>
                <center>
            {/* view=0 indicates Basic Trainings View, view=1 indicates Project Trainings View, view=2 indicates Re-Assigned Trainings View   */}
            {
                view!=2
                ?
                <div>
                <button className="btn btn-success btn-lg" disabled={!(btSwitchChecked||pstSwitchChecked||switchChecked)} onClick={()=>{setPopup(true);setPopupMessage("Are you sure you want to save these due date changes ?");}}>Save Due Date Changes</button>&emsp;
                {/*<button className="btn btn-success btn-lg" onClick={determineCount}>Re-Assign Trainings</button>*/}
                </div>
                :
                <div>
                <button className="btn btn-success btn-lg" disabled={!(btSwitchChecked||pstSwitchChecked||switchChecked)} onClick={()=>{setPopup(true);setPopupMessage("Are you sure you want to save these due date changes ?");}}>Save Due Date Changes</button>&emsp;
                {/*<button disabled={selectedRtTrainings.length>0?false:true} className="btn btn-success btn-lg" onClick={()=>{setPopup2(true);setPopupMessage2(selectedRtTrainings.length==1?"Are you sure you delete this re-assigned training ?":"Are you sure you delete these "+selectedRtTrainings.length+" re-assigned trainings ?");}}>Delete</button>*/}
                </div>
            }
            </center>
            <br/>
			</div>

           :

             <div><br/><h1>You cannot access the dashboard without logging in</h1></div>
        }

        <Popup open={popup} closeOnDocumentClick onClose={()=>{setPopup(false)}}>
            <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setPopup(false);}}/>
            <center>
              <br/>
              <p style={{color:"#003C71", fontSize:"130%", margin:"3%"}}><center>{popupMessage}</center></p>
              <br/>
              {
              
              saveChangesFlag==true
              
              ?

              <div className='btn btn-info btn-lg' onClick={()=>{setPopup(false);setSaveChangesFlag(false);setDeletionFlag(false)}}>Ok</div>

              :
              <center>

              <div className='btn btn-info btn-lg' onClick={saveChanges}>Yes</div> &emsp;
              <div className='btn btn-info btn-lg' onClick={()=>{setPopup(false)}}>No</div>
              </center>
              }
              <br/>
              <br/>
            </center>
         </Popup>
         <Popup open={popup2} closeOnDocumentClick onClose={()=>{setPopup2(false)}}>
            <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setPopup2(false);}}/>
            <center>
              <br/>
              <p style={{color:"#003C71", fontSize:"130%", margin:"3%"}}><center>{popupMessage2}</center></p>
              <br/>
              {
              deletionFlag==true

              ?

              <div className='btn btn-info btn-lg' onClick={()=>{setPopup2(false);setDeletionFlag(false)}}>Ok</div>

              :
              <center>

              <div className='btn btn-info btn-lg' onClick={deleteReAssignedTrainings}>Yes</div> &emsp;
              <div className='btn btn-info btn-lg' onClick={()=>{setPopup2(false)}}>No</div>
              </center>
              }
              <br/>
              <br/>
            </center>
         </Popup>

         {/* LOADING SCREEN */}
         <Popup open={loadingPopup} hideBackdrop closeOnDocumentClick={false} onClose={()=>{setLoadingPopup(false)}}>
              <center>
                <p style={{color:"#003C71", fontSize:"130%", margin:"3%"}}><center>PLEASE WAIT...</center></p>
                <br/>
                <CircularProgress/>
                <br/>
                <br/>
              </center>
         </Popup> 


        </div>
    )
}

export default Trainee_learning_path