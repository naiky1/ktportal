import React, {useState, useEffect} from 'react'
import Navbar from '../../Base/navbar.jsx'
import './trainee_progress_manager.css'
import axios from 'axios'
import Popup from 'reactjs-popup'
import moment from 'moment'
import {useNavigate, useLocation} from 'react-router-dom'
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid'
import BasicTrainingsProgressCard from './Cards/basic_trainings_progress_card.jsx'
import ProjectSpecificTrainingsProgressCard from './Cards/project_specific_trainings_progress_card.jsx'
import TaskAltOutlinedIcon from '@mui/icons-material/TaskAltOutlined';
import RadioButtonUncheckedIcon from '@mui/icons-material/RadioButtonUnchecked';
import {PieChart,Pie, ResponsiveContainer, Tooltip, Cell, Label} from 'recharts'

function Trainee_progress_manager(props)
{

    //Dynamic CSS Variables
    const [overallProgressStyling,setOverallProgressStyling] = useState({
        whitegradient:0,
        greengradient:0,
        hoverbgsize:0
    })
    const [ktCompletionStyling,setKtCompletionStyling] = useState({
        whitegradient:0,
        greengradient:0,
        hoverbgsize:0
    })
    const [missedTrainingsStyling,setMissedTrainingsStyling] = useState({
        whitegradient:0,
        greengradient:0,
        hoverbgsize:0
    })

    //To redirect to the appropriate dashboard if the credentials are valid
    let navigate = useNavigate();

    //To get the trainee ID from the previous page
    const location = useLocation();
    const traineeId= location.state.traineeId;
    const traineeType= location.state.traineeType;
    const projectId= location.state.projectId;
    const traineeEmail = location.state.traineeEmail
    const [missedTrainings,setMissedTrainings] = useState([])
    const [missedTrainingsNames,setMissedTrainingsNames]=useState([])
    const [missedTrainingsPercentage,setMissedTrainingsPercentage] = useState(0)

    //Trainee Status details
    const [ktFound,setKtFound] = useState(1)
    const [traineeName,setTraineeName] = useState("")
    const [overallProgress,setOverallProgress] = useState(0)
    const [ktCompletionDate,setKtCompletionDate] = useState()
    const [daysLeft,setDaysLeft] = useState(0)
    const [ktPercentageCompletion,setKtPercentageCompletion] = useState(0)
    const [smPieChartData, setSmPieChartData] = useState([{name:"Completed",value:0},{name:"Incomplete",value:0}])
    const [subModuleNames,setSubModuleNames] = useState([])
    const [subModuleStatuses,setSubModuleStatuses] = useState([])
    const COLORS = ['green', '#DB4437'];
    const [moduleName,setModuleName] = useState("")
    const [toggle,setToggle] = useState("PST")
    const [completionPercentage,setCompletionPercentage] = useState(0)
    const [ktCompleted, setktCompleted] = useState("")

    async function fetch_details()
    {
         const details = {
            userId : traineeId,
            traineeType : traineeType,
            projectId : projectId
        }
        const Params = new URLSearchParams(details);
        const URLWithParams = `http://10.220.20.246:8000/fetchUserName?${Params}`;
        await axios.get(URLWithParams).then( res=> {setTraineeName(res.data.userName)})

        const MTURLWithParams = `http://10.220.20.246:8000/fetchMissedTrainings?${Params}`;
        await axios.get(MTURLWithParams).then( res =>
        {
            setMissedTrainings(res.data.missed_trainings);
            setMissedTrainingsNames(res.data.missed_trainings_names)
            setMissedTrainingsPercentage(res.data.missed_trainings_percentage)

            if(res.data.missed_trainings_percentage<50)
            {
                missedTrainingsStyling.whitegradient=100-res.data.missed_trainings_percentage;
                missedTrainingsStyling.greengradient=res.data.missed_trainings_percentage;
                missedTrainingsStyling.hoverbgsize=100;
            }
            else
            {
                missedTrainingsStyling.whitegradient=50;
                missedTrainingsStyling.greengradient=50;
                missedTrainingsStyling.hoverbgsize=2*res.data.missed_trainings_percentage;
            }
        });

        const OverallProgressParams = new URLSearchParams(details);
        const OverallProgressURLWithParams = `http://10.220.20.246:8000/fetchOverallProgress?${OverallProgressParams}`;
        await axios.get(OverallProgressURLWithParams).then( res=>
        {
            setKtFound(res.data.kt_found)
            setktCompleted(res.data.reverse_kt_done)
            setOverallProgress(res.data.overall_progress)
            const completionDate = moment(res.data.KT_completion_date).format("DD-MM-YYYY")
            setKtCompletionDate(completionDate)
            setDaysLeft(res.data.days_remaining)
            setKtPercentageCompletion(res.data.Kt_Percentage_Completion)
            if(res.data.overall_progress<50)
            {
                overallProgressStyling.whitegradient=100-res.data.overall_progress;
                overallProgressStyling.greengradient=res.data.overall_progress;
                overallProgressStyling.hoverbgsize=100;
            }
            else
            {
                overallProgressStyling.whitegradient=50;
                overallProgressStyling.greengradient=50;
                overallProgressStyling.hoverbgsize=2*res.data.overall_progress;
            }

            if(res.data.Kt_Percentage_Completion<50)
            {
                ktCompletionStyling.whitegradient=100-res.data.Kt_Percentage_Completion;
                ktCompletionStyling.greengradient=res.data.Kt_Percentage_Completion;
                ktCompletionStyling.hoverbgsize=100;
            }
            else
            {
                ktCompletionStyling.whitegradient=50;
                ktCompletionStyling.greengradient=50;
                ktCompletionStyling.hoverbgsize=2*res.data.Kt_Percentage_Completion;
            }



        })

    }

     useEffect(() =>
    {
        fetch_details()
    },[])



    return(

        ktFound

        ?

        <div className="bg">
        <Navbar setUserEmail={props.setUserEmail} setUserType={props.setUserType} dashboard_link="/manager_dashboard"/>
            <h1>{traineeName.toUpperCase()}'S PROGRESS</h1 >
            <br/>
            <Grid container spacing={3} style={{width:"100%", height:"40%"}} >
              <Grid item sm={6} style={{paddingLeft:"3%", height:"80%"}} >
                <div className="overall_progress_manager" style={{borderRadius:"10px", overflow:"auto", display: "flex",flexDirection: "column",justifyContent:"center" , '--opwhitegradient':`${overallProgressStyling.whitegradient}%`, '--opgreengradient':`${overallProgressStyling.greengradient}%`,'--ophoverbgsize':`${overallProgressStyling.hoverbgsize}%`}}>
                <div style={{fontSize:"70%",fontWeight:"600"}}>KT Progress <br/> {overallProgress}%</div>
                </div>
                <div className='Missed_trainings_manager' style={{borderRadius:"10px", overflow:"auto", display: "flex",flexDirection: "column",justifyContent:"center", '--mtwhitegradient':`${missedTrainingsStyling.whitegradient}%`, '--mtgreengradient':`${missedTrainingsStyling.greengradient}%`,'--mthoverbgsize':`${missedTrainingsStyling.hoverbgsize}%`}}>
                   <div style={{fontWeight:"600", fontSize:"120%"}}>{missedTrainings.length} TRAININGS MISSED</div>
                </div>
              </Grid>

              <Grid item sm={6} style={{height:"83%"}}>
                <div className="KT_completion_date_manager" style={{overflow:"auto", display: "flex",flexDirection: "column",justifyContent:"center", '--ktwhitegradient':`${ktCompletionStyling.whitegradient}%`, '--ktgreengradient':`${ktCompletionStyling.greengradient}%`,'--kthoverbgsize':`${ktCompletionStyling.hoverbgsize}%`}}>
                  <div style={{fontSize:"60%"}}>KT Completion Date</div>
                  {ktCompletionDate}
                  {overallProgress==100?ktCompleted=="Y"?<div style={{fontWeight:"300", fontSize:"60%"}}>Reverse KT - Completed</div>:<div style={{fontWeight:"300", fontSize:"80%", color:"red"}}>Reverse KT - Pending</div>:<div style={{fontSize:"80%"}}>({daysLeft} day(s) remaining)</div>}
                 <Paper elevation={2} />
                 </div>
              </Grid>



              <Grid item sm={6} style={{paddingLeft:"3%", maxHeight:"120%", height:"120%"}}>
                <BasicTrainingsProgressCard setSubModuleStatuses={setSubModuleStatuses} setSubModuleNames={setSubModuleNames} setCompletionPercentage={setCompletionPercentage} setSmPieChartData={setSmPieChartData} traineeId={location.state.traineeId}  traineeEmail={traineeEmail} traineeType={traineeType} projectspecificprojectId={projectId} setModuleName={setModuleName} moduleName={moduleName}/>
              </Grid>

              <Grid item sm={6} style={{maxHeight:"120%", height:"120%"}} >
                <ProjectSpecificTrainingsProgressCard setSubModuleStatuses={setSubModuleStatuses} setSubModuleNames={setSubModuleNames} setCompletionPercentage={setCompletionPercentage} setSmPieChartData={setSmPieChartData} traineeId={location.state.traineeId} traineeEmail={traineeEmail} traineeType={traineeType} projectspecificprojectId={projectId} setModuleName={setModuleName} moduleName={moduleName} />
              </Grid>

            </Grid>
        </div>

        :

        <div className="bg">
        <Navbar setUserType={props.setUserType} setUserEmail={props.setUserEmail} dashboard_link="manager_dashboard"/>
        <br/>
        <h1>{traineeName} has not been assigned any KT Trainings</h1 >
        </div>

    )

}

export default Trainee_progress_manager