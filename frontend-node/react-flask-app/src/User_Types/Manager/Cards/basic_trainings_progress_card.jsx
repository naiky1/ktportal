import './basic_trainings_progress_card.css'
import {useState, useEffect} from 'react'
import axios from 'axios'
import {PieChart,Pie, ResponsiveContainer, Tooltip, Cell, Label, Sector} from 'recharts'
import TaskAltOutlinedIcon from '@mui/icons-material/TaskAltOutlined';
import RadioButtonUncheckedIcon from '@mui/icons-material/RadioButtonUnchecked';
import Paper from '@mui/material/Paper';

function Basic_Trainings_Progress_Card(param)
{

    const traineeId= param.traineeId;
    const traineeEmail = param.traineeEmail
    const traineeType= param.traineeType;
    const projectspecificprojectId = param.projectspecificprojectId;
    const [flip,setFlip] = useState(false)


    const [trainingNames,setTrainingNames] = useState([])
    const [trainingStatuses,setTrainingStatuses] = useState([])
    const [btPieChartData, setBtPieChartData] = useState([])
    const [btOverallPieChartData, setBtOverallPieChartData] = useState([])
    const [completionPercentage,setCompletionPercentage] = useState(0)
    const [smPieChartData,setSmPieChartData] = useState()
    const [subModuleNames,setSubModuleNames] = useState()
    const [moduleCompletionPercentage,setModuleCompletionPercentage] = useState()
    const [subModuleStatuses,setSubModuleStatuses] = useState()
    const [COLORS,setCOLORS] = useState([]);
    var OverallPCCOLORS = ['#60BE71','#CBCBCA'];

     const renderActiveShape = (props) =>
    {
        const RADIAN = Math.PI / 180;
        const { cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle, fill, payload, percent, value } = props;
        const sin = Math.sin(-RADIAN * midAngle);
        const cos = Math.cos(-RADIAN * midAngle);
        const sx = cx + (outerRadius + 10) * cos;
        const sy = cy + (outerRadius + 10) * sin;
        const mx = cx + (outerRadius + 30) * cos;
        const my = cy + (outerRadius + 30) * sin;
        const ex = mx + (cos >= 0 ? 1 : -1) * 22;
        const ey = my;
        const textAnchor = cos >= 0 ? 'start' : 'end';


        return (
          <g>

            <Sector
              cx={cx}
              cy={cy}
              innerRadius={innerRadius}
              outerRadius={outerRadius}
              startAngle={startAngle}
              endAngle={endAngle}
              fill={fill}

            />
            <Sector
              cx={cx}
              cy={cy}
              startAngle={startAngle}
              endAngle={endAngle}
              innerRadius={outerRadius + 6}
              outerRadius={outerRadius + 10}
              fill={fill}

            />

          </g>
        );
    }


    const [activeIndex,setActiveIndex] = useState(0)

    const onPieEnter = (_, index) => {
      setActiveIndex(index)
      param.setModuleName(trainingNames[index]+" (Basic Trainings)")
      param.setSmPieChartData(smPieChartData[index][0][0])
      param.setSubModuleNames(subModuleNames[index][0])
      param.setCompletionPercentage(moduleCompletionPercentage[index])
      param.setSubModuleStatuses(subModuleStatuses[index][0])
    };


    async function fetch_details()
    {
         const traineeDetails = {
            email : traineeEmail,
            traineeType : traineeType,
            projectId : projectspecificprojectId
        }
        const basicparams = new URLSearchParams(traineeDetails);
        const URLWithBasicParams = `http://10.220.20.246:8000/fetchTraineeCommonTrainingProjectId?${basicparams}`;
        const basictrainingprojectid = await axios.get(URLWithBasicParams).then( res => res.data.commonprojectid)

         const details = {
            userId : traineeId,
            traineeType : traineeType,
            projectId : basictrainingprojectid,
            projectspecificprojectId : projectspecificprojectId
        }
        const Params = new URLSearchParams(details);
        const URLWithParams = `http://10.220.20.246:8000/fetchTrainings?${Params}`;
        await axios.get(URLWithParams).then( res =>
        {
            setBtOverallPieChartData([{ name: 'Completed', value: res.data.completed_modules }, { name: 'Incomplete', value: res.data.incomplete_modules }])
            setTrainingNames(res.data.trainingNames)
            setCompletionPercentage(res.data.completion_percentage)
            setTrainingStatuses(res.data.trainingStatuses)

             setSmPieChartData(res.data.smPieChartData)
             setSubModuleNames(res.data.subModuleNames)
             setModuleCompletionPercentage(res.data.moduleCompletionPercentage)
             setSubModuleStatuses(res.data.subModuleStatuses)

             var final_data=[]
             var final_COLORS=[]
             res.data.trainingNames.map((el,index) =>
             {
                 var data = {name:el,value:1};
                 final_data.push(data)
                 var color;
                 if(res.data.trainingStatuses[index]=="Y")
                 {
                     color="#60BE71"
                 }
                 else
                 {
                     color="#CBCBCA"
                 }
                 final_COLORS.push(color)
             })
             setBtPieChartData(final_data)
             setCOLORS(final_COLORS)

        })
    }

     useEffect(() =>
    {
        fetch_details()
    },[])

    return(

        <div className="BT_Progress" style={{display: "flex",flexDirection: "column",justifyContent:"center",backgroundColor:"white"}}>
        <div style={{fontSize:"150%",fontWeight:"600"}}>Basic Trainings Progress</div>
        {btPieChartData.length==0?"(No any assigned training)":null}
        {
            flip

            ?

            <ResponsiveContainer width="100%" height="100%" >
                <PieChart >
                  <Pie
                    activeIndex={activeIndex}
                    activeShape={renderActiveShape}
                    dataKey="value"
                    isAnimationActive={false}
                    data={btPieChartData}
                    cx="50%"
                    cy="50%"
                    innerRadius="70%"
                    outerRadius="90%"
                     onClick={onPieEnter}

                  >

                   {btPieChartData.map((entry, index) =>
                   (
                        <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                   ))}

                   <Label value={trainingNames[activeIndex]} position="center" style={{fill:"black",fontSize:"100%"}}  />

                   </Pie>
                  <Tooltip />
                </PieChart>
            </ResponsiveContainer>


            :

            <ResponsiveContainer width="100%" height="100%" >
                <PieChart>
                  <Pie
                    dataKey="value"
                    isAnimationActive={false}
                    data={btOverallPieChartData}
                    cx="50%"
                    cy="50%"
                    innerRadius="70%"
                    outerRadius="100%"
                  >
                   {btOverallPieChartData.map((entry, index) =>
                   (
                        <Cell key={`cell-${index}`} fill={OverallPCCOLORS[index % OverallPCCOLORS.length]} />
                   ))}
                    <Label value={completionPercentage+"%"} position="center" style={{fill:completionPercentage==0?"#DB4437":"#60BE71",fontSize:"200%"}} />
                   </Pie>
                  <Tooltip />
                </PieChart>
            </ResponsiveContainer>

        }
        <Paper elevation={20} />
        </div>
    )

}

export default Basic_Trainings_Progress_Card