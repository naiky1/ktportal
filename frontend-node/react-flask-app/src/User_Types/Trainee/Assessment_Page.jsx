import React,{useState,useEffect} from 'react'
import Navbar from '../../Base/navbar.jsx'
import './trainee_dashboard.css'
import axios from 'axios'
import Popup from 'reactjs-popup'
import { FormControl,FormLabel,FormControlLabel,Radio,Paper,RadioGroup } from '@mui/material';
import CancelIcon from '@mui/icons-material/Cancel';
import Typography from '@mui/material/Typography'
import { CircularProgress } from '@mui/material'
import VerifiedIcon from '@mui/icons-material/Verified';
import { useNavigate } from 'react-router-dom'

function Assessment_Page(props)
{
       
    {/* DEV OPTIONS 
    const [clear,setClear] = useState(false)
    const [fail,setFail] =  useState(false)*/}

    //To redirect to the appropriate dashboard if the credentials are valid
    let navigate = useNavigate();

    //For inner Popup 
    const [innerPopup,setInnerPopup] = useState(false)
    const [innerPopupMessage,setInnerPopupMessage] = useState("Welcome to the Assessment Page! To clear this module, you need to score a minimum of 70%.")

    //For Feedback and Feedback Popup
    const [feedback,setFeedback] = useState("")
    const [feedbackPopup,setFeedbackPopup] = useState(false)
    const [innerFeedbackPopup,setInnerFeedbackPopup] = useState(false)

    //To store whether the trainee is eligible for the assessment or not
    const [ineligible,setIneligible] =useState(false)
    //To store whether the trainee is has already cleared the assessment or not
    const [alreadyCleared,setAlreadyCleared] =useState(false)
    
    //Loading Screen
    const [loadingPopup,setLoadingPopup] = useState(true)

    //Storing candidate's score
    const [score,setScore]=  useState(0)

    //For Popup
    const [popup,setPopup] = useState(false)
    const [popupMessage,setPopupMessage] = useState("")
    const [assessmentCompleteFlag,setAssessmentCompleteFlag] = useState(false)

    //Webpage Data:-
    const [assessmentData,setAssessmentData] = useState([])
    const [previousScore,setPreviousScore] =  useState(null)
    const [previousTime,setPreviousTime] =  useState(null)

    const fetch_data = async () =>
    {
        const traineeEmailDetails=
        {
            email:props.userEmail
        }
        const projectParams = new URLSearchParams(traineeEmailDetails);
        const projectURLWithParams = `http://10.220.20.246:8000/fetchTraineeCurrentProjectId?${projectParams}`;
        await axios.get(projectURLWithParams).then( async(res) =>
        {
            const details=
            {
                userEmail:props.userEmail,
                projectId:res.data.projectId
            }
            const Params = new URLSearchParams(details);
            const URLWithParams = `http://10.220.20.246:8000/fetchAssessmentDetails?${Params}`;
            const eligibilityCheckURLWithParams = `http://10.220.20.246:8000/checkAssessmentEligibility?${Params}`;
            await axios.get(eligibilityCheckURLWithParams).then(async (res)=>
            {
                if(res.data.ineligible==0)
                {
                    if(res.data.alreadyCleared==0)
                    {                     
                        await axios.get(URLWithParams).then( res =>
                        {  
                            setInnerPopup(true)
                            setAssessmentData(res.data.assessmentData)
                            setPreviousScore(res.data.previousScore)
                            setPreviousTime(res.data.previousTime)
                            setLoadingPopup(false)
                        })
                    }
                    else
                    {
                        setPreviousScore(res.data.previousScore)
                        setPreviousTime(res.data.previousTime)
                        setAlreadyCleared(true)
                        setLoadingPopup(false)
                    }

                }
                else
                {
                    setIneligible(true)
                    setLoadingPopup(false)
                }
            })   
        })
    }

    async function submit()
    {
        {/*DEV OPTIONS */}
        if(/*!(clear||fail)&&*/assessmentData.filter((question)=>question.selectedOption!=null).length!=assessmentData.length)
        {
            const message = "Please answer all questions! No option selected for "+assessmentData.filter((question)=>question.selectedOption==null).map((question)=>{return("Q"+question.Question_Number)})
            setInnerPopupMessage(message)
            setInnerPopup(true)
        }
        else
        {
            setLoadingPopup(true)
            {/*DEV OPTIONS */}
            const details=
            {
                userEmail:props.userEmail,
                selectedOptions:/*clear?[2,8,10,17,19,24,26,31,33,36]:fail?[1,3,4,5,6,7,9,11,12,13]:*/assessmentData.filter((question)=>question.selectedOption!=null).map((question)=>{return(question.selectedOption)})
            }
            await axios.post("http://10.220.20.246:8000/assessmentCorrection",details).then( async(res) =>
            {
                setAssessmentCompleteFlag(true)
                setLoadingPopup(false)
                setScore(res.data.score)
                setInnerPopupMessage([res.data.score>=70?<p style={{color:"#0F9D58",width:"auto"}}>Result : <VerifiedIcon/> Cleared</p>:<p style={{color:"#DB4437"}}>Result : <CancelIcon/> Not Cleared</p>,"Your score - "+res.data.score+"%",<br/>,res.data.score>=70?"":"Do you wish to retake the assessment now ? "])
                setInnerPopup(true)
                assessmentData.map((question)=>question.selectedOption=null)
                {/*DEV OPTIONS 
                clear?setClear(false):setFail(false)*/}
            })
        }
    }

    useEffect(() =>
    {
        fetch_data()
    },[])

    async function sendKtFeedback()
    {
        setLoadingPopup(true)
        const details = {
            userEmail : props.userEmail,
            feedback : feedback
        }
        await axios.post("http://10.220.20.246:8000/addKtFeedback",details).then( async(res) =>
        {
            setLoadingPopup(false)
            setInnerFeedbackPopup(true)
            setFeedbackPopup(false)
            setFeedback("")
        });
    }
    

    return(

        <div className="bg">
        <Navbar setUserEmail={props.setUserEmail} setUserType={props.setUserType} dashboard_link="/trainee_dashboard"/>                     
        <div>
            <h1>ASSESSMENT PAGE</h1>
            {
                ineligible
                ?
                <h2 style={{padding:"2%"}}>It appears that you have not completed all trainings required to give the final assessment. Please complete them before giving the assessment.</h2>
                :
                alreadyCleared
                ?
                <h2 style={{padding:"2%"}}>It appears that you have already cleared the assessment on {previousTime} with a score of {previousScore+"%"}.</h2>
                :
                <div>
                    <h3>Previous Score :  {previousScore!=null&&previousTime?previousScore+"%"+" - "+previousTime:"NA"} </h3>
                    <br/>
                    <div style={{width:"50%", margin:"auto"}}>
                    {
                        assessmentData.map((question) =>
                        {
                            return(
                                <div style={{textAlign:"left"}}>
                                    <Paper elevation={20}>
                                    <FormControl style={{width:"100%"}}>
                                        <p style={{fontSize:"100%",paddingLeft:"20px", paddingTop:"2%", color:"#003C71"}}>Topic : {question.project}</p>
                                        <FormLabel style={{fontSize:"150%",paddingLeft:"20px", color:"#003C71"}} id={"Q"+question.Question_Number}>{"Q"+question.Question_Number+". "+question.Question_Title}</FormLabel>
                                        <RadioGroup
                                          name={"Q"+question.Question_Number+" options"}
                                          style={{paddingLeft:"55px", color:"#003C71" }} 
                                          onChange={(e)=>{question.selectedOption=e.target.value}}
                                        >
                                        {
                                            question.options.map((option)=>
                                            {           
                                                return(                  
                                                    <FormControlLabel value={option.Option_ID} control={<Radio />} label={<Typography style={{fontSize:"110%"}}>{option.Option_Title}</Typography>} />
                                                )
                                            })
                                        }
                                        </RadioGroup>
                                    </FormControl>
                                    </Paper>
                                    <br/>
                                </div>
                            )
                        })
                    }
                    {assessmentData.length>0 ? <button className='btn btn-lg btn-success' onClick={()=>{setPopupMessage("Are you sure you want to submit your answers ?");setPopup(true)}}>Submit</button> : <></> } 
                    <br/>
                    <br/>
                    </div>
                </div>
            }
        </div> 
            
        {/*DEV OPTIONS
        <button className='btn btn-lg btn-success' onClick={()=>{setClear(true);submit()}}>Clear</button>&emsp;
        <button className='btn btn-lg btn-success' onClick={()=>{setFail(true) ;submit()}}>Fail</button>*/}
        <Popup open={popup} closeOnDocumentClick onClose={()=>{setPopup(false)}}>
            <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setPopup(false);}}/>
            <center>
              <br/>
              <p style={{color:"#003C71", fontSize:"130%"}}><center>{popupMessage}</center></p>
              <br/>
              <div className='btn btn-info btn-lg' onClick={submit}>Yes</div> &emsp;
              <div className='btn btn-info btn-lg' onClick={()=>{setPopup(false)}}>No</div>
              <br/>
              <br/>
            </center>
         </Popup>

         <Popup open={innerPopup} closeOnDocumentClick onClose={()=>{setInnerPopup(false);if(assessmentCompleteFlag==true){setFeedbackPopup(true)}}}>
            <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setInnerPopup(false);if(assessmentCompleteFlag==true){setFeedbackPopup(true)}}}/>
            <center>
              <br/>
              <p style={{color:"#003C71", fontSize:"130%"}}><center>{innerPopupMessage}</center></p>
              <br/>
              {
                assessmentCompleteFlag&&score<70
                ?
                <div>
                    <div className='btn btn-info btn-lg' onClick={()=>{setInnerPopup(false);setAssessmentCompleteFlag(false);window.location.reload()}}>Yes</div>  &emsp;                        
                    <div className='btn btn-info btn-lg' onClick={()=>{setInnerPopup(false);setAssessmentCompleteFlag(false);navigate("/trainee_dashboard",{replace:true})}}>Not now</div>                          
                </div>
                :
                <div className='btn btn-info btn-lg' onClick={()=>{setInnerPopup(false);if(assessmentCompleteFlag){setFeedbackPopup(true)}}}>Ok</div>                          
              }
              <br/>
              <br/>
            </center>
         </Popup>

         <Popup open={feedbackPopup} closeOnDocumentClick={false}>
            <center >
              <br/>
              <h3>KT PROCESS FEEDBACK</h3>
              <h3>Please provide us with your valuable feedback on the entire KT Process : </h3>
              <br/> 
              <textarea onChange={(e)=>(setFeedback(e.target.value))} style={{width:"80%", padding:"2%", rows:"20"}} value={feedback} placeholder={"Enter Feedback..."}/>
              <br/>
              <br/>
              <button className='btn btn-success btn-lg' disabled={feedback==""||feedback.trim().length == 0} onClick={feedback==""||feedback.trim().length == 0?null:sendKtFeedback}>Submit</button> &emsp;
              <br/>
              <br/>
            </center>
         </Popup>

         <Popup open={innerFeedbackPopup} closeOnDocumentClick onClose={()=>{setInnerFeedbackPopup(false);navigate("/trainee_dashboard",{replace:true})}}>
         <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setInnerFeedbackPopup(false);navigate("/trainee_dashboard",{replace:true})}}/>
            <center >
              <br/>
              <h3>Feedback Submitted Successfully! Thank you!</h3>
              <h4>Your trainer will take reverse KT as part of next step.</h4>
              <br/>
              <div className='btn btn-info btn-lg' onClick={()=>{setInnerFeedbackPopup(false);navigate("/trainee_dashboard",{replace:true})}}>Ok</div> &emsp;
              <br/>
              <br/>
            </center>
         </Popup>


          {/* LOADING SCREEN */}
          <Popup open={loadingPopup} hideBackdrop closeOnDocumentClick={false} onClose={()=>{setLoadingPopup(false)}}>
              <center>
                <p style={{color:"#003C71", fontSize:"130%", margin:"3%"}}><center>PLEASE WAIT...</center></p>
                <br/>
                <CircularProgress/>
                <br/>
                <br/>
              </center>
         </Popup> 
        </div>
    )
}

export default Assessment_Page