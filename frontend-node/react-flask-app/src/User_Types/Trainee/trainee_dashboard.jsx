import React,{useState,useEffect} from 'react'
import Navbar from '../../Base/navbar.jsx'
import './trainee_dashboard.css'
import axios from 'axios'
import Popup from 'reactjs-popup'
import moment from 'moment'
import { CircularProgress } from '@mui/material'
import CancelIcon from '@mui/icons-material/Cancel'
import Tooltip from '@mui/material/Tooltip'

function Trainee_dashboard(props)
{

    const traineeEmail= props.userEmail
    const [traineeName,setTraineeName] = useState("")
    const [traineeType,setTraineeType] = useState("")
    const [trainerName,setTrainerName] = useState("")
    const [managerName,setManagerName] = useState("")
    const [projectspecificprojectId, setprojectspecificprojectId] = useState("")

    //Loading Screen
    const [loadingPopup,setLoadingPopup] = useState(false)

    //For Popup
    const [popup,setPopup] =  useState(false)
    const [popupMessage,setPopupMessage] = useState("")
    const [signOffCompleteFlag,setSignOffCompleteFlag] = useState(false)

    //For Button Selection
    const [bt,setBt] = useState(1)
    const [project,setProject] = useState(0)
    const [rt,setRt] = useState(0)

    //Basic Training details
    const [rowId,setRowId] = useState([])
    const [moduleNumber,setModuleNumber] = useState([])
    const [moduleName,setModuleName] = useState([])
    const [modulePath,setModulePath] = useState([])
    const [subModuleNumber,setSubModuleNumber] = useState([])
    const [subModuleName,setSubModuleName] = useState([])
    const [subModulePath,setSubModulePath] = useState([])
    const [dueDate,setDueDate] = useState([])
    const [completionDate,setCompletionDate] = useState([])
    const [signedOffFlag,setSignedOffFlag] = useState([])
    const [checkedFlag,setCheckedFlag] = useState([])

    //Project details
    const [projectData,setProjectData] = useState({})
    const [projectRowId,setProjectRowId] = useState([])
    const [projectModuleNumber,setProjectModuleNumber] = useState([])
    const [projectModuleName,setProjectModuleName] = useState([])
    const [projectModulePath,setProjectModulePath] = useState([])
    const [projectSubModuleNumber,setProjectSubModuleNumber] = useState([])
    const [projectSubModuleName,setProjectSubModuleName] = useState([])
    const [projectSubModulePath,setProjectSubModulePath] = useState([])
    const [projectDueDate,setProjectDueDate] = useState([])
    const [projectCompletionDate,setProjectCompletionDate] = useState([])
    const [projectSignedOffFlag,setProjectSignedOffFlag] = useState([])
    const [projectCheckedFlag,setProjectCheckedFlag] = useState([])

    //Re-Assigned Training details
    const [rtRowId,setRtRowId] = useState([])
    const [rtType,setRtType] = useState([]);
    const [rtModuleNumber,setRtModuleNumber] = useState([])
    const [rtModuleName,setRtModuleName] = useState([])
    const [rtModulePath,setRtModulePath] = useState([])
    const [rtSubModuleNumber,setRtSubModuleNumber] = useState([])
    const [rtSubModuleName,setRtSubModuleName] = useState([])
    const [rtSubModulePath,setRtSubModulePath] = useState([])
    const [rtDueDates,setRtDueDate] = useState([])
    const [rtCompletionDates,setRtCompletionDate] = useState([])
    const [rtSignedOffFlag,setRtSignedOffFlag] = useState([])
    const [rtFlag,setRtFlag]=useState(false)
    const [rtCheckedFlag,setRtCheckedFlag] = useState([])

    //Toggle for viewing basic trainings and project details
    const [switchView,setSwitchView]= useState(0)

    //Storing the rowid of the row the user wishes to sign-off
    const [completionId,setCompletionId]= useState("")


    //Loading all necessary details when the webpage is loaded for the first time
    const fetch_details = async () =>
    {
         const traineeDetails = {
            email : traineeEmail,
        }
        const basicparams = new URLSearchParams(traineeDetails);
        const URLWithBasicParams = `http://10.220.20.246:8000/fetchTraineeCommonTrainingProjectId?${basicparams}`;
//         setTraineeType(await axios.get(URLWithBasicParams).then( res => res.data.traineetype))
//         await axios.get(URLWithBasicParams).then( res =>
//         {
//             setTraineeType(res.data.traineetype)
//         })
        const basictrainingprojectid = await axios.get(URLWithBasicParams).then( res => res.data.commonprojectid)
        const traineeType = await axios.get(URLWithBasicParams).then( res => res.data.traineetype)
        const projectspecificprojectId = await axios.get(URLWithBasicParams).then( res => res.data.projectspecificprojectId)
        setTraineeType(traineeType)
        setprojectspecificprojectId(projectspecificprojectId)
        const basicTrainingsId = {
            trainee_email : traineeEmail,
            project_id : basictrainingprojectid,
            traineeType : traineeType,
            projectspecificprojectId : projectspecificprojectId
        }

        const traineeEmailDetails = {
            trainee_email : props.userEmail,
            traineeType : traineeType,
            project_id : projectspecificprojectId,
        }

        const basicTrainingsParams = new URLSearchParams(basicTrainingsId);
        const basicTrainingsURLWithParams = `http://10.220.20.246:8000/fetchProjectDetails?${basicTrainingsParams}`;
        const RtParams = new URLSearchParams(traineeEmailDetails);
        const RtURLWithParams = `http://10.220.20.246:8000/fetchReAssignedTrainings?${RtParams}`;

        const UserNamesParams = new URLSearchParams(traineeEmailDetails);
        const UserNamesURLWithParams = `http://10.220.20.246:8000/fetchMappingNames?${UserNamesParams}`;
        /* BASIC TRAININGS */
        await axios.get(UserNamesURLWithParams).then( res =>
        {

            setTraineeName(res.data.traineeName)
            setTrainerName(res.data.trainerName)
            setManagerName(res.data.managerName)
        })

        /* BASIC TRAININGS */
        await axios.get(basicTrainingsURLWithParams).then( res =>
        {
            const mapping =  res.data.traineeStatus
            const modules = res.data.modules
            const subModules=res.data.subModules
            const length = res.data.traineeStatus.length
            for(var i=0;i<length;i++)
            {
                for(var j=0;j<modules.length;j++)
                {
                    if(mapping[i].Module_ID==modules[j].Module_ID)
                    {
                         rowId[i]=mapping[i].TMSM_Mapping_ID
                         moduleNumber[i]=modules[j].Module_Number;
                         moduleName[i]=modules[j].Module_Name;
                         modulePath[i]=modules[j].Module_Path;
                         dueDate[i]=mapping[i].Due_Date;
//                          if(traineeType=="All Modules")
//                          {dueDate[i]=mapping[i].Due_Date}
//                          else
//                          {dueDate[i]=modules[j].Mandatory==null?"N/A":mapping[i].Due_Date;}
                         completionDate[i]=mapping[i].Completion_Date;
                         signedOffFlag[i]=mapping[i].Signed_Off_Flag;
                         checkedFlag[i]=mapping[i].Checked_Flag
                    }
                }
            }

              for(var j=0;j<subModules.length;j++)
             {
                 if(mapping[i].Sub_Module_ID==subModules[j].Sub_Module_ID)
                 {
                      subModuleNumber[i]=subModules[j].Sub_Module_Number;
                      subModuleName[i]=subModules[j].Sub_Module_Name;
                      subModulePath[i]=subModules[j].Sub_Module_Path;
                 }
                 else if(subModuleNumber[i])
                 {
                     continue;
                 }
                 else
                 {
                      subModuleNumber[i]=null;
                      subModuleName[i]=null;
                      subModulePath[i]=null;
                 }
             }


            //Sorting everything according to module number :
            // Create an array of objects with values from all arrays
            const combinedArray = moduleNumber.map((value, index) => ({
              value: value,
              rowIdValue: rowId[index],
              moduleNameValue: moduleName[index],
              modulePathValue: modulePath[index],
              subModuleNumberValue: subModuleNumber[index],
              subModuleNameValue: subModuleName[index],
              subModulePathValue: subModulePath[index],
              dueDateValue: dueDate[index],
              completionDateValue: completionDate[index],
              signedOffFlagValue: signedOffFlag[index],
              checkedFlagValue:checkedFlag[index]
            }));

            // Sort the array of objects based on the 'value' i.e. module number property
            combinedArray.sort((a, b) => a.value - b.value);
            // Extract the sorted values back to each array
            combinedArray.forEach((item, index) => {
              rowId[index]=item.rowIdValue;
              moduleNumber[index] = item.value;
              moduleName[index] = item.moduleNameValue;
              modulePath[index] = item.modulePathValue;
              subModuleNumber[index] = item.subModuleNumberValue;
              subModuleName[index] = item.subModuleNameValue;
              subModulePath[index] = item.subModulePathValue;
              dueDate[index] = item.dueDateValue;
              completionDate[index] = item.completionDateValue;
              signedOffFlag[index] = item.signedOffFlagValue;
              checkedFlag[index] = item.checkedFlagValue;
            });
        })

        /* PROJECT SPECIFIC TRAININGS */
        const details = {
            email:traineeEmail,
            traineeType : traineeType,
            projectspecificprojectId : projectspecificprojectId
        }
        const params = new URLSearchParams(details);
        const URLWithParams = `http://10.220.20.246:8000/fetchTraineeCurrentProjectId?${params}`;
        await axios.get(URLWithParams).then(async(res)=>
        {

            const assignedProjectId = {
                trainee_email : traineeEmail,
                project_id : res.data.projectId,
                traineeType : traineeType,
                projectspecificprojectId : projectspecificprojectId
            }
            const ProjectParams = new URLSearchParams(assignedProjectId);
            const ProjectURLWithParams = `http://10.220.20.246:8000/fetchProjectDetails?${ProjectParams}`;
             
            await axios.get(ProjectURLWithParams).then( res =>
            {
                setProjectData(res.data.project[0])
                const mapping =  res.data.traineeStatus
                const modules = res.data.modules
                const subModules=res.data.subModules
                const length = res.data.traineeStatus.length

                for(var i=0;i<length;i++)
                {
                    for(var j=0;j<modules.length;j++)
                    {
                        if(mapping[i].Module_ID==modules[j].Module_ID)
                        {
                             projectRowId[i]=mapping[i].TMSM_Mapping_ID
                             projectModuleNumber[i]=modules[j].Module_Number;
                             projectModuleName[i]=modules[j].Module_Name;
                             projectModulePath[i]=modules[j].Module_Path;
                             projectDueDate[i]=mapping[i].Due_Date;
//                              if(traineeType=="All Modules")
//                              {projectDueDate[i]=mapping[i].Due_Date}
//                              else
//                              {projectDueDate[i]=modules[j].Mandatory==null?"N/A":mapping[i].Due_Date}
                             projectCompletionDate[i]=mapping[i].Completion_Date;
                             projectSignedOffFlag[i]=mapping[i].Signed_Off_Flag;
                             projectCheckedFlag[i]=mapping[i].Checked_Flag
                        }
                    }
                    for(var j=0;j<subModules.length;j++)
                    {
                        if(mapping[i].Sub_Module_ID==subModules[j].Sub_Module_ID)
                        {
                            projectSubModuleNumber[i]=subModules[j].Sub_Module_Number;
                            projectSubModuleName[i]=subModules[j].Sub_Module_Name;
                            if(subModules[j].Module_ID==modules[modules.length-1].Module_ID&&subModules[j].Sub_Module_Name=="Assessment")
                            {
                                projectSubModulePath[i]="/assessment_page"
                            }
                            else
                            {
                                projectSubModulePath[i]=subModules[j].Sub_Module_Path;
                            }

                        }
                        else if(projectSubModuleNumber[i])
                        {
                            continue;
                        }
                        else
                        {
                             projectSubModuleNumber[i]=null;
                             projectSubModuleName[i]=null;
                             projectSubModulePath[i]=null;
                        }
                    }
                }
                 //Sorting everything according to module number :
                // Create an array of objects with values from all arrays
                const combinedArray = projectModuleNumber.map((value, index) => ({
                  value: value,
                  projectRowIdValue: projectRowId[index],
                  projectModuleNameValue: projectModuleName[index],
                  projectModulePathValue: projectModulePath[index],
                  projectSubModuleNumberValue: projectSubModuleNumber[index],
                  projectSubModuleNameValue: projectSubModuleName[index],
                  projectSubModulePathValue: projectSubModulePath[index],
                  projectDueDateValue: projectDueDate[index],
                  projectCompletionDateValue: projectCompletionDate[index],
                  projectSignedOffFlagValue: projectSignedOffFlag[index],
                  projectCheckedFlagValue:projectCheckedFlag[index]
                }));
                // Sort the array of objects based on the 'value' i.e. module number property
                combinedArray.sort((a, b) => a.value - b.value);
                // Extract the sorted values back to each array
                combinedArray.forEach((item, index) => {
                  projectRowId[index]=item.projectRowIdValue;
                  projectModuleNumber[index] = item.value;
                  projectModuleName[index] = item.projectModuleNameValue;
                  projectModulePath[index] = item.projectModulePathValue;
                  projectSubModuleNumber[index] = item.projectSubModuleNumberValue;
                  projectSubModuleName[index] = item.projectSubModuleNameValue;
                  projectSubModulePath[index] = item.projectSubModulePathValue;
                  projectDueDate[index] = item.projectDueDateValue;
                  projectCompletionDate[index] = item.projectCompletionDateValue;
                  projectSignedOffFlag[index] = item.projectSignedOffFlagValue;
                  projectCheckedFlag[index] = item.projectCheckedFlagValue;
                });

            })
        })

        /* RE-ASSIGNED TRAININGS */
        await axios.get(RtURLWithParams).then( res =>
        {
            const mapping = res.data.traineeStatus
            const reAssignedTrainings =  res.data.reAssignedTrainings;
            const types =  res.data.types
            const modules = res.data.modules;
            const subModules=res.data.subModules;
            const length = res.data.reAssignedTrainings.length;
            if(res.data.reAssignedTrainings.length > 0)
            {
                setRtFlag(true)
            }
            for(var i=0;i<length;i++)
            {
                rtRowId[i]=reAssignedTrainings[i].RT_ID;
                rtType[i]=types[i];
                rtDueDates[i]=reAssignedTrainings[i].Due_Date;
                rtCompletionDates[i]=reAssignedTrainings[i].Completion_Date;
                rtSignedOffFlag[i]=reAssignedTrainings[i].Signed_Off_Flag;
                rtCheckedFlag[i]=reAssignedTrainings.Checked_Flag;

                for(var j=0;j<modules.length;j++)
                {
                    if(mapping[i].Module_ID==modules[j].Module_ID)
                    {
                        rtModuleNumber[i]=modules[j].Module_Number;
                        rtModuleName[i]=modules[j].Module_Name;
                        rtModulePath[i]=modules[j].Module_Path;
                    }
                }

                for(var j=0;j<subModules.length;j++)
                {
                    if(mapping[i].Sub_Module_ID==subModules[j].Sub_Module_ID)
                    {
                        rtSubModuleNumber[i]=subModules[j].Sub_Module_Number;
                        rtSubModuleName[i]=subModules[j].Sub_Module_Name;
                        if(subModules[j].Module_ID==modules[modules.length-1].Module_ID&&subModules[j].Sub_Module_Name=="Assessment")
                        {
                            rtSubModulePath[i]="/assessment_page"
                        }
                        else
                        {
                            rtSubModulePath[i]=subModules[j].Sub_Module_Path;
                        }
                    }
                    else if(rtSubModuleNumber[i])
                    {
                        continue;
                    }
                    else
                    {
                        rtSubModuleNumber[i]=null;
                        rtSubModuleName[i]=null;
                        rtSubModulePath[i]=null;
                    }
                }
            }

        })

    }
    useEffect(() =>
    {
        fetch_details()
    },[])

    //SignOff Function
    async function signoff(completionId,reverse_training)
    {
        setPopup(false)
        setLoadingPopup(true)
        const details = {
            Mapping_ID:completionId,
            type:reverse_training?"RT":"NRT"
        }
        await axios.post("http://10.220.20.246:8000/signOffComponent",details).then((res)=>
        {
            setLoadingPopup(false)
            setPopup(true)
            setSignOffCompleteFlag(true); 
            setPopupMessage(res.data.response); 
            fetch_details()
        })

    }

    //Determining due date color
    function determine_color(ddate,cdate)
    {
        var color="black";
        const today = new Date(); //Today's Date
        today.setHours(0, 0, 0, 0); // Making the time as 12 AM (for comparison)
        const ddateParts = ddate.split("-");
        const dyear = parseInt(ddateParts[0], 10);
        const dmonth = parseInt(ddateParts[1],10) - 1; // Subtract 1 from the month as it is zero-based
        const dday = parseInt(ddateParts[2], 10);
        const due_date = new Date(dyear, dmonth, dday);
        try
        {
            const cdateParts = cdate.split("-");
            const cyear = parseInt(cdateParts[0], 10);
        	const cmonth = parseInt(cdateParts[1],10) - 1; // Subtract 1 from the month as it is zero-based
        	const cday = parseInt(cdateParts[2], 10);
        	const completion_date = new Date(cyear, cmonth, cday);

        	if(completion_date!="Invalid Date" && (due_date.getTime() < completion_date.getTime()))
            {
                color="#DB4437";
            }
            else if(completion_date=="Invalid Date" && (due_date.getTime() < today.getTime()))
            {
                color="#DB4437"
            }
        }
        catch(err)
        {
            
        }
        return color;
    }

    async function updateFlag(type,index,rowId)
    {
        if(type=="Bt")
        {
            if(checkedFlag[index]=="N")
            {
                const details = {
                    Mapping_ID:rowId,
                    type:"NRT"
                }
                await axios.post("http://10.220.20.246:8000/checkComponent",details).then((res)=>{fetch_details()})
            }
        }
        else if(type=="Pst")
        {
            if(projectCheckedFlag[index]=="N")
            {
                const details = {
                    Mapping_ID:rowId,
                    type:"NRT"
                }
                await axios.post("http://10.220.20.246:8000/checkComponent",details).then((res)=>{fetch_details()})
            }
        }
        else if(type=="Rt")
        {
            if(rtCheckedFlag[index]=="N")
            {
                const details = {
                    Mapping_ID:rowId,
                    type:"RT"
                }
                await axios.post("http://10.220.20.246:8000/checkComponent",details).then((res)=>{fetch_details()})
            }

        }
    }


    


    return(

        <div className="bg">
        <Navbar setUserEmail={props.setUserEmail} setUserType={props.setUserType} dashboard_link="/trainee_dashboard"/>
        {
           traineeEmail!=""

           ?
            <div>
                <h1>TRAINEE DASHBOARD</h1>
                <h3>Welcome {traineeName}!</h3>
                <br/>
                <p>Trainer : {trainerName} &emsp;Manager : {managerName}</p>
                <br/>
                {traineeType=="All Modules"?

                    projectModuleNumber.length+moduleNumber.length-1-projectSignedOffFlag.filter((flag)=>flag=="Y").length-signedOffFlag.filter((flag)=>flag=="Y").length==-1
                    ?

                    <></>
                    
                    :

                    projectModuleNumber.length+moduleNumber.length-1-projectSignedOffFlag.filter((flag)=>flag=="Y").length-signedOffFlag.filter((flag)=>flag=="Y").length==0
                    ?
                    <p>Final Assessment Available! (See the last training in {projectData.Project_Name} Trainings)</p>

                    :

                    <p>Trainings to be completed before Final Assessment : {projectModuleNumber.length+moduleNumber.length-1-projectSignedOffFlag.filter((flag)=>flag=="Y").length-signedOffFlag.filter((flag)=>flag=="Y").length}</p>
                :
                null
                }
                <br/>
                <button className={bt ? "btn btn-primary btn-lg" : "btn btn-info btn-lg"} onClick={()=>{setSwitchView(0); setBt(1); setProject(0); setRt(0);}} > BASIC TRAININGS </button> &emsp;
                <button className={project ? "btn btn-primary btn-lg" : "btn btn-info btn-lg"} onClick={()=>{setSwitchView(1); setBt(0); setProject(1); setRt(0);}} aria-pressed="true"> {projectData.Project_Name} TRAININGS </button>
                {rtFlag==false ? <div></div> : <span>&emsp;<button className={rt ? "btn btn-primary btn-lg" : "btn btn-info btn-lg"} onClick={()=>{setSwitchView(2); setBt(0); setProject(0); setRt(1);}} aria-pressed="true"> RE-ASSIGNED TRAININGS </button></span>}
                <br/>
                <center>
                <table className="table_content">
	            <thead>
	                {
	                 switchView==0||switchView==1

	                 ?

		            <tr style={{width:"100%",lineHeight:"300%",marginLeft:"2%"}}>
		                <th style={{width:"12%",paddingLeft:"2%",scope:"col"}}>MODULE</th>
		                <th style={{width:"27%",scope:"col"}}>CURRICULUM</th>
		                <th style={{width:"17%",scope:"col"}}>DUE DATE</th>
		                <th style={{width:"17%",scope:"col"}}>COMPLETION DATE</th>
		                <th style={{width:"15%",scope:"col"}}>STATUS</th>
		                <th style={{width:"10%",scope:"col"}}></th>
		            </tr>

		            :

		             <tr style={{width:"100%",lineHeight:"300%",marginLeft:"2%"}}>
		                <th style={{width:"15%",paddingLeft:"2%",scope:"col"}}>PROJECT</th>
		                <th style={{width:"10%",scope:"col"}}>MODULE</th>
		                <th style={{width:"20%",scope:"col"}}>CURRICULUM</th>
		                <th style={{width:"15%",scope:"col"}}>DUE DATE</th>
		                <th style={{width:"15%",scope:"col"}}>COMPLETION DATE</th>
		                <th style={{width:"15%",scope:"col"}}>STATUS</th>
		                <th style={{width:"10%",scope:"col"}}></th>
		            </tr>
		        }
	            </thead>
	            <tbody>
	            {
	                switchView==0

	                ?

	                moduleNumber.map( (el,index) =>
	                (

			        <tr>
			            <td style={{paddingLeft:"3%"}}>{subModuleNumber[index] ? el+" ("+subModuleNumber[index]+")" : el}</td>
			            <td>
                        {
                            subModulePath[index] || modulePath[index] 

                            ?

			        	    <a id={"link"+index} className="url" onClick={()=>(updateFlag("Bt",index,rowId[index]))} href={(subModulePath[index] ? subModulePath[index] : modulePath[index])} target="_blank">{subModuleName[index] ? moduleName[index]+" - "+subModuleName[index] : moduleName[index]}</a>
                            
                            :

                            <td title="Please ask your mentor to give you a lab walkthrough and then only sign-off this module">{subModuleName[index] ? moduleName[index]+" - "+subModuleName[index] : moduleName[index]}</td>

                        }
			        	</td>
			            <td style={{color:String(determine_color(dueDate[index],completionDate[index]))}}>
                        {dueDate[index]=="Not Required"?"Not Required":moment(dueDate[index]).format("DD-MM-YYYY")}
			            </td>
			        	<td>
			        	{dueDate[index]=="Not Required"?"Not Required":completionDate[index]==null?completionDate[index]:moment(completionDate[index]).format("DD-MM-YYYY")}
			            </td>
			            <td>
			            {dueDate[index]=="Not Required"?"":<progress style={{borderRadius:"50px"}} max="100" value={signedOffFlag[index]=="Y" ? "100" :  "0"}></progress>}
			            </td>
			            <td>
			            {dueDate[index]=="Not Required"?"":<button className="btn btn-success" disabled={signedOffFlag[index]=="Y" ? true : (subModuleNumber[index-1]&&moduleNumber[index]==moduleNumber[index-1]&&signedOffFlag[index-1]=="N") ? true : subModulePath[index] || modulePath[index] ? checkedFlag[index]=="N" ? true : false : false}  onClick={()=>{setCompletionId(rowId[index]);setPopupMessage("Are you sure you want to sign off on "+(subModuleName[index] ? moduleName[index]+" - "+subModuleName[index] : moduleName[index]) +" ?");setPopup(true);}}>Sign-off</button>}
			            </td>
			        </tr>
			        ))

			        :

			        switchView==1

			        ?

			        projectModuleNumber.map( (el,index) =>
	                (

			        <tr>
                        <td style={{paddingLeft:"3%"}}>{projectSubModuleNumber[index] ? el+" ("+projectSubModuleNumber[index]+")" : el}</td>
			            <td>
                        {
                            projectSubModulePath[index] || projectModulePath[index]

                            ?

                            projectSubModulePath[index]=="/assessment_page"&&projectSignedOffFlag.filter((flag)=>flag=="Y").length+signedOffFlag.filter((flag)=>flag=="Y").length!=projectModuleNumber.length+moduleNumber.length-1

                            ?

                            projectSubModuleName[index] ? projectModuleName[index]+" - "+projectSubModuleName[index] : projectModuleName[index]

                            :

			        	    <a id={"link"+index} className="url" onClick={()=>(updateFlag("Pst",index,projectRowId[index]))} href={projectSubModulePath[index] ? projectSubModulePath[index] : projectModulePath[index]} target="_blank" >{projectSubModuleName[index] ? projectModuleName[index]+" - "+projectSubModuleName[index] : projectModuleName[index]}</a>
                            
                            :

                            projectSubModuleName[index] ? projectModuleName[index]+" - "+projectSubModuleName[index] : projectModuleName[index]

                        }
                        </td>
			            <td style={{color:String(determine_color(projectDueDate[index],projectCompletionDate[index]))}}>
                        {(projectDueDate[index]=="Not Required")?"Not Required":moment(projectDueDate[index]).format("DD-MM-YYYY")}
			            </td>
			        	<td>
			        	{(projectDueDate[index]=="Not Required")?"Not Required":projectCompletionDate[index]==null?projectCompletionDate[index]:moment(projectCompletionDate[index]).format("DD-MM-YYYY")}
			            </td>
			            <td>
			        	{(projectDueDate[index]=="Not Required")?"":<progress style={{borderRadius:"50px"}} max="100" value={projectSignedOffFlag[index]=="Y" ? "100" :  "0"}></progress>}
			            </td>
			            <td>
			            {(projectDueDate[index]=="Not Required")?"":projectSubModulePath[index]=="/assessment_page"?<></>:<button className="btn btn-success" disabled={projectSignedOffFlag[index]=="Y" ? true : (projectSubModuleNumber[index-1]&&projectModuleNumber[index]==projectModuleNumber[index-1]&&projectSignedOffFlag[index-1]=="N") ? true : projectSubModulePath[index] || projectModulePath[index] ? projectCheckedFlag[index]=="N" ? true : false : false} onClick={()=>{setCompletionId(projectRowId[index]);setPopupMessage("Are you sure you want to sign off on "+ (projectSubModuleName[index] ? projectModuleName[index]+" - "+projectSubModuleName[index] : projectModuleName[index]) +" ?");setPopup(true);}}>Sign-off</button>}
			            </td>
			        </tr>
			        ))

			        :

			        rtModuleNumber.map( (el,index) =>
	                (

			        <tr>
			            <td style={{paddingLeft:"3%"}}>{rtType[index]}</td>
			            <td>{rtSubModuleNumber[index] ? el+" ("+rtSubModuleNumber[index]+")" : el}</td>
			            <td>
                        {
                            rtSubModulePath[index] || rtModulePath[index]

                            ?

			        	    <a id={"link"+index} className="url" href={rtSubModulePath[index] ? rtSubModulePath[index] : rtModulePath[index]} target="_blank" >{rtSubModuleName[index] ? rtModuleName[index]+" - "+rtSubModuleName[index] : rtModuleName[index]}</a>
                            
                            :

                            rtSubModuleName[index] ? rtModuleName[index]+" - "+rtSubModuleName[index] : rtModuleName[index]

                        }
			        	</td>
			            <td style={{color:String(determine_color(rtDueDates[index],rtCompletionDates[index]))}}>
			        	{rtDueDates[index]==null?rtDueDates[index]:moment(rtDueDates[index]).format("DD-MM-YYYY")}
			            </td>
			        	<td>
                        {rtCompletionDates[index]==null?rtCompletionDates[index]:moment(rtCompletionDates[index]).format("DD-MM-YYYY")}
			            </td>
			            <td>
			        	<progress style={{borderRadius:"50px"}} max="100" value={rtSignedOffFlag[index]=="Y" ? "100" :  "0"}></progress>
			            </td>
			            <td>
			            <button className="btn btn-success" disabled={rtSignedOffFlag[index]=="Y" ? true : (rtSubModuleNumber[index-1]&&rtModuleNumber[index]==rtModuleNumber[index-1]&&rtSignedOffFlag[index-1]=="N") ? true : rtCheckedFlag[index]=="N" ? true : false} onClick={()=>{setCompletionId(rtRowId[index]);setPopupMessage("Are you sure you want to sign off on "+ (rtSubModuleName[index] ? rtModuleName[index]+" - "+rtSubModuleName[index] : rtModuleName[index]) +" ?");setPopup(true);}}>Sign-off</button>
                        </td>
			        </tr>
			        ))

			     }
			    </tbody>
			    </table>
			    </center>
			</div>

           :

             <div><br/><h1>You cannot access the trainee dashboard without logging in</h1></div>
        }
        <Popup open={popup} closeOnDocumentClick onClose={()=>{setPopup(false)}}>
            <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setPopup(false);}}/>
            <center>
              <br/>
              <p style={{color:"#003C71", fontSize:"130%"}}><center>{popupMessage}</center></p>
              <br/>
              {
              signOffCompleteFlag

              ?

              <div className='btn btn-info btn-lg' onClick={()=>{setPopup(false); setSignOffCompleteFlag(false);}}>Ok</div>

              :
              <div>
              <div className='btn btn-info btn-lg' onClick={()=>{signoff(completionId,rt)}}>Yes</div> &emsp;
              <div className='btn btn-info btn-lg' onClick={()=>{setPopup(false)}}>No</div>
              </div>
              }
              <br/>
              <br/>
            </center>
         </Popup>

          {/* LOADING SCREEN */}
          <Popup open={loadingPopup} hideBackdrop closeOnDocumentClick={false} onClose={()=>{setLoadingPopup(false)}}>
              <center>
                <p style={{color:"#003C71", fontSize:"130%", margin:"3%"}}><center>PLEASE WAIT...</center></p>
                <br/>
                <CircularProgress/>
                <br/>
                <br/>
              </center>
         </Popup> 


        </div>
    )
}

export default Trainee_dashboard