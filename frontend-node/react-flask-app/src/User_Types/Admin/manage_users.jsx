import React,{useState,useEffect} from 'react'
import Navbar from '../../Base/navbar.jsx'
import './manage_users.css'
import axios from 'axios'
import Popup from 'reactjs-popup'
import Paper from '@mui/material/Paper';
import DataTable from 'react-data-table-component'
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import SchoolIcon from '@mui/icons-material/School';
import Switch from '@mui/material/Switch';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import Tooltip from '@mui/material/Tooltip';
import Zoom from '@mui/material/Zoom';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import SupervisorAccountIcon from '@mui/icons-material/SupervisorAccount';
import { useLocation } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { CircularProgress } from '@mui/material';
import CancelIcon from '@mui/icons-material/Cancel';

function Manage_users(props)
{
   
    //To get the trainee ID from the previous page
    const location = useLocation();
    const userType= location.state.userType;

    //Loading Screen
    const [loadingPopup,setLoadingPopup] = useState(false)

    //Table Data Table
    const [tableData,setTableData] = useState([]);
    const [filteredTableData,setFilteredTableData] = useState([]);
    const [emailSearch,setEmailSearch] = useState("");
    const [nameSearch,setNameSearch] = useState("");
    const [key,setKey] = useState(0)

    //Toggle for switch
    const [emailSwitchChecked,setEmailSwitchChecked] = useState(false)
    const [editEmailToggle,setEditEmailToggle] = useState(false);
    const [nameSwitchChecked,setNameSwitchChecked] = useState(false)
    const [editNameToggle,setEditNameToggle] = useState(false);

    //Dashboard Data
    const [totalUserCount,setTotalUserCount] = useState(0)
    const [assignedUserCount,setAssignedUserCount] = useState(0)

    //New User data
    const [newUserEmail,setNewUserEmail]= useState("")

    //Data selected for deletion
    const [selectedData,setSelectedData] = useState([])

    //row height for the table needs to be higher than default to accommodate the row height increase when project/trainer is to be edited
    const customStyles =
    {
        rows:
        {
          style: { minHeight: "10vh", /* override the row height*/ }
        }
    }


    //Searching Table Data
    useEffect(() =>
    {
        const result = tableData.filter( (row) =>
        {
            if(emailSearch=="")
                return row.userName.toLowerCase().match(nameSearch.toLowerCase());
            else if(nameSearch=="")
                return row.userEmail.toLowerCase().match(emailSearch.toLowerCase());
            else
                return (row.userName.toLowerCase().match(nameSearch.toLowerCase())&&row.userEmail.toLowerCase().match(emailSearch.toLowerCase()));
        })
        setFilteredTableData(result);

    },[emailSearch,nameSearch]);

    useEffect(() =>
    {
      try
      {
        for(var i=0;i<tableData.length;i++)
          tableData[i].newUserEmail=null

      }
      catch{}
    },[editEmailToggle])

    useEffect(() =>
    {
      try
      {
        for(var i=0;i<tableData.length;i++)
          tableData[i].newUserName=null
      }
      catch{}
        
    },[editNameToggle])



    //For Popup (Deletion)
    const [popup,setPopup] =  useState(false)
    const [popupMessage,setPopupMessage] = useState("")
    //For Popup (Adding User)
    const [addUserPopup,setAddUserPopup] =  useState(false)
    //For Inner Popup (Displaying request outcome)
    const [innerPopup,setInnerPopup] = useState(false)
    const [innerPopupMessage,setInnerPopupMessage] = useState("")
    //For Popup 2 (Saving Changes)
    const [popup2,setPopup2] =  useState(false)
    const [popupMessage2,setPopupMessage2] = useState("")


    const fetch_details = async () =>
    {  
        
        const userTypeDetails = {
            userType : userType
        }
        const userTypeParams = new URLSearchParams(userTypeDetails);
        const userTypeURLWithParams = `http://10.220.20.246:8000/fetchUserTableDetails?${userTypeParams}`;
        await axios.get(userTypeURLWithParams).then( res=> 
        {
    
            setTableData(res.data.tableData);
            setFilteredTableData(res.data.tableData);
            setTotalUserCount(res.data.totalUserCount);
            setAssignedUserCount(res.data.assignedUserCount);
        })
    }


     useEffect(() =>
    {
        fetch_details()

    },[])

    function handleChange(e,index)
    {
      const value =e.target.value
      const name =e.target.name
      setFilteredTableData(prevData => {
        const newData = [...prevData];
        newData[index] = { ...newData[index], [name]: value };
        return newData;
      });
    }


    //TABLE HEADERS
    var columns =[
      {
        name:"S. No.",
        width:"10%",
        selector: (row) => row.index,
        sortable : true,
        center:true
      },
      {
        name:"EMAIL",
        width:"20%",
        selector: (row) => row.userEmail,
        sortable : true,
        cell: (row,index) =>  <div style={{width:"100%"}}>
                        {
                            editEmailToggle
                            ?
                            <TextField style={{background:"white",color:"white"}} variant="outlined" name="newUserEmail" defaultValue={filteredTableData[index].userEmail} value={filteredTableData[index].newUserEmail} onChange={(e)=>{handleChange(e,index)}}  label="Email" />
                            :
                            <span>{row.userEmail}</span>
                                        
                        }
                        </div>,
        center:true
                        
      },     
      {
        name:"NAME",
        width:"20%",
        selector: (row) => row.userName,
        sortable : true,
        cell: (row,index) =>  <div style={{width:"100%"}}>
                        {
                            editNameToggle
                            ?
                            <TextField style={{background:"white",color:"white"}} variant="outlined" name="newUserName" defaultValue={filteredTableData[index].userName} value={filteredTableData[index].newUserName} onChange={(e)=>{handleChange(e,index)}}  label="Name" />
                            :
                            <span>{row.userName}</span>
                                        
                        }
                        </div>,
        center:true                     
      },
      {
        name:"TOTAL TRAINEES",
        width:userType!="Trainee"?"20%":"0%",
        selector: (row) => row.traineeCount,
        sortable : true,
        center:true
      },
      {
        name:"ACTIVE TRAINEES",
        width:userType!="Trainee"?"20%":"0%",
        selector: (row) => row.activeTraineeCount,
        sortable : true,
        center:true
      }
    ]


    function checkEmails()
    {
      for(var i=0;i<filteredTableData.length;i++)
      {
        if(filteredTableData[i].newUserEmail!=null&&!/^[a-zA-Z0-9]+\.[a-zA-Z0-9]+@bsci.com$/.test(filteredTableData[i].newUserEmail))
        {
          return 0;
        }
      }
      return 1

    }


    async function saveChanges()
    {

      if(editEmailToggle&&editNameToggle&&filteredTableData.filter((row)=>((row.newUserEmail!=null&&row.newUserEmail!=row.userEmail)||(row.newUserName!=null&&row.newUserName!=row.userName))).length==0)
      {
        setPopup2(false)
        setInnerPopupMessage("No changes detected!")
        setInnerPopup(true) 
        
      }
      else if(editEmailToggle&&!editNameToggle&&filteredTableData.filter((row)=>(row.newUserEmail!=null&&row.newUserEmail!=row.userEmail)).length==0)
      {
        setPopup2(false)
        setInnerPopupMessage("No Email changes detected!")
        setInnerPopup(true) 
        
      }
      else if(editNameToggle&&!editEmailToggle&&filteredTableData.filter((row)=>(row.newUserName!=null&&row.newUserName!=row.userName)).length==0)
      {
        setPopup2(false)
        setInnerPopupMessage("No Name changes detected!")
        setInnerPopup(true)       
      }
      else if(!checkEmails())
      {
        setPopup2(false)
        setInnerPopupMessage("Invalid email format detected. Please enter a valid email address (Ex : firstname.lastname@bsci.com)")
        setInnerPopup(true)  
      }
      else
      {
        setPopup2(false)
        setLoadingPopup(true)
        const alteredUsers = filteredTableData.filter((user)=>((user.newUserEmail!=null&&user.newUserEmail!=user.userEmail)||(user.newUserName!=null&&user.newUserName!=user.userName)))
        var userIds=[]
        var newUserEmails=[]
        var newUserNames=[]
        for(var i=0;i<alteredUsers.length;i++)
        {
          userIds.push(alteredUsers[i].userId)
          newUserEmails.push(alteredUsers[i].newUserEmail)
          newUserNames.push(alteredUsers[i].newUserName)
        }
        const details = {
          userIds:userIds,
          newUserEmails:newUserEmails,
          newUserNames:newUserNames
        }
        await axios.post("http://10.220.20.246:8000/ChangeEmailsOrNames",details).then((res) =>
        {
          setLoadingPopup(false)
          setInnerPopupMessage(res.data.response)
          setInnerPopup(true)     
          setEditEmailToggle(false)
          setEmailSwitchChecked(false)
          setNameSwitchChecked(false)
          setEditNameToggle(false)
          fetch_details()

        })
      }
    }

    //Extracting name from email (format : firstName.lastName@bsci.com)
    function nameExtractor(email)
    {
        const result = email.split(".");
        const firstName = result[0][0].toUpperCase() + result[0].slice(1).toLowerCase();
        const result2 = result[1].split("@");
        const lastName = result2[0][0].toUpperCase() + result2[0].slice(1).toLowerCase();
        var fullName = firstName+" "+lastName; 
        fullName=fullName.replace(/[0-9]+/g,"");
        return fullName;
    }

    async function addUser()
    {
    
      if(newUserEmail==="")
      {
          setInnerPopupMessage("No Email Entered!");
          setInnerPopup(true);
          setAddUserPopup(false);
      }
      else if(!/^[a-zA-Z0-9]+\.[a-zA-Z0-9]+@bsci.com$/.test(newUserEmail))
      {
          setInnerPopupMessage("Invalid email format detected. Please enter a valid email address (Ex : firstname.lastname@bsci.com)")
          setInnerPopup(true);
          setAddUserPopup(false);
      }
      else
      {
          setAddUserPopup(false);
          setLoadingPopup(true);
          const newUserName=nameExtractor(newUserEmail);
          const details={
            userEmail:newUserEmail,
            userName:newUserName,
            userType:userType
          }
          await axios.post("http://10.220.20.246:8000/addUser",details).then(res=>
          {
            setLoadingPopup(false);
            setInnerPopupMessage(res.data.message);
            setInnerPopup(true);
            fetch_details();
          })
          
      }
    }

    async function deleteUsers()
    {
    
      if(selectedData.length===0)
      {
          setInnerPopupMessage("No "+userType.toLowerCase()+"s Selected!");
          setInnerPopup(true);
          setPopup(false);
      }
      else
      {
        setPopup(false);
        setLoadingPopup(true)
        var deletionUserIds=[]
        for(var i=0;i<selectedData.length;i++)
        {
          deletionUserIds.push(selectedData[i].userId)
        }
        const details={
          deletionUserIds:deletionUserIds
        }
        await axios.post("http://10.220.20.246:8000/DeleteUsers",details).then((res)=>
            {
                setLoadingPopup(false)
                setInnerPopupMessage(res.data.response);
                setInnerPopup(true);
                setSelectedData([])
                setKey(key+1)
                fetch_details();
            })
          
      }
    }


    return(
        <div className="bg">
        <Navbar type="Admin" setUserEmail={props.setUserEmail} setUserType={props.setUserType} dashboard_link="/admin_dashboard" noAddTrainee={true}/>
        <h1>{userType.toUpperCase()+"S"}</h1>
        <br/>
         <Grid container spacing={0} style={{width:"70%", height:"12%", margin:"auto", marginLeft:"15%",marginRight:"15%"}} >             
              <Grid item sm={4} style={{paddingRight:"2%"}} > 
              {
              userType!="Trainee"
              ? 
              <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>Click to add a {userType}</span>}>                                                      
                <div style={{width:"100%",margin:"auto",height:"85%", cursor:"pointer"}} className="userCount" onClick={()=>{setAddUserPopup(true)}}>
                  <h1 style={{paddingTop:"3%"}}><AddCircleOutlineIcon fontSize="inherit"/> ADD </h1>
                <Paper elevation={20}/>
                </div>
              </Tooltip>
              :
              <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>Click to add a {userType} and initiate his/her KT Trainings</span>}>     
               <Link to="/add_trainee_initiate_kt" state={{type:"Admin"}} style={{ textDecoration: 'none' }}>                                                                                                   
                  <div style={{width:"100%",margin:"auto",height:"85%", cursor:"pointer"}} className="userCount" onClick={()=>{setAddUserPopup(true)}}>
                    <h1 style={{paddingTop:"3%"}}><AddCircleOutlineIcon fontSize="inherit"/> ADD </h1>
                  <Paper elevation={20}/>
                  </div>
                </Link>
              </Tooltip>

              }                                                     
            </Grid>
            {
              userType!="Trainee"
              ?
              <Grid item sm={4} >
                <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>{userType+"s"} that have been assigned some trainee(s)</span>}>                                                                     
                  <div style={{width:"100%", margin:"auto", height:"85%"}} className="userCount">
                    <h1 style={{paddingTop:"3%"}}><SchoolIcon fontSize="inherit"/>ASSIGNED - {assignedUserCount}</h1>
                  <Paper elevation={20}/>
                  </div>
                </Tooltip> 
              </Grid>  
              :
              <></>
            }
              <Grid item sm={userType=="Trainee"?8:4} style={{paddingLeft:"2%"}}>   
                <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>Total number of {userType+"s"}</span>}>                                                      
                  <div style={{width:"100%", margin:"auto", height:"85%"}} className="userCount">
                    <h1 style={{paddingTop:"3%"}}><SupervisorAccountIcon fontSize="inherit"/>TOTAL - {totalUserCount}</h1>
                  <Paper elevation={20}/>
                  </div>
                </Tooltip>                                                           
              </Grid>                                                                                       
        </Grid>
        <br/>
        <br/>
         {/* TABLE */}
         <div style={{width:"70%", margin:"auto", boxShadow: "0px 10px 10px rgba(0, 0, 0, 0.3)"}}>
           <DataTable
             title={" "}
             columns={columns}
             data={filteredTableData}
             key={key} //This  is changed after every re-assignment or delteion to ensure that the table is reset
             paginationRowsPerPageOptions={[5,10,15,25,50,100]}
             fixedHeader
             fixedHeaderScrollHeight="100vh"
             customStyles={customStyles}
             selectableRows
             onSelectedRowsChange={(e) => setSelectedData(e.selectedRows)}
             pagination
             subHeader
             subHeaderComponent =
             {
                <span style={{margin:"2%", width:"100%"}}>
                  <TextField style={{width:"25%"}} label="Search by email" variant="outlined" value={emailSearch} onChange={(e)=>{if(e.target.value[e.target.value.length-1]!="\\"){setEmailSearch(e.target.value)}}}/> &emsp;                
                  <TextField style={{width:"25%"}} label="Search by name" variant="outlined" value={nameSearch} onChange={(e)=>{if(e.target.value[e.target.value.length-1]!="\\"){setNameSearch(e.target.value)}}}/> &emsp;
                  <FormControl component="fieldset" variant="standard">
                    <FormGroup>
                      <span>
                      <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>Edit {userType} Name</span>}>                          
                        <span><FormControlLabel control={<Switch disabled={!tableData.length} checked={emailSwitchChecked} onChange={()=>{setEmailSwitchChecked(!emailSwitchChecked); setEditEmailToggle(!editEmailToggle)}}/>} style={{color:"black"}} label="Change Email" /> &emsp; </span>
                      </Tooltip>
                      <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>You can add a trainer if not found in the list</span>}>                          
                        <span><FormControlLabel control={<Switch disabled={!tableData.length} checked={nameSwitchChecked} onChange={()=>{setNameSwitchChecked(!nameSwitchChecked); setEditNameToggle(!editNameToggle)}}/>} style={{color:"black"}} label="Change Name" /> &emsp; </span>
                       </Tooltip>
                      </span>
                    </FormGroup>
                  </FormControl>                                              
                </span>
             }
             subHeaderAlign="left"
           />
         </div>
         <br/>

         <Popup open={addUserPopup} closeOnDocumentClick onClose={()=>{setAddUserPopup(false);}}>
         <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setAddUserPopup(false);}}/>
            <center >
              <br/>
              <h2>ADD {userType.toUpperCase()}</h2>
              <br/>
              <h3>Enter Official BSCI Email Address</h3>
              <input type="text" onChange={(e)=>(setNewUserEmail(e.target.value))} style={{width:"80%", padding:"2%"}} value={newUserEmail} placeholder={"Enter Email..."}/>
              <br/>
              <br/>
              <div className='btn btn-info btn-lg' onClick={addUser}>Add</div> &emsp;
              <div className='btn btn-info btn-lg' onClick={()=>{setAddUserPopup(false);}}>Cancel</div>
              <br/>         
              <br/>
            </center>
         </Popup>

         <Popup open={popup} closeOnDocumentClick onClose={()=>{setPopup(false)}}>
         <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setPopup(false);}}/>
            <center>
              <br/>
              <p style={{color:"#003C71", fontSize:"130%", margin:"3%"}}><center>{popupMessage}</center></p>
              <br/>
              {           
              <center>
              <div className='btn btn-info btn-lg' onClick={deleteUsers}>Yes</div> &emsp;
              <div className='btn btn-info btn-lg' onClick={()=>{setPopup(false)}}>No</div>
              </center>
              }
              <br/>
              <br/>
            </center>
         </Popup>

         
      

         <Popup open={innerPopup} closeOnDocumentClick onClose={()=>{setInnerPopup(false)}}>
         <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setInnerPopup(false);}}/>
            <center >
              <br/>
              <center><p style={{color:"#003C71", fontSize:"130%", maxHeight:window.innerHeight*0.4, overflow:"auto"}}>{innerPopupMessage}</p></center>
              <br/>
              <div className='btn btn-info btn-lg' onClick={()=>{setInnerPopup(false)}}>Ok</div>
              <br/>
              <br/>
            </center>
         </Popup>

         <Popup open={popup2} closeOnDocumentClick onClose={()=>{setPopup2(false)}}>
            <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setPopup2(false);}}/>
            <center>
              <br/>
              <p style={{color:"#003C71", fontSize:"130%", margin:"3%"}}><center>{popupMessage2}</center></p>
              <br/>
              {           
              <center>
              <div className='btn btn-info btn-lg' onClick={saveChanges}>Yes</div> &emsp;
              <div className='btn btn-info btn-lg' onClick={()=>{setPopup2(false)}}>No</div>
              </center>
              }
              <br/>
              <br/>
            </center>
         </Popup>

          {/* LOADING SCREEN */}
          <Popup open={loadingPopup} hideBackdrop closeOnDocumentClick={false} onClose={()=>{setLoadingPopup(false)}}>
               <center>
                 <p style={{color:"#003C71", fontSize:"130%", margin:"3%"}}><center>PLEASE WAIT...</center></p>
                 <br/>
                 <CircularProgress/>
                 <br/>
                 <br/>
               </center>
          </Popup> 

         <button className="btn btn-success btn-lg" disabled={!editEmailToggle && !editNameToggle} onClick={()=>{setPopup2(true);setPopupMessage2("Are you sure you want to save these changes ?");}}>Save Changes</button> &emsp;
         <button className="btn btn-success btn-lg" disabled={selectedData.length>0?false:true}  onClick={()=>{setPopup(true);setPopupMessage("Are you sure you want to delete "+selectedData.length+" "+userType.toLowerCase()+"(s) ? All associated "+(userType.toLowerCase()=="manager"||userType.toLowerCase()=="trainer"?" trainee(s) and their ":" ")+"KT training(s) will be deleted as well.");}}>Delete</button>
        
        <br/>
        <br/>
        </div>
    )
}

export default Manage_users