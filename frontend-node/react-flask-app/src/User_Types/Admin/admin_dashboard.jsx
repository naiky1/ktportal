import React,{useState,useEffect} from 'react'
import Navbar from '../../Base/navbar.jsx'
import './admin_dashboard.css'
import axios from 'axios'
import Popup from 'reactjs-popup'
import {Link} from 'react-router-dom'
import Paper from '@mui/material/Paper';
import { Line } from 'rc-progress';
import DataTable from 'react-data-table-component'
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import MenuBookIcon from '@mui/icons-material/MenuBook';
import ChildCareIcon from '@mui/icons-material/ChildCare';
import SchoolIcon from '@mui/icons-material/School';
import Switch from '@mui/material/Switch';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import Tooltip from '@mui/material/Tooltip';
import Zoom from '@mui/material/Zoom';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import SupervisorAccountIcon from '@mui/icons-material/SupervisorAccount';
import WorkIcon from '@mui/icons-material/Work';
import { CircularProgress } from '@mui/material';
import CancelIcon from '@mui/icons-material/Cancel';

function Admin_Dashboard(props)
{
    //Dynamic CSS Variables
    const [ktCompletionStyling,setKtCompletionStyling] = useState({
        whitegradient:0,
        greengradient:0,
        hoverbgsize:0
    })

    const adminEmail = props.userEmail
    const [adminName,setAdminName] = useState("")

    //Table Data Table
    const [tableData,setTableData] = useState([]);
    const [filteredTableData,setFilteredTableData] = useState([]);
    const [traineeSearch,setTraineeSearch] = useState("");
    const [trainerSearch,setTrainerSearch] = useState("");
    const [managerSearch,setManagerSearch] = useState("");

    //Toggle for switch
    const [projectSwitchChecked,setProjectSwitchChecked] = useState(false)
    const [editProjectToggle,setEditProjectToggle] = useState(false);
    const [trainerSwitchChecked,setTrainerSwitchChecked] = useState(false)
    const [editTrainerToggle,setEditTrainerToggle] = useState(false);
    const [managerSwitchChecked,setManagerSwitchChecked] = useState(false)
    const [editManagerToggle,setEditManagerToggle] = useState(false);
    const [flipType,setFlipType] = useState("")

    //Loading Screen
    const [loadingPopup,setLoadingPopup] = useState(false)

    //Dashboard Data
    const [managerCount,setManagerCount] = useState(0)
    const [trainerCount,setTrainerCount] = useState(0)
    const [assignedManagerCount,setAssignedManagerCount] = useState(0)
    const [assignedTrainerCount,setAssignedTrainerCount] = useState(0)
    const [ktsCompleteCount,setKtsCompleteCount] = useState(0)
    const [projectCount,setProjectCount] = useState(0)
    const [activeProjectCount,setActiveProjectCount] = useState(0)

    //New Trainer and Project values
    const [newTrainerIds,setNewTrainerIds] = useState([])
    const [newProjectIds,setNewProjectIds] = useState([])
    const [newManagerIds,setNewManagerIds] = useState([])
    const [trainers,setTrainers] = useState([])
    const [projects,setProjects] = useState([])
    const [managers,setManagers] = useState([])

    //row height for the table needs to be higher than default to accommodate the row height increase when project/trainer is to be edited
    const customStyles =
    {
        rows:
        {
          style: { minHeight: "10vh", /* override the row height*/ }
        }
    }


    //Searching Table Data
    useEffect(() =>
    {
        const result = tableData.filter( (row) =>
        {
            if(traineeSearch==""&&trainerSearch==""&&managerSearch=="")
            {
                return row;
            }
            else if(traineeSearch==""&&trainerSearch=="")
            {
                return row.managerName.toLowerCase().match(managerSearch.toLowerCase());
            }
            else if(traineeSearch==""&&managerSearch=="")
            {
                return row.trainerName.toLowerCase().match(trainerSearch.toLowerCase());
            }
            else if(managerSearch==""&&trainerSearch=="")
            {
                return row.traineeName.toLowerCase().match(traineeSearch.toLowerCase());
            }
            if(traineeSearch=="")
            {
                return (row.trainerName.toLowerCase().match(trainerSearch.toLowerCase())&&row.managerName.toLowerCase().match(managerSearch.toLowerCase()));
            }
            else if(trainerSearch=="")
            {
                return (row.traineeName.toLowerCase().match(traineeSearch.toLowerCase())&&row.managerName.toLowerCase().match(managerSearch.toLowerCase()));
            }
            else if(managerSearch=="")
            {
                return (row.trainerName.toLowerCase().match(trainerSearch.toLowerCase())&&row.traineeName.toLowerCase().match(traineeSearch.toLowerCase()));
            }
            else
                return (row.trainerName.toLowerCase().match(trainerSearch.toLowerCase())&&row.traineeName.toLowerCase().match(traineeSearch.toLowerCase())&&row.managerName.toLowerCase().match(managerSearch.toLowerCase()));
        })
        setFilteredTableData(result);

    },[trainerSearch,traineeSearch,managerSearch]);



    //For Popup
    const [popup,setPopup] =  useState(false)
    const [popupMessage,setPopupMessage] = useState([])
    const [innerPopup,setInnerPopup] = useState(false)
    const [innerPopupMessage,setInnerPopupMessage] = useState("")

    //For Popup 2 (Saving Changes)
    const [popup2,setPopup2] =  useState(false)
    const [popupMessage2,setPopupMessage2] = useState("")

    //For Adding Trainer Popup
    const [addTrainerPopup,setAddTrainerPopup] =  useState(false)
    const [newTrainerEmail,setNewTrainerEmail] = useState("")

    //For Adding Manager Popup
    const [addManagerPopup,setAddManagerPopup] =  useState(false)
    const [newManagerEmail,setNewManagerEmail] = useState("")

    var checkList = ['False', 'False'];


    const fetch_details = async () =>
    {  
        
        const adminDetails = {
            userEmail : adminEmail,
            userType : "Admin"
        }
        const adminParams = new URLSearchParams(adminDetails);
        const adminURLWithParams = `http://10.220.20.246:8000/fetchUserName?${adminParams}`;
        await axios.get(adminURLWithParams).then( res=> {setAdminName(res.data.userName)})       

        await axios.get("http://10.220.20.246:8000/fetchAdminTableDetails").then( res=>
        {
            const progress=res.data.progress
            if(progress<50)
            {
                ktCompletionStyling.whitegradient=100-progress;
                ktCompletionStyling.greengradient=progress;
                ktCompletionStyling.hoverbgsize=100;
            }
            else
            {
                ktCompletionStyling.whitegradient=50;
                ktCompletionStyling.greengradient=50;
                ktCompletionStyling.hoverbgsize=2*progress;
            }
            setTableData(res.data.tableData);
            setFilteredTableData(res.data.tableData);
            setTrainerCount(res.data.trainerCount);
            setManagerCount(res.data.managerCount);
            setProjectCount(res.data.projectCount)
            setAssignedTrainerCount(res.data.assignedTrainerCount)
            setAssignedManagerCount(res.data.assignedManagerCount)
            setActiveProjectCount(res.data.activeProjectCount)
            setKtsCompleteCount(res.data.ktsCompleteCount);

        });
        await axios.get("http://10.220.20.246:8000/fetchAllProjects").then(res=>{setProjects(res.data.projects)});
        await axios.get("http://10.220.20.246:8000/fetchAllTrainers").then(res=>{setTrainers(res.data.trainers)});
        await axios.get("http://10.220.20.246:8000/fetchAllManagers").then(res=>{setManagers(res.data.managers)});
    }


     useEffect(() =>
    {
        fetch_details()

    },[])

    useEffect(() =>
    {
      setNewTrainerIds([])

    },[editTrainerToggle])

    useEffect(() =>
    {
      setNewProjectIds([])

    },[editProjectToggle])
    useEffect(() =>
    {
      setNewManagerIds([])

    },[editManagerToggle])

    useEffect(()=>
    {
      if(flipType=="Manager")
      {
        setEditManagerToggle(true);
        setManagerSwitchChecked(true);
        setFlipType("");
      }
      if(flipType=="Trainer")
      {
        setEditTrainerToggle(true);
        setTrainerSwitchChecked(true);
        setFlipType("");
      }
      if(flipType=="Project")
      {
        setEditProjectToggle(true);
        setProjectSwitchChecked(true);
        setFlipType("");
      }

    },[flipType])


    function addNewTrainerClick()
    {
      setAddTrainerPopup(true);
    }
    function addNewManagerClick()
    {
      setAddManagerPopup(true);
    }


    //TABLE HEADERS
    var columns =[
      {
        name:"S. No.",
        width:"5%",
        selector: (row) => row.index,
        sortable : true,
        center:true
      },
      {
        name:"TRAINEE",
        width:"13%",
        selector: (row) => row.traineeName,
        sortable : true,
        cell :  (row) =>
                        <div style={{width:"100%"}}>
                          {row.traineeName+" - "+row.progress+'%'}
                          <Line percent={row.progress} strokeWidth={4} strokeColor={row.status==1?"#0F9D58":"#DB4437"} />
                        </div> ,
        center:true
      },
      {
        name:"TRAINER",
        width:"10%",
        selector: (row) => row.trainerName,
        sortable : true,
        cell: (row) =>  <div style={{width:"100%"}}>
                        {
                            editTrainerToggle
                            ?
                            <FormControl fullWidth>
                            <span>
                              <InputLabel id="Trainer_Select">Change Trainer</InputLabel>
                              <Select                            
                                labelId="Trainer_Select"
                                 value={newTrainerIds[row.index-1]}
                                 style={{width:"100%"}}
                                 label="Change Trainer"
                                 onChange={(e)=>{e.target.value=="add new"?addNewTrainerClick(e):newTrainerIds[row.index-1]=e.target.value}}                               
                                >   
                                <MenuItem value={null}>None</MenuItem>         
                                <MenuItem value="add new"><AddCircleOutlineIcon/>&emsp;Add New Trainer</MenuItem>     
                               {                                                           
                                  trainers.map( (trainer) => 
                                  (                        
                                    <MenuItem value={trainer.User_ID}>{trainer.User_Name}</MenuItem>
                                  ))
                                }
                                
                              </Select>
                              
                            </span>
                              </FormControl>

                            :
                            <span>{row.trainerName}</span>
                                        
                        }
                        </div>,
        center:true
                        
      },
      {
        name:"MANAGER",
        width:"10%",
        selector: (row) => row.managerName,
        sortable : true,
        cell: (row) =>  <div style={{width:"100%"}}>
                        {
                            editManagerToggle
                            ?
                            <FormControl fullWidth>
                            <span>
                              <InputLabel id="Manager_Select">Change Manager</InputLabel>
                              <Select
                                labelId="Manager_Select"
                                 value={newManagerIds[row.index-1]}
                                 style={{width:"100%"}}
                                 label="Change Trainer"
                                 onChange={(e)=>{e.target.value=="add new"?addNewManagerClick(e):newManagerIds[row.index-1]=e.target.value}}
                                >
                                <MenuItem value={null}>None</MenuItem>
                                <MenuItem value="add new"><AddCircleOutlineIcon/>&emsp;Add New Manager</MenuItem>
                               {
                                  managers.map( (manager) =>
                                  (
                                    <MenuItem value={manager.User_ID}>{manager.User_Name}</MenuItem>
                                  ))
                                }

                              </Select>

                            </span>
                              </FormControl>

                            :
                            <span>{row.managerName}</span>

                        }
                        </div>,
        center:true

      },
      {
        name:"PROJECT",
        width:"10%",
        selector: (row) => row.project,
        sortable : true,
        cell: (row) => <div style={{width:"100%"}}>
                        {
                            editProjectToggle
                            ?
                            <FormControl fullWidth>
                            <InputLabel id="Project_Select">Change Project</InputLabel>
                            <Select
                              labelId="Project_Select"
                               value={newProjectIds[row.index-1]}
                               style={{width:"100%"}}
                               label="Change Project"
                               onChange={(e)=>{newProjectIds[row.index-1]=e.target.value}}                               
                              >                   
                             {       
                                projects.map( (project) => 
                                (                        
                                  <MenuItem value={project.Project_ID}>{project.Project_Name}</MenuItem>
                                ))
                              }
                                </Select>
                              </FormControl>

                            :
                            <span>{row.project}</span>

                        }
                        </div>,
        center:true
      },
      {
         name:"TRAINING REQUIREMENT",
         width:"12%",
        selector: (row) => row.trainingRequirementType,
        sortable : true,
        cell: (row) => <div style={{width:"100%"}}><span>{row.trainingRequirementType}</span></div>,
        center:true
      },
      {
         name:"PROGRESS",
         cell: (row) => <Link to="/trainee_progress" className="btn btn-success" state={{traineeId:row.traineeId, traineeEmail:row.traineeEmail, projectId:row.projectId, traineeType:row.trainingRequirementType, type:"Admin"}}>
			    	     Progress Dashboard
			    	    </Link>,
	      center:true
      },
      {
        name:"LEARNING PLAN",
        cell: (row) => <Link to="/trainee_learning_plan" className="btn btn-warning" state={{projectId:row.projectId, traineeEmail:row.traineeEmail, projectId:row.projectId, traineeType:row.trainingRequirementType, type:"Admin"}}>
                Learning Plan
               </Link>,
        center:true
      },
      {
         name:"FEEDBACKS",
         cell: (row, index) =>  <button type="button" className="btn btn-primary" onClick={()=>{setPopupMessage(row.traineeFeedbacks); setPopup(true); }}>
                                  View Feedbacks
                                </button>,
	       center:true
      },


    ]

    async function saveChanges()
    {
      if(editManagerToggle&&newManagerIds.length==0)
      {
        setPopup2(false)
        setInnerPopupMessage("No Manager changes Detected!")
        setInnerPopup(true) 
      }
      else if(editTrainerToggle&&newTrainerIds.length==0)
      {
        setPopup2(false)
        setInnerPopupMessage("No Trainer changes Detected!")
        setInnerPopup(true)       
      }
      else if(editProjectToggle&&newProjectIds.length==0)
      {
        setPopup2(false)
        setInnerPopupMessage("No Project changes Detected!")
        setInnerPopup(true) 
      }
      else
      {
        setLoadingPopup(true)
        setPopup2(false)
        var traineeIds=[]
        var trainerIds=[]
        var managerIds=[]
        for(var i=0;i<tableData.length;i++)
        {
          traineeIds.push(tableData[i].traineeId)
          trainerIds.push(tableData[i].trainerId)
          managerIds.push(tableData[i].managerId)
        }
        const details = {
          newManagerIds:newManagerIds,
          newTrainerIds:newTrainerIds,
          newProjectIds:newProjectIds,
          traineeIds:traineeIds,
          trainerIds:trainerIds,
          managerIds:managerIds    
        }
        await axios.post("http://10.220.20.246:8000/ChangeManagersOrTrainersOrProjects",details).then((res) =>
        {
          setLoadingPopup(false)
          setInnerPopupMessage(res.data.response)
          setInnerPopup(true)     
          setEditProjectToggle(false)
          setProjectSwitchChecked(false)
          setTrainerSwitchChecked(false)
          setEditTrainerToggle(false)
          setManagerSwitchChecked(false)
          setEditManagerToggle(false)
          fetch_details()

        })
      }
    }

    //Extracting name from email (format : firstName.lastName@bsci.com)
    function nameExtractor(email)
    {
        const result = email.split(".");
        const firstName = result[0][0].toUpperCase() + result[0].slice(1).toLowerCase();
        const result2 = result[1].split("@");
        const lastName = result2[0][0].toUpperCase() + result2[0].slice(1).toLowerCase();
        var fullName = firstName+" "+lastName; 
        fullName=fullName.replace(/[0-9]+/g,"");
        return fullName;
    }

    async function addTrainer()
    {
     
      if(newTrainerEmail==="")
      {
          setInnerPopupMessage("No Email Entered!");
          setInnerPopup(true);
          setAddTrainerPopup(false);
      }
      else if(!/^[a-zA-Z0-9]+\.[a-zA-Z0-9]+@bsci.com$/.test(newTrainerEmail))
      {
          setInnerPopupMessage("Invalid email format detected. Please enter a valid email address (Ex : firstname.lastname@bsci.com)")
          setInnerPopup(true);
          setAddTrainerPopup(false);
      }
      else
      {
          setAddTrainerPopup(false);
          setLoadingPopup(true)
          const newTrainerName=nameExtractor(newTrainerEmail);
          const details={
            userEmail:newTrainerEmail,
            userName:newTrainerName,
            userType:"Trainer"
          }
          await axios.post("http://10.220.20.246:8000/addUser",details).then(res=>
          {
            setLoadingPopup(false)
            setInnerPopupMessage(res.data.message);
            setInnerPopup(true);
            fetch_details();
          })
          
      }
      setEditTrainerToggle(false)
      setTrainerSwitchChecked(false)
      setFlipType("Trainer")
    }

    async function addManager()
    {
    
      if(newManagerEmail==="")
      {
          setInnerPopupMessage("No Email Entered!");
          setInnerPopup(true);
          setAddManagerPopup(false);
      }
      else if(!/^[a-zA-Z0-9]+\.[a-zA-Z0-9]+@bsci.com$/.test(newManagerEmail)) 
      {
          setInnerPopupMessage("Invalid email format detected. Please enter a valid email address (Ex : firstname.lastname@bsci.com)")
          setInnerPopup(true);
          setAddManagerPopup(false);
      }
      else
      {
          setAddManagerPopup(false);
          setLoadingPopup(true)
          const newManagerName=nameExtractor(newManagerEmail);
          const details={
            userEmail:newManagerEmail,
            userName:newManagerName,
            userType:"Manager"
          }
          await axios.post("http://10.220.20.246:8000/addUser",details).then(res=>
          {
            setLoadingPopup(false)
            setInnerPopupMessage(res.data.message);
            setInnerPopup(true);

            fetch_details();
          })
          
      }
      setEditManagerToggle(false)
      setManagerSwitchChecked(false)
      setFlipType("Manager")


    }


    return(
        <div className="bg">
        <Navbar type="Admin" setUserEmail={props.setUserEmail} setUserType={props.setUserType} dashboard_link="/admin_dashboard"/>
        <h1>ADMIN DASHBOARD</h1>
        <br/>
        <h3>Welcome {adminName}!</h3>
        <br/>
         <Grid container spacing={0} style={{width:"90%", height:"50%", margin:"auto", marginLeft:"5%",marginRight:"5%"}} >             
              <Grid item sm={4} style={{paddingRight:"2%"}} >                                                
                <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>Click here to add, edit or delete managers</span>}>   
                <Link to="/manage_users" state={{userType:"Manager"}} style={{ textDecoration: 'none' }}>                                                 
                  <div style={{width:"100%", margin:"auto"}} className="userCount">
                    <h1 style={{paddingTop:"3%"}}><SupervisorAccountIcon fontSize="inherit"/> MANAGERS</h1>
                    <h2 style={{paddingBottom:"3%"}}>{managerCount+" ("+assignedManagerCount+" assigned)"}</h2>
                  <Paper elevation={20}/>
                  </div>
                </Link>  
                </Tooltip>
            </Grid>
            <Grid item sm={4} >
              <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>Click here to add, edit or delete trainers</span>}> 
                <Link to="/manage_users" state={{userType:"Trainer"}} style={{ textDecoration: 'none' }}>                                                                     
                  <div style={{width:"100%", margin:"auto"}} className="userCount">
                    <h1 style={{paddingTop:"3%"}}><SchoolIcon fontSize="inherit"/> TRAINERS</h1>
                    <h2 style={{paddingBottom:"3%"}}>{trainerCount+" ("+assignedTrainerCount+" assigned)"}</h2>
                  <Paper elevation={20}/>
                  </div>
                </Link>
              </Tooltip> 
            </Grid>  
            <Grid item sm={4} style={{paddingLeft:"2%"}}>                                                
              <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>Click here to add, edit or delete trainees</span>}>     
                <Link to="/manage_users" state={{userType:"Trainee"}} style={{ textDecoration: 'none' }}>                                                  
                  <div style={{width:"100%", margin:"auto"}} className="userCount">
                    <h1 style={{paddingTop:"3%"}}><ChildCareIcon fontSize="inherit"/> TRAINEES</h1>
                    <h2 style={{paddingBottom:"3%"}}>{tableData.length}</h2>
                  <Paper elevation={20}/>
                  </div>
                </Link>
              </Tooltip>
            </Grid>
                <Grid item sm={4} style={{paddingRight:"2%"}}>
                  {/*<Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>Click here to add, edit or delete projects</span>}>  */}                                                   
                    <div style={{width:"100%", margin:"auto"}} className="userCount">
                      <h1 style={{paddingTop:"3%"}}><WorkIcon fontSize="inherit"/> PROJECTS</h1>
                      <h2 style={{paddingBottom:"3%"}}>{projectCount+" ("+activeProjectCount+" active)"}</h2>
                    <Paper elevation={20}/>
                    </div>
                  {/*</Tooltip>*/} 
                </Grid>
                <Grid item sm={8}>
                <div className="KT_progress" style={{'--ktwhitegradient':`${ktCompletionStyling.whitegradient}%`, '--ktgreengradient':`${ktCompletionStyling.greengradient}%`,'--kthoverbgsize':`${ktCompletionStyling.hoverbgsize}%`}} >
                  <h1 style={{paddingTop:"1.5%"}}><MenuBookIcon fontSize="inherit"/> KT TRAININGS COMPLETED</h1>
                  <h2 style={{paddingBottom:"1.5%"}}>{ktsCompleteCount+"/"+tableData.length}</h2>
                <Paper elevation={20}/>
                </div>
              </Grid>                                                                                                 
        </Grid>
         {/* TABLE */}
         <div style={{width:"90%", margin:"auto", boxShadow: "0px 10px 10px rgba(0, 0, 0, 0.3)"}}>
           <DataTable
             columns={columns}
             data={filteredTableData}
             paginationRowsPerPageOptions={[5,10,15,25,50,100]}
             fixedHeader
             fixedHeaderScrollHeight="100vh"
             customStyles={customStyles}
             pagination
             subHeader
             subHeaderComponent =
             {
                <span style={{margin:"2%", width:"100%"}}>
                  <TextField style={{width:"15%"}} label="Search by manager" variant="outlined" value={managerSearch} onChange={(e)=>{if(e.target.value[e.target.value.length-1]!="\\"){setManagerSearch(e.target.value)}}}/> &emsp;                
                  <TextField style={{width:"15%"}} label="Search by trainer" variant="outlined" value={trainerSearch} onChange={(e)=>{if(e.target.value[e.target.value.length-1]!="\\"){setTrainerSearch(e.target.value)}}}/> &emsp;
                  <TextField style={{width:"15%"}} label="Search by trainee" variant="outlined" value={traineeSearch} onChange={(e)=>{if(e.target.value[e.target.value.length-1]!="\\"){setTraineeSearch(e.target.value)}}}/> &emsp;
                  <FormControl component="fieldset" variant="standard">
                    <FormGroup>
                      <span>
                      <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>You can add a manager if not found in the list</span>}>                          
                        <span><FormControlLabel control={<Switch disabled={!tableData.length} checked={managerSwitchChecked} onChange={()=>{setManagerSwitchChecked(!managerSwitchChecked); setEditManagerToggle(!editManagerToggle)}}/>} style={{color:"black"}} label="Change Manager" /> &emsp; </span>
                      </Tooltip>
                      <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>You can add a trainer if not found in the list</span>}>                          
                        <span><FormControlLabel control={<Switch disabled={!tableData.length} checked={trainerSwitchChecked} onChange={()=>{setTrainerSwitchChecked(!trainerSwitchChecked); setEditTrainerToggle(!editTrainerToggle)}}/>} style={{color:"black"}} label="Change Trainer" /> &emsp; </span>
                      </Tooltip>
                      {/*<Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>You can add a project if not found in the list</span>}>*/}
                        <span><FormControlLabel control={<Switch disabled={!tableData.length} checked={projectSwitchChecked} onChange={()=>{setProjectSwitchChecked(!projectSwitchChecked); setEditProjectToggle(!editProjectToggle)}}/>} style={{color:"black"}} label="Change Project" /> &emsp; </span>
                      {/*</Tooltip>*/}
                      </span>
                    </FormGroup>
                  </FormControl>                                              
                </span>
             }
             subHeaderAlign="left"
           />
         </div>
         <br/>


	    <Popup open={popup} closeOnDocumentClick onClose={()=>{setPopup(false);}}>
            <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setPopup(false)}}/>
            <center >
              <br/>
              <h3>FEEDBACKS</h3>
              <br/>
               <p style={{color:"#003C71", fontSize:"130%", maxHeight:window.innerHeight*0.4, overflow:"auto", textAlign:"left", paddingLeft:"5%",paddingRight:"5%"}}>
              {
                 popupMessage.length==0
                 ?
 
                 <center>No Feedback</center>
 
                 :

                popupMessage.map((el,index) =>
                (

                   <div>
                        {el}
                        <br/>
                   </div>

                ))

              }
              </p>
              <br/>
              <br/>
            </center>
         </Popup>

         <Popup open={addTrainerPopup} closeOnDocumentClick onClose={()=>{setAddTrainerPopup(false);setEditTrainerToggle(false);setTrainerSwitchChecked(false);setFlipType("Trainer");}}>
            <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setAddTrainerPopup(false);setEditTrainerToggle(false);setTrainerSwitchChecked(false);setFlipType("Trainer");}}/>
            <center >
              <br/>
              <h2>ADD TRAINER</h2>
              <br/>
              <h3>Enter Official BSCI Email Address</h3>
              <input type="text" onChange={(e)=>(setNewTrainerEmail(e.target.value))} style={{width:"80%", padding:"2%"}} value={newTrainerEmail} placeholder={"Enter Trainer Email..."}/>
              <br/>
              <br/>
              <div className='btn btn-info btn-lg' onClick={addTrainer}>Add</div> &emsp;
              <div className='btn btn-info btn-lg' onClick={()=>{setAddTrainerPopup(false);setEditTrainerToggle(false);setTrainerSwitchChecked(false);setFlipType("Trainer");}}>Cancel</div>
              <br/>         
              <br/>
            </center>
         </Popup>

         
         <Popup open={addManagerPopup} closeOnDocumentClick onClose={()=>{setAddManagerPopup(false);setEditManagerToggle(false);setManagerSwitchChecked(false);setFlipType("Manager")}}>
            <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setAddManagerPopup(false);setEditManagerToggle(false);setManagerSwitchChecked(false);setFlipType("Manager")}}/>
            <center>
              <br/>
              <h2>ADD MANAGER</h2>
              <br/>
              <h3>Enter Official BSCI Email Address</h3>
              <input type="text" onChange={(e)=>(setNewManagerEmail(e.target.value))} style={{width:"80%", padding:"2%"}} value={newManagerEmail} placeholder={"Enter Manager Email..."}/>
              <br/>
              <br/>
              <div className='btn btn-info btn-lg' onClick={addManager}>Add</div> &emsp;
              <div className='btn btn-info btn-lg' onClick={()=>{setAddManagerPopup(false);setEditManagerToggle(false);setManagerSwitchChecked(false);setFlipType("Manager")}}>Cancel</div>
              <br/>         
              <br/>
            </center>
         </Popup>

         <Popup open={innerPopup} closeOnDocumentClick onClose={()=>{setInnerPopup(false);}}>
         <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setInnerPopup(false);}}/>
            <center >
              <br/>
              <center><p style={{color:"#003C71", fontSize:"130%", maxHeight:window.innerHeight*0.4, overflow:"auto"}}>{innerPopupMessage}</p></center>
              <br/>
              <div className='btn btn-info btn-lg' onClick={()=>{setInnerPopup(false)}}>Ok</div>
              <br/>
              <br/>
            </center>
         </Popup>

         <Popup open={popup2} closeOnDocumentClick onClose={()=>{setPopup2(false)}}>
         <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setPopup2(false);}}/>
            <center>
              <br/>
              <p style={{color:"#003C71", fontSize:"130%", margin:"3%"}}><center>{popupMessage2}</center></p>
              <br/>
              {           
              <center>
              <div className='btn btn-info btn-lg' onClick={saveChanges}>Yes</div> &emsp;
              <div className='btn btn-info btn-lg' onClick={()=>{setPopup2(false)}}>No</div>
              </center>
              }
              <br/>
              <br/>
            </center>
         </Popup>

           {/* LOADING SCREEN */}
           <Popup open={loadingPopup} hideBackdrop closeOnDocumentClick={false} onClose={()=>{setLoadingPopup(false)}}>
               <center>
                 <p style={{color:"#003C71", fontSize:"130%", margin:"3%"}}><center>PLEASE WAIT...</center></p>
                 <br/>
                 <CircularProgress/>
                 <br/>
                 <br/>
               </center>
            </Popup> 

         <button className="btn btn-success btn-lg" disabled={!editProjectToggle && !editTrainerToggle && !editManagerToggle} onClick={()=>{setPopup2(true);setPopupMessage2("Are you sure you want to save these changes ?");}}>Save Changes</button>
        <br/>
        <br/>
        </div>
    )
}

export default Admin_Dashboard