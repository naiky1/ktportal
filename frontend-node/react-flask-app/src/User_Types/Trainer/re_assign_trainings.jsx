import React,{useState,useEffect} from 'react'
import Navbar from '../../Base/navbar.jsx'
import './re_assign_trainings.css'
import axios from 'axios'
import Popup from 'reactjs-popup'
import Switch from '@mui/material/Switch';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import dayjs, { Dayjs } from 'dayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import TextField from '@mui/material/TextField';
import {useNavigate, useLocation} from 'react-router-dom'
import SwipeableViews from 'react-swipeable-views';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Grid from '@mui/material/Grid'
import DataTable from 'react-data-table-component'
import moment from 'moment'
import Tooltip from '@mui/material/Tooltip';
import Zoom from '@mui/material/Zoom';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import { CircularProgress } from '@mui/material'
import CancelIcon from '@mui/icons-material/Cancel';

function Re_assign_trainings(props)
{

    let navigate = useNavigate();
    //To get the trainee ID from the previous page
    const location = useLocation();
    const traineeEmail= location.state.traineeEmail;
    const projectId= location.state.projectId;
    const traineeType= location.state.traineeType;

    const [trainee,setTrainee] = useState({});

    //Loading Screen
    const [loadingPopup,setLoadingPopup] = useState(false)

    //For tables
    const [btSearch,setBtSearch] = useState("");
    const [btTrainings,setBtTrainings] = useState([]);
    const [filteredBtTrainings,setFilteredBtTrainings] =useState([]);
    const [selectedBtTrainings,setSelectedBtTrainings] =useState([]);
    const [pstTrainings,setPstTrainings] = useState([]);
    const [pstSearch,setPstSearch] = useState("");
    const [filteredPstTrainings,setFilteredPstTrainings] =useState([]);
    const [selectedPstTrainings,setSelectedPstTrainings] =useState([]);
    const [projectName,setProjectName]= useState([]);
    const [rtSearch,setRtSearch] = useState("");
    const [rtTrainings,setRtTrainings] = useState([]);
    const [filteredRtTrainings,setFilteredRtTrainings] =useState([]);
    const [selectedRtTrainings,setSelectedRtTrainings] =useState([]);


    //Referencing the tables for de-selection of rows
    const [pstKey,setPstKey] = useState(0)
    const [btKey,setBtKey] = useState(0)
    

    //Toggle for viewing basic trainings, project details or re-assigned trainings
    const [view,setView]= useState(0);

    //row height for re-assigned trainings table needs to be higher than default to accommodate the row height increase when due date is to be edited
    const customStyles =
    {
        rows:
        {
          style: { minHeight: "10vh", /* override the row height*/ }
        }
    }

    //to display total trainings selected
    const [count,setCount] = useState(0)

    //automatic height adjustment should happen only once all data is loaded so it is false by default.
    const [animateHeightToggle,setAnimateHeightToggle] = useState(false)

    //Toggle for switch
    const [switchChecked,setSwitchChecked] = useState(false)

    //For Popup
    const [popup,setPopup] =  useState(false)
    const [popupMessage,setPopupMessage] = useState("")
    const [reAssignmentCompleteFlag,setReAssignmentCompleteFlag] = useState(false)
    const [trainingsSelectedCount,setTrainingsSelectedCount] = useState(0)
    const [saveChangesFlag,setSaveChangesFlag] = useState(false)

    //For Popup 2 (Deletion of Re-Assigned Trainings)
    const [popup2,setPopup2] =  useState(false)
    const [popupMessage2,setPopupMessage2] = useState("")
    const [deletionFlag,setDeletionFlag] = useState(false)

    //Due Date
    const [commonDueDate,setCommonDueDate] = useState(null);
    const [editAssignedDueDateToggle,setEditAssignedDueDateToggle] = useState(false);

    //Basic Training details
    const [rowId,setRowId] = useState([]);
    const [moduleNumber,setModuleNumber] = useState([]);
    const [moduleName,setModuleName] = useState([]);
    const [modulePath,setModulePath] = useState([]);
    const [subModuleNumber,setSubModuleNumber] = useState([]);
    const [subModuleName,setSubModuleName] = useState([]);
    const [subModulePath,setSubModulePath] = useState([]);
    const [expandedTrainings,setExpandedTrainings] = useState([]);

    //Project details
    const [projectData,setProjectData] = useState({});
    const [projectRowId,setProjectRowId] = useState([]);
    const [projectModuleNumber,setProjectModuleNumber] = useState([]);
    const [projectModuleName,setProjectModuleName] = useState([]);
    const [projectModulePath,setProjectModulePath] = useState([]);
    const [projectSubModuleNumber,setProjectSubModuleNumber] = useState([]);
    const [projectSubModuleName,setProjectSubModuleName] = useState([]);
    const [projectSubModulePath,setProjectSubModulePath] = useState([]);
    const [projectExpandedTrainings,setProjectExpandedTrainings] = useState([]);

    //Re-Assigned Trainings
    const [rtRowId,setRtRowId] = useState([]);
    const [rtType,setRtType] = useState([]);
    const [rtModuleNumber,setRtModuleNumber] = useState([]);
    const [rtModuleName,setRtModuleName] = useState([]);
    const [rtModulePath,setRtModulePath] = useState([]);
    const [rtSubModuleNumber,setRtSubModuleNumber] = useState([]);
    const [rtSubModuleName,setRtSubModuleName] = useState([]);
    const [rtSubModulePath,setRtSubModulePath] = useState([]);
    const [rtDueDates,setRtDueDates] = useState([]);
    const [rtCompletionDates,setRtCompletionDates] = useState([]);


    //Searching
    useEffect(() =>
    {
        const result = btTrainings.filter( (task) =>
        {
            return task.trainingName.toLowerCase().match(btSearch.toLowerCase());
        })
        setFilteredBtTrainings(result);

    },[btSearch]);
    useEffect(() =>
    {
        const result = pstTrainings.filter( (task) =>
        {
            return task.trainingName.toLowerCase().match(pstSearch.toLowerCase());
        })
        setFilteredPstTrainings(result);

    },[pstSearch]);
    useEffect(() =>
    {
        const result = rtTrainings.filter( (task) =>
        {
            return task.trainingName.toLowerCase().match(rtSearch.toLowerCase());
        })
        setFilteredRtTrainings(result);

    },[rtSearch]);



    //Loading all necessary details
    const fetch_details = async () =>
    {
        setAnimateHeightToggle(false)

        const traineeEmailId = {
            email : traineeEmail
        }
        const basicparams = new URLSearchParams(traineeEmailId);
        const URLWithBasicParams = `http://10.220.20.246:8000/fetchTraineeCommonTrainingProjectId?${basicparams}`;
        const basictrainingprojectid = await axios.get(URLWithBasicParams).then( res => res.data.commonprojectid)

        const basicTrainingsDetails = {
            trainee_email : traineeEmail,
            project_id : basictrainingprojectid,
            traineeType: traineeType,
            projectspecificprojectId : projectId
        }
        const projectTrainingsDetails = {
            trainee_email : traineeEmail,
            project_id : projectId,
            traineeType: traineeType,
        }
        const reassignedTrainingsDetails = {
            trainee_email : traineeEmail,
            project_id : projectId,
            traineeType: traineeType,
        }
        const basicTrainingsParams = new URLSearchParams(basicTrainingsDetails);
        const basicTrainingsURLWithParams = `http://10.220.20.246:8000/fetchProjectDetails?${basicTrainingsParams}`;
        const ProjectParams = new URLSearchParams(projectTrainingsDetails);
        const ProjectURLWithParams = `http://10.220.20.246:8000/fetchProjectDetails?${ProjectParams}`;
        const RtParams = new URLSearchParams(reassignedTrainingsDetails);
        const RtURLWithParams = `http://10.220.20.246:8000/fetchReAssignedTrainings?${RtParams}`;

        /* BASIC TRAININGS */
        await axios.get(basicTrainingsURLWithParams).then( res =>
        {
            const mapping =  res.data.traineeStatus;
            const modules = res.data.modules;
            const subModules=res.data.subModules;
            const length = res.data.traineeStatus.length;
            var btDetails= [];
            var expandedTrainingsDetails =  [];

            const ReAssignedTrainings = res.data.reAssignedTrainings;
            var ReAssignedTrainingIds = []
            for (var i=0;i<ReAssignedTrainings.length;i++)
            {
                ReAssignedTrainingIds.push(ReAssignedTrainings[i].TMSM_Mapping_ID)
            }
            for(var i=0;i<length;i++)
            {
                for(var j=0;j<modules.length;j++)
                {
                    if(mapping[i].Module_ID==modules[j].Module_ID)
                    {
                         rowId[i]=mapping[i].TMSM_Mapping_ID;
                         moduleNumber[i]=modules[j].Module_Number;
                         moduleName[i]=modules[j].Module_Name;
                         modulePath[i]=modules[j].Module_Path;
                    }
                }
                for(var j=0;j<subModules.length;j++)
                {
                    if(mapping[i].Sub_Module_ID==subModules[j].Sub_Module_ID)
                    {
                         subModuleNumber[i]=subModules[j].Sub_Module_Number;
                         subModuleName[i]=subModules[j].Sub_Module_Name;
                         subModulePath[i]=subModules[j].Sub_Module_Path;
                    }
                    else if(subModuleNumber[i])
                    {
                        continue;
                    }
                    else
                    {
                         subModuleNumber[i]=null;
                         subModuleName[i]=null;
                         subModulePath[i]=null;
                    }
                }
            }

            //Sorting everything according to module number :
            // Create an array of objects with values from all arrays
            const combinedArray = moduleNumber.map((value, index) => ({
              value: value,
              rowIdValue: rowId[index],
              moduleNameValue: moduleName[index],
              modulePathValue: modulePath[index],
              subModuleNumberValue: subModuleNumber[index],
              subModuleNameValue: subModuleName[index],
              subModulePathValue: subModulePath[index]
            }));
            // Sort the array of objects based on the 'value' i.e. module number property
            combinedArray.sort((a, b) => a.value - b.value);
            // Extract the sorted values back to each array
            combinedArray.forEach((item, index) => {
              rowId[index]=item.rowIdValue;
              moduleNumber[index] = item.value;
              moduleName[index] = item.moduleNameValue;
              modulePath[index] = item.modulePathValue;
              subModuleNumber[index] = item.subModuleNumberValue;
              subModuleName[index] = item.subModuleNameValue;
              subModulePath[index] = item.subModulePathValue;
            });
            for(var i=0;i<length;i++)
            {

                if(btDetails.filter((training)=>(training.trainingModule==moduleNumber[i])).length==0)
                {
                    var training = {index:i,trainingModule:moduleNumber[i],trainingName:moduleName[i],trainingId:rowId[i],selectionDisabled:(subModuleNumber[i]||ReAssignedTrainingIds.includes(rowId[i]))?true:false,expansionDisabled:subModuleNumber[i]?false:true,type:"BT",expanded:true}
                    btDetails.push(training);
                }

                if(subModuleNumber[i])
                {
                    var expandedTraining = {index:i,trainingModuleNumber:moduleNumber[i],trainingModule:subModuleNumber[i],trainingName:subModuleName[i],trainingId:rowId[i],selectionDisabled:ReAssignedTrainingIds.includes(rowId[i])?true:false,selected:false,type:"BT"}
                }
                else
                {
                    var expandedTraining = null
                }
                expandedTrainingsDetails.push(expandedTraining);

            }
            setExpandedTrainings(expandedTrainingsDetails.filter((training)=>(training!=null)));
            setBtTrainings(btDetails);
            setFilteredBtTrainings(btDetails);

        })

        /* PROJECT SPECIFIC TRAININGS */
        await axios.get(ProjectURLWithParams).then( res =>
        {
            setProjectData(res.data.project[0]);
            const mapping =  res.data.traineeStatus;
            const modules = res.data.modules;
            const subModules=res.data.subModules;
            const length = res.data.traineeStatus.length;
            const ReAssignedTrainings = res.data.reAssignedTrainings;

            var expandedTrainingsDetails =  [];
            var pstDetails= [];
            var ReAssignedTrainingIds = []
            for (var i=0;i<ReAssignedTrainings.length;i++)
            {
                ReAssignedTrainingIds.push(ReAssignedTrainings[i].TMSM_Mapping_ID)
            }
            for(var i=0;i<length;i++)
            {
          
                for(var j=0;j<modules.length;j++)
                {
               
                    if(mapping[i].Module_ID==modules[j].Module_ID)
                    {
                        
                         projectRowId[i]=mapping[i].TMSM_Mapping_ID;
                         projectModuleNumber[i]=modules[j].Module_Number;
                         projectModuleName[i]=modules[j].Module_Name;
                         projectModulePath[i]=modules[j].Module_Path;
                    }
                }
                for(var j=0;j<subModules.length;j++)
                {
                    if(mapping[i].Sub_Module_ID==subModules[j].Sub_Module_ID)
                    {
                         projectSubModuleNumber[i]=subModules[j].Sub_Module_Number;
                         projectSubModuleName[i]=subModules[j].Sub_Module_Name;
                         projectSubModulePath[i]=subModules[j].Sub_Module_Path;
                    }
                    else if(projectSubModuleNumber[i])
                    {
                        continue;
                    }
                    else
                    {
                         projectSubModuleNumber[i]=null;
                         projectSubModuleName[i]=null;
                         projectSubModulePath[i]=null;
                    }
                }
            }

            //Sorting everything according to module number :
            // Create an array of objects with values from all arrays
            const combinedArray = projectModuleNumber.map((value, index) => ({
              value: value,
              projectRowIdValue: projectRowId[index],
              projectModuleNameValue: projectModuleName[index],
              projectModulePathValue: projectModulePath[index],
              projectSubModuleNumberValue: projectSubModuleNumber[index],
              projectSubModuleNameValue: projectSubModuleName[index],
              projectSubModulePathValue: projectSubModulePath[index]
            }));
            // Sort the array of objects based on the 'value' i.e. module number property
            combinedArray.sort((a, b) => a.value - b.value);
            // Extract the sorted values back to each array
            combinedArray.forEach((item, index) => {
              projectRowId[index]=item.projectRowIdValue;
              projectModuleNumber[index] = item.value;
              projectModuleName[index] = item.projectModuleNameValue;
              projectModulePath[index] = item.projectModulePathValue;
              projectSubModuleNumber[index] = item.projectSubModuleNumberValue;
              projectSubModuleName[index] = item.projectSubModuleNameValue;
              projectSubModulePath[index] = item.projectSubModulePathValue;
            });

            for(var i=0;i<length;i++)
            {
                if(pstDetails.filter((training)=>(training.trainingModule==projectModuleNumber[i])).length==0)
                {
                    var training = {index:i,trainingModule:projectModuleNumber[i],trainingName:projectModuleName[i],trainingId:projectRowId[i],selectionDisabled:(projectSubModuleNumber[i]||ReAssignedTrainingIds.includes(projectRowId[i]))?true:false,expansionDisabled:projectSubModuleNumber[i]?false:true,type:"PST",expanded:true}
                    pstDetails.push(training);
                }


                if(projectSubModuleNumber[i])
                {
                    var expandedTraining = {index:i,trainingModuleNumber:projectModuleNumber[i],trainingModule:projectSubModuleNumber[i],trainingName:projectSubModuleName[i],trainingId:projectRowId[i],selectionDisabled:(ReAssignedTrainingIds.includes(projectRowId[i])||(i==length-1&&projectSubModuleName[i]=="Assessment"))?true:false,selected:false,type:"PST"}
                }
                else
                {
                    var expandedTraining = null
                }
                expandedTrainingsDetails.push(expandedTraining);

            }
            setProjectExpandedTrainings(expandedTrainingsDetails.filter((training)=>(training!=null)));
            setPstTrainings(pstDetails);
            setFilteredPstTrainings(pstDetails);

        })

        /* RE-ASSIGNED TRAININGS */
        await axios.get(RtURLWithParams).then( res =>
        {
            const mapping = res.data.traineeStatus
            const reAssignedTrainings =  res.data.reAssignedTrainings;
            const types =  res.data.types
            const modules = res.data.modules;
            const subModules=res.data.subModules;
            const length = res.data.reAssignedTrainings.length;
            var rtDetails= [];
            for(var i=0;i<length;i++)
            {
                rtRowId[i]=reAssignedTrainings[i].RT_ID;
                rtType[i]=types[i];
                rtDueDates[i]=reAssignedTrainings[i].Due_Date;
                rtCompletionDates[i]=reAssignedTrainings[i].Completion_Date;

                for(var j=0;j<modules.length;j++)
                {
                    if(mapping[i].Module_ID==modules[j].Module_ID)
                    {
                         rtModuleNumber[i]=modules[j].Module_Number;
                         rtModuleName[i]=modules[j].Module_Name;
                         rtModulePath[i]=modules[j].Module_Path;
                    }
                }

                for(var j=0;j<subModules.length;j++)
                {
                    if(mapping[i].Sub_Module_ID==subModules[j].Sub_Module_ID)
                    {
                         rtSubModuleNumber[i]=subModules[j].Sub_Module_Number;
                         rtSubModuleName[i]=subModules[j].Sub_Module_Name;
                         rtSubModulePath[i]=subModules[j].Sub_Module_Path;
                    }
                    else if(rtSubModuleNumber[i])
                    {
                        continue;
                    }
                    else
                    {
                         rtSubModuleNumber[i]=null;
                         rtSubModuleName[i]=null;
                         rtSubModulePath[i]=null;
                    }
                }
            }

            for(var i=0;i<length;i++)
            {
                var training = {index:i,trainingType:rtType[i],trainingModule:rtSubModuleNumber[i]?rtModuleNumber[i]+" ("+rtSubModuleNumber[i]+")":rtModuleNumber[i],trainingSubModule:rtSubModuleNumber[i],trainingName:rtSubModuleNumber[i]?rtModuleName[i]+" - "+rtSubModuleName[i]:rtModuleName[i],trainingId:rtRowId[i],trainingDueDate:rtDueDates[i],trainingDueDateNew:null,trainingCompletionDate:rtCompletionDates[i],type:"RT"}
                rtDetails.push(training);
            }
            setRtTrainings(rtDetails);
            setFilteredRtTrainings(rtDetails);

        })
        setAnimateHeightToggle(true)
    }

    useEffect(() =>
    {
        fetch_details()
    },[])

     useEffect(() =>
    {
        setCount(selectedBtTrainings.length+selectedPstTrainings.length+projectExpandedTrainings.filter((training)=>(training.selected==true)).length+expandedTrainings.filter((training)=>(training.selected==true)).length)
    },[selectedBtTrainings.length,selectedPstTrainings.length])





    function handleChange(e,type)
    {

        if(String(e)!="Invalid Date")
        {
            setCommonDueDate(e);
        }
        else
        {
            setCommonDueDate(null);
       }
    }

    //TABLE HEADERS (Basic and Project Specific Trainings)
    var columns =[
      {
        name:"MODULE",
        width:"15%",
        selector: (row) => row.trainingModule,
        sortable : true
      },
      {
        name:"CURRICULUM",
        selector: (row) => row.trainingName,
        sortable : true
      }
    ]

    //TABLE HEADERS (Re-Assigned Trainings)
    var rtColumns =[
      {
        name:"PROJECT",
        width:"12%",
        selector: (row) => row.trainingType,
        sortable : true
      },
      {
        name:"MODULE",
        width:"10%",
        selector: (row) => row.trainingModule,
        sortable : true
      },
      {
        name:"CURRICULUM",
        selector: (row) => row.trainingName,
        sortable : true
      },
      {
        name:"DUE DATE",
        width:"25%",
        selector: (row) =>row.trainingDueDate,
        cell: (row) => <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>Format : DD-MM-YYYY</span>}>
                            {
                                editAssignedDueDateToggle
                                ?
                                <LocalizationProvider dateAdapter={AdapterDayjs} >
                                <DemoContainer components={['DatePicker']} >
                                  <DatePicker format="DD-MM-YYYY" disablePast label="Due Date" value={row.trainingDueDateNew?dayjs(moment(row.trainingDueDateNew,"YYYY-MM-DD").format("MM-DD-YYYY")):null}
                                              onChange={(e)=>
                                              {
                                                if(String(e)!="Invalid Date")
                                                {
                                                    const new_date = new Date(e)
                                                    row.trainingDueDateNew=moment(new_date).format("YYYY-MM-DD")
                                                }
                                                else
                                                {
                                                    row.trainingDueDateNew=null
                                                }

                                              }}/>
                                </DemoContainer>
                                </LocalizationProvider>
                                :
                                <span>{moment(row.trainingDueDate,"YYYY-MM-DD").format("DD-MM-YYYY")}</span>

                             }
                       </Tooltip>,
        sortable : true
      },
      {
        name:"COMPLETION DATE",
        width:"16%",
        selector: (row) => row.trainingCompletionDate,
        cell: (row) => <Tooltip TransitionComponent={Zoom} title={<span style={{fontSize:"150%"}}>Format : DD-MM-YYYY</span>}>
                        <span>{row.trainingCompletionDate==null?row.trainingCompletionDate:moment(row.trainingCompletionDate,"YYYY-MM-DD").format("DD-MM-YYYY")}</span>
                       </Tooltip>,
        sortable : true
      }
    ]


    var projectCount=[]
    var maxProjectCount=[]
    var btCount=[]
    var maxBtCount=[]
    //Expandable content
    function ExpandedComponent(rowData)
    {
        setAnimateHeightToggle(false)
        const expandableRow=rowData.data
        const [expanded,setExpanded] = useState(true)
        //Expandable rows' headers
        var innerColumns =[
          {
            name:"SUB-MODULE",
            width:"15%",
            selector: (row) => row.trainingModule,
            sortable : true
          },
          {
            name:"CURRICULUM",
            selector: (row) => row.trainingName,
            sortable : true
          }
        ]
        var expandableRowData = [];

        if(expandableRow.type=="BT")
        {
            expandableRowData = expandedTrainings.filter((training)=>(training.trainingModuleNumber==expandableRow.trainingModule))
            btCount[expandableRow.index]=0;
            maxBtCount[expandableRow.index]=expandableRowData.length
        }
        else if(expandableRow.type=="PST")
        {

            expandableRowData = projectExpandedTrainings.filter((training)=>(training.trainingModuleNumber==expandableRow.trainingModule))
            projectCount[expandableRow.index]=0;
            maxProjectCount[expandableRow.index]=expandableRowData.length

        }


         //Storing selection status of rows
         function selectRows(selectedRowsData,firstRowId,type,moduleNum,index)
         {

             const selectedRows = selectedRowsData.selectedRows

             if(type=="PST")
             {
                //Only the sub-modules associated with the selected module should be considered for selection/deselection
                const projectExpandedTrainingsFiltered=projectExpandedTrainings.filter((training)=>(training.trainingModuleNumber==moduleNum))
                projectCount[index]=projectExpandedTrainingsFiltered.filter((training)=>(training.selected==true)).length

                 /*
                Every time an expandable row is opened to view sub-modules, selectedCount=0 is sent. This can reset selection status of all rows
                unintentionally as the user simply expanding or collapsing a row should not impact the selection status. So to counter this, the
                selection status of the row is stored as a property of the row itself. Furthermore, for the scenario of deselecting one row or
                all simultaneously, a count of all selected rows in each expandable and their max value had to be stored as an array.
                */

                if(selectedRowsData.selectedCount>0||projectCount[index]==1||projectCount[index]==maxProjectCount[index])
                {
                  setExpanded(false)
                }

                if(selectedRowsData.selectedCount>0)
                {
                       for(var i=0;i<projectExpandedTrainings.length;i++)
                        {
                            if(projectExpandedTrainingsFiltered.includes(projectExpandedTrainings[i]))
                            {
                                var result = selectedRows.includes(projectExpandedTrainings[i])
                                if (result == true)
                                {
                                    projectExpandedTrainings[i].selected=true
                                    setCount(selectedBtTrainings.length+selectedPstTrainings.length+projectExpandedTrainings.filter((training)=>(training.selected==true)).length+expandedTrainings.filter((training)=>(training.selected==true)).length)

                                }
                                else
                                {
                                    projectExpandedTrainings[i].selected=false;
                                    setCount(selectedBtTrainings.length+selectedPstTrainings.length+projectExpandedTrainings.filter((training)=>(training.selected==true)).length+expandedTrainings.filter((training)=>(training.selected==true)).length)

                                }

                            }
                        }
                }
                else if(selectedRowsData.selectedCount==0&&expanded==false)
                {
                    if(projectCount[index]==1||projectCount[index]==maxProjectCount[index])
                    {
                        for(var i=0;i<projectExpandedTrainings.length;i++)
                        {
                            if(projectExpandedTrainingsFiltered.includes(projectExpandedTrainings[i]))
                            {
                                 projectExpandedTrainings[i].selected=false
                                 setCount(selectedBtTrainings.length+selectedPstTrainings.length+projectExpandedTrainings.filter((training)=>(training.selected==true)).length+expandedTrainings.filter((training)=>(training.selected==true)).length)
                                 
                            }
                        }
                    }
                }
             }
             else if(type=="BT")
             {
                //Only the sub-modules associated with the selected module should be considered for selection/deselection
                const expandedTrainingsFiltered=expandedTrainings.filter((training)=>(training.trainingModuleNumber==moduleNum))
                btCount[index]=expandedTrainingsFiltered.filter((training)=>(training.selected==true)).length

                 /*
                Every time an expandable row is opened to view sub-modules, selectedCount=0 is sent. This can reset selection status of all rows
                unintentionally as the user simply expanding or collapsing a row should not impact the selection status. So to counter this, the
                selection status of the row is stored as a property of the row itself. Furthermore, for the scenario of deselecting one row or
                all simultaneously, a count of all selected rows in each expandable and their max value had to be stored as an array.
                */

                if(selectedRowsData.selectedCount>0||btCount[index]==1||btCount[index]==maxBtCount[index])
                {
                  setExpanded(false)
                }

                if(selectedRowsData.selectedCount>0)
                {
                       for(var i=0;i<expandedTrainings.length;i++)
                        {
                            if(expandedTrainingsFiltered.includes(expandedTrainings[i]))
                            {
                                var result = selectedRows.includes(expandedTrainings[i])
                                if (result == true)
                                {
                                    expandedTrainings[i].selected=true
                                    setCount(selectedBtTrainings.length+selectedPstTrainings.length+projectExpandedTrainings.filter((training)=>(training.selected==true)).length+expandedTrainings.filter((training)=>(training.selected==true)).length)
                        

                                }
                                else
                                {
                                    expandedTrainings[i].selected=false;
                                    setCount(selectedBtTrainings.length+selectedPstTrainings.length+projectExpandedTrainings.filter((training)=>(training.selected==true)).length+expandedTrainings.filter((training)=>(training.selected==true)).length)
                                    
                                }

                            }
                        }
                }
                else if(selectedRowsData.selectedCount==0&&expanded==false)
                {
                    if(btCount[index]==1||btCount[index]==maxBtCount[index])
                    {
                        for(var i=0;i<expandedTrainings.length;i++)
                        {
                            if(expandedTrainingsFiltered.includes(expandedTrainings[i]))
                            {
                                 expandedTrainings[i].selected=false
                                 setCount(selectedBtTrainings.length+selectedPstTrainings.length+projectExpandedTrainings.filter((training)=>(training.selected==true)).length+expandedTrainings.filter((training)=>(training.selected==true)).length)
                               
                            }
                        }
                    }
                }

             }
             
             
         }
         setAnimateHeightToggle(true)
         


        return(
                <center style={{width:"80%"}}>
                <DataTable
                      title={expandableRow.trainingName}
                      columns={innerColumns}
                      data={expandableRowData}
                      paginationRowsPerPageOptions={[5,10,15,25,50,100]}
                      selectableRows
                      //selectableRowsNoSelectAll
                      selectableRowsHighlight
                      selectableRowDisabled={row => row.selectionDisabled}
                      selectableRowSelected={(row)=>row.selected}
                      onSelectedRowsChange={(e)=>(selectRows(e,expandableRow.trainingId,expandableRow.type,expandableRow.trainingModule,expandableRow.index))}
                       noContextMenu={true}
                      highlightOnHover
                      pagination
                />
                
                </center>
              )
    }

    function determineCount()
    {

        const selectCount = selectedBtTrainings.length+selectedPstTrainings.length+projectExpandedTrainings.filter((training)=>(training.selected==true)).length+expandedTrainings.filter((training)=>(training.selected==true)).length
        if(selectCount==0)
        {
             setPopupMessage("Please select at least 1 training to re-assign.");
        }
        else if(commonDueDate==null)
        {
            setPopupMessage("Please enter the due date for"+(selectCount>1?" these "+selectCount+" trainings.":" this training."));
        }
        else
        {
            const dueDate = new Date(commonDueDate)
            const formattedDate = moment(dueDate)
            setPopupMessage("Are you sure you want to re-assign "+(selectCount>1?" these "+selectCount+" trainings":" this training")+" with the completion due date as "+formattedDate.format("Do MMM YYYY (dddd)")+" ?");
        }
        setTrainingsSelectedCount(count)
        setPopup(true);
    }


    async function reAssign()
    {
        const dueDate = new Date(commonDueDate)
        const newdueDate=dayjs(moment(dueDate,"YYYY-MM-DD").format("MM-DD-YYYY"))
        const today = dayjs(moment(new Date(),"YYYY-MM-DD").format("MM-DD-YYYY"))
        if(newdueDate<today)
        {
            await setCommonDueDate(null);
            setPopupMessage("You cannot re-assign training(s) in the past.");
            setPopup(true)
        }
        else
        {
            setPopup(false)
            setLoadingPopup(true)
            var formattedDate = moment(dueDate).format("YYYY-MM-DD")
            var reAssignedTrainingIds = []
            function appendTrainingIds(trainings)
            {
                for(var i=0;i<trainings.length;i++)
                {
                    reAssignedTrainingIds.push(trainings[i].trainingId)
                }
            }
            appendTrainingIds(selectedBtTrainings)
            appendTrainingIds(selectedPstTrainings)
            appendTrainingIds(expandedTrainings.filter((training)=>(training.selected==true)))
            appendTrainingIds(projectExpandedTrainings.filter((training)=>(training.selected==true)))
        
            const details = {
                reAssignedTrainingIds:reAssignedTrainingIds,
                dueDate:formattedDate
            }
            await axios.post("http://10.220.20.246:8000/ReAssignTrainings",details).then((res)=>
            {
                setLoadingPopup(false)
                setReAssignmentCompleteFlag(true);
                setPopup(true)
                setPopupMessage(res.data.response);
                setSelectedBtTrainings([])
                setSelectedPstTrainings([])
                setPstKey(pstKey+1)
                setBtKey(btKey+1)
                for(var i=0;i<expandedTrainings.length;i++)
                {
                    expandedTrainings[i].selected=false;
                }
                for(var i=0;i<projectExpandedTrainings.length;i++)
                {
                    projectExpandedTrainings[i].selected=false;
                }
                setCount(0)
                fetch_details();

            })

        }
        
    }

    async function saveChanges()
    {

        var alteredReAssignedTrainingIds=[]
        var dueDates=[]
        const alteredReAssignedTrainings = filteredRtTrainings.filter((training)=>(training.trainingDueDateNew!=null))
        if(alteredReAssignedTrainings.length==0)
        {
             setPopupMessage("No valid due date changes detected!");
             setSaveChangesFlag(true);          
        }
        else if(alteredReAssignedTrainings.filter((training)=>(dayjs(moment(training.trainingDueDateNew,"YYYY-MM-DD").format("MM-DD-YYYY"))<dayjs(moment(new Date(),"YYYY-MM-DD").format("MM-DD-YYYY")))).length>0)
        {
            setPopupMessage("Some due date(s) is/are in the past!");
            setSaveChangesFlag(true);          
        }
        else
        {
            setPopup(false)
            setLoadingPopup(true)
            for(var i=0;i<alteredReAssignedTrainings.length;i++)
            {
                alteredReAssignedTrainingIds.push(alteredReAssignedTrainings[i].trainingId)
                dueDates.push(alteredReAssignedTrainings[i].trainingDueDateNew)
            }
            const details = {
                alteredReAssignedTrainingIds:alteredReAssignedTrainingIds,
                dueDates:dueDates
            }
            await axios.post("http://10.220.20.246:8000/ChangeReAssignedTrainingsDueDates",details).then((res)=>
            {
                setLoadingPopup(false)
                setPopup(true)
                setPopupMessage(res.data.response);
                setSaveChangesFlag(true);
                fetch_details();
            })
        }
        setSwitchChecked(false)
        setEditAssignedDueDateToggle(false)
    }

    async function deleteReAssignedTrainings()
    {
        setPopup2(false)
        setLoadingPopup(true)
        var deletionReAssignedTrainingIds=[]
        for(var i=0;i<selectedRtTrainings.length;i++)
        {
            deletionReAssignedTrainingIds.push(selectedRtTrainings[i].trainingId)
        }
        const details={
            deletionReAssignedTrainingIds:deletionReAssignedTrainingIds
        }
        await axios.post("http://10.220.20.246:8000/DeleteReAssignedTrainings",details).then((res)=>
            {
                setLoadingPopup(false)
                setPopup2(true)
                setPopupMessage2(res.data.response);
                setDeletionFlag(true);
                setSelectedRtTrainings([])
                setSelectedBtTrainings([])
                setSelectedPstTrainings([])
                setPstKey(pstKey+1)
                setBtKey(btKey+1)
                for(var i=0;i<expandedTrainings.length;i++)
                {
                    expandedTrainings[i].selected=false;
                }
                for(var i=0;i<projectExpandedTrainings.length;i++)
                {
                    projectExpandedTrainings[i].selected=false;
                }
                setCount(0)
                fetch_details();
            })
    }

   
    return(

        <div className="bg">
        <Navbar setUserEmail={props.setUserEmail} setUserType={props.setUserType} dashboard_link="/trainer_dashboard"/>
        {
           traineeEmail!=""

           ?
            <div>
                <h1>RE-ASSIGN TRAININGS</h1>
                <div className="table" style={{margin:"auto"}}>
                    <Tabs variant="fullWidth" style={{width:"100%"}} value={view} onChange={(event,value)=>{setView(value);}}>
                      <Tab label="Basic Trainings" style={{fontSize:"150%"}}/>
                      <Tab label={projectData.Project_Name+" Trainings"} style={{fontSize:"150%"}}/>
                      <Tab label="Re-Assigned Trainings" style={{fontSize:"150%"}}/>
                    </Tabs>
                    <SwipeableViews animateHeight={animateHeightToggle} index={view} onChangeIndex={(event,value)=>{setView(value)}}>

                    {/*BASIC TRAININGS */}
                    <center>
                    <div style={{display: 'flex', alignItems: 'center', background:count>0?"#Add8e6":"white", color:"black", textAlign:"left", width:"100%", height:"50px", verticalAlign:"middle"}}>&emsp;{count>0 ? count+(count==1?" training":" trainings")+" selected":"" }</div> 
                        <DataTable
                        key={btKey} //This  is changed after every re-assignment or delteion to ensure that the table is reset
                        columns={columns}
                        data={filteredBtTrainings}
                        paginationRowsPerPageOptions={[5,10,15,25,50,100]}
                        noHeader
                        fixedHeader
                        fixedHeaderScrollHeight="80vh"
                        noContextMenu={true}
                        selectableRows
                        selectableRowsHighlight
                        selectableRowDisabled={row => row.selectionDisabled}
                        onSelectedRowsChange={(e) => setSelectedBtTrainings(e.selectedRows)}
                        expandableRows
                        onRowExpandToggled={(e,row)=>{row.expanded=e;setAnimateHeightToggle(true);setAnimateHeightToggle(false)}}
			            expandableRowDisabled={row => row.expansionDisabled}
			            expandableRowsComponent={ExpandedComponent}
			            expandableComponentProps={{ expandableRow: (row) => row}}
                        highlightOnHover
                        pagination
                        subHeader
                        subHeaderComponent =
                        {
                               <Grid container spacing={3} style={{width:"100%"}} >
                                    <Grid item sm={3} >
                                          <TextField style={{width:"100%"}} label="Search by curriculum" variant="outlined" value={btSearch} onChange={(e)=>{if(e.target.value[e.target.value.length-1]!="\\"){setBtSearch(e.target.value)}}}/>
                                    </Grid>
                                    <Grid item sm={6}/>
                                    <Grid item sm={3} style={{marginLeft:"auto"}}>
                                        <LocalizationProvider dateAdapter={AdapterDayjs} >
                                        <DemoContainer components={['DatePicker']} >
                                          <DatePicker disablePast label="Completion Due Date" value={commonDueDate} onChange={(e)=>{handleChange(e)}}/>
                                        </DemoContainer>
                                        </LocalizationProvider>
                                    </Grid>
                                </Grid>

                        }
                        subHeaderAlign="left"
                      />
                      </center>

                      {/*PROJECT TRAININGS */}
                      <center>
                      <div style={{display: 'flex', alignItems: 'center', background:count>0?"#Add8e6":"white", color:"black", textAlign:"left", width:"100%", height:"50px", verticalAlign:"middle"}}>&emsp;{count>0 ? count+(count==1?" training":" trainings")+" selected":"" }</div> 
                      <DataTable
                        key={pstKey} //This  is changed after every re-assignment or delteion to ensure that the table is reset
                        columns={columns}
                        data={filteredPstTrainings}
                        noHeader
                        paginationRowsPerPageOptions={[5,10,15,25,50,100]}
                        fixedHeader
                        fixedHeaderScrollHeight="80vh"
                        noContextMenu={true}
                        selectableRows
                        selectableRowsHighlight
                        selectableRowDisabled={row => row.selectionDisabled}
                        onSelectedRowsChange={(e) => setSelectedPstTrainings(e.selectedRows)}
                        expandableRows
                        onRowExpandToggled={(e,row)=>{row.expanded=e;setAnimateHeightToggle(true);setAnimateHeightToggle(false)}}
			            expandableRowDisabled={row => row.expansionDisabled}
			            expandableRowsComponent={ExpandedComponent}
			            expandableComponentProps={{ expandableRow: (row) => row }}
                        highlightOnHover
                        pagination
                        subHeader
                        subHeaderComponent =
                        {

                                <Grid container spacing={3} style={{width:"100%"}} >
                                    <Grid item sm={3} >
                                          <TextField  style={{width:"100%"}} label="Search by curriculum" variant="outlined" value={pstSearch} onChange={(e)=>{if(e.target.value[e.target.value.length-1]!="\\"){setPstSearch(e.target.value)}}}/>
                                    </Grid>
                                    <Grid item sm={6}/>
                                    <Grid item sm={3} style={{marginLeft:"auto"}}>
                                        <LocalizationProvider dateAdapter={AdapterDayjs} >
                                        <DemoContainer components={['DatePicker']} >
                                          <DatePicker disablePast label="Completion Due Date" value={commonDueDate} onChange={(e)=>{handleChange(e)}}/>
                                        </DemoContainer>
                                        </LocalizationProvider>
                                    </Grid>
                                </Grid>


                        }
                        subHeaderAlign="left"
                      />
                      </center>

                      {/*RE-ASSIGNED TRAININGS */}
                      <center>
                      <DataTable
                        title=" "
                        columns={rtColumns}
                        data={filteredRtTrainings}
                        paginationRowsPerPageOptions={[5,10,15,25,50,100]}
                        fixedHeader
                        selectableRows
                        onSelectedRowsChange={(e) => setSelectedRtTrainings(e.selectedRows)}
                        //expandableRows
			            //expandableRowDisabled={row => row.disabled}
			            //expandableRowsComponent={ExpandedComponent}
                        contextMessage={{singular:"training",plural:"trainings",message:"selected"}}
                        highlightOnHover
                        customStyles={customStyles}
                        noContextMenu={!selectedRtTrainings.length}
                        pagination
                        subHeader
                        subHeaderComponent =
                        {

                               <div style={{width:"100%", textAlign:"left"}}>
                                   <TextField style={{width:"25%"}} label="Search by curriculum" variant="outlined" value={rtSearch} onChange={(e)=>{if(e.target.value[e.target.value.length-1]!="\\"){setRtSearch(e.target.value)}}}/> &emsp;
                                    <FormControl component="fieldset" variant="standard">
                                    <FormGroup>
                                      <FormControlLabel control={<Switch disabled={!filteredRtTrainings.length||!filteredRtTrainings.filter((training)=>(training.trainingCompletionDate==null))} checked={switchChecked} onChange={()=>{setSwitchChecked(!switchChecked); setEditAssignedDueDateToggle(!editAssignedDueDateToggle)}}/>} style={{color:"black"}} label="Edit Due Date" />
                                    </FormGroup>
                                    </FormControl>
                               </div>

                        }
                        subHeaderAlign="left"
                    />
                      </center>
                    </SwipeableViews>
                </div>
                <br/>
                <center>
            {/* view=0 indicates Basic Trainings View, view=1 indicates Project Trainings View, view=2 indicates Re-Assigned Trainings View   */}
            {
                view!=2
                ?
                <button className="btn btn-success btn-lg" onClick={determineCount} >Re-Assign Trainings</button>
                :
                <div>
                <button className="btn btn-success btn-lg" disabled={!switchChecked} onClick={()=>{setPopup(true);setPopupMessage("Are you sure you want to save these due date changes ?");}}>Save Changes</button>&emsp;
                <button disabled={selectedRtTrainings.length>0?false:true} className="btn btn-success btn-lg" onClick={()=>{setPopup2(true);setPopupMessage2(selectedRtTrainings.length==1?"Are you sure you delete this re-assigned training ?":"Are you sure you delete these "+selectedRtTrainings.length+" re-assigned trainings ?");}}>Delete</button>
                </div>
            }
            </center>
            <br/>
			</div>

           :

             <div><br/><h1>You cannot access the dashboard without logging in</h1></div>
        }

        <Popup open={popup} closeOnDocumentClick onClose={()=>{setPopup(false)}}>
            <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setPopup(false);}}/>
            <center>
              <br/>
              <p style={{color:"#003C71", fontSize:"130%", margin:"3%"}}><center>{popupMessage}</center></p>
              <br/>
              {
              ((view==0||view==1) && (trainingsSelectedCount==0 || commonDueDate==null || reAssignmentCompleteFlag==true)) || (view==2 && saveChangesFlag==true)

              ?

              <div className='btn btn-info btn-lg' onClick={()=>{setPopup(false);setReAssignmentCompleteFlag(false);setSaveChangesFlag(false);setDeletionFlag(false)}}>Ok</div>

              :
              <center>

              <div className='btn btn-info btn-lg' onClick={((view==0||view==1)&&(reAssignmentCompleteFlag==false))?reAssign:(view==2&&saveChangesFlag==false)?saveChanges:null}>Yes</div> &emsp;
              <div className='btn btn-info btn-lg' onClick={()=>{setPopup(false)}}>No</div>
              </center>
              }
              <br/>
              <br/>
            </center>
         </Popup>
         <Popup open={popup2} closeOnDocumentClick onClose={()=>{setPopup2(false)}}>
            <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setPopup2(false);}}/>
            <center>
              <br/>
              <p style={{color:"#003C71", fontSize:"130%", margin:"3%"}}><center>{popupMessage2}</center></p>
              <br/>
              {
              deletionFlag==true

              ?

              <div className='btn btn-info btn-lg' onClick={()=>{setPopup2(false);setDeletionFlag(false)}}>Ok</div>

              :
              <center>

              <div className='btn btn-info btn-lg' onClick={deleteReAssignedTrainings}>Yes</div> &emsp;
              <div className='btn btn-info btn-lg' onClick={()=>{setPopup2(false)}}>No</div>
              </center>
              }
              <br/>
              <br/>
            </center>
         </Popup>

          {/* LOADING SCREEN */}
          <Popup open={loadingPopup} hideBackdrop closeOnDocumentClick={false} onClose={()=>{setLoadingPopup(false)}}>
              <center>
                <p style={{color:"#003C71", fontSize:"130%", margin:"3%"}}><center>PLEASE WAIT...</center></p>
                <br/>
                <CircularProgress/>
                <br/>
                <br/>
              </center>
         </Popup> 

        </div>
    )
}

export default Re_assign_trainings