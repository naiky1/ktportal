import React,{useState,useEffect} from 'react'
import Navbar from '../../Base/navbar.jsx'
import './trainer_dashboard.css'
import axios from 'axios'
import Popup from 'reactjs-popup'
import {Link} from 'react-router-dom'
import { CircularProgress } from '@mui/material'
import CancelIcon from '@mui/icons-material/Cancel';

function Trainer_dashboard(props)
{
    const trainerEmail= props.userEmail
    const [trainerName,setTrainerName] = useState("")
    const [traineeIds,setTraineeIds] = useState([])
    const [traineeNames,setTraineeNames] =  useState([])
    const [traineeProjectNames,settraineeProjectNames] =  useState([])
    const [trainingRequirementTypes,settrainingRequirementTypes] =  useState([])
    const [traineeEmails,setTraineeEmails] =  useState([])
    const [traineeFeedbacks,setTraineeFeedbacks] = useState([])
    const [traineeProjectIds,setTraineeProjectIds] = useState([])
    const [traineeStatuses,setTraineeStatuses] = useState([])
    const [reverseKtStatuses,SetReverseKtStatuses] = useState([])

    //Loading Screen
    const [loadingPopup,setLoadingPopup] = useState(true)

    //For Popup
    const [popup,setPopup] =  useState(false)
    const [popupMessage,setPopupMessage] = useState([])
    const [innerPopup,setInnerPopup] = useState(false)
    const [innerPopupMessage,setInnerPopupMessage] = useState("")
    const [rKtPopup,setRKtPopup] = useState(false)
    const [rKtPopupMessage,setRKtPopupMessage] = useState("")
    const [feedback,setFeedback] = useState("")
    const [activeTraineeId,setActiveTraineeId] = useState(0)
    const [activeTraineeIdRKt,setActiveTraineeIdRKt] = useState(0)
    const [activeIndex,setActiveIndex] = useState(0)
    const [rKt,setRKt] = useState(false)

     const fetch_details = async () =>
    {
        const details = {
            userEmail : trainerEmail,
            userType : "Trainer"
        }
        const Params = new URLSearchParams(details);
        const URLWithParams = `http://10.220.20.246:8000/fetchUserName?${Params}`;
        await axios.get(URLWithParams).then( res=> {setTrainerName(res.data.userName)})

        const TrainerTableURLWithParams = `http://10.220.20.246:8000/fetchTrainerTableDetails?${Params}`;
        await axios.get(TrainerTableURLWithParams).then( res=>
        {
            setTraineeIds(res.data.traineeIds);
            setTraineeNames(res.data.traineeNames);
            settraineeProjectNames(res.data.traineeProjectNames);
            settrainingRequirementTypes(res.data.trainingRequirementTypes);
            setTraineeEmails(res.data.traineeEmails);
            setTraineeFeedbacks(res.data.traineeFeedbacks);
            setTraineeStatuses(res.data.traineeStatuses);
            console.log(traineeStatuses, 'kumar1')
            SetReverseKtStatuses(res.data.reverseKtStatuses);
            setTraineeProjectIds(res.data.traineeProjectIds);
            setLoadingPopup(false);

        });

    }

     useEffect(() =>
    {
        fetch_details();
    },[])

    async function sendFeedback()
    {
        if(feedback==""||feedback.trim().length == 0)
        {
            setInnerPopup(true)
            setInnerPopupMessage("No feedback entered! Please retry.")
        }
        else
        {
          
            setLoadingPopup(true)
            const details = {
                senderEmail : trainerEmail,
                senderType : "Trainer",
                Receiver_ID : activeTraineeId,
                feedback: feedback,
                rKtActive:rKt
            }
            await axios.post("http://10.220.20.246:8000/addFeedback",details).then( async(res) =>
            {
                setLoadingPopup(false)
                setInnerPopup(true)
                setInnerPopupMessage(res.data.response)
                if(rKt)
                {
                    setRKt(false)
                }
                setPopup(false)
                fetch_details()
                setFeedback("")
            });
        }

    }

    function handleChange(e)
    {
        // 'e' denotes an event (change in value) and target stores the tag where it occurred and target.value returns the changed value that is stored in feedback
        setFeedback(e.target.value)
    }



    return(
        <div className="bg">
        <Navbar setUserType={props.setUserType} setUserEmail={props.setUserEmail} dashboard_link="/trainer_dashboard"/>
        <h1>TRAINER DASHBOARD</h1>
        <br/>
        <h3>Welcome {trainerName}!</h3>
        {/*<br/>
        <br/>
        <p>Infographics coming soon...</p>*/}
        <br/>
        <br/>
        <center>
        {
          traineeEmails.length==0
          ?
          <h3>There are currently no trainees assigned to you.</h3>
          :

        <table className="table_content">
	    <thead>
		    <tr style={{width:"80%",lineHeight:"300%",marginLeft:"2%"}}>
		        <th style={{width:"10%",scope:"col", textAlign:"center"}}>S. NO.</th>
		        <th style={{width:"10%",scope:"col", textAlign:"center"}}>TRAINEE</th>
		        <th style={{width:"12%",scope:"col", textAlign:"center"}}>PROJECT</th>
		        <th style={{width:"15%",scope:"col", textAlign:"center"}}>TRAINING REQUIREMENT</th>
		        <th style={{width:"15%",scope:"col", textAlign:"center"}}>PROGRESS</th>
            <th style={{width:"15%",scope:"col", textAlign:"center"}}>LEARNING PLAN</th>
		        <th style={{width:"10%",scope:"col", textAlign:"center"}}>REVERSE KT</th>
		        <th style={{width:"15%",scope:"col", textAlign:"center"}}>FEEDBACK</th>
		    </tr>
	    </thead>
	    <tbody>
	    {
	            traineeIds.map( (el,index) =>
	            (

			    <tr>
			        <td>
			        <center>
			          <div>
			            {index+1}
			          </div>
			        </center>
			        </td>
			        <td>
			        <center>
			          <div>
			            {traineeNames[index]}
			          </div>
			        </center>
			        </td>
			        <td>
			        <center>
			          <div>
			            {traineeProjectNames[index]}
			          </div>
			        </center>
			        </td>
			        <td>
			        <center>
			          <div>
			            {trainingRequirementTypes[index]}
			          </div>
			        </center>
			        </td>
			        <td>
			    	<center>
			    	  <Link to="/trainee_progress" className="btn btn-success" state={{traineeId:el, traineeEmail:traineeEmails[index], projectId:traineeProjectIds[index], traineeType:trainingRequirementTypes[index], type:"Trainer"}}>
			    	     Progress Dashboard
			    	  </Link>
			    	</center>
			        </td>
              <td>
			    	<center>
            <Link to="/trainee_learning_plan" className="btn btn-warning" state={{projectId:traineeProjectIds[index], traineeEmail:traineeEmails[index], traineeType:trainingRequirementTypes[index], type:"Trainer"}}>
                Learning Plan
               </Link>
			    	</center>
			        </td>
			        <td>
			    	<center>
			    	   <button type="button" disabled={traineeStatuses[index]=="N" ? true : false} class="btn btn-info" onClick={()=>{setRKtPopupMessage("Are you sure you want to give the final sign-off for "+traineeNames[index]+" ?"); setRKtPopup(true); setActiveTraineeIdRKt(el); setActiveTraineeId(el); setActiveIndex(index); setRKt(true)}}>Reverse-KT</button>
			    	</center>
			        </td>
			        <td>
			    	<center>
			    	  <button type="button" class="btn btn-primary" onClick={()=>{setPopupMessage(traineeFeedbacks[index]); setPopup(true); setActiveTraineeId(el); setActiveIndex(index);}}>{reverseKtStatuses[index]=="Y" ? "View Feedback" : "Give Feedback"}</button>
			    	</center>
			        </td>
			    </tr>
			    ))
		}
		</tbody>
	    </table>
}
	    </center>
	    <Popup open={popup} closeOnDocumentClick onClose={()=>{setPopup(false);  setRKt(false);}}>
        <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setPopup(false);  setRKt(false);}}/>
            <center >
              <br/>
              <h3>FEEDBACK</h3>
              <br/>
               <p style={{color:"#003C71", fontSize:"130%", maxHeight:window.innerHeight*0.4, overflow:"auto", textAlign:"left", paddingLeft:"5%",paddingRight:"5%"}}>
              {
                 popupMessage.length==0
                 ?
 
                 <center>No Feedback</center>
 
                 :

                popupMessage.map((el,index) =>
                (

                   <div>
                        {el}
                        <br/>
                   </div>

                ))


              }
              </p>
              <br/>
              {

                reverseKtStatuses[activeIndex] == "Y"

                ?

                <div></div>

                :


                <div>
                <textarea onChange={handleChange} style={{width:"80%", padding:"2%", rows:"20"}} value={feedback} placeholder={"Enter Feedback..."}/>
                <br/>
                <br/>
                <div className='btn btn-info btn-lg' onClick={sendFeedback}>{ rKt ? "Submit" : "Add"}</div> &emsp;
                <div className='btn btn-info btn-lg' onClick={()=>{setPopup(false); setRKt(false)}}>Cancel</div>
                <br/>
                </div>
              }
              <br/>
            </center>
         </Popup>

         <Popup open={innerPopup} closeOnDocumentClick onClose={()=>{setInnerPopup(false)}}>
          <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setInnerPopup(false)}}/>
            <center >
              <br/>
              <center><p style={{color:"#003C71", fontSize:"130%", maxHeight:window.innerHeight*0.4, overflow:"auto"}}>{innerPopupMessage}</p></center>
              <br/>
              <div className='btn btn-info btn-lg' onClick={()=>{setInnerPopup(false)}}>Ok</div>
              <br/>
              <br/>
            </center>
         </Popup>

          <Popup open={rKtPopup} closeOnDocumentClick onClose={()=>{setRKtPopup(false)}}>
          <CancelIcon fontSize="large" style={{cursor:"pointer",float: "right", marginRight:"1%", marginTop:"1%"}} onClick={()=>{setRKtPopup(false)}}/>
            <center >
              <br/>
              <center><p style={{color:"#003C71", fontSize:"130%"}}>{rKtPopupMessage}</p></center>
              <br/>
              <div className='btn btn-info btn-lg' onClick={()=>{setPopupMessage(traineeFeedbacks[activeIndex]); setPopup(true);}}>Yes</div> &emsp;
              <Link to="/reverse_kt_re_assign_trainings" className='btn btn-info btn-lg' state={{traineeEmail:traineeEmails[activeIndex],projectId:traineeProjectIds[activeIndex],traineeType:trainingRequirementTypes[activeIndex]}}>Re-Assign Training(s)</Link> &emsp;
              <div className='btn btn-info btn-lg' onClick={()=>{setRKtPopup(false)}}>Cancel</div>
              <br/>
              <br/>
            </center>
         </Popup>

         {/* LOADING SCREEN */}
         <Popup open={loadingPopup} hideBackdrop closeOnDocumentClick={false} onClose={()=>{setLoadingPopup(false)}}>
              <center>
                <p style={{color:"#003C71", fontSize:"130%", margin:"3%"}}><center>PLEASE WAIT...</center></p>
                <br/>
                <CircularProgress/>
                <br/>
                <br/>
              </center>
         </Popup> 
        </div>
    )
}

export default Trainer_dashboard