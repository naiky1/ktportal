import React, {useState, useEffect} from 'react'
import {useNavigate, useLocation} from 'react-router-dom'
import axios from 'axios'
import moment from 'moment'
import './missed_trainings_card.css'


function MissedTrainingsCard()
{
    const [missedTrainings,setMissedTrainings] = useState([])
    const [missedTrainingsNames,setMissedTrainingsNames]=useState([])
    const [missedTrainingsPercentage,setMissedTrainingsPercentage] = useState(0)

     //Dynamic CSS Variables
    const [missedTrainingsStyling,setMissedTrainingsStyling] = useState({
        whitegradient:0,
        greengradient:0,
        hoverbgsize:0
    })

    let navigate = useNavigate();
    //To get the trainee ID from the previous page
    const location = useLocation();
    const traineeId= location.state.traineeId;
    const traineeType= location.state.traineeType;
    const projectId= location.state.projectId;


    async function fetch_details()
    {
         const details = {
            userId : traineeId,
            projectId : projectId,
            traineeType : traineeType
        }
        const Params = new URLSearchParams(details);
        const URLWithParams = `http://10.220.20.246:8000/fetchMissedTrainings?${Params}`;
        await axios.get(URLWithParams).then( res =>
        {
            setMissedTrainings(res.data.missed_trainings);
            setMissedTrainingsNames(res.data.missed_trainings_names)
            setMissedTrainingsPercentage(res.data.missed_trainings_percentage)

            if(res.data.missed_trainings_percentage<50)
            {
                missedTrainingsStyling.whitegradient=100-res.data.missed_trainings_percentage;
                missedTrainingsStyling.greengradient=res.data.missed_trainings_percentage;
                missedTrainingsStyling.hoverbgsize=100;
            }
            else
            {
                missedTrainingsStyling.whitegradient=50;
                missedTrainingsStyling.greengradient=50;
                missedTrainingsStyling.hoverbgsize=2*res.data.missed_trainings_percentage;
            }
        });


    }

     useEffect(() =>
    {
        fetch_details()
    },[])

    return(

        <div className='Missed_trainings' style={{'--mtwhitegradient':`${missedTrainingsStyling.whitegradient}%`, '--mtgreengradient':`${missedTrainingsStyling.greengradient}%`,'--mthoverbgsize':`${missedTrainingsStyling.hoverbgsize}%`}}>
        <br/>
        <p style={{fontWeight:"600", fontSize:"150%"}}>{missedTrainings.length} TRAININGS MISSED</p>
        {
            missedTrainings.length==0

            ?

            <div></div>

            :

            <table className="missed_trainings_table_content center" style={{textAlign:"center"}}>
	            <thead>
		            <tr style={{width:"100%",lineHeight:"300%", fontSize:"85%", padding:"5%"}}>
		                <th style={{width:"50%",scope:"col",textAlign:"center"}}>MODULE</th>
		                <th style={{width:"25%",scope:"col",textAlign:"center"}}>DUE DATE</th>
		                <th style={{width:"25%",scope:"col",textAlign:"center"}}>COMPLETION DATE</th>
		            </tr>
	            </thead>
	            <tbody>
	            {
                    missedTrainings.map( (el,index) =>
	                            (

		            	        <tr>
		            	           <td style={{paddingLeft:"3%"}}>{missedTrainingsNames[index]}</td>
		            	            <td>
                                    {moment(el.Due_Date).format("DD-MM-YYYY")}
		            	            </td>
		            	        	<td>
                                        {(el.Completion_Date)?moment(el.Completion_Date).format("DD-MM-YYYY"):"Not Completed"}
		            	            </td>
		            	        </tr>
		            	        ))
		        }
		        </tbody>
		    </table>


		}
				    <br/>
        </div>
    )
}

export default MissedTrainingsCard