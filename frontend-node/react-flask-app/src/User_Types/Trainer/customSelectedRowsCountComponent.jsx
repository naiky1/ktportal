function CustomSelectedRowsCountComponent(props)
{
    return(<div>{props.count==1? "1 training selected" : props.count+" trainings selected"}</div>)
}

export default CustomSelectedRowsCountComponent