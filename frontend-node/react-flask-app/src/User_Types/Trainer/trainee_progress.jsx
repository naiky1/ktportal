import React, {useState, useEffect} from 'react'
import Navbar from '../../Base/navbar.jsx'
import './trainee_progress.css'
import axios from 'axios'
import Popup from 'reactjs-popup'
import moment from 'moment'
import {useNavigate, useLocation} from 'react-router-dom'
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid'
import MissedTrainingsCard from './Cards/missed_trainings_card.jsx'
import BasicTrainingsProgressCard from './Cards/basic_trainings_progress_card.jsx'
import ProjectSpecificTrainingsProgressCard from './Cards/project_specific_trainings_progress_card.jsx'
import TaskAltOutlinedIcon from '@mui/icons-material/TaskAltOutlined';
import RadioButtonUncheckedIcon from '@mui/icons-material/RadioButtonUnchecked';
import {PieChart,Pie, ResponsiveContainer, Tooltip, Cell, Label} from 'recharts'


function Trainee_progress(props)
{

    //Dynamic CSS Variables
    const [overallProgressStyling,setOverallProgressStyling] = useState({
        whitegradient:0,
        greengradient:0,
        hoverbgsize:0
    })
    const [ktCompletionStyling,setKtCompletionStyling] = useState({
        whitegradient:0,
        greengradient:0,
        hoverbgsize:0
    })

    //To redirect to the appropriate dashboard if the credentials are valid
    let navigate = useNavigate();

    //To get the trainee ID from the previous page
    const location = useLocation();
    const traineeId= location.state.traineeId;
    const traineeEmail = location.state.traineeEmail
    const traineeType = location.state.traineeType
    const projectId = location.state.projectId

    //Trainee Status details
    const [ktFound,setKtFound] = useState(1)
    const [traineeName,setTraineeName] = useState("")
    const [overallProgress,setOverallProgress] = useState(0)
    const [ktCompletionDate,setKtCompletionDate] = useState()
    const [daysLeft,setDaysLeft] = useState(0)
    const [ktPercentageCompletion,setKtPercentageCompletion] = useState(0)
    const [smPieChartData, setSmPieChartData] = useState([{name:"Completed",value:0},{name:"Incomplete",value:0}])
    const [subModuleNames,setSubModuleNames] = useState([])
    const [subModuleStatuses,setSubModuleStatuses] = useState([])
    const COLORS = ['#60BE71', '#CBCBCA'];
    const [moduleName,setModuleName] = useState("")
    const [toggle,setToggle] = useState("PST")
    const [completionPercentage,setCompletionPercentage] = useState(0)

    const [ktCompleted, setktCompleted] = useState("")

    async function fetch_details()
    {
         const details = {
            userId : traineeId,
            projectId : projectId,
            traineeType : traineeType
        }
        const Params = new URLSearchParams(details);
        const URLWithParams = `http://10.220.20.246:8000/fetchUserName?${Params}`;
        await axios.get(URLWithParams).then( res=> {setTraineeName(res.data.userName)})

        const OverallProgressParams = new URLSearchParams(details);
        const OverallProgressURLWithParams = `http://10.220.20.246:8000/fetchOverallProgress?${OverallProgressParams}`;
        await axios.get(OverallProgressURLWithParams).then( res=>
        {
            setKtFound(res.data.kt_found)
            setktCompleted(res.data.reverse_kt_done)
            setOverallProgress(res.data.overall_progress)
            const completionDate = moment(res.data.KT_completion_date).format("DD-MM-YYYY")
            setKtCompletionDate(completionDate)
            setDaysLeft(res.data.days_remaining)
            setKtPercentageCompletion(res.data.Kt_Percentage_Completion)
            if(res.data.overall_progress<50)
            {
                overallProgressStyling.whitegradient=100-res.data.overall_progress;
                overallProgressStyling.greengradient=res.data.overall_progress;
                overallProgressStyling.hoverbgsize=100;
            }
            else
            {
                overallProgressStyling.whitegradient=50;
                overallProgressStyling.greengradient=50;
                overallProgressStyling.hoverbgsize=2*res.data.overall_progress;
            }

            if(res.data.Kt_Percentage_Completion<50)
            {
                ktCompletionStyling.whitegradient=100-res.data.Kt_Percentage_Completion;
                ktCompletionStyling.greengradient=res.data.Kt_Percentage_Completion;
                ktCompletionStyling.hoverbgsize=100;
            }
            else
            {
                ktCompletionStyling.whitegradient=50;
                ktCompletionStyling.greengradient=50;
                ktCompletionStyling.hoverbgsize=2*res.data.Kt_Percentage_Completion;
            }



        })

    }

    useEffect(() =>
    {
        fetch_details()
    },[])



    return(

        ktFound

        ?

        <div className="bg">
        <Navbar dashboard_link={"/"+location.state.type.toLowerCase()+"_dashboard"} setUserEmail={props.setUserEmail} setUserType={props.setUserType} />

            <h3>{traineeName.toUpperCase()}'S PROGRESS</h3>
            <br/>
            <Grid container spacing={3} style={{width:"100%", height:"50%"}} >
              <Grid item sm={3} style={{paddingLeft:"3%", height:"50%"}} >
                <div className="overall_progress" style={{borderRadius:"10px", overflow:"auto", display: "flex",flexDirection: "column",justifyContent:"center" , '--opwhitegradient':`${overallProgressStyling.whitegradient}%`, '--opgreengradient':`${overallProgressStyling.greengradient}%`,'--ophoverbgsize':`${overallProgressStyling.hoverbgsize}%`}}>
                <div style={{fontSize:"40%",fontWeight:"600"}}>KT Progress</div>
                {overallProgress}%
                </div>
              </Grid>

              <Grid item sm={4} style={{height:"50%"}}>
                <div className="KT_completion_date" style={{overflow:"auto", display: "flex",flexDirection: "column",justifyContent:"center", '--ktwhitegradient':`${ktCompletionStyling.whitegradient}%`, '--ktgreengradient':`${ktCompletionStyling.greengradient}%`,'--kthoverbgsize':`${ktCompletionStyling.hoverbgsize}%`}}>
                  <div style={{fontSize:"50%"}}>KT Completion Date</div>
                  {ktCompletionDate}
{/*                   {overallProgress==100?"":<div style={{fontSize:"50%"}}>({daysLeft} day(s) remaining)</div>} */}
                  {overallProgress==100?ktCompleted=="Y"?<div style={{fontWeight:"300", fontSize:"60%"}}>Reverse KT - Completed</div>:<div style={{fontWeight:"300", fontSize:"80%", color:"red"}}>Reverse KT - Pending</div>:<div style={{fontSize:"80%"}}>({daysLeft} day(s) remaining)</div>}
                 <Paper elevation={2} />
                 </div>
              </Grid>

              <Grid item sm={5}  style={{ height:"50%", overflow: "auto" }}>
                <MissedTrainingsCard state={{traineeId:location.state.traineeId}}/>
                 <Paper elevation={20} />
              </Grid>

              <Grid item sm={3} style={{paddingLeft:"3%", maxHeight:"130%", height:"100%"}}>
                <BasicTrainingsProgressCard setSubModuleStatuses={setSubModuleStatuses} setSubModuleNames={setSubModuleNames} setCompletionPercentage={setCompletionPercentage} setSmPieChartData={setSmPieChartData} traineeId={location.state.traineeId} traineeEmail={traineeEmail} traineeType={traineeType} projectspecificprojectId={projectId} setModuleName={setModuleName} moduleName={moduleName}/>
              </Grid>

              <Grid item sm={4} style={{maxHeight:"130%", height:"100%"}} >
                <ProjectSpecificTrainingsProgressCard setSubModuleStatuses={setSubModuleStatuses} setSubModuleNames={setSubModuleNames} setCompletionPercentage={setCompletionPercentage} setSmPieChartData={setSmPieChartData} traineeId={location.state.traineeId} traineeEmail={traineeEmail} traineeType={traineeType} projectspecificprojectId={projectId} setModuleName={setModuleName} moduleName={moduleName} />
              </Grid>

              <Grid item sm={5}  style={{maxHeight:"130%", height:"100%", borderRadius:"10px",overflow:"auto" }}>
                <div className="Sub_Module_Progress"  style={{overflow:"auto", display: "flex",flexDirection: "column",justifyContent:"center",backgroundColor:"white"}}>
                    <div style={{fontSize:"100%",fontWeight:"600"}}>{moduleName} Progress</div>
                    <br/>
                    <Grid container spacing={3} style={{width:"100%",height:"100%"}} >
                        <Grid item sm={5}>
                          <ResponsiveContainer width="100%" height="99%" >
                          <PieChart>
                            <Pie
                              dataKey="value"
                              isAnimationActive={false}
                              data={smPieChartData}
                              cx="50%"
                              cy="50%"
                              innerRadius="60%"
                              outerRadius="90%"
                            >
                             {smPieChartData.map((entry, index) =>
                             (
                                  <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                             ))}
                              <Label value={completionPercentage+"%"} position="center" style={{fill:completionPercentage==0 ? "#CBCBCA" : "#60BE71",fontSize:"150%"}} />
                             </Pie>
                            <Tooltip />
                          </PieChart>
                          </ResponsiveContainer>
                        </Grid>
                        <Grid item sm={7} style={{margin: "auto"}}>
                        {
                            subModuleStatuses.map( (el,index) =>

                                <div style={{color:el=="Y" ? "#60BE71": "#CBCBCA", fontSize:"70%", textAlign:"left"}}>
                                {el=="Y" ? <TaskAltOutlinedIcon /> : <RadioButtonUncheckedIcon/>}{" "+subModuleNames[index]}
                                </div>
                             )
                        }
                        </Grid>
                    </Grid>
                    <Paper elevation={20} />
                </div>
              </Grid>
            </Grid>
        </div>

        :

        <div className="bg">
        <Navbar dashboard_link={"/"+location.state.type.toLowerCase()+"_dashboard"}/>
        <br/>
        <h1>{traineeName} has not been assigned any KT Trainings</h1 >
        </div>

    )

}

export default Trainee_progress