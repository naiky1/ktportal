from flask import request,jsonify
from functions import app_obj, send_email, email_list_formatter, User, Project, Users_Mapping, Modules, Sub_Modules, Trainee_Status, Feedback, Re_Assigned_Trainings, Questions, Options, Assessments
import json
from flask_cors import CORS
from datetime import datetime, timedelta
from sqlalchemy import or_
from random import randint
import time

app,db = app_obj()
CORS(app, origins=['http://localhost:3000', 'http://10.220.20.246:3000', 'http://pnq2000679:3000', 'http://crmktutility:3000'])

@app.route('/checkUser', methods=["GET"])
def checkUser():
    if request.method=="GET":
        email = request.args.get('email').lower()
        user_type = request.args.get('type')
        user = User.query.filter_by(User_Email=email, User_Type=user_type).first()
        if user is not None:
            return jsonify({'result': '0'})
        else:
            return jsonify({'result': '1'})


@app.route('/Login', methods=["POST","GET"])
def login():
    if request.method=="POST":
        details=request.get_json()
        email = details.get('email').lower()
        user_type = details.get('type')
        user = User.query.filter_by(User_Email=email, User_Type=user_type).first()
        if user is not None:
            n = 6
            otp=''.join(["{}".format(randint(0, 9)) for num in range(0, n)])
            user.User_OTP=otp
            db.session.commit()
            #Sending email to user               
            to_list=[str(user.User_Email)]
            cc_list=[]
            to_emails = email_list_formatter(to_list)
            cc_emails = email_list_formatter(cc_list)
            receivers_list=to_list+cc_list
            subject="Login OTP for CRM KT Portal : {}".format(user.User_OTP)
            body="Your One-Time-Password for logging into the CRM KT Portal is {}.".format(user.User_OTP)
            send_email(receivers_list,to_emails,cc_emails,subject,user.User_Name,body) 
            #  When multiple users were using the system it was found that this 5 minute approach slowed down the server. A potential solution is scheudling a job of 
            #  deleting the otp 5 minutes after the user logs in (like a cron job in unix systems). For now the OTP is valid till the user logs in/ requests another otp
            #time.sleep(300)
            #user.User_OTP=None
            #db.session.commit()
            return jsonify({'result': '0'})
        else:
            return jsonify({'result': '1'})


@app.route('/knowledge_base', methods=["POST","GET"])
def knowledge_base():
    if request.method == "GET":
        projectNames = []
        projectIds = []
        common_trainings_proj_ids = Project.query.filter_by(Project_Name="Basic Trainings").all()
        common_trainings_proj_ids = [item.Project_ID for item in common_trainings_proj_ids]
        names_result = Project.query.with_entities(Project.Project_Name).all()
        IDs_result = Project.query.with_entities(Project.Project_ID).all()
        # Storing Project Names
        for value in names_result:
            if value[0] != "Basic Trainings":
                projectNames.append(value[0])
        for value in IDs_result:
            if value[0] not in common_trainings_proj_ids:
                projectIds.append(value[0])

        all_projects_trainingnames = []
        all_projects_trainingpaths = []

        for proj_id in projectIds:
            training_names = []
            training_paths = []
            module_ids_for_submodule = []
            project_id = proj_id
            divisionName = Project.query.filter_by(Project_ID=project_id).first().Division
            commonprojectid = Project.query.filter_by(Division=divisionName).first().Project_ID
            for id in [commonprojectid, project_id]:
                modules_result = Modules.query.filter_by(Project_ID=id).all()
                for result in modules_result:
                    if result.Module_Path is not None:
                        training_names.append(result.Module_Name)
                        training_paths.append(result.Module_Path)
                    else:
                        module_ids_for_submodule.append(result.Module_ID)
                # modules = [result.serialize() for result in modules_result if result.Module_Path is not None]
                # moduleIds = []
                # for module in modules_result:
                #     moduleIds.append(module.Module_ID)
                sub_modules_result = Sub_Modules.query.filter(Sub_Modules.Module_ID.in_(module_ids_for_submodule)).all()
                for result in sub_modules_result:
                    if result.Sub_Module_Path is not None:
                        training_names.append(result.Sub_Module_Name)
                        training_paths.append(result.Sub_Module_Path)

            all_projects_trainingnames.append(training_names)
            all_projects_trainingpaths.append(training_paths)

        return jsonify({'projectNames': projectNames, 'training_names': all_projects_trainingnames, 'training_paths':all_projects_trainingpaths})


@app.route('/verifyOTP', methods=["POST","GET"])
def verifyOTP():
    if request.method=="GET":
        email = request.args.get('email').lower()
        user_type = request.args.get('type')
        otp = request.args.get('otp')
        user = User.query.filter_by(User_Email=email, User_Type=user_type, User_OTP=otp).first()
        if user is not None:
            user.User_OTP=None
            db.session.commit()
            return jsonify({'correctOtp': '1'})
        else:
            return jsonify({'correctOtp': '0'})

@app.route('/fetchAllProjectsDetails', methods=["POST", "GET"])
def fetchAllProjectsDetails():
    if request.method == "GET":
        projectNames=[]
        projectIds=[]
        common_trainings_proj_ids = Project.query.filter_by(Project_Name="Basic Trainings").all()
        common_trainings_proj_ids = [item.Project_ID for item in common_trainings_proj_ids]
        names_result = Project.query.with_entities(Project.Project_Name).all()
        IDs_result = Project.query.with_entities(Project.Project_ID).all()
        #Storing Project Names
        for value in names_result:
            if value[0] != "Basic Trainings":
                projectNames.append(value[0])
        for value in IDs_result:
            if value[0] not in common_trainings_proj_ids:
                projectIds.append(value[0])
        return jsonify({'projectNames':  projectNames, 'projectIds': projectIds})

@app.route('/fetchProjectDetails', methods=["GET"])
def fetchProjectDetails():
    if request.method == "GET":
        project_id = request.args.get('project_id')
        trainee_email = request.args.get('trainee_email').lower()
        traineeType = request.args.get('traineeType')
        trainee_id = User.query.filter_by(User_Email=trainee_email, User_Type="Trainee").first().User_ID
        projectspecificprojectId = request.args.get('projectspecificprojectId')
        project_result = Project.query.filter_by(Project_ID=project_id).all()
        project = [result.serialize() for result in project_result]
        modules_result = Modules.query.filter_by(Project_ID=project_id).all()
        modules = [result.serialize() for result in modules_result]
        moduleIds=[]
        trainingIds=[]
        for module in modules_result:
            moduleIds.append(module.Module_ID)
        if projectspecificprojectId:
            project_id_ttm = projectspecificprojectId
        else:
            project_id_ttm = project_id

        ttmp_mapping_id = Users_Mapping.query.filter_by(Trainee_ID=trainee_id, Trainee_Type=traineeType,
                                                        Project_ID=project_id_ttm,
                                                        Last_Active_On="Present").first().TTMP_Mapping_ID
        trainee_status_results = Trainee_Status.query.filter(Trainee_Status.Module_ID.in_(moduleIds), Trainee_Status.Trainee_ID==trainee_id, Trainee_Status.TTMP_Mapping_ID==ttmp_mapping_id).all()
        for trainee_status_result in trainee_status_results:
            trainingIds.append(trainee_status_result.TMSM_Mapping_ID)
        re_assigned_trainings = Re_Assigned_Trainings.query.filter(Re_Assigned_Trainings.TMSM_Mapping_ID.in_(trainingIds)).all()
        reAssignedTrainings = [result.serialize() for result in re_assigned_trainings]
        traineeStatus = [result.serialize() for result in trainee_status_results]
        sub_modules_result = Sub_Modules.query.filter(Sub_Modules.Module_ID.in_(moduleIds)).all()
        sub_modules = [result.serialize() for result in sub_modules_result]

        combined_data = {'traineeStatus': traineeStatus,'project': project, 'modules': modules, 'subModules' : sub_modules, 'reAssignedTrainings':reAssignedTrainings}
        return json.dumps(combined_data)

@app.route('/fetchTraineeCommonTrainingProjectId', methods=["POST", "GET"])
def fetchTraineeCommonTrainingProjectId():
    if request.method == "GET":
        email = request.args.get('email').lower()
        project_id = request.args.get('projectId')
        traineetype = request.args.get('traineeType')
        user_id = User.query.filter_by(User_Email=email, User_Type="Trainee").first().User_ID
        if not project_id:
            try:
                project_id = Users_Mapping.query.filter_by(Trainee_ID=user_id, Last_Active_On="Present", Reverse_KT_Flag="N").first().Project_ID
            except:
                project_id = Users_Mapping.query.filter_by(Trainee_ID=user_id, Last_Active_On="Present").order_by(Users_Mapping.TTMP_Mapping_ID.desc()).first().Project_ID
        if not traineetype:
            try:
                traineetype = Users_Mapping.query.filter_by(Trainee_ID=user_id, Last_Active_On="Present", Reverse_KT_Flag="N").first().Trainee_Type
            except:
                traineetype = Users_Mapping.query.filter_by(Trainee_ID=user_id, Last_Active_On="Present").order_by(Users_Mapping.TTMP_Mapping_ID.desc()).first().Trainee_Type
        divisionName = Project.query.filter_by(Project_ID=project_id).first().Division
        commonprojectid = Project.query.filter_by(Division=divisionName).first().Project_ID
        return jsonify({'commonprojectid': commonprojectid, 'divisionName':divisionName, 'traineetype':traineetype, 'projectspecificprojectId':project_id})

@app.route('/fetchTraineeCurrentProjectId', methods=["POST", "GET"])
def fetchTraineeCurrentProjectId():
    if request.method == "GET":
        email = request.args.get('email').lower()
        user_id = User.query.filter_by(User_Email=email, User_Type="Trainee").first().User_ID
        if request.args.get('projectspecificprojectId'):
            project_id = request.args.get('projectspecificprojectId')
        else:
            project_id = Users_Mapping.query.filter_by(Trainee_ID=user_id,Last_Active_On="Present").first().Project_ID
        projectName = Project.query.filter_by(Project_ID=project_id).first().Project_Name
        return jsonify({'projectId': project_id, 'projectName':projectName})

@app.route('/signOffComponent', methods=["POST"])
def signOffComponent():
    if request.method == "POST":
        details=request.get_json()
        type=details.get("type")
        if type=="NRT":
            mapping = Trainee_Status.query.filter_by(TMSM_Mapping_ID=details.get("Mapping_ID")).first()     
        elif type == "RT":
            mapping = Re_Assigned_Trainings.query.filter_by(RT_ID=details.get("Mapping_ID")).first()
        mapping.Signed_Off_Flag = "Y"
        mapping.Completion_Date = str(datetime.today().strftime('%Y-%m-%d'))
        db.session.commit()
        return jsonify({'response': 'Sign-off successful!'})
    
@app.route('/checkComponent', methods=["POST"])
def checkComponent():
    if request.method == "POST":
        details=request.get_json()
        type=details.get("type")
        if type=="NRT":
            mapping = Trainee_Status.query.filter_by(TMSM_Mapping_ID=details.get("Mapping_ID")).first()
            mapping.Checked_Flag="Y"
        elif type == "RT":
            mapping = Re_Assigned_Trainings.query.filter_by(RT_ID=details.get("Mapping_ID")).first()
            mapping.Checked_Flag = "Y"
        db.session.commit()
        return jsonify({'response': 'Sign-off successful!'})

@app.route('/fetchUserName', methods=["GET"])
def fetchUserName():
    if request.method == "GET":
        try:
            email = request.args.get('userEmail').lower()
            user_type = request.args.get('userType')
            userName = User.query.filter_by(User_Email=email, User_Type=user_type).first().User_Name
            return jsonify({'userName': userName})
        except:
            user_id = request.args.get('userId')
            userName = User.query.filter_by(User_ID=user_id).first().User_Name
            return jsonify({'userName': userName})
        
@app.route('/fetchMappingNames', methods=["GET"])
def fetchMappingNames():
    if request.method == "GET":
        email = request.args.get('trainee_email').lower()
        trainee = User.query.filter_by(User_Email=email, User_Type="Trainee").first()
        trainerID = Users_Mapping.query.filter_by(Trainee_ID=trainee.User_ID, Last_Active_On="Present").first().Trainer_ID
        managerID = Users_Mapping.query.filter_by(Trainee_ID=trainee.User_ID, Last_Active_On="Present").first().Manager_ID
        trainerName=User.query.filter_by(User_ID=trainerID).first().User_Name
        managerName=User.query.filter_by(User_ID=managerID).first().User_Name
        return jsonify({'traineeName':trainee.User_Name,'trainerName':trainerName,'managerName':managerName})

@app.route('/fetchTrainerTableDetails', methods=["GET"])
def fetchTrainerTableDetails():
    if request.method == "GET":
        email = request.args.get('userEmail').lower()
        trainerId = User.query.filter_by(User_Email=email,User_Type="Trainer").first().User_ID
        usersmappings = Users_Mapping.query.filter_by(Trainer_ID=trainerId, Last_Active_On="Present").all()
        traineeIds=[]
        traineeStatuses=[]
        for mapping in usersmappings:
            traineeIds.append(mapping.Trainee_ID)
        traineeresults = User.query.filter(User.User_ID.in_(traineeIds), User.User_Type=="Trainee").all()
        traineeNames=[]
        traineeIds=[]
        traineeEmails=[]
        traineeFeedbacks=[]
        reverseKtStatuses=[]
        traineeProjectIds=[]
        traineeProjectNames = []
        trainingRequirementTypes = []
        for trainee in usersmappings:
            try:
                # trainee_project_id = Users_Mapping.query.filter(Users_Mapping.Trainer_ID==trainerId,Users_Mapping.Trainee_ID==trainee.User_ID,Users_Mapping.Last_Active_On=="Present",Users_Mapping.Project_ID!=0).first().Project_ID
                # traineeProjectIds.append(trainee_project_id)
                traineeProjectIds.append(trainee.Project_ID)
                # traineeprojectName = Project.query.filter_by(Project_ID=trainee_project_id).first().Project_Name
                traineeprojectName = Project.query.filter_by(Project_ID=trainee.Project_ID).first().Project_Name
                traineeProjectNames.append(traineeprojectName)
                trainingRequirementTypes.append(trainee.Trainee_Type)
                trainee_feedbacks_result = Feedback.query.filter_by(Sender_User_ID=trainerId, Receiver_User_ID=trainee.Trainee_ID).order_by(Feedback.Feedback_Date.desc()).all()
                trainee_feedbacks_element=[]
                for feedback in trainee_feedbacks_result:
                    date = datetime.strptime(feedback.Feedback_Date,"%Y-%m-%d %H:%M:%S")
                    formatted_date = date.strftime("%d-%m-%y (%I:%M %p)")
                    trainee_feedbacks_element.append(formatted_date + " : " + feedback.Feedback)
                traineeFeedbacks.append(trainee_feedbacks_element)

                projectId = Users_Mapping.query.filter_by(Trainee_ID=trainee.Trainee_ID,Last_Active_On="Present").first().Project_ID
                division = Project.query.filter_by(Project_ID=projectId).first().Division
                BasicTrainingProjectId = Project.query.filter_by(Division=division,
                                                                 Project_Name="Basic Trainings").first().Project_ID
                modules= Modules.query.filter(or_(Modules.Project_ID==BasicTrainingProjectId,Modules.Project_ID==projectId)).all()
                moduleIds=[]
                for module in modules:
                    moduleIds.append(module.Module_ID)
                #There could be multiple assigned trainings due to project change,but only the trainings associated with the project assigned at present must be considered
                trainee_status_results = Trainee_Status.query.filter(Trainee_Status.Trainee_ID==trainee.Trainee_ID,Trainee_Status.Module_ID.in_(moduleIds), Trainee_Status.Ongoing_Training=="Yes").all()

                reverse_kt_status = Users_Mapping.query.filter_by(Trainee_ID=trainee.Trainee_ID, Trainer_ID=trainerId, Last_Active_On="Present").first().Reverse_KT_Flag
                reverseKtStatuses.append(reverse_kt_status)
                traineeStatusesLength = len(traineeStatuses)
                if trainee_status_results==[]:
                    traineeStatuses.append("N")
                else:
                    if reverse_kt_status == "Y":
                        traineeStatuses.append("N")
                    else:
                        for trainee_status_result in trainee_status_results:
                            if trainee_status_result.Signed_Off_Flag!="Y":
                                traineeStatuses.append("N")
                                break
                        #If the traineeStatusesLength is the same as before then nothing was appened indicating that the reverse KT is not already done and all trainings are complete.
                        if traineeStatusesLength == len(traineeStatuses):
                            traineeStatuses.append("Y")
            except:
                traineeFeedbacks=[]
                for i in range(0,len(traineeresults)):
                    traineeStatuses.append("N")
            traineeIds.append(trainee.Trainee_ID)
            userName = User.query.filter(User.User_ID==trainee.Trainee_ID).first().User_Name
            traineeNames.append(userName)
            userEmail = User.query.filter(User.User_ID == trainee.Trainee_ID).first().User_Email
            traineeEmails.append(userEmail)
        return jsonify({'traineeIds':traineeIds, 'traineeNames': traineeNames, 'traineeProjectNames':traineeProjectNames, 'trainingRequirementTypes':trainingRequirementTypes, 'traineeEmails':traineeEmails,'traineeFeedbacks':traineeFeedbacks, 'traineeStatuses':traineeStatuses, 'reverseKtStatuses':reverseKtStatuses, 'traineeProjectIds':traineeProjectIds})

@app.route('/fetchOverallProgress', methods=["POST", "GET"])
def fetchOverallProgress():
    if request.method == "GET":
        trainee_id = request.args.get('userId')
        projectId = request.args.get('projectId')
        traineeType = request.args.get('traineeType')
        try:
            # projectId = Users_Mapping.query.filter_by(Trainee_ID=trainee_id,Last_Active_On="Present").first().Project_ID
            division = Project.query.filter_by(Project_ID=projectId).first().Division
            BasicTrainingProjectId = Project.query.filter_by(Division=division,
                                                             Project_Name="Basic Trainings").first().Project_ID
            modules= Modules.query.filter(or_(Modules.Project_ID==BasicTrainingProjectId,Modules.Project_ID==projectId)).all()
            moduleIds=[]
            for module in modules:
                moduleIds.append(module.Module_ID)
            ttmp_mapping_id = Users_Mapping.query.filter_by(Trainee_ID=trainee_id, Trainee_Type=traineeType, Project_ID=projectId, Last_Active_On="Present").first().TTMP_Mapping_ID
            reverse_kt_done = Users_Mapping.query.filter_by(TTMP_Mapping_ID=ttmp_mapping_id).first().Reverse_KT_Flag
            #There could be multiple assigned trainings due to project change,but only the trainings associated with the project assigned at present must be considered 
            all_mappings = Trainee_Status.query.filter(Trainee_Status.Trainee_ID==trainee_id,Trainee_Status.Module_ID.in_(moduleIds),Trainee_Status.Due_Date!="Not Required", Trainee_Status.TTMP_Mapping_ID==ttmp_mapping_id).all()
            allModuleIds=[]
            incompleteModules = 0
            KT_start_date_comparator = datetime.strptime(all_mappings[0].Due_Date, "%Y-%m-%d")
            KT_completion_date_comparator = datetime.strptime(all_mappings[0].Due_Date, "%Y-%m-%d")
            KT_start_date = datetime.strptime(all_mappings[0].Due_Date, "%Y-%m-%d")
            KT_completion_date = datetime.strptime(all_mappings[0].Due_Date, "%Y-%m-%d")
            for mapping in all_mappings:
                b = datetime.strptime(mapping.Due_Date, "%Y-%m-%d")
                allModuleIds.append(mapping.Module_ID)
                if b>=KT_completion_date_comparator:
                    KT_completion_date_comparator=b
                    KT_completion_date=mapping.Due_Date
                if b<=KT_start_date_comparator:
                    KT_start_date_comparator=b
                    KT_start_date=mapping.Due_Date
            allModuleIds=set(allModuleIds)
            allModules =len(allModuleIds)
            for moduleId in allModuleIds:
                all_module_mappings = Trainee_Status.query.filter_by(Trainee_ID=trainee_id, Module_ID=moduleId, TTMP_Mapping_ID=ttmp_mapping_id).all()
                for module_mapping in all_module_mappings:
                    if(module_mapping.Signed_Off_Flag=="N"):
                        incompleteModules+=1
                        break
            overall_progress = int(100*(allModules-incompleteModules)/allModules)
            d1 = datetime.strptime(str(KT_completion_date), "%Y-%m-%d")
            d2 = datetime.strptime(str(datetime.today().strftime('%Y-%m-%d')), "%Y-%m-%d")
            d3 = datetime.strptime(str(KT_start_date), "%Y-%m-%d")
            KT_start_date = d3-timedelta(days=2)
            day = KT_start_date.weekday()
            if day == 6:
                KT_start_date-=timedelta(days=2)
            elif day == 5:
                KT_start_date-=timedelta(days=1)
            total_days = abs(d1-KT_start_date).days
            if((d1 - d2).days<0):
                days_remaining=0
            else:
                days_remaining = (d1 - d2).days
            Kt_Percentage_Completion = int(100*(total_days-days_remaining)/total_days)
            kt_found=1
        except Exception as e:
            kt_found=0
            overall_progress=0
            KT_completion_date = datetime.today().strftime('%Y-%m-%d')
            days_remaining=0
            Kt_Percentage_Completion=0
        return jsonify({'overall_progress': overall_progress, 'KT_completion_date': str(KT_completion_date), 'days_remaining': days_remaining, 'Kt_Percentage_Completion':Kt_Percentage_Completion, 'kt_found' : kt_found, 'reverse_kt_done' : reverse_kt_done})

@app.route('/fetchMissedTrainings', methods=["POST", "GET"])
def fetchMissedTrainings():
    if request.method == "GET":
        trainee_id = request.args.get('userId')
        projectId = request.args.get('projectId')
        traineeType = request.args.get('traineeType')
        ttmp_mapping_id = Users_Mapping.query.filter_by(Trainee_ID=trainee_id, Trainee_Type=traineeType,
                                                        Project_ID=projectId,
                                                        Last_Active_On="Present").first().TTMP_Mapping_ID
        # projectId = Users_Mapping.query.filter_by(Trainee_ID=trainee_id,Last_Active_On="Present").first().Project_ID
        division = Project.query.filter_by(Project_ID=projectId).first().Division
        BasicTrainingProjectId = Project.query.filter_by(Division=division,
                                                         Project_Name="Basic Trainings").first().Project_ID
        modules= Modules.query.filter(or_(Modules.Project_ID==BasicTrainingProjectId,Modules.Project_ID==projectId)).all()
        moduleIds=[]
        for module in modules:
            moduleIds.append(module.Module_ID)
        #There could be multiple assigned trainings due to project change,but only the trainings associated with the project assigned at present must be considered 
        all_incomplete_trainings = Trainee_Status.query.filter(Trainee_Status.Trainee_ID==trainee_id,Trainee_Status.Module_ID.in_(moduleIds),Trainee_Status.Signed_Off_Flag=="N", Trainee_Status.TTMP_Mapping_ID==ttmp_mapping_id).all()
        all_complete_trainings = Trainee_Status.query.filter(Trainee_Status.Trainee_ID==trainee_id,Trainee_Status.Module_ID.in_(moduleIds),Trainee_Status.Signed_Off_Flag=="Y", Trainee_Status.TTMP_Mapping_ID==ttmp_mapping_id).all()
        today = datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)
        missedTrainingsIncomplete = 0
        missedTrainingsNames = []
        missedTrainingsresult= []
        try:
            for mapping in all_incomplete_trainings:
                due_date = datetime.strptime(mapping.Due_Date, "%Y-%m-%d")
                if due_date<today:
                    missedTrainingsresult.append(mapping)
                    missedTrainingsIncomplete += 1
            for mapping in all_complete_trainings:
                due_date = datetime.strptime(mapping.Due_Date, "%Y-%m-%d")
                completion_date = datetime.strptime(mapping.Completion_Date, "%Y-%m-%d")
                if completion_date>due_date:
                    missedTrainingsresult.append(mapping)

            for missedTraining in missedTrainingsresult:
                name = Modules.query.filter_by(Module_ID=missedTraining.Module_ID).first().Module_Name
                if missedTraining.Sub_Module_ID:
                    sub_module_name = Sub_Modules.query.filter_by(Sub_Module_ID=missedTraining.Sub_Module_ID).first().Sub_Module_Name
                    name += " - " + sub_module_name
                missedTrainingsNames.append(name)
            missedTrainings= [result.serialize() for result in missedTrainingsresult]

            missedTrainingsPercentage = int(100*missedTrainingsIncomplete/len(missedTrainings))
        except:
            missedTrainings=[]
            missedTrainingsNames=[]
            missedTrainingsPercentage=0

        return jsonify({'missed_trainings': missedTrainings, 'missed_trainings_names': missedTrainingsNames, 'missed_trainings_percentage': missedTrainingsPercentage})

@app.route('/fetchTrainings', methods=["POST", "GET"])
def fetchTrainings():
    if request.method == "GET":
        project_id = request.args.get('projectId')
        trainee_id = request.args.get('userId')
        trainee_type = request.args.get('traineeType')
        projectspecificprojectId = request.args.get('projectspecificprojectId')
        if projectspecificprojectId:
            project_id_ttm = projectspecificprojectId
        else:
            project_id_ttm = project_id
        ttmp_mapping_id = Users_Mapping.query.filter_by(Trainee_ID=trainee_id, Trainee_Type=trainee_type,
                                                    Project_ID=project_id_ttm,
                                                    Last_Active_On="Present").first().TTMP_Mapping_ID
        # trainee_type = Users_Mapping.query.filter_by(Trainee_ID=trainee_id).first().Trainee_Type
        # if trainee_type == "Only Mandatory Modules":
        #     modules_result = Modules.query.filter_by(Project_ID=project_id, Mandatory="Yes").all()
        # else:
        #     modules_result = Modules.query.filter_by(Project_ID=project_id).all()
        smPieChartData=[]
        subModuleNames=[]
        subModuleStatuses=[]
        moduleCompletionPercentage=[]
        moduleIds = []
        trainingNames = []
        trainingStatuses = []
        Incomplete_Module_IDs = []
        completed_modules = 0
        incomplete_modules = 0

        modules_all_trainings = Modules.query.filter_by(Project_ID=project_id).all()
        modules_all_trainings_ids = [module.Module_ID for module in modules_all_trainings]
        # trainee_modules_result = Trainee_Status.query.filter(Trainee_Status.Trainee_ID == trainee_id,
        #                                                Trainee_Status.Mandatory == "Yes", Trainee_Status.TTMP_Mapping_ID==ttmp_mapping_id).all()
        trainee_modules_result = Trainee_Status.query.filter(Trainee_Status.Mandatory == "Yes",
                                                             Trainee_Status.TTMP_Mapping_ID == ttmp_mapping_id).all()
        for module in trainee_modules_result:
            id = module.Module_ID
            if id in modules_all_trainings_ids:
                moduleIds.append(id)
        moduleIds = list(set(moduleIds))
        # all_trainings = Trainee_Status.query.filter(Trainee_Status.Module_ID.in_(moduleIds), Trainee_Status.Trainee_ID==trainee_id, Trainee_Status.Due_Date!="Not Required", Trainee_Status.TTMP_Mapping_ID==ttmp_mapping_id).all()
        all_trainings = Trainee_Status.query.filter(Trainee_Status.Due_Date != "Not Required",
                                                    Trainee_Status.TTMP_Mapping_ID == ttmp_mapping_id).all()
        for training in all_trainings:
            if training.Signed_Off_Flag=="N":
                Incomplete_Module_IDs.append(training.Module_ID)
                continue

        Incomplete_Module_IDs = set(Incomplete_Module_IDs)  # Removing duplicates

        for moduleID in moduleIds:
            name = Modules.query.filter_by(Module_ID=moduleID).first().Module_Name
            trainingNames.append(name)
            if moduleID in Incomplete_Module_IDs:
                trainingStatuses.append("N")
            else:
                trainingStatuses.append("Y")
            smPieChartDataElement = []
            subModuleNamesElement = []
            subModuleStatusesElement = []
            completed_sub_modules_count = 0
            complete_module_trainings = Trainee_Status.query.filter_by(Trainee_ID=trainee_id, Module_ID=moduleID, TTMP_Mapping_ID=ttmp_mapping_id).all()
            all_module_trainings = []
            for training in complete_module_trainings:
                if training.Due_Date != "Not Required":
                    all_module_trainings.append(training)
            for module_training in all_module_trainings:
                if module_training.Due_Date != "Not Required":
                    subModuleStatusesElement.append(module_training.Signed_Off_Flag)
                    if module_training.Signed_Off_Flag == "Y":
                        completed_sub_modules_count += 1
                    if module_training.Sub_Module_ID != None:
                        subModule = Sub_Modules.query.filter_by(Sub_Module_ID=module_training.Sub_Module_ID).first()
                        subModuleNamesElement.append(subModule.Sub_Module_Name)
                    else:
                        subModuleNamesElement.append(name)
            smPieChartDataElement.append([{"name": "Completed", "value": completed_sub_modules_count},
                                          {"name": "Incomplete",
                                           "value": len(all_module_trainings) - completed_sub_modules_count}])
            smPieChartData.append([smPieChartDataElement])
            subModuleNames.append([subModuleNamesElement])
            subModuleStatuses.append([subModuleStatusesElement])
            try:
                moduleCompletionPercentage.append([int(100 * completed_sub_modules_count / len(all_module_trainings))])
            except:
                moduleCompletionPercentage.append([0])
        for trainingStatus in trainingStatuses:
            if trainingStatus=="N":
                incomplete_modules+=1
            else:
                completed_modules+=1
        try:
            completion_percentage = int(100 * completed_modules / (incomplete_modules+completed_modules) )
        except:
            completion_percentage = 0
        return jsonify({'completed_modules':completed_modules, 'incomplete_modules':incomplete_modules,
                        'completion_percentage': completion_percentage, 'trainingNames': trainingNames,
                        'trainingStatuses': trainingStatuses, 'smPieChartData':smPieChartData, 'subModuleNames':subModuleNames,
                        'subModuleStatuses':subModuleStatuses, 'moduleCompletionPercentage':moduleCompletionPercentage })

#Adding Feedback and also for giving final sign off to trainee
@app.route('/addFeedback', methods=["POST"])
def addFeedback():
    if request.method == "POST":
        details = request.get_json()
        feedbackDate = datetime.now()
        #Don't require microsecond data
        feedbackDate = feedbackDate.replace(microsecond=0)
        #SenderId is the user ID of the trainer as the the trainer is the sender of the feedback. Similarly, trainee's User ID is the Reciever_ID
        senderId= User.query.filter_by(User_Email=details.get("senderEmail").lower(), User_Type=details.get("senderType")).first().User_ID
        feedback = Feedback(Sender_User_ID=senderId, Receiver_User_ID=details.get("Receiver_ID"), Feedback_Date=feedbackDate, Feedback=details.get("feedback"))
        db.session.add(feedback)
        if details.get("rKtActive") == True:
            traineeId = details.get("Receiver_ID")
            mapping = Users_Mapping.query.filter_by(Trainer_ID=senderId,Trainee_ID=details.get("Receiver_ID"),Last_Active_On="Present").first()
            mapping.Reverse_KT_Flag = "Y"
            trainee_modules_mappings = Trainee_Status.query.filter_by(Trainee_ID=details.get("Receiver_ID"), Ongoing_Training="Yes").all()
            for mapping in trainee_modules_mappings:
                mapping.Ongoing_Training = "No"
            db.session.commit()
            #Sending email to manager,trainer and trainee
            trainerId = Users_Mapping.query.filter_by(Trainee_ID=traineeId,Last_Active_On="Present").first().Trainer_ID
            managerId = Users_Mapping.query.filter_by(Trainee_ID=traineeId,Last_Active_On="Present").first().Manager_ID
            projectId = Users_Mapping.query.filter_by(Trainee_ID=traineeId,Last_Active_On="Present").first().Project_ID
            trainee = User.query.filter_by(User_ID=traineeId).first()
            trainer = User.query.filter_by(User_ID=trainerId).first()
            manager = User.query.filter_by(User_ID=managerId).first()
            projectName = Project.query.filter_by(Project_ID=projectId).first().Project_Name
            to_list=[str(manager.User_Email)]
            cc_list=[str(trainer.User_Email),str(trainee.User_Email)]
            to_emails = email_list_formatter(to_list)
            cc_emails = email_list_formatter(cc_list)
            receivers_list=to_list+cc_list
            subject="Final sign off given for {}".format(trainee.User_Name)
            body="The final sign off for KT Trainings has been given for {} (Basic and {} Trainings) by {}.".format(trainee.User_Name,projectName,trainer.User_Name)
            send_email(receivers_list,to_emails,cc_emails,subject,manager.User_Name,body)
            return jsonify({'response': 'Reverse-KT successful!'})
        else:
            db.session.commit()
            return jsonify({'response': 'Feedback added successfully!'})

@app.route('/ReAssignTrainings', methods=["POST"])
def ReAssignTrainings():
    if request.method == "POST":
        details = request.get_json()
        trainingIds = details.get("reAssignedTrainingIds")
        trainingIds.sort()
        dueDate = details.get("dueDate")
        dueDate = datetime.strptime(dueDate,'%Y-%m-%d').strftime('%Y-%m-%d') #Keeps date in YYYY-MM-DD Format in the database
        tmsmMappingId=trainingIds[0]
        for trainingId in trainingIds:
            ReAssignedTraining = Re_Assigned_Trainings(TMSM_Mapping_ID=trainingId,Due_Date=dueDate)
            db.session.add(ReAssignedTraining)
            db.session.commit()
        #Sending email to trainer and trainee
        traineeId = Trainee_Status.query.filter_by(TMSM_Mapping_ID=tmsmMappingId).first().Trainee_ID
        trainerId = Users_Mapping.query.filter_by(Trainee_ID=traineeId,Last_Active_On="Present").first().Trainer_ID
        trainee = User.query.filter_by(User_ID=traineeId).first()
        trainer = User.query.filter_by(User_ID=trainerId).first()
        to_list=[str(trainee.User_Email)]
        cc_list=[str(trainer.User_Email)]
        to_emails = email_list_formatter(to_list)
        cc_emails = email_list_formatter(cc_list)
        receivers_list=to_list+cc_list
        subject="Re-Assigned Trainings"
        body="Your trainer ({}) has re-assigned you {} training(s). Please login to the CRM KT Portal and complete them as per the assigned due date(s).".format(trainer.User_Name,len(trainingIds))
        send_email(receivers_list,to_emails,cc_emails,subject,trainee.User_Name,body)
        return jsonify({'response': 'Training(s) Re-Assigned Successfully!'})

@app.route('/fetchReAssignedTrainings', methods=["GET"])
def fetchReAssignedTrainings():
    if request.method == "GET":
        project_id = request.args.get('project_id')
        trainee_email = request.args.get('trainee_email').lower()
        traineeType = request.args.get('traineeType')
        trainee_id = User.query.filter_by(User_Email=trainee_email, User_Type="Trainee").first().User_ID

        #First, fetch all the trainings assigned to this trainee
        # projectId = Users_Mapping.query.filter_by(Trainee_ID=trainee_id,Last_Active_On="Present").first().Project_ID
        division = Project.query.filter_by(Project_ID=project_id).first().Division
        BasicTrainingProjectId = Project.query.filter_by(Division=division, Project_Name="Basic Trainings").first().Project_ID
        modules= Modules.query.filter(or_(Modules.Project_ID==BasicTrainingProjectId,Modules.Project_ID==project_id)).all()
        moduleIds=[]
        for module in modules:
            moduleIds.append(module.Module_ID)
        ttmp_mapping_id = Users_Mapping.query.filter_by(Trainee_ID=trainee_id, Trainee_Type=traineeType,
                                                        Project_ID=project_id,
                                                        Last_Active_On="Present").first().TTMP_Mapping_ID
        #There could be multiple assigned trainings due to project change,but only the trainings associated with the project assigned at present must be considered 
        all_trainings = Trainee_Status.query.filter(Trainee_Status.Trainee_ID==trainee_id,Trainee_Status.Module_ID.in_(moduleIds), Trainee_Status.TTMP_Mapping_ID==ttmp_mapping_id).all()
        all_training_ids=[]
        re_assigned_training_ids=[]
        types=[]
        moduleIds=[]
        subModuleIds=[]
        try:
            for training in all_trainings:
                all_training_ids.append(training.TMSM_Mapping_ID)
            # Next, check which among these have been re-assigned
            re_assigned_trainings_result = Re_Assigned_Trainings.query.filter(Re_Assigned_Trainings.TMSM_Mapping_ID.in_(all_training_ids)).all()
            for re_assigned_training in re_assigned_trainings_result:
                re_assigned_training_ids.append(re_assigned_training.TMSM_Mapping_ID)
                re_assigned_training_module_id = Trainee_Status.query.filter_by(TMSM_Mapping_ID=re_assigned_training.TMSM_Mapping_ID).first().Module_ID
                moduleIds.append(re_assigned_training_module_id)
                re_assigned_training_sub_module_id = Trainee_Status.query.filter_by(TMSM_Mapping_ID=re_assigned_training.TMSM_Mapping_ID).first().Sub_Module_ID
                subModuleIds.append(re_assigned_training_sub_module_id)              
            modules_result = Modules.query.filter(Modules.Module_ID.in_(moduleIds)).all()
            modules = [result.serialize() for result in modules_result]
            sub_modules_result = Sub_Modules.query.filter(Sub_Modules.Sub_Module_ID.in_(subModuleIds)).all()
            sub_modules = [result.serialize() for result in sub_modules_result]
            filtered_trainings_result = []
            for training_id in re_assigned_training_ids:
                filtered_trainings_result.append(Trainee_Status.query.filter_by(TMSM_Mapping_ID=training_id).first())
            for filtered_training in filtered_trainings_result:
                filtered_training_module_id = Trainee_Status.query.filter_by(TMSM_Mapping_ID=filtered_training.TMSM_Mapping_ID).first().Module_ID              
                filtered_training_project_id = Modules.query.filter_by(Module_ID=filtered_training_module_id).first().Project_ID
                type = Project.query.filter_by(Project_ID=filtered_training_project_id).first().Project_Name
                types.append(type)
            filtered_trainings = [result.serialize() for result in filtered_trainings_result]
            re_assigned_trainings = [result.serialize() for result in re_assigned_trainings_result]
        except:
            re_assigned_trainings = []
            types = []
            modules = []
            sub_modules = []
            filtered_trainings=[]
        combined_data = {'traineeStatus':filtered_trainings,'reAssignedTrainings': re_assigned_trainings, 'types':types, 'modules':modules, 'subModules':sub_modules}
        return json.dumps(combined_data)

@app.route('/ChangeReAssignedTrainingsDueDates', methods=["POST"])
def ChangeReAssignedTrainingsDueDates():
    if request.method == "POST":
        details=request.get_json()
        reAssignedTrainingsIds = details.get("alteredReAssignedTrainingIds")
        reAssignedTrainingsDueDates = details.get("dueDates")
        alteredBtTrainingIds= details.get("alteredBtTrainingIds")
        btTrainingsDueDates= details.get("btTrainingsDueDates")
        alteredPstTrainingIds= details.get("alteredPstTrainingIds")
        pstTrainingsDueDates= details.get("pstTrainingsDueDates")
        alteredExpandedTrainingIds=details.get("alteredExpandedTrainingIds")
        expandedTrainingsDueDates=details.get("expandedTrainingsDueDates")
        alteredProjectExpandedTrainingIds=details.get("alteredProjectExpandedTrainingIds")
        projectExpandedTrainingsDueDates =details.get("projectExpandedTrainingsDueDates")
        try:     
            alteredTrainingIds=alteredBtTrainingIds+alteredExpandedTrainingIds+alteredPstTrainingIds+alteredProjectExpandedTrainingIds
            alteredDueDates=btTrainingsDueDates+expandedTrainingsDueDates+pstTrainingsDueDates+projectExpandedTrainingsDueDates
            oldTrainingDueDates=[]
            for (alteredTrainingId,trainingDueDate) in zip(alteredTrainingIds,alteredDueDates):
                training = Trainee_Status.query.filter_by(TMSM_Mapping_ID=alteredTrainingId).first()
                oldTrainingDueDates.append(training.Due_Date)
                training.Mandatory = "Yes"
                training.Due_Date = trainingDueDate
                training.Checked_Flag = "N"
                training.Signed_Off_Flag = "N"
            db.session.commit()    
            #Sending email to manager,trainer and trainee
            traineeId=Trainee_Status.query.filter_by(TMSM_Mapping_ID=alteredTrainingIds[0]).first().Trainee_ID
            trainerId = Users_Mapping.query.filter_by(Trainee_ID=traineeId,Last_Active_On="Present").first().Trainer_ID   
            managerId = Users_Mapping.query.filter_by(Trainee_ID=traineeId,Last_Active_On="Present").first().Manager_ID 
            projectId= Users_Mapping.query.filter_by(Trainee_ID=traineeId,Last_Active_On="Present").first().Project_ID
            division = Project.query.filter_by(Project_ID=projectId).first().Division
            BasicTrainingProjectId = Project.query.filter_by(Division=division,
                                                             Project_Name="Basic Trainings").first().Project_ID
            trainee = User.query.filter_by(User_ID=traineeId).first()
            trainer = User.query.filter_by(User_ID=trainerId).first()
            manager = User.query.filter_by(User_ID=managerId).first()      
            trainings = Trainee_Status.query.filter(Trainee_Status.TMSM_Mapping_ID.in_(alteredTrainingIds)).all()
            trainingChanges=""
            for (training,oldDueDate) in zip(trainings,oldTrainingDueDates):
                if training.Sub_Module_ID==None:
                    trainingName = Modules.query.filter_by(Module_ID=training.Module_ID).first().Module_Name
                else:
                    trainingName = "".join([Modules.query.filter_by(Module_ID=training.Module_ID).first().Module_Name,Sub_Modules.query.filter(Sub_Modules.Sub_Module_ID==training.Sub_Module_ID).first().Sub_Module_Name.join([" (",")"])]) 
                trainingProjectId=Modules.query.filter_by(Module_ID=training.Module_ID).first().Project_ID
                projectName = Project.query.filter_by(Project_ID=trainingProjectId).first().Project_Name
                trainingChange=" - ".join([" - ".join([projectName,trainingName])," --> ".join([oldDueDate,training.Due_Date])])
                trainingChanges="\n\n".join([trainingChanges,trainingChange])
            basicTrainingsName = Project.query.filter_by(Project_ID=BasicTrainingProjectId).first().Project_Name
            currentProjectName = Project.query.filter_by(Project_ID=projectId).first().Project_Name
            to_list=[str(trainee.User_Email)]
            cc_list=[str(manager.User_Email),str(trainer.User_Email)]
            to_emails = email_list_formatter(to_list)
            cc_emails = email_list_formatter(cc_list)
            receivers_list=to_list+cc_list          
            subject="Due Dates of {} Trainings changed for {}".format(len(alteredTrainingIds),trainee.User_Name)
            body="{} due date(s) was/were changed in {} and {} due date(s) was/were changed in {} Trainings. \n {}".format(len(alteredBtTrainingIds)+len(alteredExpandedTrainingIds),basicTrainingsName,len(alteredPstTrainingIds)+len(alteredProjectExpandedTrainingIds),currentProjectName,trainingChanges)
            send_email(receivers_list,to_emails,cc_emails,subject,trainee.User_Name,body) 
        except:
            oldTrainingDueDates=[]
            for (reAssignedTrainingsId,reAssignedTrainingsDueDate) in zip(reAssignedTrainingsIds,reAssignedTrainingsDueDates):
                reAssignedTraining = Re_Assigned_Trainings.query.filter_by(RT_ID=reAssignedTrainingsId).first()
                oldTrainingDueDates.append(reAssignedTraining.Due_Date)
                reAssignedTraining.Due_Date=reAssignedTrainingsDueDate
            db.session.commit()
            #Sending email to manager,trainer and trainee
            tmsmMappingId = Re_Assigned_Trainings.query.filter_by(RT_ID=reAssignedTrainingsIds[0]).first().TMSM_Mapping_ID
            traineeId=Trainee_Status.query.filter_by(TMSM_Mapping_ID=tmsmMappingId).first().Trainee_ID
            trainerId = Users_Mapping.query.filter_by(Trainee_ID=traineeId,Last_Active_On="Present").first().Trainer_ID   
            managerId = Users_Mapping.query.filter_by(Trainee_ID=traineeId,Last_Active_On="Present").first().Manager_ID 
            projectId= Users_Mapping.query.filter_by(Trainee_ID=traineeId,Last_Active_On="Present").first().Project_ID
            division = Project.query.filter_by(Project_ID=projectId).first().Division
            BasicTrainingProjectId = Project.query.filter_by(Division=division,
                                                             Project_Name="Basic Trainings").first().Project_ID
            trainee = User.query.filter_by(User_ID=traineeId).first()
            trainer = User.query.filter_by(User_ID=trainerId).first()
            manager = User.query.filter_by(User_ID=managerId).first()    
            alteredTrainingIds=[]
            btCount=0
            pstCount=0
            for reAssignedTrainingsId in reAssignedTrainingsIds:
                alteredReAssignedTraining = Re_Assigned_Trainings.query.filter_by(RT_ID=reAssignedTrainingsId).first()
                alteredTrainingId = alteredReAssignedTraining.TMSM_Mapping_ID
                alteredTrainingIds.append(alteredTrainingId)
                alteredTraining = Trainee_Status.query.filter_by(TMSM_Mapping_ID=alteredTrainingId).first()
                reAssignedTrainingProjectId = Modules.query.filter_by(Module_ID=alteredTraining.Module_ID).first().Project_ID
                if reAssignedTrainingProjectId==BasicTrainingProjectId:
                    btCount+=1
                elif reAssignedTrainingProjectId==projectId:
                    pstCount+=1
            reAssignedTrainings = Re_Assigned_Trainings.query.filter(Re_Assigned_Trainings.RT_ID.in_(reAssignedTrainingsIds)).all()
            trainings = Trainee_Status.query.filter(Trainee_Status.TMSM_Mapping_ID.in_(alteredTrainingIds)).all()
            trainingChanges=""
            for (reAssignedTraining,training,oldDueDate) in zip(reAssignedTrainings,trainings,oldTrainingDueDates):
                if training.Sub_Module_ID==None:
                    trainingName = Modules.query.filter_by(Module_ID=training.Module_ID).first().Module_Name
                else:
                    trainingName = "".join([Modules.query.filter_by(Module_ID=training.Module_ID).first().Module_Name,Sub_Modules.query.filter(Sub_Modules.Sub_Module_ID==training.Sub_Module_ID).first().Sub_Module_Name.join([" (",")"])])
                trainingProjectId=Modules.query.filter_by(Module_ID=training.Module_ID).first().Project_ID
                projectName = Project.query.filter_by(Project_ID=trainingProjectId).first().Project_Name
                trainingChange=" - ".join([" - ".join([projectName,trainingName])," --> ".join([oldDueDate,reAssignedTraining.Due_Date])])
                trainingChanges="\n\n".join([trainingChanges,trainingChange])
            basicTrainingsName = Project.query.filter_by(Project_ID=BasicTrainingProjectId).first().Project_Name
            currentProjectName = Project.query.filter_by(Project_ID=projectId).first().Project_Name
            to_list=[str(trainee.User_Email)]
            cc_list=[str(manager.User_Email),str(trainer.User_Email)]
            to_emails = email_list_formatter(to_list)
            cc_emails = email_list_formatter(cc_list)
            receivers_list=to_list+cc_list          
            subject="Due Dates of {} Re-Assigned Trainings changed for {}".format(len(alteredTrainingIds),trainee.User_Name)
            body="{} due date(s) was/were changed in {} and {} due date(s) was/were changed in {} Trainings. \n {}".format(btCount,basicTrainingsName,pstCount,currentProjectName,trainingChanges)
            send_email(receivers_list,to_emails,cc_emails,subject,trainee.User_Name,body) 

        return jsonify({'response': 'Changes Saved Successfully!'})

@app.route('/DeleteReAssignedTrainings', methods=["POST"])
def DeleteReAssignedTrainings():
    details=request.get_json()
    deletionReAssignedTrainingIds = details.get("deletionReAssignedTrainingIds")
    for deletionReAssignedTrainingId in deletionReAssignedTrainingIds:
        reAssignedTraining = Re_Assigned_Trainings.query.filter_by(RT_ID=deletionReAssignedTrainingId).first()
        db.session.delete(reAssignedTraining)
    db.session.commit()
    return jsonify({'response': 'Re-Assigned Training(s) Deleted Successfully!'})

@app.route('/fetchManagerTableDetails', methods=["GET"])
def fetchManagerTableDetails():
    if request.method == "GET":
        email = request.args.get('userEmail').lower()
        managerId = User.query.filter_by(User_Email=email,User_Type="Manager").first().User_ID
        usersmappings = Users_Mapping.query.filter_by(Manager_ID=managerId, Last_Active_On="Present").all()
        traineeIds=[]
        for mapping in usersmappings:
            traineeIds.append(mapping.Trainee_ID)
        traineeresults = User.query.filter(User.User_ID.in_(traineeIds), User.User_Type == "Trainee").all()
        tableData = []

        def serialize(index,traineeId,traineeName,traineeEmail,traineeFeedbacks,traineeKtFeedback,trainerId,trainerName,progress,status,project,projectId,traineeType):
            return {
                'index':index,
                'traineeId': traineeId,
                'traineeName': traineeName,
                'traineeEmail': traineeEmail,
                'traineeFeedbacks': traineeFeedbacks,
                'traineeKtFeedback': traineeKtFeedback,
                'trainerId':trainerId,
                'trainerName':trainerName,
                'progress':progress,
                'status':status,
                'project':project,
                'projectId':projectId,
                'trainingRequirementType':traineeType,
            }

        completeCount=0
        trainerIds=[]
        index=0
        traineeTypes = ["All Modules", "Only Mandatory Modules"]
        for user in usersmappings:
            # for traineeType in traineeTypes:
            # exist_traineeType = Users_Mapping.query.filter_by(Trainee_ID=user.Trainee_ID,Last_Active_On="Present", Trainee_Type=traineeType).first()
            # if exist_traineeType:
            index+=1
            # projectId = Users_Mapping.query.filter_by(Trainee_ID=user.Trainee_ID,Last_Active_On="Present").first().Project_ID
            projectId = user.Project_ID
            # traineeKtFeedback = [Users_Mapping.query.filter_by(Trainee_ID=user.Trainee_ID,Last_Active_On="Present").first().KT_Process_Feedback]
            traineeKtFeedback = [user.KT_Process_Feedback]
            projectName = Project.query.filter_by(Project_ID=projectId).first().Project_Name
            # trainee_final_sign_off = Users_Mapping.query.filter_by(Trainee_ID=user.Trainee_ID,Last_Active_On="Present", Trainee_Type=traineeType).first().Reverse_KT_Flag
            trainee_final_sign_off = user.Reverse_KT_Flag
            if(trainee_final_sign_off=="Y"):
                completeCount+=1
            # trainerId = Users_Mapping.query.filter_by(Manager_ID=managerId, Trainee_ID=user.Trainee_ID, Last_Active_On="Present", Trainee_Type=traineeType).first().Trainer_ID
            trainerId = user.Trainer_ID
            trainerName = User.query.filter_by(User_ID=trainerId).first().User_Name

            # projectId = Users_Mapping.query.filter_by(Trainee_ID=user.Trainee_ID,Last_Active_On="Present").first().Project_ID
            division = Project.query.filter_by(Project_ID=projectId).first().Division
            BasicTrainingProjectId = Project.query.filter_by(Division=division,
                                                             Project_Name="Basic Trainings").first().Project_ID
            modules= Modules.query.filter(or_(Modules.Project_ID==BasicTrainingProjectId,Modules.Project_ID==projectId)).all()
            moduleIds=[]
            for module in modules:
                moduleIds.append(module.Module_ID)
            #There could be multiple assigned trainings due to project change,but only the trainings associated with the project assigned at present must be considered
            # all_trainings = Trainee_Status.query.filter(Trainee_Status.Trainee_ID == trainee.User_ID,
            #                                             Trainee_Status.Module_ID.in_(moduleIds),
            #                                             Trainee_Status.TTMP_Mapping_ID == exist_traineeType.TTMP_Mapping_ID).order_by(
            #     Trainee_Status.Due_Date.desc()).all()
            # all_trainings = Trainee_Status.query.filter(Trainee_Status.TTMP_Mapping_ID == exist_traineeType.TTMP_Mapping_ID).order_by(Trainee_Status.Due_Date.desc()).all()
            all_trainings = Trainee_Status.query.filter(
                Trainee_Status.TTMP_Mapping_ID == user.TTMP_Mapping_ID).order_by(
                Trainee_Status.Due_Date.desc()).all()
            try:
                latest_training=all_trainings[0]
                current_date = datetime.now().strftime("%Y-%m-%d")
                for training in all_trainings:
                    if training.Due_Date == "Not Required":
                        continue
                    if(training.Due_Date>current_date):
                        latest_training=training
                if latest_training.Signed_Off_Flag==0:
                    status=0
                else:
                    status=1
            except:
                status=1
            trainerIds.append(trainerId)
            try:
                allModuleIds=[]
                incompleteModules = 0
                for mapping in all_trainings:
                    if mapping.Due_Date == "Not Required":
                        continue
                    allModuleIds.append(mapping.Module_ID)
                allModuleIds=set(allModuleIds)
                # allModules =len(allModuleIds)
                allModules = 0
                # for moduleId in allModuleIds:
                    # all_module_mappings = Trainee_Status.query.filter_by(Trainee_ID=user.Trainee_ID, Module_ID=moduleId, TTMP_Mapping_ID = user.TTMP_Mapping_ID).all()
                all_module_mappings = Trainee_Status.query.filter_by(TTMP_Mapping_ID=user.TTMP_Mapping_ID).all()
                for module_mapping in all_module_mappings:
                    if module_mapping.Due_Date == "Not Required":
                        continue
                    allModules += 1
                    if(module_mapping.Signed_Off_Flag=="N"):
                        incompleteModules+=1

                            # break
                progress = int(100*(allModules-incompleteModules)/allModules)
            except:
                progress=0
            try:
                traineeFeedbacks=[]
                trainee_feedbacks_result = Feedback.query.filter_by(Sender_User_ID=trainerId, Receiver_User_ID= user.Trainee_ID).order_by(Feedback.Feedback_Date.desc()).all()
                for feedback in trainee_feedbacks_result:
                    date = datetime.strptime(feedback.Feedback_Date,"%Y-%m-%d %H:%M:%S")
                    formatted_date = date.strftime("%d-%m-%y (%I:%M %p)")
                    traineeFeedbacks.append(formatted_date + " : " + feedback.Feedback)
            except:
                traineeFeedbacks=[]
            trainee_name = User.query.filter_by(User_ID=user.Trainee_ID).first().User_Name
            trainee_email = User.query.filter_by(User_ID=user.Trainee_ID).first().User_Email
            traineeType = user.Trainee_Type
            tableDataElement = serialize(index,user.Trainee_ID, trainee_name, trainee_email, traineeFeedbacks,traineeKtFeedback, trainerId,trainerName, progress,status,projectName,projectId,traineeType)
            tableData.append(tableDataElement)
        try:
            ktProgress = int(100*(completeCount/len(traineeresults)))
        except:
             ktProgress = 0
        return jsonify({'tableData':tableData, 'progress':ktProgress, 'ktsCompleteCount':completeCount, 'trainerCount':len(set(trainerIds))})



@app.route('/fetchAllProjects', methods=["POST", "GET"])
def fetchAllProjects():
    if request.method == "GET":      
        projects_result = Project.query.filter(Project.Project_Name!="Basic Trainings").all()
        projects = [result.serialize() for result in projects_result]
        return jsonify({'projects':  projects})

@app.route('/fetchAllTrainers', methods=["POST", "GET"])
def fetchAllTrainers():
    if request.method == "GET":      
        trainers_result = User.query.filter_by(User_Type="Trainer").all()
        trainers = [result.serialize() for result in trainers_result]
        return jsonify({'trainers':  trainers})
    
@app.route('/fetchAllManagers', methods=["POST", "GET"])
def fetchAllManagers():
    if request.method == "GET":      
        managers_result = User.query.filter_by(User_Type="Manager").all()
        managers = [result.serialize() for result in managers_result]
        return jsonify({'managers':  managers})

@app.route('/ChangeTrainersOrProjects', methods=["POST"])
def ChangeTrainersOrProjects():
    if request.method == "POST":
        details=request.get_json()
        newTrainerIds=details.get("newTrainerIds")
        newProjectIds=details.get("newProjectIds")
        traineeIds=details.get("traineeIds")
        trainerIds=details.get("trainerIds")
        managerEmail=details.get("managerEmail").lower() 
        maxLength=len(newTrainerIds) if len(newTrainerIds)>=len(newProjectIds) else len(newProjectIds) 
        trainerDiff = maxLength - len(newTrainerIds)
        projectDiff = maxLength - len(newProjectIds)
        if trainerDiff!=0:
            for i in range(0,trainerDiff):
                newTrainerIds.append(None)
        else:
            for i in range(0,projectDiff):
                newProjectIds.append(None)
       
        managerId=User.query.filter_by(User_Email=managerEmail,User_Type="Manager").first().User_ID
        for (traineeId,trainerId,newTrainerId,newProjectId) in zip(traineeIds,trainerIds,newTrainerIds,newProjectIds):
            mapping = Users_Mapping.query.filter_by(Trainee_ID=traineeId,Trainer_ID=trainerId,Manager_ID=managerId,Last_Active_On="Present").first()

            # #Fetch all trainings assigned to the trainee with the new project's module Ids and check if length of that is 0. If not do nothing, else assign trainings from today
            # moduleIds=[]
            # modules=Modules.query.filter_by(Project_ID=newProjectId)
            # for module in modules:
            #     moduleIds.append(module.Module_ID)
            # new_project_trainings = Trainee_Status.query.filter(Trainee_Status.Trainee_ID==traineeId,Trainee_Status.Module_ID.in_(moduleIds), Trainee_Status.Ongoing_Training=="Yes").all()
            # if len(new_project_trainings)==0:
            def add_new_project_training(ttmp_mapping_id=None, projectId=None, traineeId=None):
                def date_by_adding_business_days(from_date, add_days):
                    business_days_to_add = add_days
                    current_date = from_date
                    while business_days_to_add > 0:
                        current_date += timedelta(days=1)
                        weekday = current_date.weekday()
                        if weekday >= 5:  # sat=5, sun=6
                            continue
                        business_days_to_add -= 1
                    return current_date
                #Trainings Assignment
                def trainingsAssignment(projectId):
                    modules=Modules.query.filter_by(Project_ID=projectId).all()
                    for module in modules:
                        subModules = Sub_Modules.query.filter_by(Module_ID=module.Module_ID).all()
                        if len(subModules)==0:
                            mandatory = "Yes"
                            dueDate = date_by_adding_business_days(datetime.today(), module.Module_Completion_Time)
                            dueDate = dueDate.strftime('%Y-%m-%d')
                            signed_Off_Flag = "N"
                            checked_Flag = "N"
                            ongoing_Training = "Yes"
                            traineeAssignment = Trainee_Status(TTMP_Mapping_ID=ttmp_mapping_id, Trainee_ID=traineeId, Module_ID=module.Module_ID, Mandatory=mandatory, Due_Date=dueDate, Checked_Flag=checked_Flag , Signed_Off_Flag=signed_Off_Flag, Ongoing_Training=ongoing_Training)
                            db.session.add(traineeAssignment)
                        else:
                            for subModule in subModules:
                                mandatory = "Yes"
                                dueDate = date_by_adding_business_days(datetime.today(), subModule.Sub_Module_Completion_Time)
                                dueDate = dueDate.strftime('%Y-%m-%d')
                                signed_Off_Flag = "N"
                                checked_Flag = "N"
                                ongoing_Training = "Yes"
                                traineeAssignment = Trainee_Status(TTMP_Mapping_ID=ttmp_mapping_id, Trainee_ID=traineeId, Module_ID=module.Module_ID, Sub_Module_ID=subModule.Sub_Module_ID, Mandatory=mandatory, Due_Date=dueDate, Checked_Flag=checked_Flag, Signed_Off_Flag=signed_Off_Flag, Ongoing_Training=ongoing_Training)
                                db.session.add(traineeAssignment)
                    db.session.commit()
                trainingsAssignment(projectId)


            if (mapping.Trainer_ID==newTrainerId and mapping.Project_ID==newProjectId) or (newTrainerId == None and newProjectId== None ):
                continue
            else:
                mapping.Last_Active_On=str(datetime.today().strftime('%Y-%m-%d'))
                if newTrainerId == None :
                    old_ttmp_Mapping_Id = mapping.TTMP_Mapping_ID
                    project_trainings = Trainee_Status.query.filter(
                        Trainee_Status.TTMP_Mapping_ID == old_ttmp_Mapping_Id).all()
                    for training in project_trainings:
                        training.Ongoing_Training = "No"
                    db.session.commit()
                    new_mapping = Users_Mapping(Trainee_ID=traineeId,Trainer_ID=mapping.Trainer_ID,Manager_ID=managerId,Project_ID=newProjectId,Trainee_Type="All Modules",Last_Active_On="Present")
                    db.session.add(new_mapping)
                    db.session.commit()
                    new_ttmp_Mapping_Id = Users_Mapping.query.filter_by(Trainee_ID=traineeId,
                                                                        Trainee_Type="All Modules",
                                                                        Project_ID=newProjectId,
                                                                        Last_Active_On="Present").first().TTMP_Mapping_ID
                    add_new_project_training(ttmp_mapping_id=new_ttmp_Mapping_Id, traineeId=traineeId, projectId=newProjectId)
                    divisionName = Project.query.filter_by(Project_ID=newProjectId).first().Division
                    commonprojectid = Project.query.filter_by(Division=divisionName).first().Project_ID
                    add_new_project_training(ttmp_mapping_id=new_ttmp_Mapping_Id, traineeId=traineeId,
                                             projectId=commonprojectid)

                    #Sending email to manager,trainer and trainee
                    managerId = Users_Mapping.query.filter_by(Trainee_ID=traineeId,Last_Active_On="Present").first().Manager_ID                   
                    trainee = User.query.filter_by(User_ID=traineeId).first()
                    trainer = User.query.filter_by(User_ID=mapping.Trainer_ID).first()
                    manager = User.query.filter_by(User_ID=managerId).first()
                    newProjectName = Project.query.filter_by(Project_ID=newProjectId).first().Project_Name
                    oldProjectName = Project.query.filter_by(Project_ID=mapping.Project_ID).first().Project_Name
                    to_list=[str(trainee.User_Email)]
                    cc_list=[str(manager.User_Email),str(trainer.User_Email)]
                    to_emails = email_list_formatter(to_list)
                    cc_emails = email_list_formatter(cc_list)
                    receivers_list=to_list+cc_list
                    subject="Project changed for {}".format(trainee.User_Name)
                    body="Your assigned project was changed from {} to {}.\n\nKT Portal: http://crmktutility:3000/\n\nNote - Please get your due dates updated as per BSC Holiday list/Planned Leaves.".format(oldProjectName,newProjectName)
                    send_email(receivers_list,to_emails,cc_emails,subject,trainee.User_Name,body) 
                elif newProjectId== None :
                    old_ttmp_Mapping_Id = mapping.TTMP_Mapping_ID
                    new_mapping = Users_Mapping(Trainee_ID=traineeId,Trainer_ID=newTrainerId,Manager_ID=managerId,Project_ID=mapping.Project_ID,Trainee_Type=mapping.Trainee_Type,Last_Active_On="Present")
                    db.session.add(new_mapping)
                    db.session.commit()
                    new_ttmp_Mapping_Id = Users_Mapping.query.filter_by(Trainee_ID=traineeId, Trainee_Type=mapping.Trainee_Type,
                                                                    Project_ID=mapping.Project_ID,
                                                                    Last_Active_On="Present").first().TTMP_Mapping_ID
                    project_trainings = Trainee_Status.query.filter(Trainee_Status.TTMP_Mapping_ID == old_ttmp_Mapping_Id).all()
                    for training in project_trainings:
                        training.TTMP_Mapping_ID = new_ttmp_Mapping_Id
                    db.session.commit()
                    #Sending email to manager,trainer and trainee
                    managerId = Users_Mapping.query.filter_by(Trainee_ID=traineeId,Last_Active_On="Present").first().Manager_ID                   
                    trainee = User.query.filter_by(User_ID=traineeId).first()
                    newTrainer = User.query.filter_by(User_ID=newTrainerId).first()
                    oldTrainer = User.query.filter_by(User_ID=mapping.Trainer_ID).first()
                    manager = User.query.filter_by(User_ID=managerId).first()
                    to_list=[str(trainee.User_Email),str(newTrainer.User_Email)]
                    cc_list=[str(manager.User_Email),str(oldTrainer.User_Email)]
                    to_emails = email_list_formatter(to_list)
                    cc_emails = email_list_formatter(cc_list)
                    receivers_list=to_list+cc_list
                    subject="Trainer changed for {}".format(trainee.User_Name)
                    body="Your trainer was changed from {} to {}\n\nKT Portal: http://crmktutility:3000/\n\nNote - Please get your due dates updated as per BSC Holiday list/Planned Leaves.".format(oldTrainer.User_Name,newTrainer.User_Name)
                    send_email(receivers_list,to_emails,cc_emails,subject,trainee.User_Name,body)         
                else:
                    old_ttmp_Mapping_Id = mapping.TTMP_Mapping_ID
                    project_trainings = Trainee_Status.query.filter(
                        Trainee_Status.TTMP_Mapping_ID == old_ttmp_Mapping_Id).all()
                    for training in project_trainings:
                        training.Ongoing_Training = "No"
                    db.session.commit()
                    new_mapping = Users_Mapping(Trainee_ID=traineeId,Trainer_ID=newTrainerId,Manager_ID=managerId,Project_ID=newProjectId,Trainee_Type="All Modules",Last_Active_On="Present")
                    db.session.add(new_mapping)
                    db.session.commit()
                    new_ttmp_Mapping_Id = Users_Mapping.query.filter_by(Trainee_ID=traineeId,
                                                                        Trainee_Type="All Modules",
                                                                        Project_ID=newProjectId,
                                                                        Last_Active_On="Present").first().TTMP_Mapping_ID
                    add_new_project_training(ttmp_mapping_id=new_ttmp_Mapping_Id, traineeId=traineeId,
                                             projectId=newProjectId)
                    divisionName = Project.query.filter_by(Project_ID=newProjectId).first().Division
                    commonprojectid = Project.query.filter_by(Division=divisionName).first().Project_ID
                    add_new_project_training(ttmp_mapping_id=new_ttmp_Mapping_Id, traineeId=traineeId,
                                             projectId=commonprojectid)
                    #Sending email to manager,trainer and trainee
                    managerId = Users_Mapping.query.filter_by(Trainee_ID=traineeId,Last_Active_On="Present").first().Manager_ID                   
                    trainee = User.query.filter_by(User_ID=traineeId).first()
                    newTrainer = User.query.filter_by(User_ID=newTrainerId).first()
                    oldTrainer = User.query.filter_by(User_ID=mapping.Trainer_ID).first()
                    manager = User.query.filter_by(User_ID=managerId).first()
                    newProjectName = Project.query.filter_by(Project_ID=newProjectId).first().Project_Name
                    oldProjectName = Project.query.filter_by(Project_ID=mapping.Project_ID).first().Project_Name
                    to_list=[str(trainee.User_Email),str(newTrainer.User_Email)]
                    cc_list=[str(manager.User_Email),str(oldTrainer.User_Email)]
                    to_emails = email_list_formatter(to_list)
                    cc_emails = email_list_formatter(cc_list)
                    receivers_list=to_list+cc_list
                    subject="Project and trainer changed for {}".format(trainee.User_Name)
                    body="Your trainer was changed from {} to {} and your assigned project was changed from {} to {}.".format(oldTrainer.User_Name,newTrainer.User_Name,oldProjectName,newProjectName)
                    send_email(receivers_list,to_emails,cc_emails,subject,trainee.User_Name,body)                                    
            db.session.commit()
        return jsonify({'response': 'Changes Saved Successfully!'})

@app.route('/addUser', methods=["POST"])
def addUser():
    if request.method == "POST":
        details=request.get_json()
        userEmail=details.get("userEmail").lower()
        userName=details.get("userName")
        userType=details.get("userType")
        exist_user = User.query.filter(User.User_Email==userEmail,User.User_Type!="Admin").first()
        if not exist_user:
            user = User(User_Email=userEmail, User_Name=userName,User_Type=userType)
            db.session.add(user)           
            db.session.commit()
            #Sending email to user that he/she has been added to the system
            newUser = User.query.filter_by(User_Email=userEmail,User_Type=userType).first()
            to_list=[str(newUser.User_Email)]
            cc_list=[]
            to_emails = email_list_formatter(to_list)
            cc_emails = email_list_formatter(cc_list)
            receivers_list=to_list
            subject="Welcome to the CRM Knowledge Transfer Portal!"
            body="You have been added as a {} on the CRM Knowledge Transfer Portal.".format(userType)
            send_email(receivers_list,to_emails,cc_emails,subject,newUser.User_Name,body)
            return jsonify({'message': userType+" added successfully!"})
        else:
            return jsonify({'message': "Addition unsuccessful! This user already exists as a "+exist_user.User_Type+"!"})



@app.route('/addTraineeInitiateKt', methods=["POST"])
def addTraineeInitiateKt():
    if request.method == "POST":
        details=request.get_json()
        traineeEmail=details.get("email").lower()
        traineeName=details.get("name")
        traineeType = details.get("traineeType")
        trainerEmail=details.get("trainerEmail").lower()
        managerEmail=details.get("managerEmail").lower()
        projectId=details.get("projectAssigned")
        addTrainee = False
        # exist_user = User.query.filter(User.User_Email==traineeEmail,User.User_Type!="Admin").first()
        exist_user = User.query.filter(User.User_Email == traineeEmail, User.User_Type == "Trainee").first()
        if exist_user:
            traineeId = exist_user.User_ID
            active_user_project = Users_Mapping.query.filter(Users_Mapping.Trainee_ID == traineeId,
                                                            Users_Mapping.Last_Active_On == "Present",
                                                            Users_Mapping.Reverse_KT_Flag == "N").first()
            if active_user_project:
                projectName = Project.query.filter_by(Project_ID=active_user_project.Project_ID).first().Project_Name
                trainerName = User.query.filter_by(User_ID=active_user_project.Trainer_ID).first().User_Name
                return jsonify({'message': f"Addition unsuccessful! This user is already undergoing a training program for Project - '{projectName}' under trainer - '{trainerName}'"})
            else:
                exist_user_project = Users_Mapping.query.filter(Users_Mapping.Trainee_ID == traineeId,
                                                                Users_Mapping.Project_ID == projectId,
                                                                Users_Mapping.Trainee_Type == traineeType,
                                                                Users_Mapping.Last_Active_On == "Present").first()
                if exist_user_project:
                    projectName = Project.query.filter_by(Project_ID=projectId).first().Project_Name
                    return jsonify({'message': f"Addition unsuccessful! This user already exists as a '{exist_user.User_Type}' for Project - '{projectName}' with training requirement of {traineeType}"})
                else:
                    addTrainee = True
        else:
            addTrainee = True
        if addTrainee:
            trainee_exist = User.query.filter_by(User_Email=traineeEmail, User_Type="Trainee").first()
            if not trainee_exist:
                trainee = User(User_Email=traineeEmail, User_Name=traineeName,User_Type="Trainee")
                db.session.add(trainee)
            trainee_id = User.query.filter_by(User_Email=traineeEmail, User_Type="Trainee").first().User_ID
            trainer_id = User.query.filter_by(User_Email=trainerEmail, User_Type="Trainer").first().User_ID
            manager_id = User.query.filter_by(User_Email=managerEmail, User_Type="Manager").first().User_ID
            project_id = Project.query.filter_by(Project_ID=projectId).first().Project_ID
            division = Project.query.filter_by(Project_ID=projectId).first().Division
            common_training_proj_id = Project.query.filter_by(Project_Name="Basic Trainings", Division=division).first().Project_ID

            # exist_mapping = Users_Mapping.query.filter_by(Trainee_ID=trainee_id, Last_Active_On="Present").first()
            # if not exist_mapping:
            users_mapping = Users_Mapping(Trainee_ID=trainee_id, Trainee_Type=traineeType, Trainer_ID=trainer_id,
                                          Manager_ID=manager_id,
                                          Project_ID=project_id, Last_Active_On="Present")
            db.session.add(users_mapping)
            db.session.commit()
            ttmp_Mapping_Id = Users_Mapping.query.filter_by(Trainee_ID=trainee_id, Trainee_Type=traineeType,
                                                            Trainer_ID=trainer_id, Manager_ID=manager_id,
                                                            Project_ID=project_id,
                                                            Last_Active_On="Present").first().TTMP_Mapping_ID

            def date_by_adding_business_days(from_date, add_days):
                business_days_to_add = add_days
                current_date = from_date
                while business_days_to_add > 0:
                    current_date += timedelta(days=1)
                    weekday = current_date.weekday()
                    if weekday >= 5:  # sat=5, sun=6
                        continue
                    business_days_to_add -= 1
                return current_date

            #Trainings Assignment
            def trainingsAssignment(projectId):
                modules=Modules.query.filter_by(Project_ID=projectId).all()
                for module in modules:
                    subModules = Sub_Modules.query.filter_by(Module_ID=module.Module_ID).all()
                    if len(subModules)==0:
                        mandatory = "Yes"
                        dueDate = date_by_adding_business_days(datetime.today(), module.Module_Completion_Time)
                        dueDate = dueDate.strftime('%Y-%m-%d')
                        signed_Off_Flag = "N"
                        checked_Flag = "N"
                        ongoing_Training = "Yes"
                        if traineeType == "Only Mandatory Modules" and module.Mandatory != "Yes":
                            mandatory = "No"
                            dueDate = "Not Required"
                            signed_Off_Flag = "Y"
                            checked_Flag = "Y"
                        traineeAssignment = Trainee_Status(TTMP_Mapping_ID=ttmp_Mapping_Id, Trainee_ID=trainee_id,Module_ID=module.Module_ID, Mandatory=mandatory, Due_Date=dueDate, Checked_Flag=checked_Flag ,Signed_Off_Flag=signed_Off_Flag, Ongoing_Training=ongoing_Training)
                        db.session.add(traineeAssignment)
                    else:
                        for subModule in subModules:
                            mandatory = "Yes"
                            dueDate = date_by_adding_business_days(datetime.today(), subModule.Sub_Module_Completion_Time)
                            dueDate = dueDate.strftime('%Y-%m-%d')
                            signed_Off_Flag = "N"
                            checked_Flag = "N"
                            ongoing_Training = "Yes"
                            if traineeType == "Only Mandatory Modules" and subModule.Mandatory != "Yes":
                                mandatory = "No"
                                dueDate = "Not Required"
                                signed_Off_Flag = "Y"
                                checked_Flag = "Y"
                            traineeAssignment = Trainee_Status(TTMP_Mapping_ID=ttmp_Mapping_Id, Trainee_ID=trainee_id, Module_ID=module.Module_ID, Sub_Module_ID=subModule.Sub_Module_ID, Mandatory=mandatory,Due_Date=dueDate, Checked_Flag=checked_Flag, Signed_Off_Flag=signed_Off_Flag, Ongoing_Training=ongoing_Training)
                            db.session.add(traineeAssignment)
            trainingsAssignment(common_training_proj_id)
            trainingsAssignment(project_id)
            db.session.commit()

            #Sending email to trainee, trainer and manager
            #Determining KT Completion Date
            projectId = Users_Mapping.query.filter_by(Trainee_ID=trainee_id,Last_Active_On="Present").first().Project_ID
            division = Project.query.filter_by(Project_ID=projectId).first().Division
            BasicTrainingProjectId = Project.query.filter_by(Division=division,
                                                             Project_Name="Basic Trainings").first().Project_ID
            modules= Modules.query.filter(or_(Modules.Project_ID==BasicTrainingProjectId,Modules.Project_ID==projectId)).all()
            moduleIds=[]
            for module in modules:
                moduleIds.append(module.Module_ID)
            #There could be multiple assigned trainings due to project change,but only the trainings associated with the project assigned at present must be considered
            activeMappings = Trainee_Status.query.filter(Trainee_Status.Trainee_ID==trainee_id,Trainee_Status.Module_ID.in_(moduleIds), Trainee_Status.Ongoing_Training=="Yes").all()
            for mapping in activeMappings:
                if mapping.Due_Date == "Not Required":
                    continue
                KT_completion_date_comparator = datetime.strptime(mapping.Due_Date, "%Y-%m-%d")
                KT_completion_date = datetime.strptime(mapping.Due_Date, "%Y-%m-%d")
                break
            for mapping in activeMappings:
                if mapping.Due_Date == "Not Required":
                    continue
                b = datetime.strptime(mapping.Due_Date, "%Y-%m-%d")
                if b>=KT_completion_date_comparator:
                    KT_completion_date_comparator=b
                    KT_completion_date=mapping.Due_Date
            final_due_date= datetime.strptime(KT_completion_date, "%Y-%m-%d")
            final_due_date = final_due_date.strftime("%d-%m-%Y")
            trainee = User.query.filter_by(User_ID=trainee_id).first()
            trainer = User.query.filter_by(User_ID=trainer_id).first()
            manager = User.query.filter_by(User_ID=manager_id).first()
            projectName = Project.query.filter_by(Project_ID=projectId).first().Project_Name
            to_list=[str(trainee.User_Email)]
            cc_list=[str(trainer.User_Email),str(manager.User_Email)]
            to_emails = email_list_formatter(to_list)
            cc_emails = email_list_formatter(cc_list)
            receivers_list=to_list+cc_list
            subject="KT Trainings initiated for {}".format(trainee.User_Name)
            body="Your Knowledge Transfer trainings which include basic trainings and {} trainings have been initiated. Your trainer and manager for the same are {} and {} respectively. \nThe date of completion of your KT trainings is {} (DD-MM-YYYY). Please complete the assigned trainings as per their due dates. Wishing you a pleasant learning experience!\n\nKT Portal: http://crmktutility:3000/\n\nNote - Please get your due dates updated as per BSC Holiday list/Planned Leaves.".format(projectName,trainer.User_Name,manager.User_Name,final_due_date)
            send_email(receivers_list,to_emails,cc_emails,subject,trainee.User_Name,body)
            return jsonify({'message': "Trainee addition and KT initiation successful!",'success':1})
            # else:
            #     return jsonify({'message': "Trainee addition and KT initiation unsuccessful! This trainee is already under a trainer, manager and has been assigned a project."})
        

@app.route('/fetchAdminTableDetails', methods=["GET"])
def fetchAdminTableDetails():
    if request.method == "GET":
        usersmappings = Users_Mapping.query.filter_by(Last_Active_On="Present").all()
        traineeIds=[]
        for mapping in usersmappings:
            traineeIds.append(mapping.Trainee_ID)
        traineeresults = User.query.filter(User.User_ID.in_(traineeIds), User.User_Type == "Trainee").all()
        tableData = []

        def serialize(index,traineeId,traineeName,traineeEmail,traineeFeedbacks,trainerId,trainerName,managerId,managerName,progress,status,project,projectId,trainingRequirementType):
            return {
                'index':index,
                'traineeId': traineeId,
                'traineeName': traineeName,
                'traineeEmail': traineeEmail,
                'traineeFeedbacks': traineeFeedbacks,
                'trainerId':trainerId,
                'trainerName':trainerName,
                'managerId':managerId,
                'managerName':managerName,
                'progress':progress,
                'status':status,
                'project':project,
                'projectId':projectId,
                'trainingRequirementType': trainingRequirementType
            }

        completeCount=0
        trainerCount = len(User.query.filter_by(User_Type="Trainer").all())
        managerCount = len(User.query.filter_by(User_Type="Manager").all())
        projectCount = len(Project.query.all())
        activeTrainerIds=[]
        activeManagerIds=[]
        activeProjectIds=[]
        index=0
        for trainee in usersmappings:
            index+=1
            traineeId = trainee.Trainee_ID
            # projectId = Users_Mapping.query.filter_by(Trainee_ID=trainee.User_ID,Last_Active_On="Present").first().Project_ID
            projectId = trainee.Project_ID
            trainingRequirementType = trainee.Trainee_Type
            division = Project.query.filter_by(Project_ID=projectId).first().Division
            BasicTrainingProjectId = Project.query.filter_by(Division=division,
                                                             Project_Name="Basic Trainings").first().Project_ID
            projectName = Project.query.filter_by(Project_ID=projectId).first().Project_Name
            # managerId = Users_Mapping.query.filter_by(Trainee_ID=trainee.User_ID,Last_Active_On="Present").first().Manager_ID
            managerId = trainee.Manager_ID
            managerName = User.query.filter_by(User_ID=managerId).first().User_Name
            # trainerId = Users_Mapping.query.filter_by(Manager_ID=managerId, Trainee_ID=trainee.User_ID, Last_Active_On="Present").first().Trainer_ID
            trainerId = trainee.Trainer_ID
            trainerName = User.query.filter_by(User_ID=trainerId).first().User_Name
            traineeName = User.query.filter_by(User_ID=traineeId).first().User_Name
            traineeEmail = User.query.filter_by(User_ID=traineeId).first().User_Email
            # trainee_final_sign_off = Users_Mapping.query.filter_by(Trainee_ID=trainee.User_ID,Last_Active_On="Present").first().Reverse_KT_Flag
            trainee_final_sign_off = trainee.Reverse_KT_Flag
            activeManagerIds.append(managerId)
            activeTrainerIds.append(trainerId)
            activeProjectIds.append(projectId)
            if(trainee_final_sign_off=="Y"):
                completeCount+=1
            modules= Modules.query.filter(or_(Modules.Project_ID==BasicTrainingProjectId,Modules.Project_ID==projectId)).all()
            moduleIds=[]
            for module in modules:
                moduleIds.append(module.Module_ID)
            ttmp_mapping_id = Users_Mapping.query.filter_by(Trainee_ID=traineeId, Trainee_Type=trainee.Trainee_Type,
                                                            Project_ID=projectId,
                                                            Last_Active_On="Present").first().TTMP_Mapping_ID
            #There could be multiple assigned trainings due to project change,but only the trainings associated with the project assigned at present must be considered 
            # all_trainings = Trainee_Status.query.filter(Trainee_Status.Trainee_ID==trainee.User_ID,Trainee_Status.Module_ID.in_(moduleIds), Trainee_Status.Ongoing_Training=="Yes").order_by(Trainee_Status.Due_Date.desc()).all()
            all_trainings = Trainee_Status.query.filter(Trainee_Status.Trainee_ID == traineeId,
                                                        Trainee_Status.Module_ID.in_(moduleIds),
                                                        Trainee_Status.TTMP_Mapping_ID == ttmp_mapping_id).order_by(
                Trainee_Status.Due_Date.desc()).all()
            try:
                latest_training=all_trainings[0]              
                current_date = datetime.now().strftime("%Y-%m-%d")
                for training in all_trainings:
                    if training.Due_Date == "Not Required":
                        continue
                    if(training.Due_Date>current_date):
                        latest_training=training
                if latest_training.Signed_Off_Flag==0:
                    status=0
                else:
                    status=1
            except:
                status=1
                     
            try:
                allModuleIds=[]
                incompleteModules = 0
                for mapping in all_trainings:
                    allModuleIds.append(mapping.Module_ID)           
                allModuleIds=set(allModuleIds)
                # allModules =len(allModuleIds)
                allModules = 0
                # for moduleId in allModuleIds:
                #     all_module_mappings = Trainee_Status.query.filter_by(Trainee_ID=traineeId, Module_ID=moduleId, TTMP_Mapping_ID=ttmp_mapping_id).all()
                all_module_mappings = Trainee_Status.query.filter_by(TTMP_Mapping_ID=ttmp_mapping_id).all()
                for module_mapping in all_module_mappings:
                    if module_mapping.Due_Date == "Not Required":
                        continue
                    allModules +=1
                    if(module_mapping.Signed_Off_Flag=="N"):
                        incompleteModules+=1
                            # break
                progress = int(100*(allModules-incompleteModules)/allModules)
            except:
                progress=0
            try:
                traineeFeedbacks=[]
                trainee_feedbacks_result = Feedback.query.filter_by(Sender_User_ID=trainerId, Receiver_User_ID= trainee.User_ID).order_by(Feedback.Feedback_Date.desc()).all()
                for feedback in trainee_feedbacks_result:
                    date = datetime.strptime(feedback.Feedback_Date,"%Y-%m-%d %H:%M:%S")
                    formatted_date = date.strftime("%d-%m-%y (%I:%M %p)")
                    traineeFeedbacks.append(formatted_date + " : " + feedback.Feedback)
            except:
                traineeFeedbacks=[]
            tableDataElement = serialize(index,traineeId, traineeName, traineeEmail, traineeFeedbacks, trainerId, trainerName, managerId, managerName, progress,status,projectName,projectId,trainingRequirementType)
            tableData.append(tableDataElement)
        ktProgress = int(100*(completeCount/len(traineeresults)))
        return jsonify({'tableData':tableData, 'progress':ktProgress, 'ktsCompleteCount':completeCount, 'trainerCount':trainerCount,'assignedTrainerCount':len(set(activeTrainerIds)),'assignedManagerCount':len(set(activeManagerIds)), 'managerCount':managerCount, 'projectCount':projectCount,'activeProjectCount':len(set(activeProjectIds))})

@app.route('/ChangeManagersOrTrainersOrProjects', methods=["POST"])
def ChangeManagersOrTrainersOrProjects():
    if request.method == "POST":
        details=request.get_json()
        newTrainerIds=details.get("newTrainerIds")
        newProjectIds=details.get("newProjectIds")
        newManagerIds=details.get("newManagerIds") 
        traineeIds=details.get("traineeIds")
        trainerIds=details.get("trainerIds")
        managerIds=details.get("managerIds") 
        maxLength=max(len(newTrainerIds),len(newProjectIds),len(newManagerIds))
        trainerDiff = maxLength - len(newTrainerIds)
        projectDiff = maxLength - len(newProjectIds)
        managerDiff = maxLength - len(newManagerIds)
        if trainerDiff!=0:
            for i in range(0,trainerDiff):
                newTrainerIds.append(None)
        if projectDiff!=0:
            for i in range(0,projectDiff):
                newProjectIds.append(None)
        if managerDiff!=0:
            for i in range(0,managerDiff):
                newManagerIds.append(None)
       

        for (traineeId,trainerId,managerId,newTrainerId,newProjectId,newManagerId) in zip(traineeIds,trainerIds,managerIds,newTrainerIds,newProjectIds,newManagerIds):
            mapping = Users_Mapping.query.filter_by(Trainee_ID=traineeId,Trainer_ID=trainerId,Manager_ID=managerId,Last_Active_On="Present").first()

            # #Fetch all trainings assigned to the trainee with the new project's module Ids and check if length of that is 0. If not do nothing, else assign trainings from today
            # moduleIds=[]
            # modules=Modules.query.filter_by(Project_ID=newProjectId)
            # for module in modules:
            #     moduleIds.append(module.Module_ID)
            # new_project_trainings = Trainee_Status.query.filter(Trainee_Status.Trainee_ID==traineeId,Trainee_Status.Module_ID.in_(moduleIds), Trainee_Status.Ongoing_Training=="Yes").all()
            # if len(new_project_trainings)==0:
            def add_new_project_training(ttmp_mapping_id=None, projectId=None, traineeId=None):
                def date_by_adding_business_days(from_date, add_days):
                    business_days_to_add = add_days
                    current_date = from_date
                    while business_days_to_add > 0:
                        current_date += timedelta(days=1)
                        weekday = current_date.weekday()
                        if weekday >= 5:  # sat=5, sun=6
                            continue
                        business_days_to_add -= 1
                    return current_date
                #Trainings Assignment
                def trainingsAssignment(projectId):
                    modules=Modules.query.filter_by(Project_ID=projectId).all()
                    for module in modules:
                        subModules = Sub_Modules.query.filter_by(Module_ID=module.Module_ID).all()
                        if len(subModules)==0:
                            mandatory = "Yes"
                            dueDate = date_by_adding_business_days(datetime.today(), module.Module_Completion_Time)
                            dueDate = dueDate.strftime('%Y-%m-%d')
                            signed_Off_Flag = "N"
                            checked_Flag = "N"
                            ongoing_Training = "Yes"
                            traineeAssignment = Trainee_Status(TTMP_Mapping_ID=ttmp_mapping_id, Trainee_ID=traineeId,Module_ID=module.Module_ID, Mandatory=mandatory, Due_Date=dueDate, Checked_Flag=checked_Flag ,Signed_Off_Flag=signed_Off_Flag, Ongoing_Training=ongoing_Training)
                            db.session.add(traineeAssignment)
                        else:
                            for subModule in subModules:
                                mandatory = "Yes"
                                dueDate = date_by_adding_business_days(datetime.today(), subModule.Sub_Module_Completion_Time)
                                dueDate = dueDate.strftime('%Y-%m-%d')
                                signed_Off_Flag = "N"
                                checked_Flag = "N"
                                ongoing_Training = "Yes"
                                traineeAssignment = Trainee_Status(TTMP_Mapping_ID=ttmp_mapping_id, Trainee_ID=traineeId, Module_ID=module.Module_ID, Sub_Module_ID=subModule.Sub_Module_ID, Mandatory=mandatory, Due_Date=dueDate, Checked_Flag=checked_Flag ,Signed_Off_Flag=signed_Off_Flag, Ongoing_Training=ongoing_Training)
                                db.session.add(traineeAssignment)
                    db.session.commit()
                trainingsAssignment(projectId)


            if (mapping.Trainer_ID==newTrainerId and mapping.Project_ID==newProjectId and mapping.Manager_ID==managerId) or (newTrainerId == None and newProjectId== None and newManagerId == None):
                continue
            else:
                mapping.Last_Active_On=str(datetime.today().strftime('%Y-%m-%d'))
                old_ttmp_Mapping_Id = mapping.TTMP_Mapping_ID
                Trainer_ID = mapping.Trainer_ID if newTrainerId == None else newTrainerId
                Manager_ID = mapping.Manager_ID if newManagerId == None else newManagerId
                Project_ID = mapping.Project_ID if newProjectId == None else newProjectId
                traineeType = mapping.Trainee_Type if newProjectId == None else "All Modules"
                new_mapping = Users_Mapping(Trainee_ID=traineeId,Trainer_ID=Trainer_ID,Trainee_Type=traineeType, Manager_ID=Manager_ID,Project_ID=Project_ID,Last_Active_On="Present")
                db.session.add(new_mapping)
                db.session.commit()
                new_ttmp_Mapping_Id = Users_Mapping.query.filter_by(Trainee_ID=traineeId,
                                                                    Trainee_Type=traineeType,
                                                                    Project_ID=Project_ID,
                                                                    Last_Active_On="Present").first().TTMP_Mapping_ID
                if newProjectId:
                    project_trainings = Trainee_Status.query.filter(
                        Trainee_Status.TTMP_Mapping_ID == old_ttmp_Mapping_Id).all()
                    for training in project_trainings:
                        training.Ongoing_Training = "No"
                    db.session.commit()
                    add_new_project_training(ttmp_mapping_id=new_ttmp_Mapping_Id, traineeId=traineeId,
                                             projectId=newProjectId)
                    divisionName = Project.query.filter_by(Project_ID=newProjectId).first().Division
                    commonprojectid = Project.query.filter_by(Division=divisionName).first().Project_ID
                    add_new_project_training(ttmp_mapping_id=new_ttmp_Mapping_Id, traineeId=traineeId,
                                             projectId=commonprojectid)
                else:
                    project_trainings = Trainee_Status.query.filter(
                        Trainee_Status.TTMP_Mapping_ID == old_ttmp_Mapping_Id).all()
                    for training in project_trainings:
                        training.TTMP_Mapping_ID = new_ttmp_Mapping_Id
                    db.session.commit()
                #Sending email to manager,trainer and trainee
                trainee = User.query.filter_by(User_ID=traineeId).first()
                trainer = User.query.filter_by(User_ID=Trainer_ID).first()
                manager = User.query.filter_by(User_ID=Manager_ID).first()
                projectName = Project.query.filter_by(Project_ID=Project_ID).first().Project_Name
                newTrainerEmail = None if newTrainerId == None else User.query.filter_by(User_ID=newTrainerId).first().User_Email
                newManagerEmail = None if newManagerId == None else User.query.filter_by(User_ID=newManagerId).first().User_Email
                trainerEmail = User.query.filter_by(User_ID=mapping.Trainer_ID).first().User_Email
                managerEmail = User.query.filter_by(User_ID=mapping.Manager_ID).first().User_Email
                to_list=[str(trainee.User_Email),str(newTrainerEmail),str(newManagerEmail)]
                cc_list=[str(trainerEmail),str(managerEmail)]
                to_emails = email_list_formatter(to_list)
                cc_emails = email_list_formatter(cc_list)
                receivers_list=to_list+cc_list
                subject="Changes for {}".format(trainee.User_Name)
                body=""
                if newManagerId != None:
                    body+="Your manager was changed from {} to {}.\n\nKT Portal: http://crmktutility:3000/\n\nNote - Please get your due dates updated as per BSC Holiday list/Planned Leaves.".format(User.query.filter_by(User_ID=mapping.Manager_ID).first().User_Name,manager.User_Name)
                if newTrainerId != None:
                    body+="Your trainer was changed from {} to {}.\n\nKT Portal: http://crmktutility:3000/\n\nNote - Please get your due dates updated as per BSC Holiday list/Planned Leaves.".format(User.query.filter_by(User_ID=mapping.Trainer_ID).first().User_Name,trainer.User_Name)
                if newProjectId != None:
                    body+="Your project was changed from {} to {}.\n\nKT Portal: http://crmktutility:3000/\n\nNote - Please get your due dates updated as per BSC Holiday list/Planned Leaves.".format(Project.query.filter_by(Project_ID=mapping.Project_ID).first().Project_Name,projectName)
                send_email(receivers_list,to_emails,cc_emails,subject,trainee.User_Name,body)         
            db.session.commit()
        return jsonify({'response': 'Changes Saved Successfully!'})

@app.route("/fetchUserTableDetails",methods=["GET"])
def fetchUserTableDetails():
    if request.method == "GET":
        userType = request.args.get('userType')
        users=User.query.filter_by(User_Type=userType).all()
        totalUserCount=len(users)
        userIds=[]
        tableData=[]
        index=0
        def serialize(index,userId,userEmail,userName,assigned,traineeCount,activeTraineeCount):
           return {
               'index':index,
               'userId':userId,
               'userEmail':userEmail,
               'userName':userName,
               'newUserEmail':None,
               'newUserName':None,
               'assigned':assigned,
               'traineeCount':traineeCount,
               'activeTraineeCount':activeTraineeCount
               
           }
        for user in users:
            userIds.append(user.User_ID)
            assigned="No"
            assigned_user=None
            index+=1
            if userType=="Manager":
                assigned_user=Users_Mapping.query.filter_by(Manager_ID=user.User_ID,Last_Active_On="Present").first()
                traineeCount=len(Users_Mapping.query.filter_by(Manager_ID=user.User_ID,Last_Active_On="Present").all())
                activeTraineeCount=len(Users_Mapping.query.filter_by(Manager_ID=user.User_ID,Last_Active_On="Present",Reverse_KT_Flag="N").all())
            elif userType=="Trainer":
                assigned_user=Users_Mapping.query.filter_by(Trainer_ID=user.User_ID,Last_Active_On="Present").first()
                traineeCount=len(Users_Mapping.query.filter_by(Trainer_ID=user.User_ID,Last_Active_On="Present").all())
                activeTraineeCount=len(Users_Mapping.query.filter_by(Trainer_ID=user.User_ID,Last_Active_On="Present",Reverse_KT_Flag="N").all())
            elif userType=="Trainee":
                assigned_user=Users_Mapping.query.filter_by(Trainee_ID=user.User_ID,Last_Active_On="Present").first()
                traineeCount=None
                activeTraineeCount=None
            if assigned_user is not None:
                assigned="Yes"
            tableDataElement = serialize(index,user.User_ID, user.User_Email, user.User_Name, assigned,traineeCount,activeTraineeCount)
            tableData.append(tableDataElement)

        assignedUserIds = []
        try:
            if userType=="Manager":
                assignedUsers = Users_Mapping.query.filter(Users_Mapping.Manager_ID.in_(userIds),Users_Mapping.Last_Active_On=="Present").all()
                for assignedUser in assignedUsers:
                    assignedUserIds.append(assignedUser.Manager_ID)
            elif userType=="Trainer":
                assignedUsers=Users_Mapping.query.filter(Users_Mapping.Trainer_ID.in_(userIds),Users_Mapping.Last_Active_On=="Present").all()
                for assignedUser in assignedUsers:
                    assignedUserIds.append(assignedUser.Trainer_ID)
            elif userType=="Trainee":
                assignedUsers=Users_Mapping.query.filter(Users_Mapping.Trainee_ID.in_(userIds),Users_Mapping.Last_Active_On=="Present").all()
                for assignedUser in assignedUsers:
                    assignedUserIds.append(assignedUser.Trainee_ID)
            assignedUserCount=len(set(assignedUserIds))
        except:  
            assignedUserCount=0

        
        return jsonify({'tableData':tableData,'totalUserCount':totalUserCount, 'assignedUserCount':assignedUserCount})

@app.route("/ChangeEmailsOrNames",methods=["POST"])
def ChangeEmailsOrNames():
    if request.method=="POST":
        details=request.get_json()
        userIds=details.get("userIds")
        newUserEmails=details.get("newUserEmails")
        newUserNames=details.get("newUserNames")       

        for (userId,newUserEmail,newUserName) in zip(userIds,newUserEmails,newUserNames): 
            newUserEmail=newUserEmail.lower() if newUserEmail else newUserEmail
            existingUser = User.query.filter(User.User_Email==newUserEmail,User.User_Type!="Admin").first()  
            if existingUser:
                return jsonify({'response': existingUser.User_Email+" already exists as a "+existingUser.User_Type})
            else:
                user = User.query.filter_by(User_ID=userId).first() 
                user.User_Email = user.User_Email if newUserEmail==None else newUserEmail.lower()
                user.User_Name = user.User_Name if newUserName==None else newUserName
        db.session.commit()
        return jsonify({'response': 'Changes Saved Successfully!'})
    
@app.route('/DeleteUsers', methods=["POST"])
def DeleteUsers():
    details=request.get_json()
    deletionUserIds = details.get("deletionUserIds")
    for deletionUserId in deletionUserIds:
        user = User.query.filter_by(User_ID=deletionUserId).first()
        #Deleting all feedbacks sent/received by the user
        feedbacks = Feedback.query.filter(or_(Feedback.Sender_User_ID==deletionUserId,Feedback.Receiver_User_ID==deletionUserId)).all()
        if feedbacks is not None:
            Feedback.query.filter(or_(Feedback.Sender_User_ID==deletionUserId,Feedback.Receiver_User_ID==deletionUserId)).delete()
        userMappings = Users_Mapping.query.filter(or_(Users_Mapping.Trainee_ID==deletionUserId,Users_Mapping.Trainer_ID==deletionUserId,Users_Mapping.Manager_ID==deletionUserId)).all()
        if userMappings is not None:
            for userMapping in userMappings:
                traineeId = userMapping.Trainee_ID
                #Deleting all KT trainings asssociated with the user or user's trainee
                trainings = Trainee_Status.query.filter_by(Trainee_ID=traineeId).all()
                if trainings is not None:
                    trainingIds=[]
                    for training in trainings:
                        trainingIds.append(training.TMSM_Mapping_ID)
                    #Deleting all Re-Assigned KT trainings asssociated with the user or user's trainee
                    reAssignedTrainings = Re_Assigned_Trainings.query.filter(Re_Assigned_Trainings.TMSM_Mapping_ID.in_(trainingIds)).all()
                    if reAssignedTrainings is not None:
                       Re_Assigned_Trainings.query.filter(Re_Assigned_Trainings.TMSM_Mapping_ID.in_(trainingIds)).delete()
                    Trainee_Status.query.filter_by(Trainee_ID=traineeId).delete()
                #Deleting all trainee(s) asssociated with the user (only one if trainee is the user type being deleted)
                User.query.filter_by(User_ID=traineeId).delete()
            Users_Mapping.query.filter(or_(Users_Mapping.Trainee_ID==deletionUserId,Users_Mapping.Trainer_ID==deletionUserId,Users_Mapping.Manager_ID==deletionUserId)).delete()
        #At last, the user itself is deleted
        db.session.delete(user)
    db.session.commit()
    return jsonify({'response': 'User(s) Deleted Successfully!'})


@app.route('/fetchAssessmentDetails', methods=["POST", "GET"])
def fetchAssessmentDetails():
    if request.method == "GET":
        userEmail = request.args.get("userEmail")
        userId= User.query.filter_by(User_Email=userEmail,User_Type="Trainee").first().User_ID
        projectId = request.args.get('projectId')
        questions_result = Questions.query.filter_by(Project_ID=int(projectId)).all()
        questions = [result.serialize() for result in questions_result]
        for question in questions:
            options_result = Options.query.filter_by(Question_ID=question["Question_ID"]).all()
            options = [result.serialize() for result in options_result]
            question["options"]=(options)
            question["selectedOption"]=None
            question["project"]=Project.query.filter_by(Project_ID=question["Project_ID"]).first().Project_Name
        previousAttempt = Assessments.query.filter_by(User_ID=userId,Project_ID=projectId).first() 
        if previousAttempt is not None:
            previousScore = previousAttempt.Attempt_Score
            date = datetime.strptime(previousAttempt.Attempt_Date,"%Y-%m-%d %H:%M:%S")
            previousTime = date.strftime("%d-%m-%Y (%I:%M %p)")
        else:
            previousScore = None
            previousTime=None
        return jsonify({'assessmentData': questions,'previousScore':previousScore,'previousTime':previousTime})
    
@app.route('/checkAssessmentEligibility', methods=["POST", "GET"])
def checkAssessmentEligibility():
    if request.method == "GET":
        completion_flag=1
        userEmail = request.args.get("userEmail")
        userId= User.query.filter_by(User_Email=userEmail,User_Type="Trainee").first().User_ID
        projectId = request.args.get('projectId')
        division = Project.query.filter_by(Project_ID=projectId).first().Division
        BasicTrainingProjectId = Project.query.filter_by(Division=division,
                                                         Project_Name="Basic Trainings").first().Project_ID
        modules= Modules.query.filter(or_(Modules.Project_ID==BasicTrainingProjectId,Modules.Project_ID==projectId)).all()
        moduleIds=[]
        for module in modules:
            moduleIds.append(module.Module_ID)
        #There could be multiple assigned trainings due to project change,but only the trainings associated with the project assigned at present must be considered 
        activeMappings = Trainee_Status.query.filter(Trainee_Status.Trainee_ID==userId,Trainee_Status.Module_ID.in_(moduleIds), Trainee_Status.Ongoing_Training=="Yes").all()
        activeMappingsCount=len(activeMappings)-1 #Reduced one as we are comparing with index in the for loop below which starts from 0    
        # for index,mapping in enumerate(activeMappings):
        #     if index!=activeMappingsCount and mapping.Signed_Off_Flag=="N":
        #         completion_flag=0
        completed_training_count = 0
        for mapping in activeMappings:
            if mapping.Signed_Off_Flag == "Y":
                completed_training_count = completed_training_count + 1
        if completed_training_count!=activeMappingsCount:
            completion_flag = 0
        ineligible = 0 if completion_flag==1 else 1
        #Checking if already cleared
        questions_result = Questions.query.filter_by(Project_ID=int(projectId)).all()
        questions = [result.serialize() for result in questions_result]
        for question in questions:
            options_result = Options.query.filter_by(Question_ID=question["Question_ID"]).all()
            options = [result.serialize() for result in options_result]
            question["options"]=(options)
            question["selectedOption"]=None
            question["project"]=Project.query.filter_by(Project_ID=question["Project_ID"]).first().Project_Name
        previousAttempt = Assessments.query.filter_by(User_ID=userId,Project_ID=projectId).first() 
        if previousAttempt is not None and previousAttempt.Attempt_Score>=70:
            alreadyCleared=1
            previousScore = previousAttempt.Attempt_Score
            date = datetime.strptime(previousAttempt.Attempt_Date,"%Y-%m-%d %H:%M:%S")
            previousTime = date.strftime("%d-%m-%Y (%I:%M %p)")
        else:
            previousScore = None
            previousTime=None
            alreadyCleared=0
        return jsonify({'ineligible': ineligible,'alreadyCleared':alreadyCleared,'previousScore':previousScore,'previousTime':previousTime})
        

@app.route('/assessmentCorrection', methods=["POST", "GET"])
def assessmentCorrection():
    if request.method == "POST":
        details=request.get_json()
        userEmail = details.get("userEmail")
        userId= User.query.filter_by(User_Email=userEmail,User_Type="Trainee").first().User_ID
        projectId = Users_Mapping.query.filter_by(Trainee_ID=userId,Last_Active_On="Present").first().Project_ID
        selectedOptions=details.get("selectedOptions")
        score = 0
        attemptDate = datetime.now()
        #Don't require microsecond data
        attemptDate = attemptDate.replace(microsecond=0)
        for selectedOption in selectedOptions:
            options_result = Options.query.filter_by(Option_ID=selectedOption).first()
            if(options_result.Option_Correct_Flag=="Y"):
                score+=10
        if score>=70:
            #Signing off Project Assessment
            modules= Modules.query.filter_by(Project_ID=projectId).all()
            moduleIds=[]
            for module in modules:
                moduleIds.append(module.Module_ID)
            #There could be multiple assigned trainings due to project change,but only the trainings associated with the project assigned at present must be considered 
            all_trainings = Trainee_Status.query.filter(Trainee_Status.Trainee_ID==userId,Trainee_Status.Module_ID.in_(moduleIds)).all()
            for training in all_trainings:
                sub_module=Sub_Modules.query.filter_by(Sub_Module_ID=training.Sub_Module_ID).first()
                if sub_module is not None and sub_module.Sub_Module_Name=="Assessment":
                    training.Signed_Off_Flag = "Y"
                    training.Completion_Date = str(datetime.today().strftime('%Y-%m-%d'))
            db.session.commit()
            #Sending email to trainee, trainer,manager
            trainerId = Users_Mapping.query.filter_by(Trainee_ID=userId,Last_Active_On="Present").first().Trainer_ID
            managerId = Users_Mapping.query.filter_by(Trainee_ID=userId,Last_Active_On="Present").first().Manager_ID
            trainee = User.query.filter_by(User_ID=userId).first()
            trainer = User.query.filter_by(User_ID=trainerId).first()
            manager = User.query.filter_by(User_ID=managerId).first()
            projectName = Project.query.filter_by(Project_ID=projectId).first().Project_Name
            to_list=[str(manager.User_Email)]
            cc_list=[str(trainer.User_Email),str(trainee.User_Email)]
            to_emails = email_list_formatter(to_list)
            cc_emails = email_list_formatter(cc_list)
            receivers_list=to_list+cc_list
            subject="KT Trainings Completed by {}".format(trainee.User_Name)
            body="All basic and {} trainings have been completed by {}. {} may plan the reverse KT.".format(projectName,trainee.User_Name, trainer.User_Name)
            send_email(receivers_list,to_emails,cc_emails,subject,manager.User_Name,body)
        assessment = Assessments.query.filter_by(User_ID=userId,Project_ID=projectId).first()
        if assessment is None:
            newAssessment=Assessments(User_ID=userId,Project_ID=projectId,Attempt_Number=1,Attempt_Score=score,Attempt_Date=attemptDate)
            db.session.add(newAssessment)
            db.session.commit()
        else:
            assessment.Attempt_Number+=1
            assessment.Attempt_Score=score
            assessment.Attempt_Date=attemptDate
            db.session.commit()      
        return jsonify({'score': score})
    
@app.route('/addKtFeedback', methods=["POST", "GET"])
def addKtFeedback():
    if request.method == "POST":
        details=request.get_json()
        userEmail = details.get("userEmail")
        feedback = details.get("feedback")
        userId=User.query.filter_by(User_Email=userEmail,User_Type="Trainee").first().User_ID
        mapping = Users_Mapping.query.filter_by(Trainee_ID=userId,Last_Active_On="Present").first()
        mapping.KT_Process_Feedback=feedback
        db.session.commit()
        return "1"

if __name__ == "__main__":
    app.run(debug=True, port=8000)