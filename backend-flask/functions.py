from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import CheckConstraint, ForeignKeyConstraint, ForeignKey
from sqlalchemy.orm import relationship
from datetime import datetime, timedelta
import time, email, smtplib

db = SQLAlchemy()


def app_obj():
    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///KT_DB.db'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)
    return app, db

def email_list_formatter(email_list):
    formatted_email_list = "" 
    if len(email_list)>1:
        for email in email_list:
            if email == "" or email == "None":
                continue
            else:
                formatted_email_list="".join([formatted_email_list,email.join(["<", ">;"])])
        formatted_email_list=str(formatted_email_list[0:len(formatted_email_list)-1]) #Removing the last unnecessary';'
    if len(email_list)==1:
        formatted_email_list = email_list[0]
    return formatted_email_list

def send_email(receivers_list,to_emails,cc_emails,subject,addresseeName,body):
    # creates SMTP session
    s = smtplib.SMTP('mapintmail.bsci.bossci.com', 25) 
    message = """From: CRM KT Team <crmktteam@bsci.com>
To: {}
CC: {}
Subject: {}

Hi {},

{}

Regards,
CRM KT Team

**Note: This is a system generated e-mail. Please contact CRM KT Team for any further queries.
""".format(to_emails,cc_emails,subject,addresseeName,body)
    s.sendmail("crmktteam@bsci.com",receivers_list,message)
    s.quit()


class User(db.Model):
    __tablename__ = 'user_master_table'
    User_ID = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False, unique=True)
    User_Email = db.Column(db.String(200), nullable=False)
    User_Name = db.Column(db.String(200))
    User_Type = db.Column(db.String(200))
    User_OTP = db.Column(db.String(6))
    __table_args__ = (
        CheckConstraint("User_Type IN ('Trainee', 'Trainer', 'Manager','Admin')", name='check_type'),
    )

    def serialize(self):
        return {
            'User_ID': self.User_ID,
            'User_Email': self.User_Email,
            'User_Name': self.User_Name,
            'User_Type': self.User_Type,
            'User_OTP': self.User_OTP
        }

class Project(db.Model):
    __tablename__ = 'project_master_table'
    Project_ID = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False, unique=True)
    Project_Name = db.Column(db.String(200), nullable=False)
    Modules = db.Column(db.Integer, nullable=False)
    Division = db.Column(db.String,  nullable=False)

    def serialize(self):
        return {
            'Project_ID': self.Project_ID,
            'Project_Name': self.Project_Name,
            'Modules': self.Modules,
            'Division': self.Division
        }

class Users_Mapping(db.Model):
    __tablename__ = 'trainee_trainer_manager_project_mapping_table'
    TTMP_Mapping_ID = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False, unique=True)
    Trainee_ID = db.Column(db.Integer, nullable=False)
    Trainee_Type = db.Column(db.String(50), nullable=False)
    Trainer_ID = db.Column(db.Integer, nullable=False)
    Manager_ID = db.Column(db.Integer, nullable=False)
    Project_ID = db.Column(db.Integer, nullable=False)
    Last_Active_On = db.Column(db.String(200), nullable=False)
    Reverse_KT_Flag = db.Column(db.String(200), nullable = False, default="N")
    KT_Process_Feedback = db.Column(db.String(200))
    __table_args__ = (
        ForeignKeyConstraint(
            ['Trainee_ID', 'Trainer_ID', 'Manager_ID'],
            ['user_master_table.User_ID', 'user_master_table.User_ID', 'user_master_table.User_ID']
        ), ForeignKeyConstraint(
            ['Project_ID'],
            ['project_master_table.Project_ID']
        ),
        CheckConstraint("Reverse_KT_Flag IN ('Y', 'N')", name='Check_Reverse_KT_flag')
    )
    def serialize(self):
        return {
            'TTMP_Mapping_ID': self.TTMP_Mapping_ID,
            'Trainee_ID': self.Trainee_ID,
            'Trainee_Type':self.Trainee_Type,
            'Trainer_ID': self.Trainer_ID,
            'Manager_ID': self.Manager_ID,
            'Project_ID': self.Project_ID,
            'Last_Active_On': self.Last_Active_On,
            'Reverse_KT_Flag':self.Reverse_KT_Flag,
            'KT_Process_Feedback':self.KT_Process_Feedback
        }

class Modules(db.Model):
    __tablename__ = 'modules_table'
    Module_ID = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False, unique=True)
    Project_ID = db.Column(db.Integer, ForeignKey('project_master_table.Project_ID'), nullable=False)
    Module_Number = db.Column(db.Integer, nullable=False)
    Module_Name = db.Column(db.String(200), nullable=False)
    Mandatory = db.Column(db.String(50))
    Module_Path = db.Column(db.String(200))
    Module_Completion_Time = db.Column(db.Integer)
    project_master_table = relationship('Project')

    def serialize(self):
        return {
            'Module_ID': self.Module_ID,
            'Project_ID': self.Project_ID,
            'Module_Number': self.Module_Number,
            'Module_Name': self.Module_Name,
            'Mandatory': self.Mandatory,
            'Module_Path': self.Module_Path,
            'Module_Completion_Time': self.Module_Completion_Time
        }


class Sub_Modules(db.Model):
    __tablename__ = 'sub_modules_table'
    Sub_Module_ID = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False, unique=True)
    Module_ID = db.Column(db.Integer, ForeignKey('modules_table.Module_ID'), nullable=False)
    Sub_Module_Number = db.Column(db.Integer, nullable=False)
    Sub_Module_Name = db.Column(db.String(200), nullable=False)
    Mandatory = db.Column(db.String(50))
    Sub_Module_Path = db.Column(db.String(200))
    Sub_Module_Completion_Time = db.Column(db.Integer, nullable=False)
    modules_table = relationship('Modules')
    def serialize(self):
        return {
            'Sub_Module_ID': self.Sub_Module_ID,
            'Module_ID': self.Module_ID,
            'Sub_Module_Number': self.Sub_Module_Number,
            'Sub_Module_Name': self.Sub_Module_Name,
            'Mandatory': self.Mandatory,
            'Sub_Module_Path': self.Sub_Module_Path,
            'Sub_Module_Completion_Time':self.Sub_Module_Completion_Time
        }

class Trainee_Status(db.Model):
    __tablename__ = 'trainee_module_sub_module_mapping_table'
    TTMP_Mapping_ID = db.Column(db.Integer)
    TMSM_Mapping_ID = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False, unique=True)
    Trainee_ID = db.Column(db.Integer, nullable=False)
    Module_ID = db.Column(db.Integer, nullable=False)
    Sub_Module_ID = db.Column(db.Integer)
    Mandatory = db.Column(db.String(50))
    Due_Date = db.Column(db.String(200), nullable=False)
    Completion_Date = db.Column(db.String(200))
    Signed_Off_Flag = db.Column(db.String(1), nullable=False)
    Checked_Flag = db.Column(db.String(1),nullable=False)
    Ongoing_Training = db.Column(db.String(50))

    __table_args__ = (
        ForeignKeyConstraint(
            ['Trainee_ID'],
            ['user_master_table.User_ID']
        ), ForeignKeyConstraint(
            ['Module_ID'],
            ['modules_table.Module_ID']
        ), ForeignKeyConstraint(
            ['Sub_Module_ID'],
            ['sub_modules_table.Sub_Module_ID']
        ),
        CheckConstraint("Signed_Off_Flag IN ('Y', 'N')", name='check_flag'),
         CheckConstraint("Checked_Flag IN ('Y', 'N')", name='check_flag')
    )

    def serialize(self):
        return {

            'TTMP_Mapping_ID': self.TTMP_Mapping_ID,
            'TMSM_Mapping_ID': self.TMSM_Mapping_ID,
            'Trainee_ID': self.Trainee_ID,
            'Module_ID': self.Module_ID,
            'Sub_Module_ID': self.Sub_Module_ID,
            'Mandatory': self.Mandatory,
            'Due_Date': self.Due_Date,
            'Completion_Date': self.Completion_Date,
            'Signed_Off_Flag': self.Signed_Off_Flag,
            'Ongoing_Training':self.Ongoing_Training,

        }

class Feedback(db.Model):
    __tablename__ = 'feedback_table'
    Feedback_ID = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False, unique=True)
    Sender_User_ID = db.Column(db.Integer, ForeignKey('user_master_table.User_ID'), nullable=False)
    Receiver_User_ID = db.Column(db.Integer, ForeignKey('user_master_table.User_ID'), nullable=False)
    Feedback_Date = db.Column(db.String(200), nullable=False)
    Feedback = db.Column(db.String(200), nullable=False)

    def serialize(self):
        return {
            'Feedback_ID': self.Feedback_ID,
            'Sender_User_ID': self.Sender_User_ID,
            'Receiver_User_ID': self.Receiver_User_ID,
            'Feedback_Date': self.Feedback_Date,
            'Feedback': self.Feedback
        }


class Re_Assigned_Trainings(db.Model):
    __tablename__ = 're_assigned_trainings_table'
    RT_ID = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False, unique=True)
    TMSM_Mapping_ID = db.Column(db.Integer, ForeignKey('trainee_module_sub_module_mapping_table.TMSM_Mapping_ID'), nullable=False)
    Due_Date = db.Column(db.String(200), nullable=False)
    Completion_Date = db.Column(db.String(200))
    Signed_Off_Flag = db.Column(db.String(1), nullable=False, default="N")

    __table_args__ = (
        (
            CheckConstraint("Signed_Off_Flag IN ('Y', 'N')", name='check_flag')
        ),
    )

    def serialize(self):
        return {
            'RT_ID': self.RT_ID,
            'TMSM_Mapping_ID': self.TMSM_Mapping_ID,
            'Due_Date': self.Due_Date,
            'Completion_Date': self.Completion_Date,
            'Signed_Off_Flag': self.Signed_Off_Flag
        }

class Questions(db.Model):
    __tablename__ = 'questions_table'
    Question_ID = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False, unique=True)
    Project_ID = db.Column(db.Integer, ForeignKey('project_master_table.Project_ID'), nullable=False)
    Question_Number = db.Column(db.Integer, nullable=False)
    Question_Title = db.Column(db.String(200), nullable=False)
    Question_Description = db.Column(db.String(200))
    Question_Type = db.Column(db.String(200), nullable=False)

    __table_args__ = (
        (
            CheckConstraint("Question_Type IN ('MCQ', 'MSQ','Others')")
        ),
    )

    def serialize(self):
        return {
            'Question_ID': self.Question_ID,
            'Project_ID': self.Project_ID,
            'Question_Number': self.Question_Number,
            'Question_Title': self.Question_Title,
            'Question_Description': self.Question_Description,
            'Question_Type':self.Question_Type
        }

class Options(db.Model):
    __tablename__ = 'options_table'
    Option_ID = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False, unique=True)
    Question_ID = db.Column(db.Integer, ForeignKey('questions_table.Question_ID'), nullable=False)
    Option_Number = db.Column(db.Integer, nullable=False)
    Option_Title = db.Column(db.String(200), nullable=False)
    Option_Description = db.Column(db.String(200))
    Option_Correct_Flag = db.Column(db.String(200), nullable=False)

    __table_args__ = (
        (
            CheckConstraint("Option_Correct_Flag IN ('Y', 'N')")
        ),
    )

    def serialize(self):
        return {
            'Option_ID': self.Option_ID,
            'Question_ID': self.Question_ID,
            'Option_Number': self.Option_Number,
            'Option_Title': self.Option_Title,
            'Option_Description': self.Option_Description,
            'Option_Correct_Flag':self.Option_Correct_Flag
        }


class Assessments(db.Model):
    __tablename__ = 'assessments_table'
    Assessment_ID = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False, unique=True)
    User_ID = db.Column(db.Integer, ForeignKey('user_master_table.User_ID'), nullable=False)
    Project_ID = db.Column(db.Integer, ForeignKey('project_master_table.Project_ID'), nullable=False)
    Attempt_Number = db.Column(db.Integer, nullable=False)
    Attempt_Score = db.Column(db.Integer, nullable=False)
    Attempt_Date= db.Column(db.String(200), nullable=False)

    def serialize(self):
        return {
            'Assessment_ID': self.Assessment_ID,
            'User_ID': self.User_ID,
            'Project_ID': self.Project_ID,
            'Attempt_Number': self.Attempt_Number,
            'Attempt_Score': self.Attempt_Score,
            'Attempt_Date':self.Attempt_Date
        }


def __repr__(self):
    return f"{self.Trainee_Email}-{self.Project_Assigned}"