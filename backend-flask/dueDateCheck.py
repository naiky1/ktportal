
from functions import app_obj, User, Project, Users_Mapping, Modules, Sub_Modules, Trainee_Status, send_email, email_list_formatter
from datetime import datetime, timedelta
import schedule



app,db = app_obj()
def dueDateCheck():
    mappings = Users_Mapping.query.filter_by(Last_Active_On="Present").all()
    for mapping in mappings:
        #Identifying all incomplete trainings of the trainee
        incomplete_trainings=Trainee_Status.query.filter(Trainee_Status.Trainee_ID==mapping.Trainee_ID,Trainee_Status.Signed_Off_Flag=="N").all()
        missedTrainings=[]
        missedTrainingsNames=[]
        upcomingTrainings=[]
        upcomingTrainingsNames=[]
        today = datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)
        try:
            for training in incomplete_trainings:
                due_date = datetime.strptime(training.Due_Date, "%Y-%m-%d")
                if today==due_date+timedelta(days=7):
                    missedTrainings.append(training)
                elif today==due_date-timedelta(days=2):
                    upcomingTrainings.append(training)
            for missedTraining in missedTrainings:
                name = Modules.query.filter_by(Module_ID=missedTraining.Module_ID).first().Module_Name
                if missedTraining.Sub_Module_ID:
                    sub_module_name = Sub_Modules.query.filter_by(Sub_Module_ID=missedTraining.Sub_Module_ID).first().Sub_Module_Name
                    name += " - " + sub_module_name
                missedTrainingsNames.append(name)
            for upcomingTraining in upcomingTrainings:
                name = Modules.query.filter_by(Module_ID=upcomingTraining.Module_ID).first().Module_Name
                if upcomingTraining.Sub_Module_ID:
                    sub_module_name = Sub_Modules.query.filter_by(Sub_Module_ID=upcomingTraining.Sub_Module_ID).first().Sub_Module_Name
                    name += " - " + sub_module_name
                upcomingTrainingsNames.append(name)

        except:
            missedTrainings=[]
            missedTrainingsNames=[]
            upcomingTrainings=[]
            upcomingTrainingsNames=[]

        
        #Sending email to trainee(upcoming trainings) and trainee, trainer, manager (missed trainings)
        if(len(missedTrainings)>0):            
            trainer=User.query.filter_by(User_ID=mapping.Trainer_ID).first()
            trainee=User.query.filter_by(User_ID=mapping.Trainee_ID).first()
            manager=User.query.filter_by(User_ID=mapping.Manager_ID).first()
            trainingsMissed=""
            btCount=0
            pstCount=0
            for (missedTraining,missedTrainingName) in zip(missedTrainings,missedTrainingsNames):
                trainingProjectId=Modules.query.filter_by(Module_ID=missedTraining.Module_ID).first().Project_ID
                if trainingProjectId==0:
                    btCount+=1
                elif trainingProjectId==mapping.Project_ID:
                    pstCount+=1
                projectName = Project.query.filter_by(Project_ID=trainingProjectId).first().Project_Name
                projectName = '~  ' + projectName
                trainingMissed=" , ".join([" : ".join([projectName,missedTrainingName])," ".join(["Due Date :",missedTraining.Due_Date])])
                trainingsMissed="\n\n".join([trainingsMissed,trainingMissed])
            basicTrainingsName = Project.query.filter_by(Project_ID=0).first().Project_Name
            currentProjectName = Project.query.filter_by(Project_ID=mapping.Project_ID).first().Project_Name
            to_list=[str(trainee.User_Email)]
            cc_list=[str(manager.User_Email),str(trainer.User_Email)]
            to_emails = email_list_formatter(to_list)
            cc_emails = email_list_formatter(cc_list)
            receivers_list=to_list+cc_list          
            # subject="CRMKTUTILITY: {} Training(s) overdue by 7 days".format(len(missedTrainings),trainee.User_Name)
            subject="CRMKTUTILITY: KT Overdue notification"
            body="Following trainings missed their completion date by 7 days already, kindly complete them ASAP. \n {}\n".format(trainingsMissed)
            send_email(receivers_list,to_emails,cc_emails,subject,trainee.User_Name,body) 

        #Sending email to trainee(upcoming trainings)
        if(len(upcomingTrainings)>0):
            trainee=User.query.filter_by(User_ID=mapping.Trainee_ID).first()
            trainingsComingUp=""
            for (upcomingTraining,upcomingTrainingName) in zip(upcomingTrainings,upcomingTrainingsNames):
                trainingProjectId=Modules.query.filter_by(Module_ID=upcomingTraining.Module_ID).first().Project_ID
                projectName = Project.query.filter_by(Project_ID=trainingProjectId).first().Project_Name
                projectName = '~  ' + projectName
                trainingComingUp=" , ".join([" : ".join([projectName,upcomingTrainingName])," ".join(["Due Date :",upcomingTraining.Due_Date])])
                trainingsComingUp="\n\n".join([trainingsComingUp,trainingComingUp])
            to_list = [str(trainee.User_Email)]
            cc_list = []
            to_emails = email_list_formatter(to_list)
            cc_emails = email_list_formatter(cc_list)
            receivers_list = to_list+cc_list
            subject = "CRMKTUTILITY: {} training(s) due in 2 days".format(len(upcomingTrainings))
            body = "The following trainings needs to be completed within next 2 days, kindly complete them on time : \n {}\n".format(trainingsComingUp)
            send_email(receivers_list,to_emails,cc_emails,subject,trainee.User_Name,body) 

#Check for missed/upcoming due date(s) for all trainees at 9AM everyday and send out emails if needed
schedule.every().day.at("14:20").do(dueDateCheck)
while True:
    # Checks whether a scheduled task
    # is pending to run or not
    with app.app_context():
        schedule.run_pending()
